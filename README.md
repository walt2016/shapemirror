# shape mirror

#### 介绍
几何画图 图形计算 svg canvas webgl 

### 地址
http://walt2016.gitee.io/shapemirror

### PC截图
<img src="./screenshot/20220730151010.png" width='605'>
<img src="./screenshot/20220730151448.png" width='605'>

### H5截图
<img src="./screenshot/20220807220902.jpg" width='300'>
<img src="./screenshot/20220807220905.jpg" width='300'>

<img src="./screenshot/20220905212457.jpg" width='300'>
<img src="./screenshot/20220905212501.jpg" width='300'>
