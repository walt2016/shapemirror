import { createSvg, _defs, _patternId } from '../core/svg'
//     <linearGradient id="Gradient2" x1="0" x2="0" y1="0" y2="1">
//     <stop offset="0%" stop-color="red"/>
//     <stop offset="50%" stop-color="black" stop-opacity="0"/>
//     <stop offset="100%" stop-color="blue"/>
//   </linearGradient>

const _stops = (list, colors) => {
	let len = list.length
	let stops = list.map((t, index) => {
		return createSvg('stop', {
			offset: (100 * index) / (len - 1) + '%',
			'stop-color': colors[index % len], //options['color' + (index + 1)],
		})
	})
	return stops
}
// 渐变
export const _gradient = options => {
	let { name, id = 'shape-gradient', colors = ['green', 'red'] } = options
	id = _patternId(id, name)

	let list = [1, 2]

	let stops = _stops(list, colors)

	let grad = createSvg(
		'linearGradient',
		{
			x1: '0',
			x2: '0',
			y1: '0',
			y2: '1',
			id,
		},
		{},
		stops
	)

	let defs = _defs(grad)
	return defs
}

export const _radial = options => {
	let { name, id = 'shape-radialGradient', colors = ['green', 'red'] } = options
	id = _patternId(id, name)

	let list = [1, 2]

	let stops = _stops(list, colors)
	let grad = createSvg(
		'radialGradient',
		{
			id,
		},
		{},
		stops
	)
	let defs = _defs(grad)
	return defs
}
