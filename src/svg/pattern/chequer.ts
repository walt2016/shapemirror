import { IPathOptions } from '@/types/path'
import { createSvg, _defs, _rect, _circle, _patternId } from '../core/svg'
// 格子图案
// fill="url(#shape-chequer)"
export const _chequer = (options: IPathOptions) => {
	const { name, id = 'shape-chequer', r = 10, color1 = 'red', color2 = 'green', borderRadius1 = 1, borderRadius2 = 1 } = options
	const newID = _patternId(id, name)
	const rect1 = _rect(
		[
			[0, 0],
			[r, r],
		],
		{
			fill: color1,
			rx: borderRadius1,
			ry: borderRadius1,
		}
	)
	const rect2 = _rect(
		[
			[r, r],
			[r * 2, r * 2],
		],

		{
			fill: color2,
			rx: borderRadius2,
			ry: borderRadius2,
		}
	)

	const chequer = createSvg(
		'pattern',
		{
			id: newID,
			x: 0,
			y: 0,
			width: r * 2,
			height: r * 2,
			patternUnits: 'userSpaceOnUse',
		},
		{},
		[rect1, rect2]
	)
	const defs = _defs(chequer)
	return defs
}

// 点点
export const _dot = options => {
	const { name, r = 5, id = 'dot' } = options
	const newID = _patternId(id, name)
	const dots = [
		_circle(
			{ o: [r, r], r },
			{
				fill: 'red',
			}
		),
		_circle(
			{ o: [3 * r, 3 * r], r },
			{
				fill: 'green',
			}
		),
	]

	const chequer = createSvg(
		'pattern',
		{
			id: newID,
			x: 0,
			y: 0,
			width: r * 4,
			height: r * 4,
			patternUnits: 'userSpaceOnUse',
		},
		{},
		dots
	)

	const defs = _defs(chequer)
	return defs
}
