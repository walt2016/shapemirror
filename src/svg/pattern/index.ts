import { _chequer, _dot } from './chequer'
import { _stripe, _diagonalStripe } from './stripe'
import { _gradient, _radial } from './gradient'
import { Shape } from '@/types/shape'

// 填充图案函数
export const patternConfig = {
	[Shape.Chequer]: _chequer,
	[Shape.Stripe]: _stripe,
	[Shape.Gradient]: _gradient,
	[Shape.Radial]: _radial,
	[Shape.Dot]: _dot,
	[Shape.DiagonalStripe]: _diagonalStripe,
}
