import { createSvg, _defs, _path, _patternId } from '../core/svg'
import { _d } from '../core/path'
import { IStripeOptions } from '@/types'
// 条纹 竖直
export const _stripe = (options: IStripeOptions) => {
	let { name, id = 'shape-stripe', r = 10, color1 = 'red', color2 = 'green', radio = 0.2, skewX = 0 } = options
	id = _patternId(id, name)

	// let d = "M0,0 H10 L 20,10  V-10 Z"
	// let d = "M0,0 H10  V10 Z"
	// 三角形
	// let d = `M0,0 h${r} v${r} Z`
	// _path(d, {
	//     fill: stripeColor1
	// }, stripe)

	// let d2 = `M${r},${r} h${r} v${r} Z`
	// _path(d2, {
	//     fill: stripeColor2
	// }, stripe)

	let d = `M0,0 h${r * radio} v${r} h${r * radio * -1} Z`
	let path1 = _path(d, {
		fill: color1,
		transform: `skewX(${skewX})`,
	})

	let d2 = `M${r},0 h${r * radio} v${r} h${r * radio * -1} Z`
	let path2 = _path(d2, {
		fill: color2,
		transform: `skewX(${skewX})`,
	})

	let stripe = createSvg(
		'pattern',
		{
			id,
			x: 0,
			y: 0,
			width: r * 2,
			height: r,
			patternUnits: 'userSpaceOnUse',
		},
		{},
		[path1, path2]
	)

	let defs = _defs(stripe)
	return defs
}

// 斜条纹线
export const _diagonalStripe = (options: IStripeOptions) => {
	let { name, id = 'shape-diagonalStripe', size = 10, color1 = 'red', offset = 0 } = options
	id = _patternId(id, name)
	let r = size

	let d = _d([
		[
			[0, 0],
			[r * 2, r * 2],
			[r + offset, r * 2],
			[0, r - offset],
		],
		[
			[r + offset, 0],
			[r * 2, 0],
			[r * 2, r - offset],
		],
	])
	let path = _path(d, {
		fill: color1,
		// stroke: color1
	})

	let stripe = createSvg(
		'pattern',
		{
			id,
			x: 0,
			y: 0,
			width: r * 2,
			height: r * 2,
			patternUnits: 'userSpaceOnUse',
		},
		{},
		[path]
	)

	let defs = _defs(stripe)
	return defs
}
