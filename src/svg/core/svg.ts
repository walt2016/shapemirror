import { camel2kebab, _type } from '@/utils'
import { _events, _children } from '@/ui/core/dom'
import { _k } from '@/math'
import { ICircleOptions, IPathProps } from '@/types/path'
import { TChildren, TEvents } from '@/types/ui'
import { IPointsOptions, Point } from '@/types/point'
import { ITextOptions } from '@/types/text'

export const svgns = 'http://www.w3.org/2000/svg'
export const xlinkns = 'http://www.w3.org/1999/xlink'

const svgKebabProps = ['fillRule', 'fontSize', 'textAnchor', 'dominantBaseline', 'strokeWidth', 'strokeDasharray', 'strokeDashoffset', 'strokeLinecap', 'strokeLinejoin', 'strokeMiterlimit']
// 属性设置规则：
// 默认值排除父节点影响情况下
// 1、默认值相同，缺省删除属性 opacity="1" stroke-dasharray="0" （）
// 2、排除fill ，其他属性。值为none时，删除该属性
// 3、关联属性，当strokeLinejoin===''miter' 时strokeMiterlimit才有效
const _shouldRemoveDefaultProps = (key, val) => {
	// 默认值为1
	if (key === 'opacity' && Number(val) === 1) {
		return true
	}

	// 默认值为0
	else if (['strokeDasharray', 'strokeDashoffset', 'strokeMiterlimit'].includes(key) && Number(val) === 0) {
		return true
	}

	// 排除fill ，其他属性。值为none时，删除该属性
	else if (!['fill'].includes(key) && val === 'none') {
		return true
	}
	return false
}
export const createSvg = (tag: string, props?: IPathProps, events?: TEvents, children?: TChildren): SVGElement => {
	let el = document.createElementNS(svgns, tag)
	const _props = (props: IPathProps): void => {
		let keys = Object.keys(props)
		keys.forEach(t => {
			let key = svgKebabProps.includes(t) ? camel2kebab(t) : t
			let val = props[t]
			if (val !== undefined) {
				switch (key.toLocaleLowerCase()) {
					case 'class':
						// el.className = val
						el.setAttribute('classname', val)
						break
					case 'innertext':
					case 'id':
					case 'innerhtml':
					case 'value':
					case 'textcontent':
						el[key] = val
						break
					// case 'pattern':
					//     el.setAttribute('fill', `url("#${val}")`)
					//     break;
					case 'click':
						el.addEventListener('click', val, false)
						break
					case 'xlink:href': //case 'marker-end':
						el.setAttributeNS(xlinkns, key, val)
						break
					default:
						if (_shouldRemoveDefaultProps(t, val)) {
							el.removeAttribute(key)
						} else {
							el.setAttribute(key, val)
						}

						// el.setAttributeNS(xlinkns, key, val);
						break
				}
			}
		})
	}

	props && _props(props)

	// 事件
	events && _events(el, events)

	children && _children(el, children)
	return el
}

// 全局定义
export const _defs = (children: TChildren, props?: IPathProps): SVGElement => {
	return createSvg('defs', props, {}, children)
}

//  use 需要在defs之后才有效
// 使用use标签时，注意use的结尾标签，不要在</use>结尾标签之前再写其他标签，会显现不出来，就是所谓的被吞了。
export const _use = ({ id, x = 0, y = 0 }) => {
	return createSvg('use', {
		x,
		y,
		// x: 0,
		// y: 0,
		// width: '100%',
		// height: '100%',
		'xlink:href': '#' + id,
		// xlink:href=“#id”
	})
}
export const _symbol = (children: TChildren) => {
	return createSvg(
		'symbol',
		{
			id: 'shape',
		},
		null,
		children
	)
}
export const _circle = (options: ICircleOptions, props?: IPathProps, events?: TEvents, children?: TChildren) => {
	let { o = [], r = 100, name, id } = options
	return createSvg(
		'circle',
		{
			cx: o[0],
			cy: o[1],
			r,
			name,
			id,
			...props,
		},
		events,
		children
	)
}
const getPoints = (options: IPointsOptions | Point[]): Point[] => {
	let points = []
	if (Array.isArray(options)) {
		points = options
	} else if (_type(options) === 'object') {
		points = options.points || []
	}
	return points
}

// { stroke: 'black', fill: 'none' }
export const _line = (options: IPointsOptions | Point[], props?: IPathProps, events?: TEvents, children?: TChildren) => {
	let points = getPoints(options)
	if (points.length === 2) {
		let [p1, p2] = points
		return createSvg(
			'line',
			{
				x1: p1[0],
				y1: p1[1],
				x2: p2[0],
				y2: p2[1],
				...props,
			},
			events,
			children
		)
	} else {
		return createSvg('polyline', {
			points: points.join(','),
			...props,
		})
	}
	// todo
}
export const _rect = (options: IPointsOptions | Point[], props?: IPathProps, events?: TEvents, children?: TChildren): SVGElement => {
	const points = getPoints(options)
	const renderRect = (p1: Point, p2: Point) => {
		return createSvg(
			'rect',
			{
				x: Math.min(p1[0], p2[0]),
				y: Math.min(p1[1], p2[1]),
				width: Math.abs(p2[0] - p1[0]),
				height: Math.abs(p2[1] - p1[1]),
				...props,
			},
			events,
			children
		)
	}
	if (points.length === 2) {
		const [p1, p2] = points
		return renderRect(p1, p2)
	}
	// todo
}

export const _path = (d: string, props?: IPathProps, events?: TEvents, children?: TChildren): SVGElement => {
	return createSvg(
		'path',
		{
			d,
			...props,
		},
		events,
		children
	)
}

export const toTextOptions = ([x, y]: Point, text: string | number) => {
	return {
		x,
		y,
		text,
	}
}
// 文本
export const _text = (options: ITextOptions, props?: IPathProps, events?: TEvents, children?: TChildren): SVGElement => {
	let fn = (x: number, y: number, text: string | number, props: IPathProps, events: TEvents, children: TChildren): SVGElement => {
		return createSvg(
			'text',
			{
				x,
				y,
				textContent: String(text),
				...props,
			},
			events,
			children
		)
	}
	// if (Array.isArray(options)) {
	//     let [x, y, text] = options
	//     return fn(x, y, text, props, events, children)
	// } else {
	let { x, y, text } = options
	return fn(x, y, text, props, events, children)
	// }
}

export const _g = (children: TChildren, props?: IPathProps): SVGElement => {
	return createSvg(
		'g',
		{
			...props,
		},
		{},
		children
	)
}

export const _marker = (children: TChildren, props?: IPathProps): SVGElement => {
	return createSvg(
		'marker',
		{
			...props,
		},
		{},
		children
	)
}

export const _patternId = (id: string, name: string): string => {
	return `${name}_${id}`
}
