export const _circle = ({ o, r }): string => {
	const [cx, cy] = o

	//非数值单位计算，如当宽度像100%则移除
	if (isNaN(cx - cy)) return ''
	const path = 'M' + (cx - r) + ' ' + cy + 'a' + r + ' ' + r + ' 0 1 0 ' + 2 * r + ' 0' + 'a' + r + ' ' + r + ' 0 1 0 ' + -2 * r + ' 0' + 'z'
	return path
}
