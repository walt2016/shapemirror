import { _moveTo, _bezierCurveTo, _loop, _polyline } from './curve'
import { _random } from '@/math'
import { controlPoints } from '@/algorithm/controlPoints'
import { fillGridPoints } from '@/algorithm/fill'
import { _offset } from '@/algorithm/rough'

const _roughTo = ([from, to], angle, radio) => {
	from = _offset(from)
	to = _offset(to)
	let [c1, c2] = controlPoints([from, to], _random(10, 20), _random(0.1, 0.2))
	return _moveTo(from) + ' ' + _bezierCurveTo([c1, c2, to])
}

const _dooubleRoughTo = ([from, to], angle, radio) => {
	return _roughTo([from, to], angle, radio) + ' ' + _roughTo([from, to], angle, radio)
}

export const _rough = options => {
	let { points } = options
	let gridDensity = 10

	let ps = fillGridPoints({ points, gridDensity })
	return _loop(options, _dooubleRoughTo) + ' ' + _loop({ points: ps, step: 2 }, _roughTo)
}
