import { _path } from './svg'
import { isPoints } from '@/math'
import { _type } from '@/utils'

import { IPathProps, IPathStringOptions } from '@/types/path'
import { TChildren, TEvents } from '@/types/ui'
import { IPointsOptions, Point, PointSegment } from '@/types/point'

// 点的路径图
export const pointsPath = ({ points, closed, broken }: IPointsOptions, props?: IPathProps, events?: TEvents, children?: TChildren) => {
	return _path(
		_d(points, {
			closed,
			broken,
		}),
		props,
		events,
		children
	)
}

// 树形路径
export const treeLine = (treePoints: Point[][], props?: IPathProps) => {
	const d = treePoints
		.map((ps: Point[]) => {
			return _d(ps, {
				broken: false,
				closed: false,
			})
		})
		.join(' ')

	return _path(d, props)
}

export const polyline = (points: Point[], props: IPathProps) => {
	return pointsPath(
		{
			points,
			closed: false,
			broken: false,
		},
		props
	)
}

// M = moveto
// L = lineto
// H = horizontal lineto
// V = vertical lineto
// C = curveto
// S = smooth curveto
// Q = quadratic Bézier curve
// T = smooth quadratic Bézier curveto
// A = elliptical Arc
// Z = closepath
const pathCommandMap = {
	7: 'A',
	6: 'C',
	4: 'Q',
	2: 'L',
}
// 链接点 [p1,p2]  =>[[x,y],[x,y]]  通用链接方式 polyline
export const _d = (points: Point[] | Point[][], options?: IPathStringOptions): string => {
	const { closed, broken } = options ?? {}
	const n = typeof broken === 'number' ? broken : broken ? 2 : 0
	return (
		points
			.map((t, index) => {
				if (isPoints(t)) {
					return _d(t, {
						closed: false,
						broken,
					})
				}
				const len = t.length
				const stack = [t.join(' ')]
				if (broken ? index % n === 0 : index === 0) {
					stack.unshift('M')
				} else {
					stack.unshift(pathCommandMap[len] || pathCommandMap[2])
				}
				return stack.join('')
				// return `${(broken ? index % 2 === 0 : index === 0) ? "M" : len === 7 ? "A" : len === 4 ? "Q" : "L"}${t.join(" ")}`
				// return Array.isArray(t[0]) ? _d(t, {
				//     closed: false,
				//     broken
				// }) : `${(broken ? index % 2 === 0 : index === 0) ? "M" : t.length === 7 ? "A" : t.length === 4 ? "Q" : "L"}${t.join(" ")}`
			})
			.join(' ') + (closed ? ' z' : '')
	)
}

// 闭合线段[p1,p2,p3] p1->p2->p3->p1
export const closedLinePath = (points: Point[]): string => {
	return points
		.map((t, index) => {
			return (index === 0 ? 'M' : 'L') + t.join(' ')
		})
		.concat(['z'])
		.join(' ')
}
// 折线[p1,p2,p3,p4]  p1->p2  p3->p4
export const brokenLinePath = (points: Point[]): string => {
	return points
		.map((t, index) => {
			return (index % 2 === 0 ? 'M' : 'L') + t.join(' ')
		})
		.join(' ')
}
// 线段，二维点[[p1,p2],[p3,p4]] => [[[x,y],[x,y]],[[x,y],[x,y]]]
export const _d2 = (segments: PointSegment[], closed: boolean, broken: boolean): string => {
	return segments
		.map(t => {
			// return `M${t[0].join(" ")} L${t[1].join(" ")}`
			return _d(t, {
				closed,
				broken,
			})
		})
		.join(' ')
}
// 长方形
export const rect = (p: Point, width: number, height: number): string => {
	return _d([p, [p[0] + width, p[1]], [p[0] + width, p[1] + height], [p[0], p[1] + height]])
}
// 正方形
export const squarePath = (p: Point, d: number) => {
	const path = _d([p, [p[0] + d, p[1]], [p[0] + d, p[1] + d], [p[0], p[1] + d]], {
		closed: true,
		broken: false,
	})
	return _path(path)
}
// 圆形
export const circle = (p, r) => {
	return ellipse2path(p[0], p[1], r, r)
}

export const rect2path = (x: number, y: number, width: number, height: number, rx: number, ry: number): string => {
	/* * rx 和 ry 的规则是： * 1. 如果其中一个设置为 0 则圆角不生效 * 2. 如果有一个没有设置则取值为另一个 */
	rx = rx || ry || 0
	ry = ry || rx || 0 //非数值单位计算，如当宽度像100%则移除
	if (isNaN(x - y + width - height + rx - ry)) return
	rx = rx > width / 2 ? width / 2 : rx
	ry = ry > height / 2 ? height / 2 : ry //如果其中一个设置为 0 则圆角不生效
	if (0 == rx || 0 == ry) {
		// var path =
		// 'M' + x + ' ' + y +
		// 'H' + (x + width) + 不推荐用绝对路径，相对路径节省代码量
		// 'V' + (y + height) +
		// 'H' + x +
		// 'z';
		return 'M' + x + ' ' + y + 'h' + width + 'v' + height + 'h' + -width + 'z'
	}
	return 'M' + x + ' ' + (y + ry) + 'a' + rx + ' ' + ry + ' 0 0 1 ' + rx + ' ' + -ry + 'h' + (width - rx - rx) + 'a' + rx + ' ' + ry + ' 0 0 1 ' + rx + ' ' + ry + 'v' + (height - ry - ry) + 'a' + rx + ' ' + ry + ' 0 0 1 ' + -rx + ' ' + ry + 'h' + (rx + rx - width) + 'a' + rx + ' ' + ry + ' 0 0 1 ' + -rx + ' ' + -ry + 'z'
}

export const ellipse2path = (cx: number, cy: number, rx: number, ry: number): string | null => {
	//非数值单位计算，如当宽度像100%则移除
	if (isNaN(cx - cy + rx - ry)) return null
	const path = 'M' + (cx - rx) + ' ' + cy + 'a' + rx + ' ' + ry + ' 0 1 0 ' + 2 * rx + ' 0' + 'a' + rx + ' ' + ry + ' 0 1 0 ' + -2 * rx + ' 0' + 'z'
	return path
}

export const line2path = (x1, y1, x2, y2): string | null => {
	//非数值单位计算，如当宽度像100%则移除
	if (isNaN(x1 - y1 + x2 - y2)) return
	x1 = x1 || 0
	y1 = y1 || 0
	x2 = x2 || 0
	y2 = y2 || 0
	const path = 'M' + x1 + ' ' + y1 + 'L' + x2 + ' ' + y2
	return path
}

// polygon折线转换 points = [x1, y1, x2, y2, x3, y3 ...];
export const polyline2path = (points: Point[]): string => {
	const path = 'M' + points.slice(0, 2).join(' ') + 'L' + points.slice(2).join(' ')
	return path
}
// polygon多边形转换 points = [x1, y1, x2, y2, x3, y3 ...];
export const polygonColorpath = (points: Point[]): string => {
	const path = 'M' + points.slice(0, 2).join(' ') + 'L' + points.slice(2).join(' ') + 'z'
	return path
}
