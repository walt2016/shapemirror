import { _polyline, _arcTo } from './curve'
import { _path, _g, _text, _circle } from './svg'
import { markInfo } from '@/algorithm/mark'
import { _drag } from './drag'
import { IPathProps } from '@/types/path'
import { TextAnchor } from '@/types/text'
import { IMarkOptions } from '@/types/mark'

export const _mark = ({ points, mark }: IMarkOptions, props: IPathProps) => {
	let mi = markInfo(points, mark)
	let { angle = true, edge = true, radius = false, labels = false, boundingSize = false, boundingRect = false } = mark

	let g = []
	if (angle) {
		let { angleInfo } = mi
		angleInfo.forEach(({ points, a, ia, sweepFlag, o, r, label, labelPos }) => {
			let path = _polyline({ points })

			// 凹多边形（Concave Polygon
			// 优角（Reflexive Angle）   sweepFlag==1  顺时针

			path += _arcTo({
				r,
				to: points[0],
				sweepFlag: sweepFlag === 1,
			})

			g[g.length] = _path(path, { ...props, strokeDasharray: 0 })
			g[g.length] = _text(
				{
					x: labelPos[0],
					y: labelPos[1],
					text: label,
				},
				{ fontSize: 10, ...props, strokeDasharray: 0, textAnchor: TextAnchor.Middle }
			)
		})
	}

	if (edge) {
		let { edgeInfo } = mi

		edgeInfo.forEach(({ points, label, labelPos }) => {
			g[g.length] = _path(_polyline({ points }), { strokeDasharray: 4, ...props, fill: 'none' })

			g[g.length] = _text(
				{
					x: labelPos[0],
					y: labelPos[1],
					text: label,
				},
				{ fontSize: 10, ...props, strokeDasharray: 0, textAnchor: TextAnchor.Middle }
			)
		})
	}

	if (radius) {
		let { radiusInfo } = mi
		radiusInfo.forEach(({ points, label, labelPos }) => {
			g[g.length] = _path(_polyline({ points }), { strokeDasharray: 4, ...props, fill: 'none' })

			g[g.length] = _text(
				{
					x: labelPos[0],
					y: labelPos[1],
					text: label,
				},
				{ fontSize: 10, ...props, strokeDasharray: 0, textAnchor: TextAnchor.Middle }
			)
		})
	}

	if (labels) {
		let { labelsInfo } = mi
		labelsInfo.forEach(({ label, labelPos }) => {
			g[g.length] = _text(
				{
					x: labelPos[0],
					y: labelPos[1],
					text: label,
				},
				{ fontSize: 10, ...props, strokeDasharray: 0, textAnchor: TextAnchor.Middle }
			)
		})
	}

	if (boundingSize) {
		let { boundingSizeInfo } = mi
		boundingSizeInfo.forEach(({ points, label, labelPos }) => {
			g[g.length] = _path(_polyline({ points }), { strokeDasharray: 4, ...props, fill: 'none' })

			g[g.length] = _text(
				{
					x: labelPos[0],
					y: labelPos[1],
					text: label,
				},
				{ fontSize: 10, ...props, strokeDasharray: 0, textAnchor: TextAnchor.Middle }
			)
		})
	}
	if (boundingRect) {
		let { boundingRectPoints } = mi

		g[g.length] = _path(_polyline({ points: boundingRectPoints, loop: true }), { ...props, stroke: 'gray', strokeDasharray: 0, fill: 'none', name: 'boundingRect' })

		// 拖动
		g.push(_drag(boundingRectPoints, _polyline))
	}

	return _g(g, {
		name: 'mark',
	})
}
