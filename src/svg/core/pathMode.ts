import { IPathModeOptions, PathMode } from '@/types/pathMode'
import { toMatrix } from '@/math/array'
import { _o } from '@/math/index'
import { _curve, _bezierCurveTo, _moveTo, _quadraticCurveTo } from './curve'
import { _circle } from './circle'
import { rayMix } from '@/math/arrayMix'
import { linkPoints, stripPoints, stripLoopPoints, stripFanPoints } from '@/algorithm/link'
import { _traversal } from '@/algorithm/traversal'
import { IPloygonOptions } from '@/types'
import { IPointOptions, Point } from '@/types/point'
import { ICurve } from '@/types/curve'

// 点
export const _points = ({ points }: IPathModeOptions, options?: IPointOptions) => {
	let { r = 3 } = options ?? {}
	return points
		.map(t => {
			return _circle({ o: t, r })
		})
		.join(' ')
}

// 线段
export const _lines = ({ points, curve }: IPathModeOptions) => {
	return _curve({ points, curve, step: 2, discrete: true })
}
// 中心射线
export const _ray = ({ points, curve }: IPathModeOptions) => {
	let o = _o(points)
	let ps = rayMix(o, points)
	return _curve({ points: ps, curve, step: 2 })
}
export const _rayOne = ({ points, curve }: IPathModeOptions) => {
	let o = _o(points)
	return _curve({ points: [o, points[0]], curve })
}

// 带状
const _lineStrip = ({ points, curve }: IPathModeOptions) => {
	return _curve({ points, curve })
}
// LINE_LOOP
export const _lineLoop = ({ points, curve }: IPathModeOptions) => {
	return _curve({ points, curve, loop: true })
}
// 扇
const _rayFan = ({ points, curve }: IPathModeOptions) => {
	return _ray({ points, curve }) + _lineLoop({ points })
}

// 多边形
export const _ploygon = ({ points, n, curve }: IPloygonOptions) => {
	let matrix = toMatrix(points, n)
	return matrix
		.map(ps => {
			return _curve({ points: ps, curve, loop: true })
		})
		.join(' ')
}
// 三角形
export const _triangles = ({ points, curve }: IPathModeOptions) => {
	return _ploygon({ points, n: 3, curve })
}

const _serial = (points: Point[], n: number, loop: boolean, curve?: string | ICurve) => {
	let iter = ({ points }) => {
		return _curve({ points, curve, loop: true })
	}
	return _traversal({
		points,
		n,
		iter,
		loop,
	})
}

const _triangleSerial = ({ points, curve }: IPathModeOptions) => {
	let n = 3
	return _serial(points, n, false, curve)
}
const _squareSerial = ({ points, curve }: IPathModeOptions) => {
	let n = 4
	return _serial(points, n, false, curve)
}

const _triangleSerialLoop = ({ points, curve }: IPathModeOptions) => {
	let n = 3
	return _serial(points, n, true, curve)
}
const _squareSerialLoop = ({ points, curve }: IPathModeOptions) => {
	let n = 4
	return _serial(points, n, true, curve)
}

const _curveMode = (points: Point[], loop = false): string => {
	let n = 3
	return _traversal({
		points,
		n,
		iter: ({ points: [p1, p2, p3] }) => {
			return _quadraticCurveTo([p2, p3])
		},
		init: ({ point }) => _moveTo(point),
		loop,
	}).join(' ')
}

const _curveStrip = ({ points }: IPathModeOptions) => {
	return _curveMode(points, false)
}
//
const _curveLoop = ({ points }: IPathModeOptions) => {
	return _curveMode(points, true)
}
// 离散
const _discreteBezier = ({ points }: IPathModeOptions) => {
	let n = 4
	let discrete = true
	return _traversal({
		points,
		n,
		iter: ({ points: [p1, p2, p3, p4] }) => {
			return _moveTo(p1) + ' ' + _bezierCurveTo([p2, p3, p4])
		},
		discrete,
	}).join(' ')
}
// 连续
const _bezierMode = (points: Point[], loop = false) => {
	let n = 4
	return _traversal({
		points,
		n,
		iter: ({ points: [p1, p2, p3, p4] }) => {
			return _bezierCurveTo([p2, p3, p4])
		},
		init: ({ point }) => _moveTo(point),
		loop,
	}).join(' ')
}
const _bezierStrip = ({ points }: IPathModeOptions) => {
	return _bezierMode(points, false)
}

const _bezierLoop = ({ points }: IPathModeOptions) => {
	return _bezierMode(points, true)
}

const _triangleStrip = ({ points, curve }: IPathModeOptions) => {
	let matrix = stripPoints(points, 3)
	return matrix
		.map(ps => {
			return _curve({ points: ps, curve, loop: true })
		})
		.join(' ')
}

const _triangleStripLoop = ({ points, curve }: IPathModeOptions) => {
	let matrix = stripLoopPoints(points, 3)
	return matrix
		.map(ps => {
			return _curve({ points: ps, curve, loop: true })
		})
		.join(' ')
}
const _triangleFan = ({ points, curve }: IPathModeOptions) => {
	let matrix = stripFanPoints(points, 2)
	let last = matrix.pop()
	return (
		matrix
			.map(ps => {
				return _curve({ points: ps, curve })
			})
			.join(' ') + _curve({ points: last, curve, loop: true })
	)
}

// 四边形
export const _squares = ({ points, curve }: IPathModeOptions) => {
	return _ploygon({ points, n: 4, curve })
}

const _squareStrip = ({ points, curve }: IPathModeOptions) => {
	let matrix = stripPoints(points, 4)
	return matrix
		.map(ps => {
			return _curve({ points: ps, curve, loop: true })
		})
		.join(' ')
}

const _squareStripLoop = ({ points, curve }: IPathModeOptions) => {
	let matrix = stripLoopPoints(points, 4)
	return matrix
		.map(ps => {
			return _curve({ points: ps, curve, loop: true })
		})
		.join(' ')
}
// 扇线
const _lineFan = ({ points, curve }: IPathModeOptions) => {
	let [p, ...rest] = points
	let ps = rayMix(p, rest)
	return _curve({ points: ps, curve })
}

export const _link = ({ points, curve }: IPathModeOptions) => {
	let lps = linkPoints(points)
	return _curve({ points: lps, curve, step: 2 })
}

// interface IPathModeOptions
const pathModeConfig = {
	[PathMode.POINTS]: _points,
	[PathMode.LINE_STRIP]: _lineStrip,
	[PathMode.LINE_LOOP]: _lineLoop,

	[PathMode.LINES]: _lines,
	[PathMode.TRIANGLES]: _triangles,
	[PathMode.TRIANGLE_SERIAL]: _triangleSerial,
	[PathMode.TRIANGLE_SERIAL_LOOP]: _triangleSerialLoop,
	[PathMode.SQUARES]: _squares,
	[PathMode.LINE_FAN]: _lineFan,
	[PathMode.RAY]: _ray,
	[PathMode.RAY_FAN]: _rayFan,
	[PathMode.LINK]: _link,
	[PathMode.TRIANGLE_STRIP]: _triangleStrip,
	[PathMode.SQUARE_STRIP]: _squareStrip,
	[PathMode.TRIANGLE_STRIP_LOOP]: _triangleStripLoop,
	[PathMode.SQUARE_STRIP_LOOP]: _squareStripLoop,
	[PathMode.SQUARE_SERIAL]: _squareSerial,
	[PathMode.SQUARE_SERIAL_LOOP]: _squareSerialLoop,
	[PathMode.TRIANGLE_FAN]: _triangleFan,

	[PathMode.CURVE_STRIP]: _curveStrip, // null
	[PathMode.CURVE_LOOP]: _curveLoop, // null
	[PathMode.BEZIER_STRIP]: _bezierStrip, // null
	[PathMode.BEZIER_LOOP]: _bezierLoop, // null
	[PathMode.DISCRETE_BEZIER]: _discreteBezier, // null
}

export const _pathMode = ({ pathMode, points, curve, closed }: IPathModeOptions): string => {
	if (closed && !pathMode) {
		pathMode = PathMode.LINE_LOOP
	}
	const fn = pathModeConfig[pathMode] || _lineStrip
	return fn({ points, curve })
}
