import { createSvg } from './svg'
import { _d } from './path'
import { _k, _move, _o } from '@/math'
import { _lineLoop } from './pathMode'
import { IPathOptions, IPathProps } from '@/types/path'
// const pointsFnMap = {
//     polygon: polarPoints,
//     grid: gridCrossPoints,
//     gridCell: gridCellPoints,
//     polarGrid: polarGridPoints,
//     spiral: spiralPoints
// }
// export const _motion = (options = {}) => {
//     let { id, dur = '6s', begin = "0s", path = {}, type = "dashoffset" } = options

//     if (type === 'dashoffset') {
//         // dashoffset动画
//         let to = _k(Math.PI * 100)

//         return createSvg("animate", {
//             attributeType: 'XML',
//             attributeName: 'stroke-dashoffset',
//             repeatCount: 'indefinite',
//             from: '0',
//             to,
//             dur,

//         })
//     }
//     let mpath
//     if (id) {
//         mpath = createSvg("mpath", {
//             'xlink:href': '#' + id
//         })
//     }

//     let { shape = 'polygon', r = 100, n = 3, broken = false, closed = true } = path
//     let points = pointsFnMap[shape]({
//         r, n,
//         ...path,
//         o: [0, 0]
//     })
//     let d = _d(points, {
//         broken,
//         closed
//     })

//     // 路径动画
//     return createSvg("animateMotion", {
//         dur,
//         begin,
//         repeatCount: 'indefinite',
//         rotate: 'auto',
//         fill: 'remove',
//         path: d
//     }, {}, mpath)

// }

// 动画
export const _animateTransform = ({ type = 'rotate', from, to, dur }: IPathProps) => {
	return createSvg('animateTransform', {
		attributeType: 'XML',
		attributeName: 'transform',
		begin: '0s',
		dur,
		type,
		from,
		to,
		repeatCount: 'indefinite',
		// fill:'freeze'
	})
}

const _animateMotion = ({ path, dur }: IPathProps) => {
	return createSvg('animateMotion', {
		attributeType: 'XML',
		attributeName: 'transform',
		begin: '0s',
		dur,
		path,
		// type,
		// from,
		// to,
		repeatCount: 'indefinite',
		// fill:'freeze'
	})
}

export const _motion = ({ type, speed = 1, origin, points }: IPathOptions) => {
	let centre, from, to
	let dur = _k(10 / speed) + 's'
	let o = _o(points)

	switch (origin) {
		case 'centre':
			centre = o
			break
		case 'vertex':
			centre = points[0]
			break
	}

	switch (type) {
		case 'rotate':
			from = `0 ${centre.map(t => _k(t)).join(' ')}`
			to = `360 ${centre.map(t => _k(t)).join(' ')}`
			break
		case 'scale':
			from = '1 1'
			to = '2 2'
			break
		case 'skewX':
		case 'skewY':
			from = `0 ${centre.map(t => _k(t)).join(' ')}`
			to = `0 ${centre.map(t => _k(t * 2)).join(' ')}`
			break
		case 'path':
			let ps = _move(points, o, [0, 0])
			let path = _lineLoop({ points: ps })
			return _animateMotion({ path, dur })
	}

	return _animateTransform({ type, from, to, dur })
}
