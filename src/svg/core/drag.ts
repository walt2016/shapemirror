import { _closest, _events, _query, _removeEvents, _style, _attr } from '@/ui/core/dom'
import { _circle, _g } from './svg'
import { _merge, isVisible, _colorProps } from '@/common'
import { IDragCallback, IDragOptions } from '@/types'
import { Point } from '@/types/point'

export const _drag = (points: Point[], callback?: IDragCallback, options?: IDragOptions): SVGElement => {
	// 拖动事件
	let dragedPoint
	let dragedPointIndex
	let boundingRectBorder
	let container
	let _dragPoint = (e: MouseEvent) => {
		let cx = dragedPoint.getAttribute('cx')
		let cy = dragedPoint.getAttribute('cy')
		let x = e.movementX
		let y = e.movementY
		let point = [Number(cx) + x, Number(cy) + y]
		_attr(dragedPoint, {
			cx: point[0],
			cy: point[1],
		})
		points[dragedPointIndex] = point
		let d = callback({ ...options, points, container })
		if (d) {
			_attr(boundingRectBorder, 'd', d)
		}
	}
	let { color } = options
	let _props
	if (isVisible(color)) {
		// 彩色
		let n = points.length
		_props = _colorProps(
			{
				type: color,
				fill: color,
			},
			n
		)
	}

	let result = points.map((t, i) => {
		return _circle(
			{
				o: t,
				r: 3,
				name: 'boundingRectPoint',
			},
			{
				fill: 'gray',
				stroke: 'gray',
				..._props(i),
			},
			{
				mouseenter: (e: MouseEvent) => {
					const el = e.target as HTMLElement
					_attr(el, {
						r: 5,
						fill: 'red',
					})
					_style(el, {
						cursor: 'pointer',
					})
					dragedPoint = el
					dragedPointIndex = i

					let svg = _closest(el, 'svg')
					boundingRectBorder = _query('path[name="edge"]', svg)
					container = svg
				},
				mousedown: (e: MouseEvent) => {
					const el = e.target as HTMLElement
					let svg = _closest(el, 'svg')
					container = svg
					_events(svg, {
						mousemove: _dragPoint,
						mouseup: () => {
							_removeEvents(svg, {
								mousemove: _dragPoint,
							})
						},
					})
				},
				mouseout: e => {
					const el = e.target as HTMLElement
					_attr(el, {
						r: 3,
						fill: 'gray',
					})
				},
			}
		)
	})

	return _g(result, {
		name: options.name,
	})
}
