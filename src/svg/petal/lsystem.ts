import { lSystemPoints } from '@/algorithm/lsystem'
import { IPathProps } from '@/types/path'
import { makeSvgElementByMatrix } from '../helper'
import { ILSystemOptions } from '@/types/lsystem'

export const lsystem = (options: ILSystemOptions, props: IPathProps): SVGElement[] => {
	const matrix = lSystemPoints(options)
	// return colorPath(matrix, options, props)
	return makeSvgElementByMatrix(matrix, options, props)
}
