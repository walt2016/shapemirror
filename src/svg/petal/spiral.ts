import { _colors } from '@/color'
import { spiralPoints } from '@/algorithm/spiral'
import { IPolarOptions } from '@/types/polar'
import { _colorProps } from '@/common'
import { makeSvgElementByMatrix } from '../helper'
import { IPathProps } from '@/types/path'

// 螺旋线
export const _spiral = (options: IPolarOptions, props: IPathProps): SVGElement[] => {
	let matrix = spiralPoints(options)
	return makeSvgElementByMatrix(matrix, options, props)
}
