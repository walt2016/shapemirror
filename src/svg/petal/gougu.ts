import { gouguPoints } from '@/algorithm/gougu'
import { IPathOptions, IPathProps } from '@/types/path'
import { makeSvgElementByMatrix } from '../helper'
export const _gougu = (options: IPathOptions, props: IPathProps): SVGElement[] => {
	const matrix = gouguPoints(options)
	return makeSvgElementByMatrix(matrix, options, props)
}
