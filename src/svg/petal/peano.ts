import { peanoPoints } from '@/algorithm/peano'
import { toMatrix } from '@/math/array'
import { IPathOptions, IPathProps } from '@/types/path'
import { makeSvgElementByMatrix } from '../helper'

export const _peano = (options: IPathOptions, props: IPathProps): SVGElement[] => {
	const { n, o, r, depth, a, noseToTail } = options
	const points = peanoPoints({ o, r, depth, a })
	const len = points.length
	const matrix = toMatrix(points, len / n, noseToTail)
	return makeSvgElementByMatrix(matrix, options, props)
}
