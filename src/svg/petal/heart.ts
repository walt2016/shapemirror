import { _curvePoints } from '@/algorithm/curve'
import { _colorProps, _mirrorColor } from '@/common'
import { IPathOptions, IPathProps } from '@/types/path'
import { makeSvgElementByMatrix } from '../helper'
import { Point } from '@/types/point'
export const _heart = (options: IPathOptions, props: IPathProps): SVGElement[] => {
	const { curveShape } = options
	const matrix: Point[][] = _curvePoints(curveShape, options)
	return makeSvgElementByMatrix(matrix, options, props)
}
