import { rosePoints } from '@/algorithm/rose'
import { IPathOptions, IPathProps } from '@/types/path'
import { makeSvgElementByMatrix } from '../helper'
export const _rose = (options: IPathOptions, props: IPathProps): SVGElement[] => {
	const matrix = rosePoints(options)
	return makeSvgElementByMatrix(matrix, options, props)
}
