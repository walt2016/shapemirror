import { circleMazePoints } from '@/algorithm/maze/circleMaze'
import { mazePoints } from '@/algorithm/maze/maze'
import { waveMazePoints } from '@/algorithm/maze/waveMaze'
import { IPathOptions, IPathProps } from '@/types/path'
import { makeSvgElementByMatrix } from '../helper'

// 迷宫
export const _maze = (options: IPathOptions, props: IPathProps) => {
	const matrix = mazePoints(options)
	return makeSvgElementByMatrix(matrix, options, props)
}

export const _circleMaze = (options: IPathOptions, props: IPathProps) => {
	const matrix = circleMazePoints(options)
	return makeSvgElementByMatrix(matrix, options, props)
}

export const _waveMaze = (options: IPathOptions, props: IPathProps) => {
	const matrix = waveMazePoints(options)
	return makeSvgElementByMatrix(matrix, options, props)
}
