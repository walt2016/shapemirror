import { kochPoints } from '@/algorithm/koch'
import { _polar } from '@/math'
import { polarPoints } from '@/algorithm/polar'
import { IPathProps } from '@/types/path'
import { makeSvgElementByMatrix } from '../helper'
import { Point } from '@/types/point'
import { ICurveOptions } from '@/types/curve'

export const _koch = (options: ICurveOptions, props: IPathProps): SVGElement[] => {
	let matrix: Point[][] = polarPoints(options)
	let { depth = 0, skew = 0, amplitude = 1, loop = true } = options
	const matrix2 = matrix.map(points => {
		return kochPoints({ points, depth, skew, amplitude, loop })
	})
	return makeSvgElementByMatrix(matrix2, options, props)
}
