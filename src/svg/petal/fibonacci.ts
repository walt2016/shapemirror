import { fibonacciPoints } from '@/algorithm/fibonacci'
import { _colorProps } from '@/common'
import { IPolarOptions } from '@/types/polar'
import { makeSvgElementByMatrix } from '../helper'
import { IPathProps } from '@/types/path'
export const _fibonacci = (options: IPolarOptions, props: IPathProps): SVGElement[] => {
	const matrix = fibonacciPoints(options)
	return makeSvgElementByMatrix(matrix, options, props)
}
