import { sierpinskiPoints } from '@/algorithm/sierpinski'
import { plainMatrix, _polar } from '@/math'
import { polarPoints } from '@/algorithm/polar'
import { _traversal } from '@/algorithm/traversal'
import { _bezierCurveTo, _moveTo } from '../core/curve'
import { _path } from '../core/svg'
import { IPathProps } from '@/types/path'
import { makeSvgElementByMatrix } from '../helper'
import { IPointsOptions, Point } from '@/types/point'

export const _sierpinski = (options: IPointsOptions, props: IPathProps): SVGElement[] => {
	const points: Point[] = polarPoints(options)[0]
	const matrix = sierpinskiPoints({
		...options,
		points,
		endToEnd: true,
		loop: true,
	})

	return makeSvgElementByMatrix(matrix, options, props)
}

// 杨辉三角贝塞尔
export const _sierpinskiBezier = (options: IPointsOptions, props: IPathProps) => {
	const points: Point[] = polarPoints(options)[0]
	const { depth = 0, skew = 0, amplitude = 0, loop = true, bottomControlGear } = options
	const matrix = sierpinskiPoints({
		points,
		depth,
		loop,
		bottomControlGear,
		amplitude,
	})
	const n = 4
	const path = _traversal({
		points: plainMatrix(matrix),
		n,
		iter: ({ points: [p1, p2, p3, p4] }) => {
			return _bezierCurveTo([p2, p3, p4])
		},
		init: ({ point }) => _moveTo(point),
		loop,
	}).join(' ')

	return _path(path, {
		stroke: 'red',
		fill: 'none',
		...props,
	})
}
