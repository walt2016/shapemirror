import { plainMatrix } from '@/math'
import { pointsPath } from '../core/path'
import { _colors } from '@/color'
import { _g } from '../core/svg'
import { treePoints, treeModePoints } from '@/algorithm/tree'
import { _colorProps, _merge } from '@/common'
import { IPathOptions, IPathProps } from '@/types/path'
import { makeSvgElementByMatrix } from '../helper'

// 二叉树
export const _tree = (options: IPathOptions, props: IPathProps): SVGElement[] => {
	// let { depth = 5, color = "none", alpha = 1, a = 0, edge } = options
	let matrix = treePoints(options)
	return makeSvgElementByMatrix(matrix, options, props)
}

export const _treeMode = (options: IPathOptions, props: IPathProps): SVGElement[] => {
	let matrix = treeModePoints(options)
	return makeSvgElementByMatrix(matrix, options, props)
}

export const _treeCircle = (options, props) => {
	let { depth = 5, color = 'none' } = options
	let ps = treePoints(options)
	// 彩色
	if (color && color !== 'none') {
		let colors = _colors(color, depth + 1, 1)
		let children = ps.map((t, i) => {
			return pointsPath(
				{
					points: t,
					closed: false,
				},
				{
					...props,
					fill: 'none',
					stroke: colors[i],
				}
			)
		})
		return _g(children)
	} else {
		// 单色
		return pointsPath(
			{
				points: plainMatrix(ps),
				closed: false,
			},
			{
				stroke: 'red',
				fill: 'none',
				...props,
			}
		)
	}
}
