import { _colors } from '@/color'
import { plainMatrix } from '@/math'
import { gridCellPoints } from '@/algorithm/grid'
import { _polygon } from './polygon'
import { _g } from './core/svg'
import { Color } from '@/types/color'

export const _gridPolygon = (options, props) => {
	let { r = 25, a = 0, n, transform, grid, color = Color.ColorCircle } = options

	let gcps = plainMatrix(
		gridCellPoints({
			r: 50,
			...grid,
		})
	)

	let colors = _colors(color, gcps.length, 0.5)
	let children = gcps.map((t, i) => {
		return _polygon(
			{
				o: t,
				n,
				r,
				a,
				transform,
			},
			{
				fill: colors[i], //   'red',
				stroke: 'none',
				...props,
			}
		)
	})
	return _g(children)
}

export const _gridRect = (options, props) => {
	return _gridPolygon(
		{
			...options,
			n: 4,
		},
		props
	)
}
