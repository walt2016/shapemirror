import { _colorProps, _mirrorColor, isVisible } from '@/common'
import { _transform } from '@/math/transform'
import { IPathOptions, IPathProps } from '@/types/path'
import { createSvgPathShape } from '../pathShape'
import { Point } from '@/types/point'

export const makeSvgElementByMatrix = (matrix: Point[][], options: IPathOptions, props: IPathProps): SVGElement[] => {
	const { edge, mirror, transform } = options
	const n = matrix.length
	const color = isVisible(mirror) && mirror && mirror.color ? _mirrorColor(mirror) : options.color
	const _props = _colorProps(color, n)
	return matrix.map((t, i) => {
		const ps = _transform(t, transform)
		return createSvgPathShape(
			{
				...options,
				points: ps, //t,
				mirror: null,
				edge: {
					...props,
					...edge,
					..._props(i),
				},
			},
			props
		)
	})
}

export const makeSvgElementByPoints = (points: Point[], options: IPathOptions, props: IPathProps): SVGElement => {
	const { transform } = options
	const points2 = points.map(t => _transform(t, transform))
	return createSvgPathShape(
		{
			...options,
			points: points2,
		},
		props
	)
}
