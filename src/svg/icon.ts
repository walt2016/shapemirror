import { _lerp, _mid } from '@/math'
import { _squarePoints } from '@/algorithm/square'
import { createPathFn } from './pathShape'
import { IPathProps } from '@/types/path'
import { Point } from '@/types/point'
import { PathMode } from '@/types/pathMode'

const _icon = (points: Point[] | Point[][], props?: IPathProps) => {
	return createPathFn(points)(
		{
			pathMode: PathMode.LINE_FAN, // 'POINTS'
			// labels:{}
		},
		{
			fill: 'red',
			stroke: 'none',
			...props,
		}
	)
}
export const _iconStop = (options, props) => {
	let { o = [400, 300], edge = 36 } = options

	let r = edge / 2 || 18
	let squarePoints = _squarePoints(o, r)
	return _icon(squarePoints, props)
}
export const _iconPlay = (options, props) => {
	let { o = [400, 300], edge = 36 } = options

	let r = edge / 2 || 18

	let squarePoints = _squarePoints(o, r)

	let [p1, p2, p3, p4] = squarePoints
	let p = _mid(p1, p4)

	return _icon([p2, p, p3], props)
}

export const _iconNext = (options, props) => {
	return _iconPlay(options, props)
}

export const _iconPrev = (options, props) => {
	let { o = [400, 300], edge = 36 } = options

	let r = edge / 2 || 18

	let squarePoints = _squarePoints(o, r)

	let [p1, p2, p3, p4] = squarePoints
	let p = _mid(p2, p3)

	return _icon([p1, p, p4], props)
}

export const _iconPause = (options, props) => {
	let { o = [400, 300], edge = 36 } = options
	let r = edge / 2 || 18
	let squarePoints = _squarePoints(o, r)
	let [p1, p2, p3, p4] = squarePoints
	let [p5, p6] = [
		[p1, p2],
		[p4, p3],
	].map(t => _lerp(t[0], t[1], 0.3))
	let [p7, p8] = [
		[p1, p2],
		[p4, p3],
	].map(t => _lerp(t[0], t[1], 0.7))
	// return _icon([p2, p7, p8, p3])
	// return _icon([p1, p5, p6, p4])
	return _icon(
		[
			[p1, p5, p6, p4],
			[p2, p7, p8, p3],
		],
		props
	)
}

export const iconsConfig = {
	iconPlay: _iconPlay,
	iconStop: _iconStop,
	iconPause: _iconPause,
	iconNext: _iconNext,
	iconPrev: _iconPrev,
}
