import { _circle, _g } from './core/svg'
import { renderShape } from './renderShape'
import { linkPoints, linkCrossPoints } from '@/algorithm/link'
import { _dis, _mid } from '@/math'
import { _fractal } from './fractal/fractal'
import { _transform } from '@/math/transform'
import { polarPoints } from '@/algorithm/polar'
import { polylinePoints } from '@/algorithm/line'
import { _colors } from '@/color/index'
import { _motion } from './core/motion'
import { rayMix } from '@/math/arrayMix'
import { isVisible } from '@/common'
import { IPathOptions, IPathProps } from '@/types/path'
import { Shape } from '@/types/shape'
import { PathMode } from '@/types/pathMode'
import { MotionType } from '@/types/motion'
import { Point } from '@/types/point'
import { ILabelOptions, ITextOptions, TextAnchor } from '@/types/text'

// 边
export const _dege = (points: Point[], options?: IPathOptions, props?: IPathProps) => {
	return renderShape(
		{
			shape: Shape.Path, //默认path
			points,
			pathMode: PathMode.LINE_LOOP,
			closed: true,
			...options,
		},
		{
			name: 'edge',
			...props,
			...options.props,
		}
	)
}

// 中心点
export const _centre = ({ o, r }, options?: IPathOptions, props?: IPathProps) => {
	return renderShape(
		{
			shape: Shape.Circle,
			r: r || 5,
			...options,
			o,
		},
		{
			name: 'centre',
			...props,
			...options.props,
		}
	)
}

// 外切圆
export const _excircle = ({ o, r }, options?: IPathOptions, props?: IPathProps) => {
	return renderShape(
		{
			shape: Shape.Circle,
			o,
			r,
		},
		{
			name: 'excircle',
			...props,
			fill: 'none',
			...options.props,
		}
	)
}
// 内切圆，
export const _incircle = (o: Point, points: Point[], opitons?: IPathOptions, props?: IPathProps) => {
	let r = _dis(o, _mid.apply(null, points.slice(0, 2)))
	return renderShape(
		{
			shape: Shape.Circle,
			o,
			r,
		},
		{
			name: 'incircle',
			...props,
			fill: 'none',
			...opitons.props,
		}
	)
}
// 顶点
export const _vertex = (points: Point[], options?: IPathOptions, props?: IPathProps) => {
	// 默认向心
	let { orient = true, n = points.length } = options
	return points.map((t, i) => {
		return renderShape(
			{
				shape: Shape.Circle, //默认圆形
				r: 5,
				o: t,
				a: orient ? (360 * i) / n : 0,
				...options,
			},
			{
				name: 'vertex',
				...props,
				...options.props,
			}
		)
	})
}

// 半径
export const _radius = (o: Point, points: Point[], options?: IPathOptions, props?: IPathProps) => {
	let rps = rayMix(o, points)
	return renderShape(
		{
			shape: Shape.Path, //默认path
			points: rps,
			closed: true,
			...options,
		},
		{
			name: 'radius',
			...props,
			...options.props,
		}
	)
}
export const _labels = ({ points, render, label }: ILabelOptions, opitons?: ITextOptions, props?: IPathProps) => {
	return points.map((t: Point, index) => {
		let [x, y] = t
		return renderShape(
			{
				shape: Shape.Text,
				x,
				y,
				text: render ? render(index) : label || index,
			},
			{
				name: Shape.Text,
				fontSize: 12,
				textAnchor: TextAnchor.Middle,
				dominantBaseline: 'middle',
				...props,
				...opitons,
			}
		)
	})
}

// 连线
export const _links = (points: Point[], options: IPathOptions, props: IPathProps) => {
	let lps = linkPoints(points)

	let children = []
	if (options.crossPoints) {
		let lcps = linkCrossPoints(points)

		children = lcps.map(t => {
			return _circle(
				{ o: t, r: 3 },
				{
					fill: 'red',
					stroke: 'none',
				}
			)
		})
	}

	children[children.length] = renderShape(
		{
			shape: Shape.Path, //默认path
			points: lps,
			closed: false,
			...options,
		},
		{
			name: 'link',
			...props,
			// ...options
		}
	)

	return _g(children)
}

// export const _polygon=(options,props)=>{
//     let points = polarPoints(options)
//     let { transform, mirror, pathMode = 'LINE_LOOP' } = options
//     // 变形函数
//     points = _transform(points, transform)
//     points = _mirrorPoints(points, mirror)
//     return createSvgPathShape({
//         ...options,
//         points,
//         pathMode,
//         mirror: null,
//         color: mirror ? mirror.color : null
//     },props)
// }
// 多边形
export const _polygon = (options: IPathOptions, props: IPathProps) => {
	let {
		width = 800,
		height = 600,
		r = 100,
		n = 6,
		o = [800 / 2, 600 / 2],
		a = 0,
		centre,
		vertex,
		radius,
		excircle,
		incircle,
		links,
		labels,
		fractal,
		// points,
		edge = {}, //默认有边
		transform,
		axis,
		// 路径点
		pathPoints,
		// 动画
		motion,
		color,
	} = options
	//  两种方式：直接传points优先，或者 根据orn计算点，
	let points: Point[] = polarPoints({
		o,
		r,
		n,
		a,
	})[0]

	// 变形函数
	points = _transform(points, transform)

	let container = []
	// 坐标
	if (axis) {
		container.push(
			renderShape({
				shape: Shape.Axis,
				width,
				height,
				o,
				...axis,
			})
		)
	}
	// 路径点
	if (pathPoints) {
		// 默认向心
		let { orient = true, m } = pathPoints
		let pps = polylinePoints(points, {
			m,
		})
		let colors = _colors(color, pps.length, 0.5)
		pps.map((t, i) => {
			let v = renderShape(
				{
					shape: Shape.Circle, //默认圆形
					r: 5,
					o: t,
					a: orient ? (360 * i) / n : 0,
					...pathPoints,
				},
				{
					name: 'pathPoints',
					// stroke: 'none',
					// fill: colors[i],

					fill: 'none',
					stroke: colors[i],
					...props,
					...pathPoints.props,
				}
			)
			container.push(v)
		})
	}

	// 边
	if (edge) {
		let children = []
		// 动画
		if (motion) {
			if (motion.type === MotionType.Dashoffset) {
				// edge.['stroke-dasharray'] = motion['stroke-dasharray'] || '10'

				Object.assign(edge, {
					props: {
						'stroke-dasharray': motion['stroke-dasharray'] || '10',
					},
				})
			}
			children.push(_motion(motion))
		}
		container.push(_dege(points, edge, props))
	}

	// 中心点
	if (isVisible(centre)) {
		container.push(_centre({ o, r: 5 }, centre, props))
	}
	// 顶点
	if (isVisible(vertex)) {
		container.push(..._vertex(points, vertex, props))
	}
	// 半径
	if (isVisible(radius)) {
		container.push(_radius(o, points, radius, props))
	}

	// 外切圆
	if (isVisible(excircle)) {
		container.push(_excircle({ o, r }, excircle, props))
	}
	// 内切圆
	if (isVisible(incircle)) {
		container.push(_incircle(o, points, incircle, props))
	}

	// 连接线
	if (isVisible(links)) {
		container.push(_links(points, links, props))
	}

	if (isVisible(labels)) {
		container.push(..._labels({ points }, labels, props))
	}

	// 分形
	if (fractal) {
		let fn = t => {
			let fs = _fractal(
				{
					...options,
					fractal: t,
					points,
					n,
					o,
				},
				props
			)
			container.push(...fs)
		}
		if (Array.isArray(fractal)) {
			fractal.forEach(t => fn(t))
		} else {
			fn(fractal)
		}
	}

	let len = container.length
	return len
		? len === 1
			? container[0]
			: _g(container, {
					name: Shape.Polygon,
					fill: 'none',
					stroke: 'black',
					...props,
			  })
		: null
}

// 三角形
export const _triangle = (options: IPathOptions, props: IPathProps) => {
	return _polygon(
		{
			shape: Shape.Polygon,
			n: 3,
			...options,
		},
		props
	)
}

// 正方形
export const _square = (options: IPathOptions, props: IPathProps) => {
	return _polygon(
		{
			shape: Shape.Polygon,
			n: 4,
			...options,
		},
		props
	)
}
// 五边形
export const _pentagon = (options: IPathOptions, props: IPathProps) => {
	return _polygon(
		{
			shape: Shape.Polygon,
			n: 5,
			...options,
		},
		props
	)
}
// 六边形
export const _hexagon = (options: IPathOptions, props: IPathProps) => {
	return _polygon(
		{
			shape: Shape.Polygon,
			n: 6,
			...options,
		},
		props
	)
}
// 七边形
export const _heptagon = (options: IPathOptions, props: IPathProps) => {
	return _polygon(
		{
			shape: Shape.Polygon,
			n: 7,
			...options,
		},
		props
	)
}
// 射线
export const _ray = (options: IPathOptions, props: IPathProps) => {
	let radius = options.radius || {}
	let ray = _polygon(
		{
			...options,
			edge: null,
			radius,
		},
		props
	)
	return ray
}
