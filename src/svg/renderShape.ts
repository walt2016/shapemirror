import { factory } from './factory'
import { _labels } from './polygon'
import { createSvgPathShape } from './pathShape'
import { _merge } from '@/common'
import { IPathOptions, IPathProps } from '@/types/path'
import { Point } from '@/types/point'
export const renderPath = (points: Point[], options?: IPathOptions, props?: IPathProps) => {
	return createSvgPathShape(
		{
			...options,
			points,
		},
		_merge(
			{
				stroke: 'blue',
				fill: 'none',
			},
			props
		)
	)
}

const renderUnregistry = (options: IPathOptions) => {
	const { shape, o } = options
	// 提示未注册shape
	const label = `未注册${shape}`
	const labels = _labels(
		{
			points: [o],
			label,
		},
		{},
		{
			'font-size': '20px',
		}
	)
	return labels
}

export const renderShape = (options: IPathOptions, props?: IPathProps) => {
	const { name, shape, pattern, points } = options

	// 填充图案
	if (pattern) {
		props = {
			...props,
			fill: `url(#${name}_${pattern})`,
		}
	}

	const registry = factory(shape, options, props)
	if (registry) {
		return registry
	} else {
		if (points) {
			//         if (isCurve(shape)) {
			//             // 曲线函数
			//             points = _curve(points, options)
			//         }
			return renderPath(points, options, props)
		} else {
			return renderUnregistry(options)
		}
	}
}
