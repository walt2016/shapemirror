import { curvePoints, ellipticalPoints, sawtoothPoints, semicirclePoints, wavePoints } from '@/algorithm/curvebak'
import { sinPoints } from '@/algorithm/sin'
import { CurveKey } from '@/types/curve'
import { Point } from '@/types/point'
import { Shape } from '@/types/shape'
import { _type } from '@/utils'

// 曲线函数
export const CurvePointsConfig = {
	[Shape.Curve]: curvePoints,
	[Shape.Wave]: wavePoints,
	[Shape.Sawtooth]: sawtoothPoints,
	[Shape.Semicircle]: semicirclePoints,
	[Shape.Elliptical]: ellipticalPoints,
	[Shape.Sin]: sinPoints,
}

const getCurveKey = (curve: CurveKey) => {
	let key: string = ''
	if (typeof curve === 'string') {
		key = curve
	} else {
		key = curve.shape || curve.type
	}
	return key
}
export const isCurve = (curve: CurveKey) => {
	let key = getCurveKey(curve)
	return !!CurvePointsConfig[key]
}
export const _curve = (points: Point[], curve: CurveKey) => {
	let key = getCurveKey(curve)
	return CurvePointsConfig[key] ? CurvePointsConfig[key](points, curve) : points
}
