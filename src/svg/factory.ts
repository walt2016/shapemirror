import { _circle, _rect, _line, _text } from './core/svg'
import { patternConfig } from './pattern/index'
import { axisConfig } from './axis/index'
import { fractalConfig } from './fractal/index'
import { _polygon, _triangle, _ray, _square, _pentagon, _hexagon, _heptagon } from './polygon'
import { _star } from './star'
import { _spiral } from './petal/spiral'
import { _maze, _circleMaze, _waveMaze } from './petal/maze'
import { _gridRect, _gridPolygon } from './gridRect'
import { spiralShape, ringShape, rayShape, gridShape, isometricShape, polygonShape } from './polarShape/polarShape'
import { iconsConfig } from './icon'
import { lsystem } from './petal/lsystem'
import { _oneline } from './polarShape/oneline'
import { polarPoints, polarGridPoints } from '@/algorithm/polar'
import { spiralPoints } from '@/algorithm/polarMatrix'
import { gridCrossPoints, gridCellPoints } from '@/algorithm/grid'
import { _koch } from './petal/koch'
import { _sierpinski, _sierpinskiBezier } from './petal/sierpinski'
import { _peano } from './petal/peano'
import { _gougu } from './petal/gougu'
import { _heart } from './petal/heart'
import { _fibonacci } from './petal/fibonacci'
import { _rose } from './petal/rose'
import { _tree, _treeMode } from './petal/tree'
import { renderPath } from './renderShape'
import { Shape } from '@/types/shape'

// 图形函数
export const shapeConfig = {
	[Shape.Circle]: _circle,
	[Shape.Rect]: _rect,
	[Shape.Line]: _line,
	[Shape.Polygon]: _polygon,
	[Shape.Star]: _star,
	[Shape.Ray]: _ray,
	[Shape.Text]: _text,
	[Shape.Triangle]: _triangle,
	[Shape.Square]: _square,
	[Shape.Pentagon]: _pentagon,
	[Shape.Hexagon]: _hexagon,
	[Shape.Heptagon]: _heptagon,
	[Shape.Spiral]: _spiral,
	[Shape.Maze]: _maze,
	[Shape.CircleMaze]: _circleMaze,
	[Shape.WaveMaze]: _waveMaze,
	[Shape.GridRect]: _gridRect,
	[Shape.GridPolygon]: _gridPolygon,
	// twoSpiral: _twoSpiral,
	[Shape.PolarShape]: _oneline, // 镜像函数升级，支持一笔画
	[Shape.SpiralShape]: spiralShape,
	[Shape.RingShape]: ringShape,
	[Shape.RayShape]: rayShape,
	[Shape.GridShape]: gridShape,
	[Shape.IsometricShape]: isometricShape,
	[Shape.PolygonShape]: polygonShape,
	[Shape.LSystem]: lsystem,
	[Shape.LSystemPlant]: lsystem,
	[Shape.Oneline]: _oneline,
	...fractalConfig,
	...axisConfig,
	...patternConfig,
	...iconsConfig,
	[Shape.Koch]: _koch,
	[Shape.Sierpinski]: _sierpinski,
	[Shape.SierpinskiBezier]: _sierpinskiBezier,
	[Shape.Peano]: _peano,
	[Shape.Gougu]: _gougu,
	[Shape.Heart]: _heart,
	[Shape.Fibonacci]: _fibonacci,
	[Shape.Rose]: _rose,
	[Shape.Tree]: _tree,
	[Shape.TreeMode]: _treeMode,
}

// 点阵函数
export const pointsConfig = {
	gridCrossPoints,
	gridCellPoints,
	polarPoints,
	polarGridPoints,
	spiralPoints,
}

export const patternTypes = Object.keys(patternConfig)
export const shapeTypes = Object.keys(shapeConfig)

export const checkRegisteredShape = (shape: string) => {
	return !!shapeConfig[shape]
}

export const renderRegisteredShape = (shape: string, options, props) => {
	return shapeConfig[shape](options, props)
}

export const checkRegisteredPoints = (shape: string) => {
	return !!pointsConfig[shape]
}

export const renderRegisteredPoints = (shape: string, options, props) => {
	let points = pointsConfig[shape](options, props)
	return renderPath(points, options, props)
}

export const factory = (shape: string, options, props) => {
	if (checkRegisteredShape(shape)) {
		return renderRegisteredShape(shape, options, props)
	} else if (checkRegisteredPoints(shape)) {
		return renderRegisteredPoints(shape, options, props)
	}
}
