import { createSvg, svgns, xlinkns } from './core/svg'
import { renderShape } from './renderShape'
import { _append } from '@/ui/core/dom'
import { _screen } from '@/store'
import { translateY } from '@/math'
import { _layout } from '@/common/layout'
import { isObject } from '@/utils'
import { isVisible } from '@/common'
import { IContent } from '@/types/ui'
import { Shape } from '@/types/shape'

// svg包围
export const _svg = (options: IContent): SVGElement => {
	let {
		name,
		width,
		height,
		shapes = [],
		pattern = [],
		props,
		axis,
		// grid,
		// polar,
		layout,
		o,
	} = options

	const screen = _screen(width, height)
	height = screen.height
	width = screen.width
	if (!o) {
		o = screen.o
	}

	const svg = createSvg('svg', {
		width,
		height,
		xmlns: svgns,
		'xmlns:xlink': xlinkns,
		viewBox: `0,0,${width},${height}`,
		// preserveAspectRatio: "XMidYMid meet"
	})

	const fn = t => {
		if (!t) return
		const { shape } = t
		if (shape === Shape.LSystemPlant && (layout == null || layout === '0')) {
			// 植物lsystem的原点下移
			if (typeof height === 'number') {
				o = translateY(o, height / 2 - 100)
			}
		}
		const svgDom = renderShape(
			{
				o,
				...t,
				name,
				width,
				height,
			},
			{
				...props,
				...t.props,
			}
		)
		_append(svg, svgDom)
	}

	// const axisMap = {
	//   axis, grid, polar
	// }
	// Object.keys(axisMap).forEach(t => {

	//   axisMap[t] && fn({
	//     shape: t,
	//     ...axisMap[t]
	//   })
	// })
	if (isVisible(axis)) {
		let type: Shape
		if (isObject(axis)) {
			type = axis.type
		} else {
			type = axis
		}
		if (type !== Shape.None) {
			fn({ shape: type, ...axis })
		}
	}

	pattern.forEach(t => fn(t))

	_layout({ layout, width, height, o }, shapes, fn)

	return svg
}
