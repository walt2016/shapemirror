// 颜色渐变
// 坐标变换
// userSpaceOnUse =》 objectBoundingBox

import { Point } from '@/types/point'

type IViewPoint = {
	x: number
	y: number
	width: number
	height: number
}
export const toObjectBoundingBox = (
	from: Point,
	to: Point,
	viewport?: IViewPoint
): {
	from: Point
	to: Point
} => {
	let [x1, y1] = from
	let [x2, y2] = to
	let { x = 0, y = 0, width = 100, height = 100 } = viewport
	//
	let w = Math.abs(x1 - x2)
	let h = Math.abs(y1 - y2)
	width = Math.max(width, w)
	height = Math.max(height, h)

	let obb = {
		from: [0, 0] as Point,
		to: [0, 1] as Point,
	}
	if (x1 === x2) {
		obb.from[1] = y1 / height
		obb.to[1] = y2 / height
	} else if (y1 === y2) {
		obb.from[0] = x1 / width
		obb.to[0] = x2 / width
		obb.to[1] = 0
	} else {
		let xc = (width + x) / 2
		let yc = (height + y) / 2
		let gtransform = 2 / (w / h + h / w)
		let dx = (xc - width) / gtransform
		let dy = (yc - height) / gtransform

		let _x0 = xc - dy
		let _y0 = yc - dx
		let _x1 = xc + dy
		let _y1 = yc + dx
		// Convert userspace coords (u_*) to objectBoundingBox coords (o_*)
		let o_x0 = (_x0 - x1) / w
		let o_y0 = (_y0 - y1) / h
		let o_x1 = (_x1 - x1) / w
		let o_y1 = (_y1 - y1) / h
		obb = {
			from: [o_x0, o_y0],
			to: [o_x1, o_y1],
		}
	}
	return obb
}

// import {toObjectBoundingBox} from './gradient'
// console.log(toObjectBoundingBox([0,0],[0,500]))

// let a=toObjectBoundingBox([0,0],[0,500])
// console.log(a)

// Convert objectBoundingBox coords to their userspace equivalents, compensating for the obb transform
// x0,y0,w,h are the element (rect) attributes
// o_x0, o_y0, o_x1, o_y1 are the objectBoundingBox coords
//   function toUserSpaceOnUse(x0, y0, w, h, o_x0, o_y0, o_x1, o_y1) {
//     // Convert objectBoundingBox coords (o_*) to userspace coords (u_*)
//     let u_x0 = x0 + o_x0 * w;
//     let u_y0 = y0 + o_y0 * h;
//     let u_x1 = x0 + o_x1 * w;
//     let u_y1 = y0 + o_y1 * h;
//     // Now recalculate the coords to simulate the effect of the objectBoundingBox implicit transformation
//     let gtransform = 2 / (w / h + h / w);
//     let xc = (u_x1 + u_x0) / 2;
//     let yc = (u_y1 + u_y0) / 2;
//     let dx = gtransform * (u_x1 - u_x0) / 2;
//     let dy = gtransform * (u_y1 - u_y0) / 2;
//     let rx0 = xc - dy;
//     let ry0 = yc - dx;
//     let rx1 = xc + dy;
//     let ry1 = yc + dx;
//     return [rx0,ry0,rx1,ry1];
//   }

// Convert userspace coords to their objectBoundingBox equivalents, compensating for the obb transform
// x0,y0,w,h are the element (rect) attributes
// u_x0, u_y0, u_x1, u_y1 are the userspace coords
//   function toObjectBoundingBox(x0, y0, w, h, u_x0, u_y0, u_x1, u_y1) {
//     // Recalculate the coords to simulate the effect of the reverse objectBoundingBox implicit transformation
//     let gtransform = 2 / (w / h + h / w);
//     let xc = (u_x1 + u_x0) / 2;
//     let yc = (u_y1 + u_y0) / 2;
//     let dx = (xc - u_x0) / gtransform;
//     let dy = (yc - u_y0) / gtransform;
//     let _x0 = xc - dy;
//     let _y0 = yc - dx;
//     let _x1 = xc + dy;
//     let _y1 = yc + dx;
//     // Convert userspace coords (u_*) to objectBoundingBox coords (o_*)
//     let o_x0 = (_x0 - x0) / w;
//     let o_y0 = (_y0 - y0) / h;
//     let o_x1 = (_x1 - x0) / w;
//     let o_y1 = (_y1 - y0) / h;
//     return [o_x0, o_y0, o_x1, o_y1];
//   }
