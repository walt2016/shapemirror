import { polarPoints } from '@/algorithm/polar'
import { _transform } from '@/math/transform'
import { IPolarOptions } from '@/types/polar'
import { makeSvgElementByMatrix } from '../helper'
import { Point } from '@/types/point'
import { IPathProps } from '@/types/path'

// 镜像函数升级，支持一笔画  ，替代polarShape
export const _oneline = (options: IPolarOptions, props?: IPathProps): SVGElement[] => {
	let matrix: Point[][] = polarPoints(options)
	const { transform } = options

	matrix = _transform(matrix, transform)

	return makeSvgElementByMatrix(matrix, options, props)
}
