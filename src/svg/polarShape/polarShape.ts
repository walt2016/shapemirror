import { plainMatrix } from '@/math'
import { createSvgPathShape } from '../pathShape'
import { _transform } from '@/math/transform'
import { polarPoints, isometricPoints, polygonPoints } from '@/algorithm/polar'
import { spiralPoints, ringPoints, rayPoints } from '@/algorithm/polarMatrix'
import { gridCellPoints } from '@/algorithm/grid'
import { _colorProps, _mirrorColor } from '@/common'
import { TSweepFlag } from '@/types'
import { makeSvgElementByMatrix, makeSvgElementByPoints } from '../helper'
import { Point } from '@/types/point'
import { IPathProps } from '@/types/path'
import { IPolarOptions } from '@/types/polar'
// 被oneline取代
export const polarShape = (options: IPolarOptions, props: IPathProps): SVGElement[] => {
	let matrix: Point[][] = polarPoints(options)
	let { transform } = options

	// 变形函数
	matrix = _transform(matrix, transform)
	return makeSvgElementByMatrix(matrix, options, props)
}

export const spiralShape = (options: IPolarOptions, props: IPathProps) => {
	let { transform } = options
	let matrix = spiralPoints(options).map(t => _transform(t, transform))
	let mirrorPoints = matrix[matrix.length - 1]
	let points = plainMatrix(matrix)

	return createSvgPathShape(
		{
			...options,
			points,
			mirrorPoints,
		},
		props
	)
}

export const ringShape = (options: IPolarOptions, props: IPathProps): SVGElement[] => {
	// const { transform, mirror, edge } = options
	// const color = _mirrorColor(mirror)
	const matrix = ringPoints(options)

	return makeSvgElementByMatrix(matrix, options, props)
}

export const rayShape = (options: IPolarOptions, props: IPathProps) => {
	const matrix = rayPoints(options)
	return makeSvgElementByMatrix(matrix, options, props)
}

export const isometricShape = (options: IPolarOptions, props: IPathProps) => {
	const points = isometricPoints(options)
	return makeSvgElementByPoints(points, options, props)
}

// 多边形 迭代
export const polygonShape = (options: IPolarOptions, props: IPathProps): SVGElement[] => {
	let matrix = []
	// 变形函数
	// points = _transform(points, transform)
	let { transform, n } = options

	for (let i = 0; i < n; i++) {
		let last = matrix[matrix.length - 1]
		let points = polygonPoints({
			...options,
			o: last ? last[last.length - 1] : options.o,
			end: last ? last[0] : options.end,
			n: n - i,
			sweepFlag: (i % 2) as TSweepFlag,
		})
		matrix.push(points)
	}

	matrix = matrix.map(t => _transform(t, transform))

	return makeSvgElementByMatrix(matrix, options, props)
}

export const gridShape = (options: IPolarOptions, props: IPathProps): SVGElement[] => {
	let { transform } = options
	let matrix = gridCellPoints(options)
	matrix = matrix.map(t => _transform(t, transform))
	return makeSvgElementByMatrix(matrix, options, props)
}
