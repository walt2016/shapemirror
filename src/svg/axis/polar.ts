import { _g, _circle, _text } from '../core/svg'
import { pointsPath } from '../core/path'
import { rayMix } from '@/math/arrayMix'
import { plainMatrix } from '@/math/index'
import { polarPoints, polarGridPoints } from '@/algorithm/polar'
import { Point } from '@/types/point'
import { TextAnchor } from '@/types/text'

// 极坐标
export const _polar = options => {
	let { width = 800, height = 600, r = 100, n = 12, a = 0, props = {}, crossPoints = {}, labels } = options
	let o = [width / 2, height / 2]
	let m = Math.ceil((width > height ? width : height) / r / 2)
	let circles = Array.from({ length: m + 1 }, (_, i) =>
		_circle(
			{ o, r: r * i },
			{
				'stroke-dasharray': 4,
			}
		)
	)
	let children: SVGElement[] = [...circles]

	// 最外层点
	let points: Point[] = polarPoints({
		o,
		r: r * m,
		n,
		a,
	})[0]

	let pgp = []

	if (crossPoints || labels) {
		pgp = polarGridPoints({
			o,
			r,
			n,
			a,
			m,
		})
		pgp = plainMatrix(pgp)
	}

	// 交叉点
	if (crossPoints) {
		children.push(
			...pgp.map(t =>
				_circle(
					{
						o: t,
						r: 2,
					},
					{
						fill: 'red',
						stroke: 'none',
					}
				)
			)
		)
	}

	if (labels) {
		children.push(
			...pgp.map(([x, y], i) =>
				_text(
					{
						x,
						y,
						text: i,
					},
					{
						'font-size': 12,
						'text-anchor': TextAnchor.Middle,
						'dominant-baseline': 'middle',
						stroke: 'gray',
					}
				)
			)
		)
	}

	// 对角点  排序
	let rps = rayMix(o, points)

	// 轴
	let axis = pointsPath(
		{
			points: rps,
			closed: false,
			broken: true,
		},
		{
			'stroke-dasharray': 4,
		}
	)
	children.push(axis)

	let g = _g(children, {
		name: 'polar',
		fill: 'none',
		stroke: 'black',
		...props,
	})
	return g
}
