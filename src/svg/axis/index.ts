import { _polar } from './polar'
import { _axis, _grid } from './axis'
import { Shape } from '@/types/shape'

export const axisConfig = {
	[Shape.Grid]: _grid,
	[Shape.Polar]: _polar,
	[Shape.Axis]: _axis,
	[Shape.None]: () => {},
}
