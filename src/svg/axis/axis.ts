// 坐标轴
import { _g, _text, _line, _marker, _defs, _circle, toTextOptions } from '../core/svg'
import { pointsPath } from '../core/path'
import { translateX, translateY, translate, vertical, plainMatrix, unitMatrix } from '@/math'
import { gridCrossPoints, gridCellPoints } from '@/algorithm/grid'
import { _labels } from '../polygon'
import { _polar } from './polar'
import { IAxisOptions } from '@/types/axis'
import { Point } from '@/types/point'
import { TextAnchor } from '@/types/text'
// 箭头
export const _axisArrow = () => {
	let path = pointsPath(
		{
			points: [
				[0, 0],
				[10, 5],
				[0, 10],
			],
			closed: true,
		},
		{
			fill: '#f00',
			stroke: 'none',
		}
	)
	let id = 'markerArrow'

	let market = _marker(path, {
		id,
		viewBox: '0 0 10 10',
		markerUnits: 'strokeWidth',
		markerWidth: 10,
		markerHeight: 10,
		refX: 1,
		refY: 5,
		orient: 'auto',
	})
	return _defs(market, {
		id,
	})
}

export const _grid = options => {
	let children = []
	let { id = 'grid', width = 800, height = 600, padding = 30, o = [400, 300], r = 50, yPoints, xPoints, crossPoints, cellPoints, props = {} } = options
	let ps = []

	// x轴
	xPoints = xPoints || [
		[0 + padding, o[1]],
		[width - padding, o[1]],
	]
	yPoints = yPoints || [
		[o[0], height - padding],
		[o[0], 0 + padding],
	]
	// x轴
	let n = Math.ceil((width / 2 - padding) / r)
	Array.from({ length: n }).forEach((t, i) => {
		if (i === 0) {
			ps.push(yPoints)
		} else {
			ps.push(translateX(yPoints, -i * r))
			ps.push(translateX(yPoints, i * r))
		}
	})

	let m = Math.ceil((height / 2 - padding) / r)
	Array.from({ length: m }).forEach((t, i) => {
		if (i === 0) {
			ps.push(xPoints)
		} else {
			ps.push(translateY(xPoints, -i * r))
			ps.push(translateY(xPoints, i * r))
		}
	})

	let path = pointsPath(
		{
			points: ps,
			broken: true,
			closed: false,
		},
		{
			'stroke-dasharray': 4,
			stroke: 'gray',
			...props,
		}
	)

	children.push(path)

	// 交叉点
	if (crossPoints) {
		let gps = gridCrossPoints({
			height,
			width,
			o,
			r,
			padding,
		})
		let plaingps = plainMatrix(gps)
		children.push(
			...plaingps.map(t =>
				_circle(
					{
						o: t,
						r: 2,
					},
					{
						fill: 'red',
						stroke: 'none',
					}
				)
			)
		)
		if (crossPoints.label) {
			let coors = plainMatrix(unitMatrix(gps))
			let labels = _labels(
				{
					points: plaingps,
					render: i => `[${coors[i]}]`,
				},
				crossPoints.label
			)
			children.push(...labels)
		}
	}

	// 单元格中心点
	if (cellPoints) {
		let gcps = gridCellPoints({
			height,
			width,
			o,
			r,
			padding,
		})
		let plaincps = plainMatrix(gcps)
		children.push(
			...plaincps.map(t =>
				_circle(
					{
						o: t,
						r: 2,
					},
					{
						fill: 'blue',
						stroke: 'none',
					}
				)
			)
		)

		if (cellPoints.label) {
			let coors = plainMatrix(unitMatrix(gcps))
			let labels = _labels(
				{
					points: plaincps,
					render: i => `[${coors[i]}]`,
				},
				cellPoints.label
			)
			children.push(...labels)
		}
	}
	// return children
	return _g(children, {
		name: id,
		stroke: 'black',
		...props,
	})
}

// // x轴
// const _xAxis = (options) => {
//     _axis(options, 'xAxis')
// }
// // y轴
// const _yAxis = (options) => {
//     _axis(options, 'yAxis')
// }

// 坐标轴 id = xAxis yAxis
export const _axis = (options: IAxisOptions) => {
	let {
		// shape = 'axis',
		id = 'axis',
		width = 800,
		height = 600,
		padding = 30,
		o = [400, 300],
		r = 50,

		// svg,
		sticks = true,
		// marker = {},
		grid,
		props = {},
	} = options

	let children = []

	// let arrow = _axisArrow()
	// arrow && children.push(arrow)

	// x轴
	let xPoints = [
		[0 + padding, o[1]],
		[width - padding, o[1]],
	]
	let xAxis = _line(xPoints, {
		...props,
		stroke: 'black',
		// 'marker-end': 'url(#markerArrow)'
	})

	let xArrow: Point[] = [
		[0, -5],
		[10, 0],
		[0, 5],
	]

	let xAxisArrow = pointsPath(
		{
			points: xArrow.map(t => translate(xPoints[1], t)),
			closed: true,
		},
		{
			fill: 'black',
		}
	)

	// y轴
	let yPoints: Point[] = [
		[o[0], height - padding],
		[o[0], 0 + padding],
	]
	let yAxis = _line(yPoints, {
		...props,
		stroke: 'black',
		// 'marker-end': 'url(#markerArrow)'
	})

	let yArrow = vertical(xArrow)

	let yAxisArrow = pointsPath(
		{
			points: yArrow.map(t => translate(yPoints[1], t)),
			closed: true,
		},
		{
			fill: 'black',
		}
	)

	children.push(xAxis, yAxis)
	children.push(xAxisArrow, yAxisArrow)

	// 刻度
	if (sticks) {
		let { stickWidth = 5 } = sticks //r = 50,

		let ps = []
		let texts = []
		let text = _text(toTextOptions([o[0] - stickWidth - 10, o[1] + stickWidth + 15, 0], ''))
		texts.push(text)

		// x轴
		let n = Math.ceil((width / 2 - padding) / r)
		Array.from({ length: n }).forEach((t, i) => {
			if (i === 0) return

			let stick = [o, translateY(o, stickWidth)]
			ps.push(translateX(stick, -i * r))
			ps.push(translateX(stick, i * r))

			texts.push(_text(toTextOptions(translate(o, [-i * r, stickWidth + 15]), -i)))
			texts.push(_text(toTextOptions(translate(o, [i * r, stickWidth + 15]), i)))
		})

		// y轴
		let m = Math.ceil((height / 2 - padding) / r)
		Array.from({ length: m }).forEach((t, i) => {
			if (i === 0) return
			let stick = [o, translateX(o, -stickWidth)]
			ps.push(translateY(stick, -i * r))
			ps.push(translateY(stick, i * r))

			texts.push(_text(toTextOptions(translate(o, [-stickWidth - 10, -i * r]), i)))
			texts.push(_text(toTextOptions(translate(o, [-stickWidth - 10, i * r]), -i)))
		})

		let path = pointsPath(
			{
				points: ps,
				broken: true,
				closed: false,
			},
			{
				stroke: 'black',
				strokeWidth: 2,
			}
		)
		let labels = _g(texts, {
			name: 'sticks',
			'font-size': 12,
			'text-anchor': TextAnchor.Middle,
			'dominant-baseline': 'middle',
		})

		children.push(path, labels)
	}

	// 网格
	if (grid) {
		children.push(
			_grid({
				width,
				height,
				padding,
				o,
				r,
				yPoints,
				xPoints,
				...grid,
			})
		)
	}

	return _g(children, {
		id,
		name: id,
		stroke: 'black',
		...props,
	})

	// let axis = _defs(_g(children, {
	//     id,
	//     name: id,
	//     stroke: 'black',
	//     ...props

	// }), {
	//     id
	// })
	// let use = _use({ id })
	// return [axis, use]
}

const axisFnMap = {
	axis: _axis,
	grid: _grid,
	polar: _polar,
}

export const _axisFn = options => {
	let { axis, grid, polar } = options
	let children = []
	if (axis) {
		let { shape = 'axis' } = axis
		children.push(
			axisFnMap[shape]
				? axisFnMap[shape](axis)
				: _axis({
						grid: {},
				  })
		)
	}
	if (grid) {
		children.push(_grid(grid))
	}
	if (polar) {
		children.push(_polar(polar))
	}
	return children
}
