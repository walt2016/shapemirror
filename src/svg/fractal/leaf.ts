import { _polar, _mid, _move, _random } from '@/math'
import { _polygon, _square } from '../polygon'
import { _circle, _g } from '../core/svg'
import { polarPoints } from '@/algorithm/polar'
import { _colors } from '@/color'
import { IPathProps } from '@/types/path'
import { Point } from '@/types/point'
import { Color } from '@/types/color'
import { IFractalOptions } from '@/types/fractal'

const squarePoints = (options): Point[] => {
	const { o = [400, 300], r = 100, a = 0 } = options
	const p = _polar(o, r, a)

	// 线段
	const points = [o, p]

	// 中点
	const mp = _mid(o, p)

	// 垂线
	const vPoints = points.map((t, i) => _polar(mp, r / 2, a + 90 + 180 * i))
	// 4个点
	points.push(..._move([p, o], vPoints[0], vPoints[1]))
	return points
}
// 菱形树叶
export const _leaf = (options: IFractalOptions, props?: IPathProps): SVGElement => {
	const { r = 100, a = 0, vertex, fractal, direct = 1 } = options
	const points = squarePoints(options)
	const children = []

	const square = _square(
		{
			points,
		},
		{
			fill: 'none',
			stroke: 'green',
			...props,
		}
	)

	children.push(square)

	// 顶点
	if (vertex) {
		const cirlces = points.map(t => {
			return _circle(
				{
					o: t,
					r: 3,
				},
				{
					fill: 'blue',
				}
			)
		})

		children.push(...cirlces)
	}

	// 分形迭代
	if (fractal) {
		if (fractal.fractalLevel < 0) return
		const angle = _random(30, 45) //45

		const leaf1 = _leaf({
			o: direct ? points[3] : points[2],
			r: r / 2,
			a: a - angle,
			fractal: {
				fractalLevel: fractal.fractalLevel - 1,
			},
			direct: 1,
		})

		const leaf2 = _leaf({
			o: direct ? points[2] : points[1],
			r: r / 2,
			a: a + angle,
			fractal: {
				fractalLevel: fractal.fractalLevel - 1,
			},
			direct: 0,
		})
		children.push(leaf1, leaf2)
	}
	return _g(children)
}

export const _leaves = options => {
	const { o = [400, 300], r = 100, a = 0, n = 6, assemble = true } = options
	const ps = polarPoints({
		o,
		r,
		n,
		a,
	})

	return ps.map((t, i) => {
		return _leaf({
			...options,
			o: assemble ? o : t,
			r,
			a: a + (i * 360) / n,
		})
	})
}

const trianglePoints = options => {
	const { o = [400, 300], r = 100, a = 0 } = options
	const p = _polar(o, r, a)

	// 线段
	const points = [o, p]

	// 中点
	const mp = _mid(o, p)

	// 垂线
	const vPoints = points.map((t, i) => _polar(mp, r / 2, a + 90 + 180 * i))
	// 3个点
	points.push(..._move([p], vPoints[0], vPoints[1]))
	return points
}
// 三角叶
export const _triangleLeaf = (options, props) => {
	const { o = [400, 300], n = 3, r = 100, a = 0, vertex, assemble, color = Color.ColorCircle } = options
	const children = []
	const colors = _colors(color, n)
	const fn = ({ o, r, a, stroke }) => {
		const points = trianglePoints({
			o,
			r,
			a,
		})
		const pg = _polygon(
			{
				points,
			},
			{
				fill: 'none',
				stroke,
				// stroke: colors[] //'green',
				...props,
			}
		)
		children.push(pg)
		// 顶点
		if (vertex) {
			const cirlces = points.map(t => {
				return _circle(
					{
						o: t,
						r: 3,
					},
					{
						fill: 'blue',
					}
				)
			})

			children.push(...cirlces)
		}
		// return pg
	}

	const ps = polarPoints({
		o,
		r,
		n,
		a,
	})
	ps.forEach((t, i) =>
		fn({
			...options,
			o: assemble ? o : t,
			r,
			a: a + (i * 360) / n,
			stroke: colors[i],
		})
	)

	return _g(children)
}
