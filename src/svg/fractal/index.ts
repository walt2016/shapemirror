import { sierpinskiTriangle } from './sierpinskiTriangle'
import { koch, mitsubishi } from './koch'
import { hexaflake } from './hexaflake'
import { _leaf, _leaves, _triangleLeaf } from './leaf'
import { _wanhuachi } from './wanhuachi'
import { _squareFractal } from './square'
// import { _gougu } from './gougu'
import { _isometric } from './isometric'
import { _tree } from './tree'
import { _plant } from './plant'
import { _dragon } from './dragon'
import { _peano } from './peano'
import { Shape } from '@/types/shape'
export const fractalConfig = {
	[Shape.SquareFractal]: _squareFractal,
	[Shape.Isometric]: _isometric,
	[Shape.SierpinskiTriangle]: sierpinskiTriangle,
	[Shape.Koch]: koch,
	[Shape.Mitsubishi]: mitsubishi,
	[Shape.Hexaflake]: hexaflake,
	// gougu: _gougu,
	[Shape.Leaf]: _leaf,
	[Shape.Leaves]: _leaves,
	[Shape.TriangleLeaf]: _triangleLeaf,
	[Shape.Wanhuachi]: _wanhuachi,
	[Shape.Tree]: _tree,
	[Shape.Plant]: _plant,
	[Shape.Dragon]: _dragon,
	[Shape.Peano]: _peano,
}
