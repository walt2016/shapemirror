import { isometricPoints } from '@/algorithm/polar'
import { _circle, _g, _line } from "../core/svg"
// 等角
export const _isometric = (options) => {
    let ips = isometricPoints(options)
    let { vertex } = options

    let children = []
    if (vertex) {
        children.push(...ips.map(t => _circle({
            r: 3, o: t,
        }, {
            fill: 'red'
        })))
    }
    let line = _line(ips)
    children.push(line)
    return _g(children)


}