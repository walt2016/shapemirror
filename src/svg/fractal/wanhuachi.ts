import { _polar } from '@/math'
import { IPathProps } from '@/types/path'
import { pointsPath } from '../core/path'
import { _circle } from '../core/svg'

// 圆的摆线原理
// 两圆内切，o1固定，o2内切滚动
export const _wanhuachi = (options, props: IPathProps) => {
	const { o = [400, 300], r = 100, a = 0, r2 = 50, n = 6, m = 10 } = options
	const children = []
	const outterCircle = _circle(
		{
			o,
			r,
		},
		{
			fill: 'none',
			stroke: 'black',
		}
	)

	children.push(outterCircle)
	const ps = Array.from({ length: n * m }, (t, i) => {
		let a2 = a + (360 * i) / n
		let o2 = _polar(o, r - r2, a2)
		// let innerCircle = _circle({
		//     o: o2,
		//     r: r2
		// }, {
		//     fill: 'none',
		//     stroke: 'black'
		// })

		let p = _polar(o2, r2, a2 + (a2 * r) / r2)
		// let circles = [p].map(t => _circle({
		//     o: t, r: 3
		// }, {
		//     fill: 'blue'
		// }))
		return p

		// innerCircle,
		// children.push(...circles)
	})

	let path = pointsPath(
		{
			points: ps,
		},
		{
			fill: 'none',
			stroke: 'blue',
			...props,
		}
	)

	children.push(path)
	return children
}
