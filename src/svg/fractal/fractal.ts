// 分形
import { _colors } from '@/color'
import { _polygon } from '../polygon'
import { _dis, _mid, _polar, _squarePoints } from '@/math'
import { _mirror } from '@/math/mirror'
import { eachNext, _type, enum2array } from '@/utils'
import { _circle } from '../core/svg'
import { pointsPath } from '../core/path'
import { IPathProps } from '@/types/path'
import { Fractal, IFractalOptions } from '@/types/fractal'
import { Point } from '@/types/point'

export const _fractal = (options: IFractalOptions, props: IPathProps) => {
	if (Array.isArray(options)) {
		return options.map(t => _fractal(t, props))
	}
	let { fractal, points = [], n, o, r, color } = options
	let result = []
	let colors
	if (typeof color === 'string') {
		colors = _colors(color, n, 0.5)
	} else {
		colors = _colors(color.type, n, color.alpha ?? 0.5)
	}
	// let colors = _colors(color, n, 0.5)
	let _props = (index: number) => {
		let color = colors[index % n]
		return {
			stroke: color,
			...props,
		}
	}
	let fn = ({ o, r, a, index, points, closed, broken }: IFractalOptions) => {
		let pg = _polygon(
			{
				...options,
				o,
				r,
				a,
				points,
				closed,
				broken,
				fractal: null,
				axis: null, //去掉坐标
			},
			_props(index)
		)
		result.push(pg)
	}
	let fractalType = ''
	let type = _type(fractal)
	if (type === 'string') {
		fractalType = fractal
	} else if (type == 'object') {
		fractalType = fractal.type
		let radio = fractal.radio
		//缩放
		r *= radio
	}
	switch (fractalType) {
		case Fractal.EdgeFractal:
			{
				// 边中点分形
				let midPoints = _mid(points)
				midPoints.forEach((t, index) => fn({ o: t, index, r }))
			}

			break
		case Fractal.VertexFractal:
			// 顶点分形
			points.forEach((t, index) => fn({ o: t, index, r }))
			break
		case Fractal.EdgeMirror:
			{
				// 边镜像
				let midPoints = _mid(points)
				midPoints.forEach((t, index) => {
					let o2 = _mirror(o, t)
					let a = (index * 360) / midPoints.length
					fn({ o: o2, index, r, a })
				})
			}
			break
		case Fractal.VertexMirror:
			// 顶点镜像
			points.forEach((t, index) => {
				let o2 = _mirror(o, t)
				let a = (index * 360) / points.length
				fn({ o: o2, index, r, a })
			})
			break
		case Fractal.EdgeMid:
			{
				// 边中点连线
				let midFn = (points, level) => {
					if (level <= 0) return []
					let midPoints = _mid(points)
					fn({ o, points: midPoints, closed: true, index: level })
					midFn(midPoints, level - 1)
				}
				midFn(points, fractal.level || 3)
			}
			break
		case Fractal.Yanghui:
			{
				// 杨辉三角
				let midFn = (points: Point[], level: number) => {
					if (level <= 0) return []
					let midPoints = _mid(points)
					let len = midPoints.length
					fn({ points: midPoints, closed: true, index: level })
					points.forEach((t, index) => {
						midFn([t, midPoints[index], midPoints[index - 1 < 0 ? len - 1 : index - 1]], level - 1)
					})
				}
				midFn(points, fractal.level || 3)
			}
			break
		case Fractal.Gougu:
			{
				// 勾股树
				let midPoints = _mid(points)
				let r = _dis(...points.slice(0, 2)) / 2

				eachNext(points, (t, index, next) => {
					let a = 0 + (index * 360) / n
					// a=0
					let mp = midPoints[index]
					let p = _polar(mp, r, a)
					result.push(
						_circle(
							{
								o: p,
								r: 3,
							},
							{
								fill: 'red',
								stroke: 'none',
							}
						)
					)

					result.push(
						_circle(
							{
								o: mp,
								r,
							},
							{
								'stroke-dasharray': 4,
							}
						)
					)

					// 直角三角形
					result.push(
						pointsPath({
							points: [t, p, next],
							closed: true,
							broken: false,
						})
					)

					// 正方形1
					result.push(
						pointsPath({
							points: _squarePoints(t, p),
							closed: true,
							broken: false,
						})
					)

					// 正方形2
					result.push(
						pointsPath({
							points: _squarePoints(p, next),
							closed: true,
							broken: false,
						})
					)
				})
			}
			break
	}
	return result
}

export const fractalTypes = enum2array(Fractal)
