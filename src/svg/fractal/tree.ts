import { plainMatrix } from '@/math'
import { pointsPath } from '../core/path'
import { _colors } from '@/color'
import { _g } from '../core/svg'
import { treePoints } from '@/algorithm/tree'
import { ITreeOptions } from '@/types/tree'

// 二叉树
export const _tree = (options: ITreeOptions, props) => {
	const { depth = 5, color = 'none', alpha = 1 } = options
	const ps = treePoints(options)

	// 彩色
	if (color && color !== 'none') {
		const colors = _colors(color, depth + 1, alpha)
		const children = ps.map((t, i) => {
			return pointsPath(
				{
					points: t,
					closed: false,
				},
				{
					...props,
					fill: 'none',
					stroke: colors[i],
				}
			)
		})

		return _g(children)
	} else {
		// 单色
		return pointsPath(
			{
				points: plainMatrix(ps),
				closed: false,
			},
			{
				stroke: 'red',
				fill: 'none',
				...props,
			}
		)
	}
}

export const _treeCircle = (options, props) => {
	const { depth = 5, color = 'none' } = options
	const ps = treePoints(options)
	// 彩色
	if (color && color !== 'none') {
		const colors = _colors(color, depth + 1, 1)
		const children = ps.map((t, i) => {
			return pointsPath(
				{
					points: t,
					closed: false,
				},
				{
					...props,
					fill: 'none',
					stroke: colors[i],
				}
			)
		})
		return _g(children)
	} else {
		// 单色
		return pointsPath(
			{
				points: plainMatrix(ps),
				closed: false,
			},
			{
				stroke: 'red',
				fill: 'none',
				...props,
			}
		)
	}
}
