import { polarPoints } from '@/algorithm/polar'
import { _rotate, plainMatrix } from '@/math'
import { createPathFn } from '../pathShape'
import { eachNext } from '@/utils'
import { IFractalOptions } from '@/types/fractal'
import { Point } from '@/types/point'

const turnout = (p1: Point, p2: Point): Point[] => {
	let [x1, y1] = p1
	let [x2, y2] = p2
	let x3 = (x2 - x1) / 3 + x1
	let y3 = (y2 - y1) / 3 + y1
	let x4 = ((x2 - x1) / 3) * 2 + x1
	let y4 = ((y2 - y1) / 3) * 2 + y1
	let p5 = _rotate([x4, y4], [x3, y3], -60)
	return [p1, [x3, y3], p5, [x4, y4], p2]
	// return [p1, [x3, y3], [...p5,x4, y4], p2]
}
export const kochPoints = (options: IFractalOptions): Point[] => {
	const { o = [300, 200], r = 200, n = 3, depth = 3, a = 0 } = options
	const points: Point[] = polarPoints({
		o,
		r,
		n,
		a,
	})[0]
	const _iter = (ps, depth): Point[] => {
		const len = ps.length
		if (depth === 0) {
			const result = ps.map((t, index) => {
				const next = ps[(index + 1) % len]
				return turnout(t, next)
			})
			return plainMatrix(result)
		} else {
			const result = ps.map((t, index) => {
				const next = ps[(index + 1) % len]
				return _iter(turnout(t, next), depth - 1)
			})
			return plainMatrix(result)
		}
	}

	return _iter(points, depth)
}

export const koch = createPathFn(kochPoints)

// 内拐
const turnin = (p1: Point, p2: Point): Point[] => {
	let [x1, y1] = p1
	let [x2, y2] = p2

	let x3 = (x2 - x1) / 3 + x1
	let y3 = (y2 - y1) / 3 + y1
	let x4 = ((x2 - x1) / 3) * 2 + x1
	let y4 = ((y2 - y1) / 3) * 2 + y1
	// let x5 = x3 + ((x2 - x1) - (y2 - y1) * Math.sqrt(3)) / 6;
	// let y5 = y3 + ((x2 - x1) * Math.sqrt(3) + (y2 - y1)) / 6;
	// return [p1, [x3, y3], [x5, y5], [x4, y4], p2]

	let p5 = _rotate([x4, y4], [x3, y3], 60)
	return [p1, [x3, y3], p5, [x4, y4], p2]
}

// 三菱
export const mitsubishiPoints = (options: IFractalOptions): Point[] => {
	const { o = [300, 200], r = 200, n = 3, depth = 3, a = 0 } = options

	const points = polarPoints({
		o,
		r,
		n,
		a,
	})

	const result = []
	const _iter = (ps, depth) => {
		if (depth === 0) {
			eachNext(ps, (t, index, next) => {
				result.push(...turnin(t, next))
			})
		} else {
			eachNext(ps, (t, index, next) => {
				_iter(turnin(t, next), depth - 1)
			})
		}
	}
	_iter(points, depth)
	return result
}

export const mitsubishi = createPathFn(mitsubishiPoints)
