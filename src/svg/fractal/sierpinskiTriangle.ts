import { createPathFn } from '../pathShape'
import { sierpinskiTrianglePoints } from '@/algorithm/sierpinski'

export const sierpinskiTriangle = createPathFn(sierpinskiTrianglePoints)
