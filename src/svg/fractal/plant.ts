import { colorPath } from '../pathShape'
import { treePoints } from '@/algorithm/tree'

// 植物 tree的升级
export const _plant = (options, props) => {
	// const { depth = 5, color = "none", alpha = 1, pathMode } = options
	const ps = treePoints(options)
	return colorPath(ps, options, props)
}
