import { polarPoints } from '@/algorithm/polar'
import { IPathProps } from '@/types/path'
import { _leaf } from './leaf'
import { Point } from '@/types/point'
import { IFractalOptions } from '@/types/fractal'

export const _gougu = (options: IFractalOptions, props: IPathProps): SVGElement[] => {
	const { o = [400, 300], r = 100, a = 0, n = 6 } = options
	const ps: Point[] = polarPoints({
		o,
		r,
		n,
		a,
	})[0]

	return ps.map((t, i) => {
		return _leaf(
			{
				o,
				r,
				a: a + (i * 360) / n,
				...options,
			},
			props
		)
	})
}
