import { _colors } from '@/color'
import { _polar } from '@/math'
import { polarPoints } from '@/algorithm/polar'
import { _g } from '../core/svg'
import { createSvgPathShape } from '../pathShape'
import { Point } from '@/types/point'

export const _squareFractal = (options, props) => {
	const { o = [400, 300], r = 100, a = 0, fractalLevel = 3, assemble, color = 'rd', pathMode } = options
	const n = 4
	const points: Point[] = polarPoints({
		o,
		r,
		a,
		n,
	})[0]

	const children = []
	// const sf = pointsPath({
	//     points,
	//     closed: true

	// }, {
	//     stroke: 'blue',
	//     fill: 'none',
	//     ...props
	// })
	const sf = createSvgPathShape(
		{
			...options,
			points,
			mode: pathMode,
		},
		{
			stroke: 'blue',
			fill: 'none',
			...props,
		}
	)

	children.push(sf)

	if (fractalLevel > 0) {
		const colors = _colors(color, n, 0.5)
		const sfs = points.map((t, i) => {
			const r2 = r / 2
			const o2 = assemble ? t : _polar(t, r2, a + (i * 360) / n)
			// (i+2) %n
			const a2 = a
			return _squareFractal(
				{
					...options,
					o: o2,
					r: r / 2,
					a: a2,
					fractalLevel: fractalLevel - 1,
					assemble,
					color,
				},
				{
					fill: colors[i], //i === 0 ? colors[i] : 'none' //'red'
				}
			)
		})
		children.push(...sfs)
	}
	return _g(children)
}
