import { polarPoints } from '@/algorithm/polar'
import { createPathFn } from '../pathShape'
import { plainMatrix } from '@/math'
import { IFractalOptions } from '@/types/fractal'
import { Point } from '@/types/point'

export const _peanoPoints = (options: IFractalOptions): Point[] => {
	const { o = [400, 300], r = 100, a = 0, depth = 3 } = options
	const n = 4
	const points: Point[] = polarPoints({
		o,
		r,
		a,
		n,
	})[0]

	const _iter = (points: Point[], { depth, a }): Point[] => {
		// const stack = []
		const ps = points.map((t, i) => {
			const m = i % 4
			let a2 = a
			let direction = 1
			if (m === 0) {
				direction = -1
				a2 = a + 90
			} else if (m === 3) {
				a2 = a - 90
				direction = -1
			}

			return polarPoints({
				o: t,
				r: r / Math.pow(2, depth + 1),
				a: a2,
				n,
				direction,
			})[0]
			// if (i === 0) {
			//     stack.push(t)
			// }
			// const next = points[i + 1]
			// if (next) {
			//     stack.push(..._item(t, next))

			// }
		})
		return plainMatrix(ps)
	}

	if (depth > 0) {
		let ps = points
		for (let i = 0; i < depth; i++) {
			ps = _iter(ps, {
				depth: i,
				a,
			})
		}
		return ps
	}
	return points
}

export const peanoPoints = (options): Point[] => {
	const _iter = (options, { depth }) => {
		if (depth === 0) {
			const { o = [400, 300], r = 100, a = 0 } = options
			const n = 4
			const points: Point[] = polarPoints({
				o,
				r,
				a,
				n,
			})[0]
			return points
		} else if (depth == 1) {
			const points = _iter(options, { depth: 0 })
			const d = Math.pow(4, depth - 1)
			const { a, r } = options
			const ps = points.map((t, i) => {
				let a2 = a
				const r2 = r / Math.pow(2, depth)
				const m = Math.round(i / d)
				if (m === 0) {
					a2 = a + 90
					return _iter(
						{
							...options,
							o: t,
							r: r2,
							a: a2,
						},
						{ depth: 0 }
					).reverse()
				} else if (m === 3) {
					a2 = a - 90
					return _iter(
						{
							...options,
							o: t,
							r: r2,
							a: a2,
						},
						{ depth: 0 }
					).reverse()
				}

				return _iter(
					{
						...options,
						o: t,
						r: r2,
						a: a2,
					},
					{ depth: 0 }
				)
			})

			return plainMatrix(ps)
		} else {
			const points = _iter(options, { depth: depth - 1 })

			const d = Math.pow(4, depth - 1)
			const { a, r } = options
			const ps = points.map((t, i) => {
				let a2 = a
				const r2 = r / Math.pow(2, depth)
				const m = Math.round(i / d)
				const o = t
				if (m === 0) {
					a2 = a + 90
					return _iter(
						{
							...options,
							o,
							r: r2,
							a: a2,
						},
						{ depth: 0 }
					).reverse()
				} else if (m === 3) {
					a2 = a - 90
					return _iter(
						{
							...options,
							o,
							r: r2,
							a: a2,
						},
						{ depth: 0 }
					).reverse()
				}

				return _iter(
					{
						...options,
						o,
						r: r2,
						a: a2,
					},
					{ depth: 0 }
				)
			})
			return plainMatrix(ps)
		}
	}
	const { depth } = options
	const ps = _iter(options, { depth })
	return ps
	// const d = Math.pow(4, depth)
	// return ps.slice(0,d )
}

export const _peano = createPathFn(peanoPoints)
