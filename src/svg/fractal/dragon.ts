import { polarPoints } from '@/algorithm/polar'
import { createPathFn } from '../pathShape'
import { rotatePoints } from '@/math'
import { Point } from '@/types/point'

export const _dragonPoints = options => {
	let { o = [400, 300], n = 2, r = 100, a = 0, depth = 3 } = options
	let points: Point[] = polarPoints({
		o,
		r,
		a,
		n,
	})[0]
	let _item = (p1: Point, p2: Point, i: number) => {
		let [c1, c2] = rotatePoints([p1, p2], 90)
		return [i % 2 ? c1 : c2, p2]
	}
	const _iter = (points: Point[]) => {
		let stack = []
		points.forEach((t, i) => {
			if (i == 0) {
				stack.push(t)
			}
			let next = points[i + 1]
			if (next) {
				stack.push(..._item(t, next, i))
				// let [c1, c2] = rotatePoints([t, next], 90)
				// switch (type) {
				//     case 'dragon':
				//         stack.push(i % 2 ? c1 : c2, next)
				//         break
				//     case 'c':
				//         stack.push(c2, next)
				//         break
				//     default:
				//         // stack.push(c1, next)
				//         stack.push(i % 2 ? c1 : c2, next)

				// }
			}
		})
		return stack
	}

	if (depth > 0) {
		let ps = points
		for (let i = 0; i < depth; i++) {
			ps = _iter(ps)
		}
		return ps
	}
	return points
}

export const _dragon = createPathFn(_dragonPoints)
