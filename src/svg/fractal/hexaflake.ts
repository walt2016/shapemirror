import { _mirror } from '@/math/mirror'
import { polarPoints } from '@/algorithm/polar'
import { pointsPath } from '../core/path'
import { Point } from '@/types/point'

export const hexaflakePoints = (options): Point[] => {
	const { o = [300, 200], r = 200, n = 3, depth = 3, a = 0 } = options

	const result: Point[] = []
	const _iter = (o, r, n, a, depth) => {
		if (depth === 0) {
			const points: Point[] = polarPoints({
				o,
				r,
				n,
				a,
			})[0]
			result.push(...points)
			points.forEach(t => {
				result.push(..._mirror(points, t))
			})
		} else {
			const points: Point[] = polarPoints({
				o,
				r,
				n,
				a,
			})[0]
			points.forEach(t => {
				const o2 = _mirror(t, o)
				_iter(o2, r / 3, n, a, depth - 1)
			})
		}
	}

	_iter(o, r, n, a, depth)
	return result
}

// export const hexaflake = createPathFn(hexaflakePoints)

export const hexaflake = (options, props) => {
	const stps = hexaflakePoints(options)
	const { n } = options
	return pointsPath(
		{
			points: stps,
			closed: false,
			broken: n,
		},
		props
	)
}
