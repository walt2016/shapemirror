import { _path } from '../core/svg'
import { _move, _o } from '@/math'
import { _drag } from '../core/drag'
import { _merge } from '@/common'
import { _pathMode, _points } from '../core/pathMode'
import { IPathProps } from '@/types/path'
import { IPathModeOptions } from '@/types/pathMode'
export const _centre = ({ points, centre, pathMode, curve }: IPathModeOptions, props: IPathProps) => {
	const o = _o(points)
	const { r, draggable } = centre
	if (draggable) {
		return _drag(
			[o],
			options => {
				const o2 = options.points[0]
				const ps = _move(points, o, o2)
				return _pathMode({ pathMode, points: ps, curve })
			},
			{
				curve,
				pathMode,
			}
		)
	}
	return _path(_points({ points: [o] }, { r }), { ...props, ...centre, name: 'centre' })
}
