import { _o } from '@/math'
import { _taichiToSeg } from '../core/curve'
import { Point } from '@/types/point'
import { _path } from '../core/svg'
import { IPathProps } from '@/types/path'

export enum FillPattern {
	taichi = 'taichi',
}

export const _taichi = (points: Point[]) => {
	const o = _o(points)
	return _taichiToSeg(o, [points[0], points[1]])
}
const fillPatterConfig = {
	[FillPattern.taichi]: _taichi,
}

export const createFillByPattern = (pattern: string, points: Point[], props: IPathProps) => {
	const fn = fillPatterConfig[pattern as FillPattern]
	if (fn) {
		return _path(fn(points), {
			stroke: 'red',
			fill: 'none',
			...props,
		})
	}
}
