// 填充
import { fillPatternMap } from '@/algorithm/fill'
import { createSvgPathShape } from '.'
import { IPathProps } from '@/types/path'
import { FillPattern, createFillByPattern } from './fillPattern'
import { IPathModeOptions } from '@/types/pathMode'

export const _fill = (options: IPathModeOptions, props: IPathProps) => {
	const { points = [], fill = {}, curve } = options
	const { pattern, inheritCurve = true } = fill

	if (pattern in FillPattern) {
		return createFillByPattern(pattern, points, props)
	}

	const fp = fillPatternMap({
		...fill,
		points,
	})

	return createSvgPathShape(
		{
			name: 'fill_' + pattern,
			curve: inheritCurve ? curve : 'none',
			...fp,
		},
		props
	)
}
