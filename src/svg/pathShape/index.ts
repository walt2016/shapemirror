import { _path, _g, _text } from '../core/svg'
import { _o, _dis, _mid, plainMatrix, _moveDis } from '@/math/index'
import { linkCrossPoints } from '@/algorithm/link'
import { _colors } from '@/color/index'
import { isVisible, _mirrorPath, _borderPoints, _colorProps, _mergeProps, _merge } from '@/common'
import { _motion } from '../core/motion'
import { _pathMode, _lines, _points, _link } from '../core/pathMode'
import { _circle } from '../core/circle'
import { _mark } from '../core/mark'
import { _fill } from './fill'
import { _mirrorColor } from '@/common'
import { _vertex } from './vertex'
import { _centre } from './centre'
import { _radius } from './radius'
import { _edge } from './edge'
import { IPathProps, IPathOptions } from '@/types/path'
import { Point } from '@/types/point'
import { _labels } from './labels'
import { _dashLines } from './dashLines'
import { _edgeExtension } from './edgeExtension'
import { _excircle } from './excircle'
import { _incircle } from './incircle'

/**
 * 生成svg路径图形
 * @param options
 * @param props
 * @returns
 */
export const createSvgPathShape = (options: IPathOptions, props?: IPathProps): SVGElement => {
	const { name, points, pathMode, curve, mirror, vertex, centre, excircle, incircle, radius, labels, linkCross, links, motion, edgeExtension, edge, mark, fill } = options

	const gs = []
	// 填充
	if (isVisible(fill)) {
		gs[gs.length] = _fill({ points, fill, curve }, _merge(props, fill))
	}

	// 边
	if (edge) {
		if (isVisible(edge)) {
			gs[gs.length] = _edge({ pathMode, points, curve }, _merge(props, edge))
		}
	} else {
		gs[gs.length] = _edge({ pathMode, points, curve }, props)
	}

	// 边延长线
	if (isVisible(edgeExtension)) {
		gs[gs.length] = _edgeExtension({ points, edgeExtension }, props)
	}
	// 半径
	if (isVisible(radius)) {
		gs[gs.length] = _radius({ points, radius }, props)
	}
	// 顶点
	if (isVisible(vertex)) {
		gs[gs.length] = _vertex({ points, vertex, pathMode, curve }, props)
	}
	// 中点
	if (isVisible(centre)) {
		gs[gs.length] = _centre({ points, centre, pathMode, curve }, props)
	}
	// 外切圆
	if (isVisible(excircle)) {
		gs[gs.length] = _excircle({ points, excircle }, props)
	}
	// 内切圆
	if (isVisible(incircle)) {
		gs[gs.length] = _incircle({ points, incircle }, props)
	}

	// 标注
	if (isVisible(labels)) {
		// 与边的颜色一致
		if (edge && labels.sameColorAsEdge) {
			labels.stroke = edge.stroke
		}
		gs[gs.length] = _labels({ points }, labels, _merge(props, labels))
	}

	// 连线交叉点
	if (isVisible(linkCross)) {
		if (points.length > 50) {
			// '截取了50个节点，linkCross'
			points.splice(50)
		}

		const lcps = linkCrossPoints(points)
		gs[gs.length] = _path(_points({ points: lcps }), _merge(props, linkCross))
	}

	// 连线 与 交叉点
	if (isVisible(links)) {
		gs[gs.length] = _path(_link({ points }), links)
		if (links.crossPoints) {
			const lcps = linkCrossPoints(points)
			gs[gs.length] = _path(_points({ points: lcps }), _merge({ fill: 'red' }, links))
		}
	}

	// 标注
	if (isVisible(mark)) {
		gs[gs.length] = _mark({ points, mark }, { ...props, ...mark })
	}

	// 镜像
	if (isVisible(mirror)) {
		gs.push(..._mirrorPath(createSvgPathShape, options, props))
	}

	// motion
	if (isVisible(motion)) {
		gs[gs.length] = _motion({ ...motion, points })
	}

	return _g(gs, {
		name,
	})
}

export const createPathFn = genPoints => {
	return (options: IPathOptions, props: IPathProps): SVGElement => {
		const points = typeof genPoints === 'function' ? genPoints(options) : genPoints
		return createSvgPathShape(
			{
				...options,
				points,
			},
			{
				stroke: 'blue',
				fill: 'none',
				...props,
			}
		)
	}
}

// 彩色
export const colorPath = (matrix: Point[][], options: IPathOptions, props: IPathProps): SVGElement => {
	const { color, alpha, oneLine = false } = options
	const _fn = colors => {
		const children = matrix.map((t, i) => {
			const newProps = {
				...props,
				fill: 'none',
				stroke: Array.isArray(colors) ? colors[i] : colors,
			}
			return createSvgPathShape(
				{
					...options,
					points: t,
					motion: null,
				},
				newProps
			)
		})
		return _g(children)
	}
	if (oneLine) {
		// 单色 一笔画
		const points = plainMatrix(matrix)
		return createSvgPathShape(
			{
				...options,
				points,
			},
			{
				stroke: 'red',
				fill: 'none',
				...props,
			}
		)
	} else if (color) {
		if (color === 'none') {
			return _fn('red')
		} else {
			// 彩色
			const colors = _colors(color, matrix.length, alpha)
			return _fn(colors)
		}
	}
}
