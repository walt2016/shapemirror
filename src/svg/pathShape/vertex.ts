import { _path, _circle as createCircle } from '../core/svg'
import { _circle } from '../core/circle'
import { _merge, isVisible, _colorProps } from '@/common'
import { _drag } from '../core/drag'
import { _pathMode, _points, _ray } from '../core/pathMode'
import { _attr, _query } from '@/ui/core/dom'
import { _o } from '@/math'
import { IPathOptions, IPathProps } from '@/types/path'
import { Point } from '@/types/point'

export const _vertex = ({ points, vertex, curve, pathMode }: IPathOptions, props?: IPathProps) => {
	const { one, r, draggable, color } = vertex
	if (one) {
		return _path(_circle({ o: points[0], r }), _merge(props, vertex))
	}
	if (draggable) {
		// 可拖动
		const updateEdgePath = (points: Point[], container: HTMLElement) => {
			const edge = _query('path[name="edge"]', container)
			const path = _pathMode({ points, curve, pathMode })
			_attr(edge, {
				d: path,
			})
		}
		const updateCentrePath = (points: Point[], container: HTMLElement) => {
			const centre = _query('path[name="centre"]', container)
			const o = _o(points)
			const path = _points({ points: [o] }, { r })
			_attr(centre, {
				d: path,
			})
		}
		const updateRadiusPath = (points: Point[], container: HTMLElement) => {
			const radius = _query('path[name="radius"]', container)
			const path = _ray({ points })
			_attr(radius, {
				d: path,
			})
		}
		return _drag(
			points,
			({ points, container }) => {
				updateEdgePath(points, container)
				updateCentrePath(points, container)
				updateRadiusPath(points, container)
			},
			{ color }
		)

		// return _drag(points, _pathMode, {
		//     curve, pathMode,
		//     name: 'vertex'
		// })
	}
	if (isVisible(color)) {
		// 彩色
		const n = points.length
		const _props = _colorProps(
			{
				type: color,
				fill: color,
			},
			n
		)

		return points.map((t, i) => {
			return createCircle(
				{
					o: t,
					r,
				},
				_merge(vertex, _props(i))
			)
		})
	}
	// 单色
	return _path(_points({ points }, { r }), _merge(props, vertex))
}
