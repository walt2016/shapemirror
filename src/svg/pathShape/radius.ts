import { _path } from '../core/svg'
import { _ray, _rayOne } from '../core/pathMode'
import { _merge } from '@/common'
import { IPathProps } from '@/types/path'
export const _radius = ({ points, radius }, props: IPathProps) => {
	const { one } = radius
	if (one) {
		return _path(_rayOne({ points }), _merge(props, radius))
	}
	return _path(_ray({ points }), { ...props, ...radius, name: 'radius' })
}
