import { edgeExtensionCrossPoints, edgeExtensionPoints } from '@/algorithm/link'
import { _dashLines } from './dashLines'
import { _points } from '../core/pathMode'
import { _path } from '../core/svg'
import { _merge } from '@/common'
import { _labels } from './labels'
import { IPathProps } from '@/types/path'

// 边延长线
export const _edgeExtension = ({ points, edgeExtension }, props: IPathProps) => {
	const gs = []
	const { crossPoints, labels } = edgeExtension
	const ps = edgeExtensionPoints(points, edgeExtension.iterNum)

	gs.push(_dashLines(ps, { ...props, ...edgeExtension }))

	if (crossPoints || labels) {
		const cps = edgeExtensionCrossPoints(points)
		// 交点
		if (crossPoints) {
			gs.push(_path(_points({ points: cps }, { r: edgeExtension.r }), _merge(props, edgeExtension)))
		}

		// 标注
		if (labels) {
			gs.push(
				_labels(
					{ points: cps },
					{},
					{
						stroke: 'blue',
					}
				)
			)
		}
	}
	return gs
}
