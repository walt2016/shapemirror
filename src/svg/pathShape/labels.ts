import { genLabel } from '@/common/labels'
import { _moveDis, _o } from '@/math'
import { IPathProps } from '@/types/path'
import { _text } from '../core/svg'
import { ILabelOptions, ITextOptions, TextAnchor } from '@/types/text'

// 标注
export const _labels = ({ points, render }: ILabelOptions, opitons?: ITextOptions, props?: IPathProps) => {
	const { type } = props
	const o = _o(points)
	return points.map((t, index) => {
		const text = genLabel(type, index)
		const pos = _moveDis(t, o, 10)
		const [x, y] = pos
		return _text(
			{
				shape: 'text',
				x,
				y,
				text: render ? render(index) : text,
			},
			{
				name: 'text',
				fontSize: 12,
				textAnchor: TextAnchor.Middle,
				dominantBaseline: 'middle',
				...props,
				...opitons,
			}
		)
	})
}
