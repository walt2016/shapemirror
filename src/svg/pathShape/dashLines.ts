import { Point } from '@/types/point'
import { _lines } from '../core/pathMode'
import { _path } from '../core/svg'

export const _dashLines = (points: Point[], props) => {
	return _path(_lines({ points }), props)
}
