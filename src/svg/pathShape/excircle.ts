import { _dis, _o } from '@/math'
import { _path } from '../core/svg'
import { _merge } from '@/common'
import { IPathProps } from '@/types/path'
import { _circle } from '../core/circle'

export const _excircle = ({ points, excircle }, props: IPathProps) => {
	const o = _o(points)
	const r = _dis(o, points[0])
	const circle = _circle({ o, r })
	return _path(circle, _merge(props, excircle))
}
