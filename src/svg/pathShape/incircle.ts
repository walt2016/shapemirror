import { _dis, _mid, _o } from '@/math'
import { _circle } from '../core/circle'
import { _path } from '../core/svg'
import { _merge } from '@/common'

export const _incircle = ({ points, incircle }, props) => {
	const o = _o(points)
	const r = _dis(o, _mid.apply(null, points.slice(0, 2)))
	const circle = _circle({ o, r })
	return _path(circle, _merge(props, incircle))
}
