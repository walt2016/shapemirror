import { StrokeMode } from '@/types/stroke'
import { _pathMode } from '../core/pathMode'
import { _path } from '../core/svg'
import { isometricalScale } from '@/algorithm/isometricalScale'
import { IPathProps } from '@/types/path'
import { IPathModeOptions } from '@/types/pathMode'
// 边
export const _edge = ({ pathMode, points, curve }: IPathModeOptions, props: IPathProps) => {
	const { strokeMode, strokeWidth } = props
	switch (strokeMode) {
		case StrokeMode.InnerStroke:
			points = isometricalScale(points, -strokeWidth / 2)
			break
		case StrokeMode.OuterStroke:
			points = isometricalScale(points, +strokeWidth / 2)
			break
	}

	const path = _pathMode({ pathMode, points, curve })

	return _path(path, {
		stroke: 'red',
		fill: 'none',
		name: 'edge',
		...props,
	})
}
