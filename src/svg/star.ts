// 星星
import { _polygon } from './polygon'
import { starPoints } from '@/algorithm/star'
import { polarPoints } from '@/algorithm/polar'
import { createSvgPathShape } from './pathShape'
import { _g } from './core/svg'
import { PathMode } from '@/types/pathMode'
import { IPolarOptions } from '@/types/polar'
import { Point } from '@/types/point'
import { IPathProps } from '@/types/path'

export const _star = (options: IPolarOptions, props: IPathProps) => {
	const { o, r, n, a, interiorPolygon = true } = options

	if (n > 4) {
		const points: Point[] = polarPoints({ o, r, n, a })[0]
		const ps = starPoints(points)

		const g = []
		if (interiorPolygon) {
			// ploygon
			g[g.length] = createSvgPathShape(
				{
					// ...options,
					points,
					pathMode: PathMode.LINE_LOOP,
				},
				{
					...props,
					strokeDasharray: 4,
				}
			)
		}

		// star
		g[g.length] = createSvgPathShape(
			{
				...options,
				points: ps,
			},
			props
		)

		return _g(g)
	} else {
		const star = _polygon(
			{
				...options,
				transform: 'paritySort',
			},
			props
		)
		return star
	}
}
