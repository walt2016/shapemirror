export const copyToClipBoard = (value: string, callback?: () => any) => {
	if (navigator.clipboard) {
		navigator.clipboard.writeText(value).then(() => {
			callback && callback()
		})
	} else if (document.execCommand) {
		var element = document.createElement('SPAN')
		element.textContent = value
		document.body.appendChild(element)
		// if (document.selection) {
		//     var range = document.body.createTextRange();
		//     range.moveToElementText(element);
		//     range.select();
		// } else
		if (window.getSelection) {
			var range = document.createRange()
			range.selectNode(element)
			window.getSelection().removeAllRanges()
			window.getSelection().addRange(range)
		}
		document.execCommand('copy')
		element.remove()
		// element.remove ? element.remove() : element.removeNode(true);
		callback && callback()
	}
}
