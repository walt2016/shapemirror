// 判断类型

import { IMenu } from '@/types/menu'

// enum EType {
//   string = 'string',
//   null = 'null',
//   undefined = 'undefined',
//   object = 'object',
//   function = 'function',
//   number = 'number',
//   infinity = 'infinity'
// }
export const _type = (o: any): string => {
	if (o === null) return 'null'
	if (o === undefined) return 'undefined' //兼容ie8
	var s = Object.prototype.toString.call(o)
	var t = s.match(/\[object\s(\w*?)\]/)[1].toLowerCase()
	return t === 'number' ? (isNaN(o) ? 'nan' : !isFinite(o) ? 'infinity' : t) : t
}

export const isUndefined = (o: any): boolean => {
	return _type(o) === 'undefined'
}
export const isObject = (o: any): boolean => {
	return _type(o) === 'object'
}

export const isFunction = (o: any): o is Function => {
	return _type(o) === 'function'
}

export function isCustomEvent(event: Event): event is CustomEvent {
	return 'detail' in event
}

//首字母大写 _capitalize
export const _capitalize = (t: string): string => {
	return t.slice(0, 1).toUpperCase() + t.slice(1)
}
//小驼峰
export const _camelCase = (arr: string[]): string => {
	return arr.map((t, index) => (index === 0 ? t : _capitalize(t))).join('')
}

// 判断属性
export const _has = (obj = {}, property = ''): boolean => {
	return Object.prototype.hasOwnProperty.call(obj, property)
}

export const deepClone = (obj: any) => {
	return JSON.parse(JSON.stringify(obj))
}

// 修改属性名
export const changeProperty = (obj: any, from: string, to: string) => {
	let target = deepClone(obj)
	if (!_has(target, to)) {
		target[to] = target[from]
		deleteProperty(target, from)
	}
	return target
}
// 删除属性
export const deleteProperty = (obj: { [x: string]: any }, property: string) => {
	delete obj[property]
	return obj
}

// foreach增加一个next参数
export const eachNext = (points: any[], callback: (arg0: any, arg1: any, arg2: any) => any) => {
	let n = points.length
	points.forEach((t, index) => {
		let next = points[(index + 1) % n]
		callback && callback(t, index, next)
	})
}

export const plainTreeMenu = (tree: IMenu[]): IMenu[] => {
	const list = []
	const fn = (t: IMenu[]) => {
		t.forEach((t: IMenu) => {
			if (t.children) {
				fn(t.children)
			} else {
				list.push(t)
			}
		})
	}
	fn(tree)
	return list
}

export const exportJSON = (data: string, name?: string) => {
	let type = 'json'
	let link = document.createElement('a')
	let exportName = name ? name : 'data'
	let url = 'data:text/' + type + ';charset=utf-8,\uFEFF' + encodeURI(data)
	link.href = url
	link.download = exportName + '.' + type
	document.body.appendChild(link)
	link.click()
	document.body.removeChild(link)
}

export const exportSVG = (data: string, name?: string) => {
	let type = 'svg'
	let link = document.createElement('a')
	let exportName = name ? name : 'data'
	let url = 'data:text/' + type + ';charset=utf-8,\uFEFF' + encodeURI(data)
	link.href = url
	link.download = exportName + '.' + type
	document.body.appendChild(link)
	link.click()
	document.body.removeChild(link)
}

export const exportCanvasAsPNG = (data: string, name: string) => {
	var MIME_TYPE = 'image/png'
	var link = document.createElement('a')
	link.download = name
	link.href = data
	link.dataset.downloadurl = [MIME_TYPE, link.download, link.href].join(':')

	document.body.appendChild(link)
	link.click()
	document.body.removeChild(link)
}

// 小驼峰命名法：camelCase
// 下划线命名法：snake_case
// 中划线命名法：kebab-case
export const camel2kebab = (key: string): string => {
	return key.replace(/([A-Z])/g, (p, m) => `-${m.toLowerCase()}`)
}
// 将 camel 命名转 sanke 命名
export const camel2snake = (key: string) => {
	return key.replace(/([A-Z])/g, (p, m) => `_${m.toLowerCase()}`)
}
// 将 sanke 命名转 camel 命名
export const snake2camel = (key: string) => {
	return key.replace(/_([a-z])/g, (p, m) => m.toUpperCase())
}

export const enum2array = (e: object): string[] => {
	const arr: string[] = []
	for (var n in e) {
		if (typeof e[n] === 'number') {
			arr.push(n)
		} else {
			arr.push(e[n])
		}
	}
	return arr
}
