import { _cos } from '@/math'
import { IColor } from '@/types/color'
import { isUndefined } from '@/utils'
import { hsvToRgb } from './colorTransfer'
import { Color } from '@/types/color'

export const rgbaWrapper = (rgbaArr: number[]): string => {
	//[1,1,1,1]
	return (rgbaArr.length === 4 ? 'rgba(' : 'rgb(') + rgbaArr + ')'
}

//色相环
export const colorCircle = (n: number = 36, alpha: number = 1): string[] => {
	let a = 0
	const step = 360 / n
	const arr = []
	for (var i = 0; i < n; i++) {
		a += step
		const r = (_cos(a) * 127 + 128) << 0
		const g = (_cos(a + 120) * 127 + 128) << 0
		const b = (_cos(a + 240) * 127 + 128) << 0
		arr[arr.length] = rgbaWrapper(isUndefined(alpha) ? [r, g, b] : [r, g, b, alpha])
	}
	return arr
}

export const genColor = (color: string, alpha: number): string => {
	//分类颜色
	//红 100
	//绿 010
	//青 001
	//黄 110
	//紫 101
	//全彩 111
	//黑白灰 000
	const colorMap: {
		[key: string]: [number, number, number]
	} = {
		red: [1, 0, 0],
		green: [0, 1, 0],
		syan: [0, 0, 1],
		yellow: [1, 1, 0],
		purple: [1, 0, 1],
		gray: [0, 0, 0],
		colorful: [1, 1, 1],
	}

	let arr: number[] = color in colorMap ? colorMap[color] : colorMap['colorful']
	const [r, g, b] = arr
	if ((r & g & b) === 1) {
		//全彩
		arr = arr.map(() => {
			return (Math.random() * 255) << 0
		})
	} else if (r + g + b === 0) {
		//灰
		var t = (Math.random() * 255) << 0
		arr = [t, t, t]
	} else {
		var rgb = 155
		var c = ((Math.random() * (255 - rgb)) << 0) + rgb
		arr = arr.map(t => {
			return t === 1 ? ((Math.random() * (255 - rgb)) << 0) + rgb : (Math.random() * (c / 2)) << 0
		})
	}
	if (alpha) arr[arr.length] = alpha
	// this.color =this.rgbaWrapper(arr)
	return rgbaWrapper(arr)
}

const random = Math.random

/**
 * 获取随机颜色
 * @returns { Object } 颜色对象
 */
export const randomColor = (): IColor => {
	return {
		r: random() * 255,
		g: random() * 255,
		b: random() * 255,
		a: random() * 1,
	}
}
// 生成颜色
export const genColors = (color: string, n: number = 10, alpha: number = 1): IColor[] => {
	let colors = []
	for (let i = 0; i < n; i++) {
		colors[colors.length] = genColor(color, alpha)
	}
	return colors
}

// const redColors = (n, alpha) => {
//     return genColors('red', n, alpha)
// }

// const greenColors = (n, alpha) => {
//     return genColors('green', n, alpha)
// }

export const colorsConfig = {
	[Color.None]: '',
	[Color.ColorCircle]: colorCircle,
}
// 'yellow',
;['red', 'green', 'syan', 'purple', 'gray', 'random'].forEach(t => {
	colorsConfig[`${t}Colors`] = (n: number, alpha: number) => {
		return genColors(t, n, alpha)
	}
})

export const colorTypes = Object.keys(colorsConfig)

export const _colors = (color: string, n: number, alpha: number = 1): string => {
	return colorsConfig[color] ? colorsConfig[color](n, alpha) : colorCircle(n, alpha)
}

// let colors = colorsConfig[color] ? colorsConfig[color](n, 0.5) : colorCircle(n, 0.5)

//生成11*11的二维数组（颜色）要求：色卡左上角为白色，左下角为黑色，右上角为输入的颜色，右下角亮度为0输入颜色
export function createColorList(h: number, n: number, m: number): string[] {
	const list = []
	for (var i = 0; i <= 10; i++) {
		for (var y = 0; y <= 10; y++) {
			if (y === 0) {
				list[i] = []
			}
			var s = y * (n / 10)
			var l = 100 - i * 10
			var _m = m - (m / 10) * i
			l = l - y * ((l - _m) / 10)
			var color = hsvToRgb([h, s / 100, l / 100])
			var item = 'rgb(' + color[0] + ',' + color[1] + ',' + color[2] + ')'
			list[i].push(item)
		}
	}
	return list
}
