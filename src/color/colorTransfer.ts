//hsv颜色转rgb颜色
export function hsvToRgb(arr: number[]): number[] {
  const [h, s, v] = arr;
  let r = 0,
    g = 0,
    b = 0;
  const i = Math.round((h / 60) % 6);
  const f = h / 60 - i;
  const p = v * (1 - s);
  const q = v * (1 - f * s);
  const t = v * (1 - (1 - f) * s);
  switch (i) {
    case 0:
      r = v;
      g = t;
      b = p;
      break;
    case 1:
      r = q;
      g = v;
      b = p;
      break;
    case 2:
      r = p;
      g = v;
      b = t;
      break;
    case 3:
      r = p;
      g = q;
      b = v;
      break;
    case 4:
      r = t;
      g = p;
      b = v;
      break;
    case 5:
      r = v;
      g = p;
      b = q;
      break;
    default:
      break;
  }
  r = Math.round(r * 255.0);
  g = Math.round(g * 255.0);
  b = Math.round(b * 255.0);
  return [r, g, b];
}
//rgb颜色转hsv颜色
export function rgbToHsv(arr: number[]): number[] {
  const [r, g, b] = arr;
  const max = Math.max(...arr);
  const min = Math.min(...arr);
  let h = 0,
    s = 0,
    v = 0;
  v = max / 255;
  s = max === 0 ? 0 : 1 - min / max;
  if (max === min) {
    // max===min的时候，h无论为多少都无所谓
  } else if (max === r && g >= b) {
    h = 60 * ((g - b) / (max - min)) + 0;
  } else if (max === r && g < b) {
    h = 60 * ((g - b) / (max - min)) + 360;
  } else if (max === g) {
    h = 60 * ((b - r) / (max - min)) + 120;
  } else if (max === b) {
    h = 60 * ((r - g) / (max - min)) + 240;
  }
  h = Math.round(h);
  s = Math.round(s * 100);
  v = Math.round(v * 100);
  return [h, s, v];
}
