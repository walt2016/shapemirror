/**
 * A class to parse color values
 * @author Stoyan Stefanov <sstoo@gmail.com>
 * {@link   http://www.phpied.com/rgb-color-parser-in-javascript/}
 * @license Use it if you like it
 */

import { rgbToHsv } from "./colorTransfer";
import { hexNamedColors } from "./namedColors";

export class RGBColor {
  private r: number;
  private g: number;
  private b: number;
  private channels: number[];
  public ok: boolean;
  constructor(color_string: string) {
    color_string = color_string || "";
    this.ok = false;

    // strip any leading #
    if (color_string.charAt(0) == "#") {
      // remove # if any
      color_string = color_string.substr(1, 6);
    }
    color_string = color_string.replace(/ /g, "");
    color_string = color_string.toLowerCase();
    // before getting into regexps, try simple matches
    // and overwrite the input
    color_string = hexNamedColors[color_string] || color_string;
    this.parse(color_string);
    this.validate();
  }

  parse(color_string: string) {
    // array of color definition objects
    const color_defs = [
      {
        re: /^rgb\((\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3})\)$/,
        example: ["rgb(123, 234, 45)", "rgb(255,234,245)"],
        process: function (bits: string[]): number[] {
          return [parseInt(bits[1]), parseInt(bits[2]), parseInt(bits[3])];
        },
      },
      {
        re: /^(\w{2})(\w{2})(\w{2})$/,
        example: ["#00ff00", "336699"],
        process: function (bits: string[]): number[] {
          return [
            parseInt(bits[1], 16),
            parseInt(bits[2], 16),
            parseInt(bits[3], 16),
          ];
        },
      },
      {
        re: /^(\w{1})(\w{1})(\w{1})$/,
        example: ["#fb0", "f0f"],
        process: function (bits: string[]): number[] {
          return [
            parseInt(bits[1] + bits[1], 16),
            parseInt(bits[2] + bits[2], 16),
            parseInt(bits[3] + bits[3], 16),
          ];
        },
      },
    ];

    // search through the definitions to find a match
    color_defs.find((t) => {
      const { re, process } = t;
      var bits = re.exec(color_string);
      if (bits) {
        this.channels = process(bits);
        const [r, g, b] = this.channels;
        this.r = r;
        this.g = g;
        this.b = b;
        this.ok = true;
        return true;
      }
      return false;
    });
  }
  // validate/cleanup values
  validate() {
    this.r = this.r < 0 || isNaN(this.r) ? 0 : this.r > 255 ? 255 : this.r;
    this.g = this.g < 0 || isNaN(this.g) ? 0 : this.g > 255 ? 255 : this.g;
    this.b = this.b < 0 || isNaN(this.b) ? 0 : this.b > 255 ? 255 : this.b;
  }

  toRGB() {
    return "rgb(" + this.r + ", " + this.g + ", " + this.b + ")";
  }
  toHex() {
    var r = this.r.toString(16);
    var g = this.g.toString(16);
    var b = this.b.toString(16);
    if (r.length == 1) r = "0" + r;
    if (g.length == 1) g = "0" + g;
    if (b.length == 1) b = "0" + b;
    return "#" + r + g + b;
  }

  toHsv() {
    return rgbToHsv(this.channels);
  }
}
