import { store } from '@/store'
import { _type } from '../../utils'
import { IMenuItem } from '@/types/menu'
import { IContent } from '@/types/ui'

const objectToQuery = (shapeObj): string => {
	const params = Object.keys(shapeObj)
		.filter(t => !['shape'].includes(t) && ['string', 'number'].includes(_type(shapeObj[t])))
		.map(t => {
			return `${t}=${shapeObj[t]}`
		})
		.join('&')
	return params
}
interface IState {
	[k: string]: any
}
const pushState = (state: IState, title: string, url: string) => {
	try {
		history.pushState(state, title, url)
	} catch (e) {
		console.log(e)
	}
}

// scheme://host:port/path?query#fragment
export const updateRoute = (t: IMenuItem): void => {
	if (t) {
		const { content, title, name } = t
		let url = location.origin + '/#' + name
		if (content) {
			// return
			const { shapes } = content as IContent
			// 参数
			url = ''
			if (Array.isArray(shapes) && shapes.length >= 1) {
				const shapeObj = shapes[0]
				const query = objectToQuery(shapeObj)
				url += `?${query}`
			}
			url += '#' + name
		}
		// 路由

		const state = {}
		pushState(state, title, url)
	}
}

export const updateRouteFragment = (t: IMenuItem) => {
	if (t) {
		const state = {}
		let { title, name } = t
		let url = ''
		url += '#' + name
		pushState(state, title, url)
	}
}

export const updateRouteQuery = (t: IMenuItem) => {
	if (t) {
		const state = {}
		let { content, title } = t
		let { shapes } = content as IContent
		let url = ''
		if (shapes.length >= 1) {
			let shapeObj = shapes[0]
			let query = objectToQuery(shapeObj)
			url += `?${query}`
		}
		// url += '#' + name
		pushState(state, title, url)
	}
}

export const getRoute = () => {
	let hash = location.hash
	if (hash.indexOf('#') === 0) {
		return hash.substring(1)
	}
	return ''
}

export const resetRoute = () => {
	let { hash, origin, pathname } = location
	let url = `${origin}${pathname}${hash}`
	let state = {}
	let title = ''
	history.pushState(state, title, url)
	location.reload()
}

export const queryToObject = () => {
	const res = {}
	const search = location.search
	const paramArr = new URLSearchParams(search)
	paramArr.forEach((val, key) => {
		res[key] = isNaN(+val) ? val : +val
	})
	return res
}

export const goNextPage = (): IMenuItem => {
	const page = store.getNextPage()
	// store.setPage(page)
	return page
}

export const goPrevPage = (): IMenuItem => {
	const page = store.getPrevPage()
	// store.setPage(page)
	return page
}

export const goPage = (name: string) => {
	// console.log(name,history)
	const page = store.getPageByName(name)
	store.setPage(page)
	updateRoute(page)

	// history.go(-1)
}
