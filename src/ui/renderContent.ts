import { _form } from '.'
import { _grid } from './core/grid'
import { _list } from './core/list'
import { _svg } from '../svg'
import { _canvas } from '../canvas'
import { _btn } from './core/btn'
import { _type } from '../utils'
import { _webgl } from '../webgl'
import { updateRoute } from './rotute'
import { _lottie } from '@/lottie'
import { IMenuItem } from '@/types/menu'
import { IContent } from '@/types/ui'

type Content = string | HTMLElement | SVGElement | HTMLCanvasElement | ((panel: HTMLElement) => void)
export const renderContent = (
	t: IMenuItem
): {
	content: Content
	isHtml: boolean
	callback?: (el: HTMLElement) => void
} => {
	let { name, content, textContent, contentType, render } = t

	// 更新路由
	updateRoute(t)
	const contentConfig = content as IContent

	contentConfig.name = name

	let isHtml: boolean
	let str: Content = ''
	switch (contentType.toLowerCase()) {
		case 'form':
			str = _form(contentConfig)
			break
		case 'grid':
			str = _grid(contentConfig, contentConfig.data)
			break
		// case 'list':
		//   str = _list(contentConfig, render)
		//   break
		case 'html':
			str = textContent
			isHtml = true
			break
		case 'svg':
			str = _svg(contentConfig)
			break
		case 'canvas':
			str = _canvas(contentConfig)
			break
		// case 'btns':
		//   str = contentConfig.map(t => _btn(t))
		//   break
		case 'webgl':
			str = _webgl(contentConfig)
			break
		case 'lottie':
			str = _lottie(contentConfig)
			break
		default:
			str = textContent
	}

	return {
		content: str,
		isHtml,
	}
}
