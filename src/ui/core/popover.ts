import { IElementOptions } from '@/types/ui'
import { dom, _query, _closest } from './dom'
import { Transform } from '@/types/transform'

const removepopover = () => {
	const popover = _query('div.popover')
	if (popover) {
		popover.remove()
		document.body.removeEventListener('mousedown', removepopoverEvent)
	}
}
const removepopoverEvent = (e: MouseEvent) => {
	const el = e.target as HTMLElement
	const parent = _closest(el, '.popover')
	if (!parent) {
		removepopover()
	}
}

export const _popover = (options: IElementOptions, target: HTMLElement) => {
	removepopover()
	document.body.addEventListener('mousedown', removepopoverEvent)
	const pos = target.getBoundingClientRect()
	const { title, content } = options
	const children = [
		dom.div(title, {
			class: 'popover__title',
		}),
		dom.div(content, {
			class: 'popover__content',
		}),
	]
	const popover = dom.div(children, {
		class: 'popover',
		style: {
			top: pos.top + pos.height + 'px',
			left: pos.left + 'px',
			transform: Transform.None,
		},
	})
	return popover
}
