import { IElementProps, TChildren, TEvents } from '@/types/ui'
import { dom, _toggleClassName, _closest, _query } from './dom'

export const _checkbox_native = (val: any, props?: { options: any }, events?: TEvents): HTMLElement => {
	const { options } = props
	if (Array.isArray(options)) {
		const checkboxs = options.map(t => {
			return dom.input(
				t.label,
				{
					...t,
					type: 'checkbox',
				},
				events
			)
		})
		const group = dom.div(checkboxs, {
			class: 'checkbox_group',
		})

		return group
	} else {
		return dom.input(
			'',
			{
				...props,
				type: 'checkbox',
			},
			events
		)
	}
}

export const _checkboxItem = (val: TChildren, props: IElementProps, events?: TEvents): HTMLElement => {
	const { checked, name } = props
	const isChecked = checked ? ' is-checked' : ''
	const inner = dom.span(
		'',
		{
			class: 'checkbox__inner',
		},
		{
			click: e => {
				const el = e.target
				const parent = _closest(el, '.checkbox__input')
				_toggleClassName(parent, 'is-checked')
				const input = _query('input', parent) as HTMLInputElement
				input.checked = parent.classList.contains('is-checked')
				e.stopPropagation()
				e.preventDefault()
				// return false
			},
		}
	)
	const input = dom.input(
		'',
		{
			type: 'checkbox',
			checked: checked ? true : undefined,
			name,
		},
		{}
	)

	const checkboxInput = dom.span([inner, input], {
		class: 'checkbox__input' + isChecked,
	})
	const label = dom.span(
		val,
		{
			class: 'checkbox__label',
		},
		events
	)
	return dom.label([checkboxInput, label], {
		class: 'checkbox', //+ isChecked
	})
}

export const _checkbox = (val: string, props?: IElementProps, events?: TEvents): HTMLElement => {
	const { options, name } = props ?? {}
	if (Array.isArray(options)) {
		const checkboxs = options.map(t => {
			return _checkboxItem(t.label, {
				checked: t.value === val,
				name,
			})
		})
		const group = dom.div(
			checkboxs,
			{
				class: 'checkbox_group',
			},
			events
		)

		return group
	} else {
		return _checkboxItem('', {})
	}
}
