import { dom, _append, _attr, _closest, _toggleClassName, _style } from './dom'

import { _popover } from './popover'
import { groupBy } from '../../math/array'
import { _formItem } from './fromItem'
import { makeModel } from '../../common/propsData'
import { IFormOptions, IOperation, TEvents } from '@/types/ui'
import { IFeild } from '@/types/field'

const _formBtns = (fields: IFeild[], form: HTMLFormElement, op: IOperation[]): HTMLElement => {
	const btns = dom.div(
		op.map(t =>
			dom.div(
				t.text,
				{
					class: `form-btn${t.className ? ' ' + t.className : ''} ${t.popover ? ' popover' : ''}`,
					type: 'button',
				},
				{
					click: (e: MouseEvent) => {
						// 弹窗
						if (t.popover) {
							document.body.appendChild(_popover(t.popover, e.target as HTMLElement))
						}

						const scope = makeModel(fields, form)
						t.click.call(scope, e, scope)
					},
				}
			)
		),
		{
			class: 'form-btn-group',
		}
	)
	return btns
}

// 表单
export const _form = (options: IFormOptions, events?: TEvents): HTMLElement => {
	const { name, title, fields = [], op = [] } = options
	const form = dom.form(
		[],
		{
			name,
		},
		events
	) as HTMLFormElement
	const formTitle: HTMLElement = dom.div(title, {
		class: 'form-title',
	})

	let formBody: HTMLElement
	// 分组
	if (fields.some(t => t.group)) {
		const group = groupBy(fields, t => {
			return t.group || 'none'
		})

		const formItemsGroup = Object.keys(group).map(key => {
			const fields = group[key]
			const formItems = fields.filter(t => t.field).map(field => _formItem(field, options))
			const itcon = dom.i('', {
				class: 'icon-arrow',
			})
			const groupTitle = dom.div(
				[key, itcon],
				{
					class: 'form-item-group__title',
				},
				{
					click: e => {
						const el = e.target
						const groupContainer = _closest(el, '.form-item-group')
						if (groupContainer) {
							_toggleClassName(groupContainer, 'active')
							groupContainer.scrollIntoView()
						}
					},
				}
			)
			const groupBody = dom.div(formItems, {
				class: 'form-item-group__body',
			})
			return dom.div(key === 'none' ? groupBody : [groupTitle, groupBody], {
				class: `form-item-group ${key} ${key === 'none' ? 'active' : ''}`,
			})
		})

		formBody = dom.div([formItemsGroup], {
			class: 'form-body',
		})
	} else {
		const formItems = fields.filter(t => t.field).map(field => _formItem(field, options))
		formBody = dom.div([formItems], {
			class: 'form-body',
		})
	}

	//

	const btns = _formBtns(fields, form, op)
	const formFoot: HTMLElement = dom.div(btns, {
		class: 'form-foot',
	})

	return _append(form, [formTitle, formBody, formFoot])
}
