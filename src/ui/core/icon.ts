import { TEvents } from '@/types/ui'
import { dom } from './dom'
import iconClose from '@/assets/icon/close.svg'

export const _iconClose = (events?: TEvents) => {
	return dom.svg(
		iconClose,
		{
			class: 'icon-close',
		},
		events
	)
}
