import { _query, _addClassName, _removeClassName, _append } from './dom'
import { _layout } from './layout'
// 手风琴
export const _accordion = (panel: HTMLElement, current: string, target: string | HTMLElement, root: HTMLElement) => {
	const el = _query(current)
	if (el) {
		// 更新
		el.parentNode.childNodes.forEach((t: HTMLElement) => _addClassName(t, 'hide'))
		_removeClassName(el, 'hide')
	} else {
		// 新增
		const container = target ? _layout(target, root) : root
		if (container) {
			container.childNodes.forEach((t: HTMLElement) => _addClassName(t, 'hide'))
			_append(container, panel)
		}
	}
}

export const _accordionPanel = (name: string) => {
	const panel = _query(`.screen-viewer .panel[name="${name}"]`)
	// 更新
	panel.parentNode.childNodes.forEach((t: HTMLElement) => _addClassName(t, 'hide'))
	_removeClassName(panel, 'hide')
}

// export const _accordion=(el,cls)=>{

// }
