import { dom, _closest } from './dom'
import { emit } from '../eventBus'
import { _checkbox } from './checkbox'
import { deleteProperty, changeProperty } from '../../utils'
import { IContent, IStatis, TEvents } from '@/types/ui'
import { IFeild, IRow } from '@/types/field'

// 表格
export const _grid = (options: IContent, data = [], events?: TEvents) => {
	let { fields, statis, props } = options

	// 删除属性
	fields = fields.map(t => {
		t = changeProperty(t, 'field', 'name')
		deleteProperty(t, 'placeholder')
		return t
	})

	let thead = _gridHead(fields)
	let tbody = _gridBody(fields, data)
	let children = [thead, tbody]
	if (statis) {
		let tfoot = _gridFoot(fields, data, statis)
		children.push(tfoot)
	}

	return dom.table(
		children,
		{
			cellspacing: 0,
			cellpadding: 0,
			border: 0,
			...props,
		},
		events
	)
}
// 表头
const _gridHead = (fields: IFeild[]): HTMLElement => {
	const cols = fields.map(t => {
		let label: string | HTMLElement = t.label
		const type = t.type
		if (type === 'selection') {
			label = _checkbox(
				'',
				{},
				{
					click: () => {},
				}
			)
		} else {
			label = t.label
		}
		delete t.label
		return dom.th(label, t)
	})
	const tr = dom.tr(cols)
	const thead = dom.thead(tr)
	return thead
}
// 表体
const _gridBody = (fields: IFeild[], data = []): HTMLElement => {
	const rows = data.map((row, rowIndex) => _gridRows(fields, row, rowIndex))
	return dom.tbody(rows)
}

// 行
export const _gridRows = (fields: IFeild[], row: IRow, rowIndex = 0): HTMLElement => {
	const cols = fields.map(t => {
		const type = t.type
		const key = t.field || t.name || ''
		const value = row[key] || ''
		let text: string | number | HTMLElement | HTMLElement[]
		switch (type) {
			case 'index':
				text = row.index || rowIndex + 1
				break
			case 'selection':
				text = _checkbox('')
				t.class = 'center'
				break
			case 'operation':
				text = t.options.map(op => {
					const btn = dom.div(
						op.label,
						{
							class: op.name,
						},
						{
							click: e => {
								const tr = _closest(btn, 'tr'),
									table = _closest(tr, 'table')
								op.click(e, {
									row,
									rowIndex,
									btn,
									tr,
									table,
									emit, //事件
								})
							},
						}
					)
					return btn
				})
				break
			default:
				text = value
		}
		return dom.td(text, t)
	})
	return dom.tr(cols)
}
// 表尾
export const _gridFoot = (fields: IFeild[], data = [], statis: IStatis): HTMLElement => {
	const statisFields = statis.fields
	// 合计
	const getsum = (field: string): number => {
		return data.reduce((a, b) => {
			return a + b[field]
		}, 0)
	}

	const cols = fields.map((t, index) => {
		let label = ''
		if (index === 0) {
			label = statis.label
		} else {
			const field = t.field || t.name || ''
			if (statisFields.includes(field)) {
				label = '' + getsum(field)
			}
		}
		return dom.td(label, t)
	})
	const tr = dom.tr(cols)
	const tfoot = dom.tfoot(tr)
	return tfoot
}
