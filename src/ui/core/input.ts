import { dom } from './dom'
import { IElementOptions, INumberOptions, TEvents } from '@/types/ui'

// 对于不同的输入类型，value 属性的用法也不同：
// type="button", "reset", "submit" - 定义按钮上的显示的文本
// type="text", "password", "hidden" - 定义输入字段的初始值
// type="checkbox", "radio", "image" - 定义与输入相关联的值
export const _input = (val: string | number, options?: IElementOptions, events?: TEvents): HTMLInputElement => {
	return dom.input(
		val,
		{
			...options,
			type: 'text',
		},
		events
	)
}
export const _hidden = (val: string | number, options?: IElementOptions): HTMLInputElement => {
	return dom.input('', {
		...options,
		value: val,
		type: 'hidden',
	})
}

// 数字
export const _number = (val: string | number, options?: INumberOptions, events?: TEvents) => {
	// 范围
	const range = (range => {
		return range
			? {
					max: range[1],
					min: range[0],
			  }
			: {}
	})(options.range ? options.range : [options.min, options.max])

	const newOptions = {
		...options,
		type: 'number',
		...range,
	}

	return dom.input(val, newOptions, events)
}
export const _checkbox = (val: string | number, options?: IElementOptions, events?: TEvents) => {
	if (val) {
		const newOptions = {
			checked: true,
			...options,
			type: 'checkbox',
		}
		return dom.input('', newOptions, events)
	}

	const newOptions = {
		...options,
		type: 'checkbox',
	}
	return dom.input('', newOptions, events)
}
