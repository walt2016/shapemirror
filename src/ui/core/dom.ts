import { IDom, IElementOptions, IStyleOptions, TChildren, TElement, TEvents } from '@/types/ui'
import { isObject, _type } from '../../utils'

// dom操作

// 事件
export const _events = (el: TElement, events: TEvents) => {
	if (events) {
		switch (typeof events) {
			case 'function':
				el.addEventListener('click', events)
				break
			case 'object':
				Object.keys(events).forEach(key => {
					// let fn = (e) => {
					//   events[key](e, {
					//     model: e.detail //自定义
					//   })
					// }
					// let fn = events[key]

					el.addEventListener(key, events[key])
				})
				break
		}
	}
	return el
}
// 清除事件
export const _removeEvents = (el: TElement, events: TEvents) => {
	Object.keys(events).forEach(key => {
		el.removeEventListener(key, events[key])
	})
}

// 样式
export const _style = (el: TElement, options: IStyleOptions): void => {
	if (options) {
		const keys = Object.keys(options)
		el.setAttribute(
			'style',
			keys
				.map(t => {
					const val = options[t]
					if (typeof val === 'number') {
						return `${t}:${val}px`
					}
					return `${t}:${val}`
				})
				.join(';')
		)
	}
}

// 属性
export const _attr = (el: TElement, key: string | object, val?: string) => {
	if (!el) {
		return
	} else if (typeof key === 'string') {
		if (val === undefined) {
			return el.getAttribute(key)
		} else if (val != null) {
			el.setAttribute(key, val)
		}
	} else if (isObject(key)) {
		Object.keys(key).forEach(t => {
			let v = key[t]
			_attr(el, t, v)
		})
	}
}

export const _children = (el: TElement, children: TChildren) => {
	if (Array.isArray(children)) {
		children.forEach(t => _children(el, t))
	} else {
		children && el.appendChild(children as HTMLElement)
	}
}

export const createElement = (tag: string, options?: IElementOptions, events?: TEvents, children?: TChildren): HTMLElement => {
	const el = document.createElement(tag)
	const addContent = (val: string) => {
		if (options && options.isHtml) {
			el.innerHTML += val
		} else {
			el.textContent += val
		}
	}

	// 子节点
	const _children = (val: TChildren) => {
		if (!val) return
		if (Array.isArray(val)) {
			val.forEach(t => _children(t))
			return
		}
		if (val instanceof SVGElement) {
			el.appendChild(val)
		} else if (val instanceof HTMLElement) {
			el.appendChild(val)
		} else if (val instanceof Text) {
			el.appendChild(val)
		} else {
			switch (typeof val) {
				case 'string':
					addContent(val)
					break
				case 'number':
					addContent(String(val))
					break
				case 'object':
					addContent(JSON.stringify(val))
					break
				default:
					el.appendChild(val)
			}
		}
	}

	// 属性 样式
	const _props = (key: string | object, val: any): void => {
		switch (key) {
			case 'children':
				_children(val)
				break
			case 'text':
			case 'label':
				addContent(val)
				break
			case 'width':
				_attr(el, key, val)
				break
			case 'style':
				_style(el, val)
				break
			case 'disabled':
				if (val) {
					_attr(el, key, val)
				}
				break
			default: {
				let type = _type(val)
				if (!['array', 'object'].includes(type)) {
					_attr(el, key, val)
				}
			}
		}
	}
	// 属性
	if (options) {
		Object.keys(options).forEach(key => {
			let val = options[key]
			_props(key, val)
		})
	}

	// 事件
	if (events) {
		_events(el, events)
	}
	//子节点
	if (children) {
		_children(children)
	}
	return el
}

export const _c = (tag: string, options: IElementOptions, events: TEvents, children: TChildren): HTMLElement => {
	return createElement(tag, options, events, children)
}

// css转属性
// .a.b   .a[b=c] #a
export const toProp = (selector: string): { id?: string; class?: string } => {
	let first = selector.slice(0, 1)
	let prop = selector.slice(1)
	if ('#' === first) {
		return {
			id: prop,
		}
	} else if ('.' === first) {
		let reg = /(.*?)\[(.*?)=(.*?)\]/
		if (reg.test(prop)) {
			let arr = prop.match(reg)
			return {
				class: arr[1],
				[arr[2]]: arr[3],
			}
		}
		return {
			class: prop.split('.').join(' '),
		}
	}
}

// 插入节点
export const _append = <T extends TElement>(el: string | T, children: TChildren): T => {
	let $el: T
	if (typeof el === 'string') {
		let target = el
		$el = _query(el) as T
		if (!$el) {
			$el = dom['div']('', {
				...toProp(target),
			}) as T
			document.body.appendChild($el)
		}
	} else {
		$el = el
	}

	if (typeof children === 'string') {
		$el.appendChild(_textNode(children))
	} else if (typeof children === 'number') {
		$el.appendChild(_textNode(String(children)))
	} else if (Array.isArray(children)) {
		children.forEach(t => _append($el, t))
	} else {
		children && $el.appendChild(children)
	}

	return $el
}
export const _content = (el: HTMLElement, children: TChildren): void => {
	_empty(el)
	_append(el, children)
}

export const _div = (children: TChildren, options: IElementOptions, events?: TEvents): HTMLElement => {
	return _c('div', options, events, children)
}

export const _textNode = (text: string): Text => {
	return document.createTextNode(text)
}

// 移除
export const _remove = (el: HTMLElement, index: string | number): HTMLElement => {
	let children = el.childNodes
	if (children && children.length) {
		switch (index) {
			case 'last':
				el.removeChild(children[children.length - 1])
				break
			case 'frist':
				el.removeChild(children[0])
				break
			default:
				if (_type(index) === 'number') {
					el.removeChild(children[index])
				}
		}
	}
	return el
}

// 查找
export const _query = (condition: string | HTMLElement, el?: HTMLElement): HTMLElement | undefined => {
	if (typeof condition === 'string') {
		if (el) {
			return el.querySelector(condition)
		} else {
			return document.querySelector(condition)
		}
	} else if (condition) {
		return condition
	}
	return undefined
}

export const _queryAll = (condition: any, el?: HTMLElement): HTMLElement[] => {
	if (condition) {
		if (el) {
			return Array.from(el.querySelectorAll(condition))
		} else {
			return Array.from(document.querySelectorAll(condition))
		}
	}

	return []
}

// 清空子节点
export const _empty = (el: HTMLElement): HTMLElement => {
	while (el.hasChildNodes()) {
		//当el下还存在子节点时 循环继续
		el.removeChild(el.firstChild)
	}
	return el
}

export const _closest = (el: HTMLElement, cls: string): HTMLElement | undefined => {
	if (!el) return undefined
	if (/\.\S*?/.test(cls)) {
		if (el.classList && el.classList.contains(cls.slice(1))) {
			return el
		} else if (el.parentNode) {
			return _closest(el.parentNode as HTMLElement, cls)
		}
	} else if (_type(cls) === 'string') {
		if (el.nodeName && el.nodeName.toLowerCase() === cls.toLowerCase()) {
			return el
		} else if (el.parentNode) {
			return _closest(el.parentNode as HTMLElement, cls)
		}
	}
	return undefined
}

export const _toggleClassName = (el: HTMLElement, cls: string, cls2?: string, fn?: () => void, fn2?: () => void): void => {
	if (!el) return
	if (el.classList.contains(cls)) {
		fn2 && fn2()
		el.classList.remove(cls)
		if (cls2) {
			el.classList.add(cls2)
		}
	} else {
		fn && fn()
		el.classList.add(cls)
		if (cls2) {
			el.classList.remove(cls2)
		}
	}
}

export const _removeClassName = (el: HTMLElement | HTMLElement[], cls: string): void => {
	if (!el) return
	if (Array.isArray(el)) {
		el.forEach(t => _removeClassName(t, cls))
	} else {
		el.classList.remove(cls)
	}
}

export const _addClassName = (el: HTMLElement, cls: string): void => {
	if (!el) return
	if (Array.isArray(el)) {
		el.forEach(t => _addClassName(t, cls))
	} else {
		el.classList.add(cls)
	}
}

const checkStyle = (el: HTMLElement, css: IStyleOptions): boolean => {
	let key = Object.keys(css)[0]
	let val = '' + css[key]
	if (['width', 'height', 'left', 'right', 'top', 'bottom'].includes(key)) {
		if (parseInt(window.getComputedStyle(el)[key]) === parseInt(val)) {
			return true
		}
	} else {
		if (window.getComputedStyle(el)[key] === val) {
			return true
		}
	}
	return false
}

// let toggleStyleFlag = true
export const _toggleStyle = (el: HTMLElement, cssA: IStyleOptions, cssB: IStyleOptions): void => {
	if (!checkStyle(el, cssA)) {
		_style(el, cssA)
	} else {
		_style(el, cssB)
	}
}

export const _img = (data: string, props: { [x: string]: any }, events: any): HTMLImageElement => {
	let img = new Image()
	img.src = data
	Object.keys(props).forEach(t => _attr(img, t, props[t]))

	// 事件
	if (events) {
		_events(img, events)
	}
	return img
}

export const dom: IDom = {}
;['form', 'select', 'option', 'button', 'div', 'span', 'font', 'i', 'label', 'tr', 'thead', 'tbody', 'tfoot', 'table', 'td', 'th', 'ul', 'li'].forEach(t => {
	dom[`${t}`] = (children?: TChildren, options?: IElementOptions, events?: TEvents) => _c(t, options, events, children)
})

dom['input'] = (children?: TChildren, options?: IElementOptions, events?: TEvents) => _c('input', options, events, children) as HTMLInputElement

dom['html'] = (str: string, options: IElementOptions, events: TEvents) => dom['div'](str, { ...options, isHtml: true }, events)

dom['svg'] = (svg: string, options: { width: any; height: any }, events: any) => {
	let { width, height } = options
	svg = svg
		.replace(/width="(\d+)"/, () => {
			return `width=${width}`
		})
		.replace(/height="(\d+)"/, () => {
			return `height=${height}`
		})
	return dom['html'](svg, options, events)
}
