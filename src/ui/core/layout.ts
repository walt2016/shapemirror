import { dom, _append, _query, toProp } from './dom'
import { _type } from '../../utils'
// 根据css选择器 创建节点
// .a .b
export const _layout = (selector: string | string[] | HTMLElement, root: HTMLElement = document.body) => {
	if (Array.isArray(selector) && selector.length) {
		return selector.map(t => {
			return _layout(t, root)
		})
	} else if (typeof selector === 'string') {
		const index = selector.indexOf(' ')
		const first = index > 0 ? selector.slice(0, index) : selector
		const rest = index > 0 ? selector.slice(index + 1) : ''
		let el = _query(first, root)
		if (!el && first) {
			el = dom.div('', {
				...toProp(first),
			})
			_append(root, el)
		}
		if (rest && el) {
			_layout(rest, el)
		}
		return _query(selector, root)
	}
	return selector
}
