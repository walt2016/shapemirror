import { IElementOptions, TEvents } from '@/types/ui'
import { dom } from './dom'

export const _option = (value: string, label: string, selected: boolean) => {
	return dom.option(value, {
		value: value,
		selected: selected ? true : undefined,
	})
}
export const _select = (val: number | string, options: IElementOptions, events?: TEvents) => {
	let children = options.options.map(t => _option(t.value, t.label, val === t.value))
	delete options.label
	let newOptions = {
		class: 'select',
		...options,
	}
	let select = dom.select(children, newOptions, events)
	return select
}
