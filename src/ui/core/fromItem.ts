import { changeProperty } from '../../utils'
import { dom, _append } from './dom'
import { _select } from './select'
import { _input, _number, _checkbox, _hidden } from './input'
import { _progress } from './progress'
import { IFormItemOptions } from '@/types/ui'
import { IFeild } from '@/types/field'

const _formItemLabel = (field: IFeild, options: IFormItemOptions): HTMLElement => {
	const style = options.labelWidth
		? {
				width: options.labelWidth + 'px',
		  }
		: null
	const itemLabel = dom.div(field.label, {
		class: 'form-item-label',
		title: field.label,
		style,
	})
	return itemLabel
}

const _formItemBody = (field: IFeild, options: IFormItemOptions): HTMLElement => {
	const { type, value, disabled } = field
	let itemBodyContent
	const onChange = e => {
		window.postMessage(
			{
				eventType: 'change',
				options: {
					field,
					// fields: options.fields,
					name: options.name,
				},
			},
			'*'
		)
	}
	switch (type) {
		case 'select': {
			itemBodyContent = _select(
				value,
				{
					...field,
					class: 'form-item-' + type,
					width: options.inputWidth,
					options: field.options,
				},
				{
					change: onChange,
				}
			)
			break
		}

		case 'number':
		case 'random': {
			itemBodyContent = _number(
				value,
				{
					...field,
					width: options.inputWidth,
					class: 'form-item-' + type,
				},
				{
					change: onChange,
				}
			)
			break
		}

		// case 'progress':
		//     {
		//         itemBodyContent = _progress({
		//             name,
		//             value,
		//             max: 1
		//         })
		//         break;
		//     }

		case 'boolean':
		case 'checkbox': {
			itemBodyContent = _checkbox(
				value,
				{
					...field,
					class: 'form-item-' + type,
				},
				{
					change: onChange,
				}
			)
			break
		}

		default: {
			if (disabled) {
				itemBodyContent = dom.div([
					value,
					_hidden(value, {
						...field,
					}),
				])
			} else {
				itemBodyContent = _input(
					value,
					{
						...field,
						width: options.inputWidth,
						class: 'form-item-input',
					},
					{
						change: onChange,
					}
				)
			}
		}
	}

	const itemBody = dom.div(itemBodyContent, {
		class: 'form-item-body',
	})

	return itemBody
}

export const _formItem = (field: IFeild, options: IFormItemOptions): HTMLElement => {
	// 改下属性名
	field = changeProperty(field, 'field', 'name')
	const { type, value, name } = field
	let itemContent

	const itemLabel = _formItemLabel(field, options)
	const itemBody = _formItemBody(field, options)

	itemContent = [itemLabel, itemBody]

	if (type === 'progress') {
		itemContent = _progress(
			{
				...field,
				name,
				value,
				max: 1,
			},
			{
				change: e => {
					window.postMessage(
						{
							eventType: 'change',
							options: {
								field,
								// fields: options.fields,
								name: options.name,
							},
						},
						'*'
					)
				},
			}
		)
	}

	const item = dom.div(itemContent, {
		class: 'form-item',
		width: options.width,
	})

	return item
}
