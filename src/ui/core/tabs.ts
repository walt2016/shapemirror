import { dom, _attr } from './dom'
import { updateRoute, getRoute } from '../rotute'
import { renderContent } from '../renderContent'
import { IMenu, ITabsConfig } from '@/types/menu'
export const _tabs = (config: ITabsConfig) => {
	let { tabs, activeName } = config

	// 初始化路由
	const route = getRoute()
	if (route) {
		activeName = route
	}

	const activeBar = dom.div('', {
		class: 'tabs__active-bar',
	})

	const updateActiveBar = (el: HTMLElement, t: IMenu) => {
		// 等dom加载完毕后计算
		setTimeout(() => {
			const offsetLeft = el.offsetLeft
			const width = el.offsetWidth
			activeBar.setAttribute('style', `width:${width}px;transform: translateX(${offsetLeft}px);`)
		})

		// 更新路由
		updateRoute(t)
	}
	const isActive = (t: IMenu) => {
		return t.name === activeName
	}

	const items = tabs.map((t: IMenu) => {
		const item = dom.div(
			t.title,
			{
				class: 'tabs__item' + (isActive(t) ? ' active' : ''),
				name: t.name,
			},
			{
				click: e => {
					const el = e.target
					updateActiveBar(el, t)
					panels.forEach(panel => {
						const role = _attr(panel, 'role')
						if (role === t.name) {
							panel.classList.add('active')
						} else {
							panel.classList.remove('active')
						}
					})
				},
			}
		)

		if (isActive(t)) {
			updateActiveBar(item, t)
		}
		return item
	})

	const tabsHeader = dom.div([activeBar, ...items], {
		class: 'tabs__header',
	})

	const panels = tabs.map((t: IMenu) => {
		const { content, isHtml } = renderContent(t)
		let panel: HTMLElement
		if (typeof content === 'function') {
			panel = dom.div('', {
				role: t.name,
				class: 'tabpanel' + (isActive(t) ? ' active' : ''),
				isHtml,
			})
			content(panel)
		} else {
			panel = dom.div(content, {
				role: t.name,
				class: 'tabpanel' + (isActive(t) ? ' active' : ''),
				isHtml,
			})
		}

		return panel
	})

	const tabsContent = dom.div(panels, {
		class: 'tabs__content',
	})

	const container = dom.div([tabsHeader, tabsContent], {
		class: 'tabs',
	})

	return container
}
