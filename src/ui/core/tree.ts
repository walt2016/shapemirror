import { IMenu, IRenderMenu } from '@/types/menu'
import { dom } from './dom'

export const _tree = (treeData: IMenu[], render: IRenderMenu, levelLimited: number = -1) => {
	const fn = (treeData: IMenu[], render: IRenderMenu, level: number) => {
		const items = []
		Array.isArray(treeData) &&
			treeData.forEach((t: IMenu) => {
				const title = render ? render(t, level) : t.title
				const children = [title]
				if (levelLimited === -1 || level < levelLimited) {
					if (t.children) {
						children.push(fn(t.children, render, level + 1))
					}
				}
				items.push(dom.li(children))
			})
		const ul = dom.ul(items)
		return ul
	}
	return fn(treeData, render, 0)
}
