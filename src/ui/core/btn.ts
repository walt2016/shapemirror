import { IElementOptions, TChildren, TEvents } from '@/types/ui'
import { dom } from './dom'

export const _btnNative = (text: TChildren, options: IElementOptions, events: TEvents) => {
	return dom.button(
		text,
		{
			class: 'button',
			...options,
			type: 'button',
		},
		events
	)
}

export const _btn = (text: TChildren, options?: IElementOptions, events?: TEvents): HTMLElement => {
	const className = options.class
	const popover = options.popover

	return dom.div(
		text,
		{
			class: `button${className ? ' ' + className : ''} ${popover ? ' popover' : ''}`,
		},
		events

		// {
		//   click: (e) => {
		//     // let model = {}
		//     // fields.filter(t => t.field).forEach(t => { //['text','number','select'].includes(t.type)
		//     //   let key = t.field
		//     //   let item = _query( `[name=${key}]`,form)
		//     //   model[key] = item.value
		//     // })
		//     // let scope = {
		//     //   model,
		//     //   form,
		//     //   emit //事件
		//     // }
		//     // 弹窗
		//     // if (t.popover) {
		//     //   document.body.appendChild(_popover(t.popover, e.target))
		//     // }
		//     // let scope = this
		//     // t.click.call(scope, e, scope)

		//     // t.click.call(scope, e, scope)
		//   }
		// }
	)
}
