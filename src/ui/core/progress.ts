import { dom, _attr, _c, _events, _style } from './dom'
import { _k } from '../../math'
import { _hidden } from './input'
import { IElementOptions, TEvents } from '@/types/ui'

export const _progress = (options: IElementOptions, events?: TEvents) => {
	const { label, value, max, name } = options
	const hidden = _hidden(value, {
		name,
	})
	const pLabel = dom.div(label, {
		class: 'progress-info-label',
	})

	const pValue = dom.div(value, {
		class: 'progress-info-value',
	})
	const info = dom.div([pLabel, pValue, hidden], {
		class: 'progress-info',
	})
	const thumb = dom.div(
		'',
		{
			class: 'progress-thumb',
		},
		{
			// mousedown: (e) => {
			//     e.stopPropagation();
			//     document.body.addEventListener('mousemove', changeProgressValue, false);
			// },
			// mouseup: (e) => {
			//     e.stopPropagation();
			//     document.body.removeEventListener('mousemove', changeProgressValue, false);
			// },
			// mouseleave: (e) => {
			//     e.stopPropagation();
			//     document.body.removeEventListener('mousemove', changeProgressValue, false);
			// }
		}
	)

	const activeBar = dom.div(
		thumb,
		{
			class: 'progress-bar-active',
			style: {
				width: `${value * 100}%`,
			},
		},
		{
			// click: (e) => {
			//     e.stopPropagation();
			//     changeProgressValue(e)
			// },
		}
	)
	const changeProgressValue = (e: MouseEvent) => {
		const { left, width } = bar.getBoundingClientRect()

		const dis = e.clientX - left
		let value = _k(dis / width, 1)

		if (value <= 0) value = 0
		if (value >= 1) value = 1

		_attr(hidden, { value })
		pValue.innerHTML = '' + value

		_style(activeBar, {
			width: `${value * 100}%`,
		})

		if (events.change && typeof events.change === 'function') {
			events.change()
		}
	}

	let isMouseDown: boolean = false
	const bar = dom.div(
		activeBar,
		{
			class: 'progress-bar',
		},
		{
			mousedown: (e: MouseEvent) => {
				e.stopPropagation()
				if (e.button == 0) {
					// 判断点击左键
					isMouseDown = true
					changeProgressValue(e)
				}
			},
		}
	)

	document.addEventListener('mousemove', function (e: MouseEvent) {
		if (isMouseDown) {
			changeProgressValue(e)
		}
	})
	document.addEventListener('mouseup', function (e: MouseEvent) {
		if (e.button == 0) {
			isMouseDown = false
		}
	})

	return dom.div([info, bar], {
		class: 'form-item-progress',
	})
}
