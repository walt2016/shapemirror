import { createElement, _textNode, _append } from './dom'
import { _type } from '../../utils'
import { IElementOptions, IRender, TEvents } from '@/types/ui'

interface IListItem {
	name?: string
	[k: string]: any
}
export const _listItem = (item: IListItem, render?: (arg0: any) => any, active?: boolean, events?: TEvents) => {
	const children = render ? render(item) : JSON.stringify(item)
	return children
		? createElement(
				'li',
				{
					class: `item ${active ? ' active' : ''}`,
				},
				events,
				children
		  )
		: ''
}

// 列表
export const _list = (items: IListItem[], render?: IRender, options?: IElementOptions, events?: TEvents) => {
	const ul = createElement('ul', options)
	_append(
		ul,
		items.map(t => {
			const active = t.name === options.activeName
			return _listItem(t, render, active, events)
		})
	)
	return ul
}
