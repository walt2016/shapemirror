export default class EventBus {
	private callbacks
	constructor() {
		this.callbacks = {}
	}
	emit(name: string, args: any) {
		if (this.callbacks[name]) {
			this.callbacks[name].forEach(cb => cb(args))
		}
	}
	on(name: string, fn: any) {
		this.callbacks[name] = this.callbacks[name] || []
		this.callbacks[name].push(fn)
	}
}

// 创建并分发事件
export function emit(el: HTMLElement, key: string, detail: any) {
	var event = new CustomEvent(key, {
		detail,
	})
	el.dispatchEvent(event)
}
// 添加一个适当的事件监听器
export function on(el: HTMLElement, key: any, fn: (this: HTMLElement, ev: any) => any) {
	el.addEventListener(key, fn)
}

// CustomEvent 用法示例
// // 添加一个适当的事件监听器
// obj.addEventListener("cat", function(e) { process(e.detail) })

// // 创建并分发事件
// var event = new CustomEvent("cat", {"detail":{"hazcheeseburger":true}})
// obj.dispatchEvent(event)

// document.body.addEventListener("veb",function(e){
//   alert(e.eventType)
// })
// var event = document.createEvent('HTMLEvents');
// // initEvent接受3个参数：
// // 事件类型，是否冒泡，是否阻止浏览器的默认行为
// event.initEvent("veb", false, true);
// //通过eventType传递事件信息
// event.eventType="I love Veblen"
// //触发document上绑定的click事件
// document.body.dispatchEvent(event);

// export function emit(name){
//   document.body.dispatchEvent(name);
// }

// export function on()
