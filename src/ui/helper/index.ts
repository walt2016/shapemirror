import { GraphicDataType, ICanvasData, ISvgData } from '@/saver'
import { store } from '@/store'
import { _query, _queryAll } from '../core/dom'

export const getGraphicData = (panel: HTMLElement): ISvgData | ICanvasData => {
	if (panel) {
		const name = panel.getAttribute('name')
		const node = panel.firstChild as HTMLElement
		const width = node.getAttribute('width')
		const height = node.getAttribute('height')

		// 可视大小
		// const { width, height } = firstchild.getBoundingClientRect()
		const size = {
			width: parseFloat(width) || 100,
			height: parseFloat(height) || 100,
		}
		if (node instanceof HTMLCanvasElement) {
			const data = node
			const type = GraphicDataType.canvas
			return { data, type, size, name }
		} else {
			const data = panel.innerHTML
			const type = GraphicDataType.svg
			return { data, type, size, name }
		}
	}
}

export const getGraphicDataByName = (name: string): ISvgData | ICanvasData => {
	const panel = _query(`.screen-viewer .panel[name=${name}]`)
	return getGraphicData(panel)
}

export const getGraphicDataList = (): (ISvgData | ICanvasData)[] => {
	const panels = _queryAll('.screen-viewer .panel')
	return Array.from(panels).map(t => getGraphicData(t))
}

export const getCanvas = (panel: HTMLElement): HTMLCanvasElement => {
	const firstchild = panel.firstChild
	return firstchild as HTMLCanvasElement
}

export const getDataLength = ({ data, type }: ISvgData | ICanvasData): number => {
	if (type === GraphicDataType.svg) {
		return data.length
	}
	if (type === GraphicDataType.canvas) {
		return data.toDataURL().length
	}
}

export const getCheckedOpenList = () => {
	const checkboxList = _queryAll('.graphic-open-list input[type="checkbox"]')
	return checkboxList
		.filter(t => {
			return (t as HTMLInputElement).checked
		})
		.map(t => getGraphicDataByName(t.getAttribute('name')))
}

export const getCurrentPanel = () => {
	const page = store.getPage()
	const name = page.name
	const panel = _query(`.screen-viewer .panel[name=${name}]`)
	return panel
}

export const getCurrentGraphicData = (): ISvgData | ICanvasData => {
	const panel = getCurrentPanel()
	return getGraphicData(panel)
}

export const getPanelByName = (name: string): HTMLElement => {
	const panel = _query(`.screen-viewer .panel[name=${name}]`)
	return panel
}
