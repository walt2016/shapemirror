import { _tree } from '../core/tree'
import { dom, _queryAll, _query, _removeClassName, _addClassName, _closest, _toggleClassName, _append } from '../core/dom'
import { _accordion } from '../core/accordion'
import { rerenderInspector } from './g2'
import { store } from '../../store'
import { queryToObject } from '../rotute'
import { renderPanel } from './panel'
import { parseTreeItem } from '@/dataParser'
import { ContentType, TNavTreeConfig } from '@/types/ui'
import { IMenu, IMenuItem } from '@/types/menu'
import { ToolType } from '@/svgEditor/tools/ToolAbstract'

export const setNavTreeConfig = ({ target, root }: TNavTreeConfig) => {
	store.screenViewer = typeof target === 'string' ? _query(target) : target
	store.root = root
}

export const _navTree = (data: IMenu) => {
	const navTree = new NavTree(data)
	navTree.render()
	store.setNavTree(navTree)
	return navTree
}

export class NavTree {
	private data: IMenu
	constructor(data: IMenu) {
		this.data = data
	}
	render() {
		// _navTree(this.data)
		const { name, children } = this.data
		const tree = _tree(children, this.renderItem)
		const navtree = dom.div([tree], {
			class: 'navtree',
			role: name,
		})
		_accordion(navtree, `.navtree[role=${name}]`, '.aside', store.root)
	}
	renderItem = (t: IMenuItem, treeLevel: number) => {
		const title = t.title
		if (treeLevel === 0) {
			// 菜单分组
			const icon = dom.i('', {
				class: 'icon-arrow',
			})
			return dom.div(
				[title, icon],
				{
					role: t.name,
				},
				{
					click: e => {
						const el = e.target
						const li = _closest(el, 'li')
						_toggleClassName(li, 'active')
						// const icon = _query('i', li)
						// _toggleClassName(icon, 'icon-arrow-right', 'icon-arrow-down')
					},
				}
			)
		} else if (treeLevel === 1) {
			// 画图菜单
			return dom.div(
				[title],
				{
					class: 'tree-item',
					role: t.name,
				},
				{
					click: e => {
						const el = e.target
						this.handleTreeItemClick(t, el)
					},
				}
			)
		}
	}

	handleClick = (t: IMenu, enterFromUrl = false) => {
		console.log(t)

		if (t.contentType === ContentType.Editor) {
			store.editor.setCurrentTool(t.name as ToolType)
			return
		}

		parseTreeItem(t).then((t: IMenu) => {
			console.log(t)

			// 从url种获取参数
			if (enterFromUrl) {
				if (t.content && t.content.shapes && Array.isArray(t.content.shapes) && t.content.shapes.length >= 1) {
					const q = queryToObject()
					const s = t.content.shapes[0]
					Object.assign(s, q)
				}
			}

			renderPanel(t, true)

			// const panel = renderPanel(t)

			// 存储，展示
			// store.setPage(t)
			// _accordion(panel, `.panel[name=${t.name}]`, store.screenViewer, store.root)

			//   图形配置表单
			if (t.content && [ContentType.SVG, ContentType.Canvas].includes(t.contentType)) {
				rerenderInspector(t)
			}
		})
	}
	handleTreeItemClick = (t: IMenuItem, el: HTMLElement, enterFromUrl?: boolean) => {
		const items = _queryAll('.tree-item')
		_removeClassName(items, 'active')
		_addClassName(el, 'active')

		// 当前组

		const li = _closest(el, 'li')
		const ul = _closest(li, 'ul')
		const lis = _queryAll('li', ul)
		_removeClassName(lis, 'active')
		_addClassName(li, 'active')

		// 分组折叠
		const groups = _queryAll('.navtree>ul>li')
		if (groups.length) {
			_removeClassName(groups, 'active')
			const ul = _closest(el, 'ul')
			const li = _closest(ul, 'li')
			_addClassName(li, 'active')
		}

		// if (!t.content && !t.url) return
		this.handleClick(t, enterFromUrl)
	}

	clickTreeItem = (t: IMenuItem, enterFromUrl: boolean) => {
		const el = _query(`.tree-item[role=${t.name}]`)
		if (el) {
			this.handleTreeItemClick(t, el, enterFromUrl)

			setTimeout(() => {
				el.scrollIntoView()
			}, 300)
		}
	}
}
