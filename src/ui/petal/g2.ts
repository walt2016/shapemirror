import { _btn } from '../core/btn'
import { dom, _closest, _query, _style, _events, _content, _addClassName, _removeClassName, _attr, _img, _queryAll, _textNode, _div } from '../core/dom'
import { _inspector } from './inspector'
import { exportJSON } from '../../utils'
import { copyToClipBoard } from '../../utils/clipboard'
import { _k } from '../../math'
import downloadIcon from '@/assets/icon/download.svg'
import grammarIcon from '@/assets/icon/grammar.svg'
import configIcon from '@/assets/icon/config.svg'
import { GraphicDataType, saveAsPdf, saveSvgFiles, savePngFiles } from '@/saver'
import { _checkbox } from '../core/input'
import { getCheckedOpenList, getCurrentGraphicData, getDataLength, getGraphicData } from '../helper'
import { _list } from '../core/list'
import { _checkboxItem } from '../core/checkbox'
import { getOpenListBox } from './openList'
import { IMenuItem } from '@/types/menu'

const visibleMap = {}

export const inspectorList = ['inspector', 'grammar', 'graphic']

inspectorList.forEach(t => [(visibleMap[t] = true)])

const _hide = (item: HTMLElement): void => {
	_removeClassName(item, 'active')
}

const isVisible = (item: HTMLElement) => {
	_addClassName(item, 'active')
}

const _hideRightAside = () => {
	const rightside = _query('.main .main-right')
	_style(rightside, {
		width: 0,
	})
}
const isVisibleRightAside = () => {
	const rightside = _query('.main .main-right')
	_style(rightside, {
		width: '200px',
	})
}

const isRightAsideShow = () => {
	const rightside = _query('.main .main-right')
	return rightside.style.width === '200px'
}

export const hideInspector = () => {
	inspectorList.forEach(t => {
		const item = _query(`.main .${t}`)
		if (item) {
			_hide(item)
			visibleMap[t] = true
		}
	})
	_hideRightAside()
}

export const visibleInspector = (key: string) => {
	inspectorList.forEach(t => {
		const item = _query(`.main .${t}`)
		if (item) {
			_hide(item)
			visibleMap[t] = true
			if (t === key) {
				if (visibleMap[key]) {
					isVisible(item)
					visibleMap[key] = false
					isVisibleRightAside()
				}
			}
		}
	})
}

export const toggleInspector = (key: string) => {
	if (isRightAsideShow()) {
		const item = _query(`.main .${key}.active`)
		if (item) {
			_hide(item)
			_hideRightAside()
			const icon = _query(`.${key === 'inspector' ? 'config' : key}_enter svg`)
			// icon && icon.removeAttribute('style')
			if (icon) {
				_removeClassName(icon, 'active')
			}
		} else {
			inspectorList.forEach(t => {
				const item = _query(`.main .${t}`)
				if (item) {
					if (t === key) {
						isVisible(item)
					} else {
						_hide(item)
					}
				}
			})
		}
	} else {
		const item = _query(`.main .${key}`)
		isVisible(item)
		isVisibleRightAside()
	}
}

const fill = (e: MouseEvent) => {
	const el = e.target as HTMLElement
	const g2 = _closest(el, '.g2')
	const icons = _queryAll('svg', g2)
	icons.forEach(t => {
		// t.removeAttribute('style')
		_removeClassName(t, 'active')
	})
	const svg = _closest(el, 'svg')
	// _style(svg, {
	//     fill: 'var(--primary-color-active)'
	// })
	_addClassName(svg, 'active')
}

export const _g2 = () => {
	const grammar = dom.svg(
		grammarIcon,
		{
			class: 'grammar_enter',
			width: '25',
			height: '25',
		},
		{
			click: e => {
				fill(e)
				toggleInspector('grammar')
			},
		}
	)

	const graphic = dom.svg(
		downloadIcon,
		{
			class: 'graphic_enter',
			width: '25',
			height: '25',
		},
		{
			click: e => {
				fill(e)
				toggleInspector('graphic')
			},
		}
	)

	const config = dom.svg(
		configIcon,
		{
			class: 'config_enter',
			width: '25',
			height: '25',
		},
		{
			click: e => {
				fill(e)
				toggleInspector('inspector')
			},
		}
	)

	const g2 = dom.div([grammar, graphic, config], {
		class: 'g2',
	})
	return g2
}

// 语法
export const _grammar = (t: IMenuItem) => {
	const { content } = t
	if (!content) return
	const data = JSON.stringify(content)
	const str = data.replace(/.*?,/g, a => `${a}<br>`)

	const grammar = dom.div(str, {
		class: 'grammar-content',
		isHtml: true,
	})

	const btn = _btn(
		'saveAs',
		{
			class: 'primary',
		},
		{
			click: () => {
				exportJSON(data, t.name)
			},
		}
	)

	return dom.div([grammar, btn], {
		class: '',
	})
}

// 图形
export const _graphic = (t: IMenuItem) => {
	const name = t.name
	// const panel = _query(`.screen-viewer .panel[name=${name}]`)

	const btn = _btn(
		'copy svg',
		{
			class: 'primary',
		},
		{
			click: () => {
				const { data, type } = getCurrentGraphicData()
				if (type === GraphicDataType.svg) {
					copyToClipBoard(data)
				}
			},
		}
	)

	const btn2 = _btn(
		'save svg',
		{
			class: 'primary',
		},
		{
			click: () => {
				// 重新获取数据
				// const { data, type } = getCurrentGraphicData()
				// if (type === GraphicDataType.svg) {
				//     saveSvg({ svg: data, name })
				// } else if (type === GraphicDataType.canvas) {
				//     saveCanvasAsPng({ canvas: data, name })
				// }

				const data = getCheckedOpenList()
				saveSvgFiles(data)
			},
		}
	)

	const btn3 = _btn(
		'save png',
		{
			class: 'primary',
		},
		{
			click: () => {
				// 重新获取数据
				// const { data, type, size } = getCurrentGraphicData()

				// saveCanvasOrSvgAsPng({
				//     data,
				//     type, size, name
				// })

				const data = getCheckedOpenList()
				savePngFiles(data)
			},
		}
	)

	const btn4 = _btn(
		'save pdf',
		{
			class: 'primary',
		},
		{
			click: () => {
				// 全部打开的图形
				// const data = getGraphicDataList()
				// 打开列表
				const data = getCheckedOpenList()
				saveAsPdf(data)

				// 当前图形
				// const { data, type, size } = getCurrentGraphicData()
				// saveCanvasOrSvgAsPdf({ data, type, size, name })
			},
		}
	)

	const output = getCurrentGraphicData()
	const info = dom.html(`filetype:${output.type} <br> size:${_k(getDataLength(output) / 1024)}kb`)

	const graphic = dom.div(info, {
		class: 'graphic-content',
		rolename: name,
	})

	const openListBox = getOpenListBox()

	// output.type === GraphicDataType.svg ? : [btn3, btn4]
	const btns = dom.div([btn, btn2, btn3, btn4], {
		class: 'btns-group',
	})
	return dom.div([graphic, btns, openListBox], {
		class: '',
	})
}

_events(document.body, {
	mousedown: (e: MouseEvent) => {
		const el = e.target as HTMLElement
		const inScreen = ['screen-viewer', 'nav', 'navtree', 'toolbar', 'grammar', 'graphic', ...inspectorList].some(t => _closest(el, `.${t}`))
		if (inScreen) return
		hideInspector()
	},
})

export const inspectorFnMap = {
	inspector: _inspector,
	grammar: _grammar,
	graphic: _graphic,
}

export const rerenderInspector = (t: IMenuItem, list = inspectorList) => {
	list.forEach(key => {
		const conent = inspectorFnMap[key](t)
		const container = _query(`.main .${key}`)
		container && _content(container, conent)
	})
}
