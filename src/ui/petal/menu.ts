import { dom, _query } from '../core/dom'
import { _accordion } from '../core/accordion'
import { _tree } from '../core/tree'
import { plainTreeMenu } from '../../utils'
import { _playBar } from './playBar'
import { renderPanel } from './panel'
import { store } from '@/store'
import { IMenu, IMenuConfig, IMenuItem } from '@/types/menu'

// let currentMenu = ""
const handleClick = (t: IMenuItem, target: HTMLElement, root: HTMLElement) => {
	const panel = renderPanel(t)
	// _accordion(panel, `.panel[role=${t.name}]`, target, root)
}

export const _menu = (treeData: IMenu[], config: IMenuConfig) => {
	const { target, root = document.body } = config

	const $target: HTMLElement = _query(target)

	const render = (t: IMenu) => {
		return dom.div(
			t.title,
			{
				class: 'item',
			},
			{
				click: () => {
					handleClick(t, $target, root)
				},
			}
		)
	}
	const tree = _tree(treeData, render)
	return dom.div(tree, {
		class: 'menu',
	})
}

export const _fallmenu = (treeData: IMenu[], config: IMenuConfig) => {
	const { target, root = document.body } = config
	const $target: HTMLElement = _query(target)

	const list: IMenu[] = plainTreeMenu(treeData)
	store.setAllPages(list)

	const render = t => {
		return dom.div(
			t.title,
			{
				class: 'item',
			},
			{
				click: () => {
					handleClick(t, $target, root)
				},
			}
		)
	}

	let tree = _tree(treeData, render)

	let playBar = _playBar(t => {
		handleClick(t, $target, root)
	})
	return dom.div([tree, playBar], {
		class: 'fallmenu',
	})
}
