import { dom, _closest, _img } from '../core/dom'
import svgIconPlay from '@/assets/icon/next.svg'
import svgIconPrev from '@/assets/icon/prev.svg'
import { goNextPage, goPrevPage } from '../rotute'
import { IMenu } from '@/types/menu'

export const _playBar = (handleClick: (e: IMenu) => void) => {
	const goNext = () => {
		const page = goNextPage()
		handleClick && handleClick(page)

		// timer = setTimeout(goNext, 2000)
	}

	const goPrev = () => {
		const page = goPrevPage()
		handleClick && handleClick(page)

		// timer = setTimeout(goPrev, 2000)
	}

	const handleToolbar = (e: MouseEvent) => {
		const el = e.target as HTMLElement
		if (_closest(el, '.next')) {
			goNext()
		} else if (_closest(el, '.prev')) {
			goPrev()
		}
		// let cls = e.target.className
		// if (cls === 'next') {
		//     goNext()
		// } else if (cls === 'prev') {
		//     goPrev()
		// } else if (cls === 'stop') {
		//     // force = true
		// }
	}

	const prev = dom.svg(
		svgIconPrev,
		{
			class: 'prev',
			width: '25',
			height: '25',
		},
		{
			click: e => {
				handleToolbar(e)
			},
		}
	)

	const next = dom.svg(
		svgIconPlay,
		{
			class: 'next',
			width: '25',
			height: '25',
		},
		{
			click: e => {
				handleToolbar(e)
			},
		}
	)

	const playBar = dom.div([prev, next], {
		class: 'toolbar',
	})

	return playBar
}
