// import { store } from '../store'
import { _query, _toggleStyle, _style } from '../core/dom'

export const toggleAside = () => {
	// for H5
	// if (store.isH5) {
	let aside = _query('.aside')
	_toggleStyle(
		aside,
		{
			left: 0,
		},
		{ left: '-220px' }
	)
	// }
}
export const hideAside = () => {
	let aside = _query('.aside')
	_style(aside, {
		width: '0px',
	})
}

export const showAside = () => {
	let aside = _query('.aside')
	_style(aside, {
		width: '180px',
	})
}
