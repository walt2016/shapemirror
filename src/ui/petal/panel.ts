import { dom, _content, _query } from '../core/dom'
import { renderContent } from '../renderContent'
import { store } from '../../store'
import { _accordion } from '../core/accordion'
import { _layout } from '../core/layout'
import { IMenuItem } from '@/types/menu'
export const _panel = (name: string, content: string | HTMLElement | SVGElement | HTMLCanvasElement, isHtml: boolean): HTMLElement => {
	const panel = dom.div(content, {
		name,
		role: name,
		class: 'panel',
		isHtml,
	})
	return panel
}

export const activePanel = (name: string) => {
	const panel = _layout(`.screen-viewer .panel`) //[name=${name}]
	// const panel = _query(`.panel[name=${name}]`, store.screenViewer)
	_accordion(panel, `.panel[name=${name}]`, store.screenViewer, store.root)
	return panel
}

// 渲染
export const renderPanel = (t: IMenuItem, active: boolean = true): HTMLElement => {
	const { content, isHtml } = renderContent(t)
	let callback: (panel: HTMLElement) => void
	let panel: HTMLElement
	if (typeof content === 'function') {
		callback = content
		panel = _panel(t.name, '', isHtml)
	} else {
		panel = _panel(t.name, content, isHtml)
	}
	// const callback = typeof content === 'function'   ?content :()=>void

	// 存储，展示
	if (active) {
		store.setPage(t)
		_accordion(panel, `.panel[name=${t.name}]`, store.screenViewer, store.root)
		callback && callback(panel)
	}

	return panel
}

// 重新渲染
export const rerenderPanel = (t: IMenuItem) => {
	const { content } = renderContent(t)
	// const viewer = _query(".screen-viewer")
	const panel = _query(`.panel[name="${t.name}"]`, store.screenViewer)
	if (typeof content === 'function') {
		content(panel)
	} else {
		_content(panel, content)
	}
}
