import { dom, _query, _addClassName, _queryAll, _removeClassName } from '../core/dom'
import { _playBar } from './playBar'
import { _tree } from '../core/tree'
import { getRoute, updateRoute } from '../rotute'
import { _g2 } from './g2'
import { NavTree, _navTree, setNavTreeConfig } from './navTree'
import { showAside, hideAside } from './aside'
import { store } from '../../store'
import { initEditor } from '@/svgEditor'
import { _accordion } from '../core/accordion'
import { _panel } from './panel'
import { IMenu } from '@/types/menu'
import { ContentType, TNavTreeConfig } from '@/types/ui'

export const _nav = (treeData: IMenu[], config: TNavTreeConfig) => {
	const { target, root = document.body } = config //activeName

	setNavTreeConfig({
		target,
		root,
	})

	/**
	 *  左侧导航菜单
	 * @param data
	 */
	const buildNavTree = (data: IMenu, initAside?: boolean) => {
		if (data) {
			console.log(data)

			if (data.contentType === ContentType.Editor) {
				buildSvgEditor(data)
			} else {
				initAside && showAside()
				_navTree(data)
			}
		}
	}

	/**
	 * 编辑器
	 * @param t
	 */
	const buildSvgEditor = (t: IMenu) => {
		let editor = store.getEditor()
		if (!editor) {
			editor = initEditor()
			store.setEditor(editor)
		}

		const panel = _panel('svgEditor', editor.svgContainer, true)
		store.setPage(t)
		_accordion(panel, `.panel[name=${t.name}]`, store.screenViewer, store.root)
		showAside()

		// const tools = editor.getToolNavTree()
		// _navTree(tools)

		editor.buildNavTree()
	}

	/**
	 * 头部导航菜单
	 * @param name
	 * @param enterFromUrl
	 */
	const initNavRoot = (name: string, enterFromUrl: boolean) => {
		const list = store.getAllPages()
		const item = list.find(t => t.name === name)
		if (item && item.contentType) {
			const data: IMenu = treeData.find(t => t.name === item.contentType)
			setTimeout(() => {
				const items = _queryAll('.nav .nav-item')
				_removeClassName(items, 'active')

				const el = _query(`.nav-item[role="${item.contentType}"]`)
				_addClassName(el, 'active')
			})
			// debugger

			buildNavTree(data)
			store.getNavTree().clickTreeItem(item, enterFromUrl)
		}
	}

	const render = (t: IMenu): HTMLElement => {
		return dom.div(
			t.title,
			{
				class: 'nav-item',
				role: t.name,
			},
			{
				click: e => {
					// 更新路由
					updateRoute(t)

					let items = _queryAll('.nav-item')
					_removeClassName(items, 'active')
					let el = e.target
					_addClassName(el, 'active')

					buildNavTree(t, true)
				},
			}
		)
	}

	const nav = _tree(treeData, render, 0)

	const playBar = _playBar((t: IMenu) => {
		hideAside()
		const navtree = _query('.aside .navtree')
		if (navtree) {
			// 显示菜单 模式
			initNavRoot(t.name, false)
		} else {
			// 隐藏菜单 模式
			// clickTreeItem(t, false)
			store.getNavTree().clickTreeItem(t, false)
		}
	})

	// 初始化路由
	const route = getRoute()
	if (route) {
		// activeName = route
		initNavRoot(route, true)
	}

	const tools = dom.div(store.isH5 ? playBar : [playBar, _g2()], {
		class: 'nav-right',
	})

	return dom.div([nav, tools], {
		class: 'nav',
	})
}
