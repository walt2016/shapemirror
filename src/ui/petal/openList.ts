import { store } from '@/store'
import { _accordionPanel } from '../core/accordion'
import { _checkboxItem } from '../core/checkbox'
import { dom, _addClassName, _closest, _div, _query, _removeClassName } from '../core/dom'
import { _iconClose } from '../core/icon'
import { _list } from '../core/list'
import { getPanelByName } from '../helper'
import { goPage } from '../rotute'
import { rerenderInspector } from './g2'

export const getOpenListBox = () => {
	const openList = store.getOpenList()
	// const data = getGraphicDataList()
	const list = _list(
		openList,
		item => {
			const { name } = item
			const checkbox = _checkboxItem(name, {
				checked: true,
				name,
			})

			const icon = _iconClose({
				click: e => {
					const el = e.target
					const item = _closest(el, '.item')
					if (item) {
						const input = _query('input', item)
						const name = input.getAttribute('name')
						const panel = getPanelByName(name)
						panel && panel.remove()
						item.remove()

						store.removePage(name)
					}

					e.stopPropagation()
					e.preventDefault()

					// console.log(e.target)
				},
			})

			return [checkbox, icon]
		},
		{
			activeName: store.currentPage.name,
		},
		{
			click: e => {
				//
				const el = e.target
				const item = _closest(el, 'li')

				const input = _query('input', item)
				const name = input.getAttribute('name')
				_accordionPanel(name)

				item.parentNode.childNodes.forEach((t: HTMLElement) => _removeClassName(t, 'active'))
				_addClassName(item, 'active')

				const page = store.getPageByName(name)

				rerenderInspector(page, ['inspector', 'grammar']) //, 'graphic'

				// const page=getPageByName(name)
				// store.setPage(page)

				goPage(name)
			},
		}
	)
	const listTitle = _div('open list', {
		class: 'title',
	})
	const openListBox = _div([listTitle, list], {
		class: 'graphic-open-list',
	})

	return openListBox
}
