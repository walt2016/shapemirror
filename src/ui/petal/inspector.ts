import { _form } from '../core/form'
import { _query, _content } from '../core/dom'
import { rerenderInspector } from './g2'
import { resetRoute } from '../rotute'
import { store } from '../../store'
import { mapFormFields, contentAssignModel, makeModel } from '../../common/propsData'
import { rerenderPanel } from './panel'
import { IModel } from '@/types/ui'
import { IMenuItem } from '@/types/menu'

const rerender = (model: IModel) => {
	// 获取数据
	const t = store.getPage()
	contentAssignModel(t.content, model)

	rerenderPanel(t)
	// const { content } = renderContent(t)
	// const viewer = _query(".screen-viewer")
	// const panel = _query(`.panel[name="${t.name}"]`, viewer)
	// _content(panel, content)
	// 输入输出参数
	rerenderInspector(t, ['grammar', 'graphic'])
}
// 事件
window.addEventListener('message', e => {
	const data = e.data
	const { eventType, options } = data
	if ('change' === eventType) {
		const { name } = options
		const form = _query(`.inspector form[name="${name}"]`) as HTMLFormElement
		// 获取数据
		const fields = store.getFields()
		const { model } = makeModel(fields, form)
		rerender(model)
	}
})

export const _inspector = (t: IMenuItem) => {
	const { content, name } = t
	if (!content) return
	const fields = mapFormFields(content)

	// 保持数据
	store.setFields(fields)

	const formData = {
		name,
		fields,
		op: [
			{
				type: 'btn',
				text: 'rerender',
				className: 'primary',
				click: (e, { model }) => {
					rerender(model)
				},
			},
		],
	}
	if (!store.isH5) {
		// 暂不支持h5中reset
		formData.op.push({
			type: 'btn',
			text: 'reset',
			className: 'default',
			click: () => {
				resetRoute()
			},
		})
	}
	return _form(formData)
}
