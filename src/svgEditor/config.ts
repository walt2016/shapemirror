import { _screen } from '@/store'
import { ToolType } from './tools/ToolAbstract'

const { width, height } = _screen()
const editorDefaultConfig = {
	svgRootW: width, //  3000,
	svgRootH: height, //1500,
	svgStageW: width, //800,
	svgStageH: height, //520,

	tool: ToolType.Select, //'pen',  //select  pencil

	fill: '#fff',
	stroke: '#000',
	strokeWidth: '1px',

	frameSelectFill: 'rgba(200, 200, 200, .2)',
	frameSelectStroke: '#888',

	outlineColor: '#5183fb',
	scaleGridSize: 7,
} as const

export default editorDefaultConfig
