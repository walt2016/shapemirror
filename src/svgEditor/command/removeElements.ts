import { Editor } from '../Editor'
import { FElement } from '../element/baseElement'
import { BaseCommand, CommandName } from './baseCommand'

/**
 * remove elements
 */
export class RemoveElements extends BaseCommand {
	private els: Array<FElement>
	private parents: Array<HTMLElement>
	private nextSiblings: Array<Element>

	constructor(editor: Editor) {
		super(editor)

		this.els = this.editor.activeElementsManager.getEls()

		const size = this.els.length
		this.parents = new Array(size)
		this.nextSiblings = new Array(size)
		this.els.forEach((el, idx) => {
			this.nextSiblings[idx] = el.el().nextElementSibling
			this.parents[idx] = el.el().parentElement
		})
		this.execute()
	}
	static cmdName() {
		return CommandName.RemoveElements
	}
	cmdName() {
		return CommandName.RemoveElements
	}
	private execute() {
		this.els.forEach(item => {
			item.remove()
		})
		this.editor.activeElementsManager.clear()
	}
	redo() {
		this.execute()
	}
	undo() {
		for (let idx = this.els.length - 1; idx >= 0; idx--) {
			const element = this.els[idx]
			const el = element.el()
			if (this.nextSiblings[idx]) {
				this.parents[idx].insertBefore(el, this.nextSiblings[idx])
			} else {
				this.parents[idx].appendChild(el)
			}
		}

		this.editor.activeElementsManager.setEls(this.els)
	}
}
