import { Editor } from '../Editor'
import { FElement } from '../element/baseElement'
import { EditorSetting } from '../setting/EditorSetting'

export enum CommandName {
	Undo = 'undo',
	AddPath = 'addPath',
	AddPathSeg = 'addPathSeg',
	AddRect = 'addRect',
	RemoveElements = 'removeElements',
	SetAttr = 'setAttr',
	DMove = 'dmove',
	Group = 'group',
	AddGroup = 'addGroup',
	AddCircle = 'addCircle',
	AddEllipse = 'AddEllipse',
}
export abstract class BaseCommand {
	protected editor: Editor

	constructor(editor: Editor, ...args: any[]) {
		this.editor = editor
	}
	// TODO: abstract static method
	abstract cmdName(): string
	abstract undo(): void
	abstract redo(): void
	afterRedo() {
		/** */
	}
	afterUndo() {
		/** */
	}
}

export function setDefaultAttrsBySetting(el: FElement, setting: EditorSetting) {
	const fill = setting.get('fill')
	const stroke = setting.get('stroke')
	const strokeWidth = setting.get('stroke-width')
	el.setAttr('fill', fill)
	el.setAttr('stroke', stroke)
	el.setAttr('stroke-width', strokeWidth)
}
