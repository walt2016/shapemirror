import { Editor } from '../Editor'
import { FElement } from '../element/baseElement'
import { BaseCommand, CommandName } from './baseCommand'

/**
 * setAttr
 */
interface IAttrs {
	[prop: string]: string
}
export class SetAttr extends BaseCommand {
	private els: Array<FElement>
	private attrs: IAttrs
	private beforeAttrs: { [prop: string]: string[] } = {}

	constructor(editor: Editor, els: Array<FElement> | FElement, attrs: IAttrs) {
		super(editor)
		if (!Array.isArray(els)) els = [els]
		this.els = els

		// this.attrs = attrs
		for (const key in attrs) {
			this.beforeAttrs[key] = []
			this.els.forEach(el => {
				const value = el.getAttr(key)
				this.beforeAttrs[key].push(value)

				el.setAttr(key, attrs[key])
			})
		}
		this.attrs = attrs
	}
	static cmdName() {
		return CommandName.SetAttr
	}
	cmdName() {
		return CommandName.SetAttr
	}
	redo() {
		const attrs = this.attrs
		for (const key in attrs) {
			this.els.forEach(el => {
				el.setAttr(key, attrs[key])
			})
		}

		this.editor.activeElementsManager.heighligthEls()
	}
	undo() {
		const attrs = this.attrs
		for (const key in attrs) {
			this.els.forEach((el, index) => {
				el.setAttr(key, this.beforeAttrs[key][index])
			})
		}

		this.editor.activeElementsManager.heighligthEls()
	}
}
