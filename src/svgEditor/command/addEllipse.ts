/**
 * AddRect
 *
 * add rect svg element
 */

import { Editor } from '../Editor'
import { FSVG, IFSVG } from '../element/factory'
import { BaseCommand, CommandName, setDefaultAttrsBySetting } from './baseCommand'

export class AddEllipse extends BaseCommand {
	nextSibling: Element
	parent: Element
	// circle : IFSVG['Circle']
	ellipse: IFSVG['Ellipse']

	constructor(editor: Editor, x: number, y: number, rx: number, ry: number) {
		super(editor)
		// const circle = new FSVG.Circle(x, y, r)

		const ellipse = new FSVG.Ellipse(x, y, rx, ry)
		// editor.setting.setFill('none')

		setDefaultAttrsBySetting(ellipse, editor.setting)
		editor.getCurrentLayer().addChild(ellipse)

		this.nextSibling = ellipse.el().nextElementSibling
		this.parent = ellipse.el().parentElement
		this.ellipse = ellipse

		this.editor.activeElementsManager.setEls(this.ellipse)
	}
	static cmdName() {
		return CommandName.AddEllipse
	}
	cmdName() {
		return CommandName.AddEllipse
	}
	redo() {
		const el = this.ellipse.el()
		if (this.nextSibling) {
			this.parent.insertBefore(el, this.nextSibling)
		} else {
			this.parent.appendChild(el)
		}
		this.editor.activeElementsManager.setEls(this.ellipse)
	}
	undo() {
		this.ellipse.el().remove()
		this.editor.activeElementsManager.clear()
	}
}
