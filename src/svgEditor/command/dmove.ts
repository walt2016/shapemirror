import { Editor } from '../Editor'
import { FElement } from '../element/baseElement'
import { BaseCommand, CommandName } from './baseCommand'

/**
 * DMove
 *
 * dmove elements
 */
export class DMove extends BaseCommand {
	private els: Array<FElement>
	private dx: number
	private dy: number

	constructor(editor: Editor, els: Array<FElement>, dx: number, dy: number) {
		super(editor)
		this.dx = dx
		this.dy = dy
		this.els = els

		this.els.forEach(el => {
			el.dmove(this.dx, this.dy)
		})
	}
	static cmdName() {
		return CommandName.DMove
	}
	cmdName() {
		return CommandName.DMove
	}
	redo() {
		this.els.forEach(el => {
			el.dmove(this.dx, this.dy)
		})
	}
	undo() {
		this.els.forEach(el => {
			el.dmove(-this.dx, -this.dy)
		})
	}
	afterRedo() {
		this.editor.activeElementsManager.setEls(this.els)
	}
	afterUndo() {
		this.editor.activeElementsManager.setEls(this.els)
	}
}
