import { Editor } from '../Editor'
import { FSVG, IFSVG } from '../element/factory'
import { ISegment } from '../types'
import { BaseCommand, CommandName, setDefaultAttrsBySetting } from './baseCommand'

/**
 * AddPath
 *
 * add path element
 */
export class AddPath extends BaseCommand {
	// private editor: Editor
	nextSibling: Element
	parent: Element
	el: IFSVG['Path']

	constructor(
		editor: Editor,
		params: {
			d: string
			path?: IFSVG['Path']
			seg: ISegment
		}
	) {
		super(editor)
		const el = params.path || new FSVG.Path()

		setDefaultAttrsBySetting(el, editor.setting)
		el.setAttr('d', params.d)
		if (params.seg) {
			el.setMetaData('handleOut', params.seg.handleOut)
		}

		editor.getCurrentLayer().addChild(el)
		this.nextSibling = el.el().nextElementSibling
		this.parent = el.el().parentElement
		this.el = el

		this.editor.activeElementsManager.setEls(this.el)
	}
	static cmdName() {
		return CommandName.AddPath
	}
	cmdName() {
		return CommandName.AddPath
	}
	redo() {
		const el = this.el.el()
		if (this.nextSibling) {
			this.parent.insertBefore(el, this.nextSibling)
		} else {
			this.parent.appendChild(el)
		}
		this.editor.activeElementsManager.setEls(this.el)
	}
	undo() {
		this.el.el().remove()
		this.editor.activeElementsManager.clear()
	}
}
