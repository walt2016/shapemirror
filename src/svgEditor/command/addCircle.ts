/**
 * AddRect
 *
 * add rect svg element
 */

import { Editor } from '../Editor'
import { FSVG, IFSVG } from '../element/factory'
import { BaseCommand, CommandName, setDefaultAttrsBySetting } from './baseCommand'

export class AddCircle extends BaseCommand {
	nextSibling: Element
	parent: Element
	circle: IFSVG['Circle']

	constructor(editor: Editor, x: number, y: number, r: number) {
		super(editor)
		const circle = new FSVG.Circle(x, y, r)
		// editor.setting.setFill('none')

		setDefaultAttrsBySetting(circle, editor.setting)
		editor.getCurrentLayer().addChild(circle)

		this.nextSibling = circle.el().nextElementSibling
		this.parent = circle.el().parentElement
		this.circle = circle

		this.editor.activeElementsManager.setEls(this.circle)
	}
	static cmdName() {
		return CommandName.AddCircle
	}
	cmdName() {
		return CommandName.AddCircle
	}
	redo() {
		const el = this.circle.el()
		if (this.nextSibling) {
			this.parent.insertBefore(el, this.nextSibling)
		} else {
			this.parent.appendChild(el)
		}
		this.editor.activeElementsManager.setEls(this.circle)
	}
	undo() {
		this.circle.el().remove()
		this.editor.activeElementsManager.clear()
	}
}
