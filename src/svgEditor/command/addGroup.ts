/**
 * AddRect
 *
 * add rect svg element
 */

import { Editor } from '../Editor'
import { FSVG, IFSVG } from '../element/factory'
import { BaseCommand, CommandName, setDefaultAttrsBySetting } from './baseCommand'

export class AddGroup extends BaseCommand {
	// private editor: Editor
	nextSibling: Element
	parent: Element
	rect: IFSVG['Rect']
	group: IFSVG['Group']

	constructor(editor: Editor, x: number, y: number, w: number, h: number) {
		super(editor)
		// this.editor = editor
		// const rect = new FSVG.Rect(x, y, w, h)
		// // editor.setting.setFill('none')

		// setDefaultAttrsBySetting(rect, editor.setting)
		// editor.getCurrentLayer().addChild(rect)

		// this.nextSibling = rect.el().nextElementSibling
		// this.parent = rect.el().parentElement
		// this.rect = rect

		// this.editor.activeElementsManager.setEls(this.rect)

		const group = new FSVG.Group()

		const els = this.editor.activeElementsManager.getEls()

		els.forEach(t => {
			group.append(t)
		})

		editor.getCurrentLayer().addChild(group)

		this.group = group
		this.editor.activeElementsManager.setEls(this.group)
	}
	static cmdName() {
		return CommandName.Group
	}
	cmdName() {
		return CommandName.Group
	}
	redo() {
		// const el = this.rect.el()
		// if (this.nextSibling) {
		//     this.parent.insertBefore(el, this.nextSibling)
		// } else {
		//     this.parent.appendChild(el)
		// }
		// this.editor.activeElementsManager.setEls(this.rect)
	}
	undo() {
		// this.rect.el().remove()
		// this.editor.activeElementsManager.clear()
	}
}
