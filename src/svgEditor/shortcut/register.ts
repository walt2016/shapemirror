import { Editor } from '../Editor'
import { ToolType } from '../tools/ToolAbstract'

export const registerShortcut = (editor: Editor) => {
	// 注册全局快捷键
	// 考虑使用 hotkeys-js https://www.npmjs.com/package/hotkeys-js 貌似很好用
	editor.shortcut.register('Undo', 'Cmd+Z', () => {
		editor.executeCommand('undo')
	}) // 撤销
	editor.shortcut.register('Undo', 'Ctrl+Z', () => {
		editor.executeCommand('undo')
	})
	editor.shortcut.register('Redo', 'Cmd+Shift+Z', () => {
		editor.executeCommand('redo')
	}) // 重做
	editor.shortcut.register('Redo', 'Ctrl+Shift+Z', () => {
		editor.executeCommand('redo')
	})
	editor.shortcut.register('Delete', ['Backspace', 'Delete'], () => {
		// 删除
		if (editor.activeElementsManager.isNotEmpty()) {
			editor.executeCommand('removeElements')
		}
	})

	editor.shortcut.register('Rect', 'R', () => {
		editor.setCurrentTool(ToolType.Rect)
	})
	editor.shortcut.register('Circle', 'C', () => {
		editor.setCurrentTool(ToolType.Circle)
	})

	editor.shortcut.register('Ellipse', 'E', () => {
		editor.setCurrentTool(ToolType.Ellipse)
	})
	editor.shortcut.register('Pencil', 'P', () => {
		editor.setCurrentTool(ToolType.Pencil)
	})
	editor.shortcut.register('Pen', 'V', () => {
		editor.setCurrentTool(ToolType.Pen)
	})
	editor.shortcut.register('Select', 'S', () => {
		editor.setCurrentTool(ToolType.Select)
	})

	editor.shortcut.register('Undo', 'Ctrl+G', () => {
		editor.executeCommand('addGroup')
	})
}
