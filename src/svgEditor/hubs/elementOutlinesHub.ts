/**
 * 元素轮廓
 */

import editorDefaultConfig from '../config'
import { FElement } from '../element/baseElement'
import { FSVG, IFSVG, SVGTagName } from '../element/factory'

export class ElementOutlinesHub {
	container: IFSVG['Group']

	constructor(parent: FElement) {
		this.container = new FSVG.Group()
		this.container.setID('element-outlines-hud')
		parent.append(this.container)
	}
	draw(els: Array<FElement>) {
		this.clear()

		let baseOutline: FElement
		for (const el of els) {
			if (el.tagName() === SVGTagName.Rect) {
				const rect = el as IFSVG['Rect']
				const pos = rect.getPos()
				const outline = new FSVG.Rect(pos.x, pos.y, rect.getWidth(), rect.getHeight())
				baseOutline = outline
			} else if (el.tagName() === SVGTagName.Circle) {
				const circle = el as IFSVG['Circle']
				const pos = circle.getPos()
				// const outline = new FSVG.Rect(pos.x, pos.y, circle.getRadius(), circle.getRadius())

				const outline = new FSVG.Circle(pos.x, pos.y, circle.getRadius())
				baseOutline = outline
			} else if (el.tagName() === SVGTagName.Ellipse) {
				const ellipse = el as IFSVG['Ellipse']
				const pos = ellipse.getPos()
				// const outline = new FSVG.Rect(pos.x, pos.y, circle.getRadius(), circle.getRadius())

				const outline = new FSVG.Ellipse(pos.x, pos.y, ellipse.getWidth(), ellipse.getWidth())
				baseOutline = outline
			}

			if (el.tagName() === SVGTagName.Path) {
				const path = el as IFSVG['Path']
				const outline = new FSVG.Path()
				outline.setAttr('d', path.getAttr('d'))
				baseOutline = outline
			}
			// baseOutline.setAttr('fill', 'none')
			baseOutline.setAttr('fill', editorDefaultConfig.frameSelectFill)
			baseOutline.setAttr('stroke', editorDefaultConfig.outlineColor)
			baseOutline.setAttr('stroke-width', '1px')
			baseOutline.setNonScalingStroke()
			this.container.append(baseOutline)
		}
	}
	translate(x: number, y: number) {
		this.container.translate(x, y)
	}
	clear() {
		this.container.clear()
		this.container.removeAttr('transform')
	}
}
