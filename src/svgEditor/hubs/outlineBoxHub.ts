/**
 * elements outline box
 *
 */

import { Editor } from '../Editor'
import editorDefaultConfig from '../config'
import { FElement } from '../element/baseElement'
import { IBox } from '../element/box'
import { FSVG, IFSVG } from '../element/factory'
import { CenterGrip } from './centerGrip'
import { ResizeGrips } from './resizeGrips'

export class OutlineBoxHud {
	private x = 0
	private y = 0
	private w = 0
	private h = 0
	private container: IFSVG['Group']
	outline: IFSVG['Path']
	resizeGrips: ResizeGrips
	centerGrip: CenterGrip
	private resizeGripVisible_ = false

	constructor(parent: FElement, editor: Editor) {
		this.container = new FSVG.Group()
		this.container.setID('outline-box-hud')

		this.outline = new FSVG.Path()
		this.outline.setAttr('fill', 'none')
		// this.outline.setAttr('fill', editorDefaultConfig.frameSelectFill)
		this.outline.setAttr('stroke', editorDefaultConfig.outlineColor)
		this.outline.setAttr('stroke-width', '1px')
		this.outline.setNonScalingStroke()

		this.container.append(this.outline)
		parent.append(this.container)

		this.resizeGrips = new ResizeGrips(parent, editor)
		this.centerGrip = new CenterGrip(parent, editor)
	}
	enableResizeGrip(immediate = false) {
		this.resizeGripVisible_ = true
		// 8个点
		immediate && this.resizeGrips.drawPoints(this.getBox())
		// 中心点
		immediate && this.centerGrip.drawPoints(this.getBox())
	}
	disableResizeGrip(immediate = false) {
		this.resizeGripVisible_ = false
		immediate && this.resizeGrips.clear()
		immediate && this.centerGrip.clear()
	}
	drawRect(x: number, y: number, w: number, h: number) {
		this.x = x
		this.y = y
		this.w = w
		this.h = h

		this.drawRectWithPoints(x, y, x + w, y, x + w, y + h, x, y + h)
	}
	private drawRectWithPoints(x1: number, y1: number, x2: number, y2: number, x3: number, y3: number, x4: number, y4: number) {
		const d = `M ${x1} ${y1} L ${x2} ${y2} L ${x3} ${y3} L ${x4} ${y4} Z`
		this.outline.setAttr('d', d)
		this.outline.visible()

		if (this.resizeGripVisible_) {
			this.enableResizeGrip(true)
		}
	}
	clear() {
		this.outline.hide()
		this.resizeGrips.clear()
		this.centerGrip.clear()
	}
	findGrip(el: SVGElement): IFSVG['Rect'] {
		return this.resizeGrips.findGrip(el) || this.centerGrip.findGrip(el)
	}
	getWidth() {
		return this.w
	}
	getHeight() {
		return this.h
	}
	getX() {
		return this.x
	}
	getY() {
		return this.y
	}
	getBox(): IBox {
		return {
			x: this.getX(),
			y: this.getY(),
			width: this.getWidth(),
			height: this.getHeight(),
		}
	}
}
