/**
 * guide line layer
 */

import { PencilDrawer } from './pencilDrawer'
import { PathDraw } from './pathDraw'
import { PredictedCurve } from './PredictedCurve'
import { OutlineBoxHud } from './outlineBoxHub'
import { ElementOutlinesHub } from './elementOutlinesHub'
import { FrameSelect } from './frameSelect'
import { Editor } from '../Editor'
import { FSVG, IFSVG } from '../element/factory'

/**
 * 辅助线
 */
export class Huds {
	container: IFSVG['Group']
	frameSelect: FrameSelect
	outlineBoxHud: OutlineBoxHud
	pencilDraw: PencilDrawer
	pathDraw: PathDraw
	elementOutlinesHub: ElementOutlinesHub
	predictedCurve: PredictedCurve

	constructor(private editor: Editor) {
		this.container = new FSVG.Group()
		this.container.setID('huds')
		// 这里的顺序是由讲究的
		this.predictedCurve = new PredictedCurve()
		this.predictedCurve.mount(this.container)

		this.elementOutlinesHub = new ElementOutlinesHub(this.container)
		this.outlineBoxHud = new OutlineBoxHud(this.container, editor)
		this.frameSelect = new FrameSelect(this.container)

		this.pencilDraw = new PencilDrawer(this.container)
		this.pathDraw = new PathDraw(this.container, editor)
	}
	mount() {
		this.editor.svgStage.appendChild(this.container.el())
	}
	clear() {
		this.elementOutlinesHub.clear()
		this.outlineBoxHud.clear()
		this.predictedCurve.clear()
		this.pathDraw.clear()
	}
}
