import { FSVG, IFSVG } from '../element/factory'

export class BaseGrip {
	protected container: IFSVG['Group']

	constructor() {
		this.container = new FSVG.Group()
	}
	findGrip(el: SVGElement): IFSVG['Rect'] {
		return this.container.children.find(t => t.el() === el) as IFSVG['Rect']
	}
}
