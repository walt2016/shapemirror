import config from '../config'
import { FElement } from '../element/baseElement'
import { FSVG, IFSVG } from '../element/factory'

/**
 * select area
 */
export class FrameSelect {
	x = 0
	y = 0
	w = 0
	h = 0
	container: IFSVG['Group']
	outline: IFSVG['Path']

	constructor(parent: FElement) {
		this.container = new FSVG.Group()
		this.container.setID('select-area')
		parent.append(this.container)

		this.outline = new FSVG.Path()
		this.outline.setAttr('fill', config.frameSelectFill)
		this.outline.setAttr('stroke', config.frameSelectStroke)
		this.outline.setAttr('vector-effect', 'non-scaling-stroke')

		this.container.append(this.outline)
	}
	clear() {
		this.x = this.y = this.w = this.h = 0
		this.outline.hide()
	}
	drawRect(x: number, y: number, w: number, h: number) {
		this.x = x
		this.y = y
		this.w = w
		this.h = h

		// why don't I use rect, just solve the condition when width or height is 0 the outline is disapper
		const d = `M ${x} ${y} L ${x + w} ${y} L ${x + w} ${y + h} L ${x} ${y + h} Z`
		this.outline.setAttr('d', d)

		/* this.outline.setAttribute('x', x)
        this.outline.setAttribute('y', y)
        this.outline.setAttribute('width', w)
        this.outline.setAttribute('height', h) */
		this.outline.visible()
	}
	getWidth() {
		return this.w
	}
	getHeight() {
		return this.h
	}
	getX() {
		return this.x
	}
	getY() {
		return this.y
	}
	getBox() {
		return {
			x: this.x,
			y: this.y,
			width: this.w,
			height: this.h,
			w: this.w,
			h: this.h,
		}
	}
}
