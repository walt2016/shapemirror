import { FElement } from '../element/baseElement'
import { FSVG, IFSVG } from '../element/factory'

export class PencilDrawer {
	container: IFSVG['Group']
	path: IFSVG['Path']

	constructor(parent: FElement) {
		this.container = new FSVG.Group()
		this.path = new FSVG.Path()
		parent.append(this.container)

		this.path.setAttr('fill', 'none')
		this.path.setAttr('stroke', 'red') //#054
		this.path.setAttr('vector-effect', 'non-scaling-stroke')

		this.container.append(this.path)
	}
	addPoint(x: number, y: number) {
		this.path.visible()

		let d = this.getD()
		if (d === '' || d === null) {
			d = `M ${x} ${y}`
		} else {
			d += ` L ${x} ${y}`
		}
		// this.path.points.push({x,y})
		this.path.setAttr('d', d)
	}
	getD() {
		return this.path.getD()
	}
	clear() {
		this.path.hide()
		// this.path.points=[]
		this.path.removeAttr('d')
	}
}
