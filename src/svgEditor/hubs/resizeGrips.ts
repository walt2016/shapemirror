import { Editor } from '../Editor'
import editorDefaultConfig from '../config'
import { FElement } from '../element/baseElement'
import { IBox } from '../element/box'
import { FSVG, IFSVG } from '../element/factory'
import { CursorType } from '../tools/ToolAbstract'
import { BaseGrip } from './baseGrip'

// class Grip extends Rect{
//   private size = editorDefaultConfig.scaleGridSize
//   constructor (){
//     super()

//     // const grip = new FSVG.Rect(0, 0, this.size, this.size)
//     // const prefix = 'resize-grip_'
//     // grip.setID(prefix + id)
//     // grip.setAttr('stroke', editorDefaultConfig.outlineColor)
//     // grip.setAttr('fill', '#fff')
//     // grip.setNonScalingStroke()
//     // grip.hide()
//     // grip.hover(editorDefaultConfig.outlineColor, () => {
//     //   this.editor.setCursor(cusor)
//     // }, () => {
//     //   this.editor.setCursor(CursorType.Default)
//     // })

//   }
// }

export enum ResizeDirection {
	TopLeft = 'topLeft',
	TopRight = 'topRight',
	BottomLeft = 'bottomLeft',
	BottomRight = 'bottomRight',
	Top = 'top',
	Right = 'right',
	Bottom = 'bottom',
	Left = 'left',
}

export class ResizeGrips extends BaseGrip {
	private topLeft: IFSVG['Rect']
	private topRight: IFSVG['Rect']
	private bottomLeft: IFSVG['Rect']
	private bottomRight: IFSVG['Rect']

	private top: IFSVG['Rect']
	private right: IFSVG['Rect']
	private bottom: IFSVG['Rect']
	private left: IFSVG['Rect']

	private size = editorDefaultConfig.scaleGridSize

	private grips = {
		topLeft: CursorType.nwResize,
		topRight: CursorType.neResize,
		bottomRight: CursorType.seResize,
		bottomLeft: CursorType.swResize,
		top: CursorType.nResize,
		right: CursorType.eResize,
		bottom: CursorType.sResize,
		left: CursorType.wResize,
	}
	constructor(parent: FElement, private editor: Editor) {
		super()
		this.container.setID('segment-draw')

		for (let key in this.grips) {
			const cusor = this.grips[key]
			this[key] = this.createGrip(key, cusor)
			this.container.append(this[key])
		}

		parent.append(this.container)
		this.changeSizeWhenZoom()
	}

	/**
	 * 小把手,调大小
	 * @param id
	 * @returns
	 */
	private createGrip(id: string, cusor?: CursorType): IFSVG['Rect'] {
		const grip = new FSVG.Rect(0, 0, this.size, this.size)
		const prefix = 'resize-grip_'
		grip.setID(prefix + id)
		grip.setAttr('stroke', editorDefaultConfig.outlineColor)
		grip.setAttr('fill', '#fff')
		grip.setNonScalingStroke()
		grip.hide()
		grip.hover(
			editorDefaultConfig.outlineColor,
			() => {
				this.editor.setCursor(cusor)
			},
			() => {
				this.editor.setCursor(CursorType.Default)
			}
		)

		return grip
	}
	getDirection(grip: IFSVG['Rect']): ResizeDirection {
		return grip.getID().split('_')[1] as ResizeDirection
	}
	getOppositeGrip(grip: IFSVG['Rect']): IFSVG['Rect'] {
		let targetGrip = null
		if (grip === this.topLeft) targetGrip = this.bottomRight
		else if (grip === this.topRight) targetGrip = this.bottomLeft
		else if (grip === this.bottomRight) targetGrip = this.topLeft
		else if (grip === this.bottomLeft) targetGrip = this.topRight

		if (grip === this.top) {
			return this.bottomLeft
		}

		if (grip === this.bottom) {
			return this.topLeft
		}

		if (grip === this.left) {
			return this.topRight
		}

		if (grip === this.right) {
			return this.topLeft
		}
		return targetGrip
	}

	private changeSizeWhenZoom() {
		this.editor.viewport.onZoomChange(zoom => {
			const size = this.size / zoom
			const grips = this.container.children

			grips.forEach((grip: IFSVG['Rect']) => {
				const { x, y } = grip.getCentre()
				grip.setAttr('width', String(size))
				grip.setAttr('height', String(size))
				grip.setCenterPos(x, y)
			})
		})
	}
	drawPoints(box: IBox) {
		const { x, y, width, height } = box

		this.topLeft.setCenterPos(x, y)
		this.topRight.setCenterPos(x + width, y)
		this.bottomRight.setCenterPos(x + width, y + height)
		this.bottomLeft.setCenterPos(x, y + height)

		this.top.setCenterPos(x + width / 2, y)
		this.right.setCenterPos(x + width, y + height / 2)
		this.bottom.setCenterPos(x + width / 2, y + height)
		this.left.setCenterPos(x, y + height / 2)

		for (let key in this.grips) {
			this[key].visible()
		}
	}
	clear() {
		for (let key in this.grips) {
			this[key].hide()
		}
	}
}
