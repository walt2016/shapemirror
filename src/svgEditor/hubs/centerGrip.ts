import { Editor } from '../Editor'
import editorDefaultConfig from '../config'
import { FElement } from '../element/baseElement'
import { IBox } from '../element/box'
import { FSVG, IFSVG } from '../element/factory'
import { CursorType } from '../tools/ToolAbstract'
import { BaseGrip } from './baseGrip'

export class CenterGrip extends BaseGrip {
	private size = editorDefaultConfig.scaleGridSize
	private center: IFSVG['Rect']
	constructor(parent: FElement, private editor: Editor) {
		super()
		this.center = this.createGrip('')
		this.container.append(this.center)
		parent.append(this.container)
	}
	/**
	 * 小把手,调大小
	 * @param id
	 * @returns
	 */
	private createGrip(id?: string, cusor?: CursorType): IFSVG['Rect'] {
		const grip = new FSVG.Rect(0, 0, this.size, this.size)
		const prefix = 'center-grip'
		grip.setID(prefix + id)
		grip.setAttr('stroke', editorDefaultConfig.outlineColor)
		grip.setAttr('fill', '#fff')
		grip.setNonScalingStroke()
		grip.hide()
		grip.hover(
			editorDefaultConfig.outlineColor,
			() => {
				this.editor.setCursor(cusor)
			},
			() => {
				this.editor.setCursor(CursorType.Default)
			}
		)

		return grip
	}

	drawPoints(box: IBox) {
		const { x, y, width, height } = box
		this.center.setCenterPos(x + width / 2, y + height / 2)
		this.center.visible()
	}
	clear() {
		this.center.hide()
	}
}
