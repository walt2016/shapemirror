import { Editor } from './Editor'
import CommandManager from './command'
import editorDefaultConfig from './config'
import { createContextMenu, removeContentMenu } from './contextMenu/view'
import { EditorSetting } from './setting/EditorSetting'
import { registerShortcut } from './shortcut/register'
import { ToolManager } from './tools'

export function initEditor() {
	const editor = new Editor()
	// ; (window as any).editor = editor // debug in devtool

	const commandManager = new CommandManager(editor)
	editor.setCommandManager(commandManager)
	editor.setSetting(new EditorSetting())
	const tools = new ToolManager(editor)
	editor.setToolManager(tools)
	tools.setCurrentTool(editorDefaultConfig.tool)
	tools.initToolEvent()

	registerShortcut(editor)

	editor.contextMenu.on('show', createContextMenu)
	editor.contextMenu.on('hide', removeContentMenu)

	return editor
}
