/**
 * 对 rect 元素的简单封装
 */

import { IPoint } from '../types'
import { createSvgCircle } from '../util/svgHelper'
import { FElement } from './baseElement'

export class Circle extends FElement {
	el_: SVGCircleElement

	constructor(x: number, y: number, r: number)
	constructor(el: SVGElement)
	constructor(x: number | SVGElement, y?: number, r?: number) {
		super()
		if (typeof x === 'object') {
			this.el_ = x as SVGCircleElement
		} else {
			this.el_ = createSvgCircle()
			this.setAttr('cx', x + '')
			this.setAttr('cy', y + '')
			this.setAttr('r', r + '')
		}
	}
	setPos(x: number, y: number) {
		this.setAttr('cx', String(x))
		this.setAttr('cy', String(y))
	}
	//   setCenterPos(cx: number, cy: number) {
	//     const w = this.getWidth()
	//     const h = this.getHeight()
	//     this.setPos(cx - w / 2, cy - h / 2)
	//   }
	//   getCenterPos(): IPoint {
	//     const { x, y } = this.getPos()
	//     const w = this.getWidth()
	//     const h = this.getHeight()
	//     return {
	//       x: x + w / 2,
	//       y: y + h / 2,
	//     }
	//   }
	getPos(): IPoint {
		const x = parseFloat(this.getAttr('cx')) || 0
		const y = parseFloat(this.getAttr('cy')) || 0
		return { x, y }
	}
	getRadius(): number {
		return parseFloat(this.getAttr('r'))
	}
	//   getWidth(): number {
	//     return parseFloat(this.getAttr('width'))
	//   }
	//   getHeight(): number {
	//     return parseFloat(this.getAttr('height'))
	//   }
	// dmove(dx: number, dy: number) {
	//   const pos = this.getPos()
	//   this.setAttr('x', pos.x + dx + '')
	//   this.setAttr('y', pos.y + dy + '')
	// }
}
