/**
 * 对 rect 元素的简单封装
 */

import { IScale } from '@/types/base'
import { IBounds, IPoint } from '../types'
import { createSvgRect } from '../util/svgHelper'
import { FElement } from './baseElement'
import { resizeRect } from '../util/resizeHelper'
import { getCentre } from '../util/math'

export class Rect extends FElement {
	el_: SVGElement

	constructor(x: number, y: number, w: number, h: number)
	constructor(el: SVGElement)
	constructor(x: number | SVGElement, y?: number, w?: number, h?: number) {
		super()
		if (typeof x === 'object') {
			this.el_ = x
		} else {
			this.el_ = createSvgRect()
			this.setAttr('x', x + '')
			this.setAttr('y', y + '')
			this.setAttr('width', w + '')
			this.setAttr('height', h + '')
		}
	}
	setPos(x: number, y: number) {
		this.setAttr('x', String(x))
		this.setAttr('y', String(y))
	}
	setCenterPos(cx: number, cy: number) {
		const w = this.getWidth()
		const h = this.getHeight()
		this.setPos(cx - w / 2, cy - h / 2)
	}
	getBounds(): IBounds {
		const { x, y } = this.getPos()
		const w = this.getWidth()
		const h = this.getHeight()
		return {
			x,
			y,
			width: w,
			height: h,
		}
	}
	getCentre(): IPoint {
		const bounds = this.getBounds()
		return getCentre(bounds)
	}
	getPos(): IPoint {
		const x = parseFloat(this.getAttr('x')) || 0
		const y = parseFloat(this.getAttr('y')) || 0
		return { x, y }
	}
	getWidth(): number {
		return parseFloat(this.getAttr('width'))
	}
	getHeight(): number {
		return parseFloat(this.getAttr('height'))
	}
	resize(scale: IScale, basePoint: IPoint) {
		const bounds = resizeRect(this, scale, basePoint)

		this.setAttr(bounds)
	}
	// dmove(dx: number, dy: number) {
	//   const pos = this.getPos()
	//   this.setAttr('x', pos.x + dx + '')
	//   this.setAttr('y', pos.y + dy + '')
	// }
}
