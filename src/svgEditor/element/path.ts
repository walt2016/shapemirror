import { IScale } from '@/types/base'
import { IPoint } from '../types'
import { parsePath } from '../util/pathHelper'
import { resizePath } from '../util/resizeHelper'
import { createSvgPath } from '../util/svgHelper'
import { FElement } from './baseElement'

export class Path extends FElement {
	el_: SVGPathElement
	cacheTail: IPoint
	cacheD: string
	// points: IPoint[] = []

	constructor(el?: SVGElement) {
		super()
		if (el) {
			this.el_ = el as SVGPathElement
		} else {
			this.el_ = createSvgPath()
		}
	}
	getD(): string {
		return this.getAttr('d')
	}
	/**
	 * 对象数组
	 * @returns
	 */
	getSegments() {
		const d = this.getD()

		const segments = parsePath(d)
		console.log(segments)

		return segments

		// const commands = d.split(/(?=[LMC])/);
		// console.log(commands)
		// const points = commands.map((d) => {
		//   const arr = d.split(/\s+/);

		//   if (arr[0] === 'C') {
		//     return {
		//       type: arr[0],
		//       c1: {
		//         x: +arr[1],
		//         y: +arr[2]
		//       },
		//       c2: {
		//         x: +arr[3],
		//         y: +arr[4]
		//       },
		//       p: {
		//         x: +arr[5],
		//         y: +arr[6]
		//       }
		//     }
		//   }
		//   return {
		//     type: arr[0],
		//     p: {
		//       x: +arr[1],
		//       y: +arr[2]
		//     }
		//   }
		// });
		// return points
	}
	resize(scale: IScale, basePoint?: IPoint) {
		const d = this.getD()
		const newD = resizePath(d, scale, basePoint)
		this.setAttr('d', newD)
		return newD
	}
	dmove(dx: number, dy: number) {
		const d = this.getD()
		let offset = dx
		let s: string

		// TODO: to optimize the algorithm
		this.setAttr(
			'd',
			d.replace(/\s+(-?[\d.]+)/g, (match, p1) => {
				s = ' ' + (parseFloat(p1) + offset)
				offset = offset === dx ? dy : dx
				return s
			})
		)
	}
	/**
	 * 获取 path 的最后一个点坐标
	 */
	tail(): IPoint {
		let d = this.getD().trim()
		if (!d) return null
		if (d === this.cacheD) {
			return this.cacheTail
		}
		this.cacheD = d

		let pos = d.lastIndexOf(' ') // TODO: 优化算法
		const y = parseFloat(d.slice(pos + 1))

		d = d.slice(0, pos).trim()
		pos = d.lastIndexOf(' ')
		const x = parseFloat(d.slice(pos + 1))

		this.cacheTail = { x, y }
		return this.cacheTail
	}
}
