/**
 * 对 SVG 元素的简单封装
 */

import { FSVG } from './factory'

export enum SvgTagName {
	Rect = 'rect',
	Path = 'path',
	Circle = 'circle',
}

interface hashMap {
	[key: string]: any
}
interface IProps {
	[key: string]: string | number
}

export class FElement {
	protected el_: SVGElement

	children: FElement[] = []

	constructor() {
		this.el_ = null
	}
	el() {
		return this.el_
	}
	tagName() {
		if (!this.el_) return ''
		return this.el_.tagName
	}
	isRect() {
		return this.tagName() === SvgTagName.Rect
	}
	isCircle() {
		return this.tagName() === SvgTagName.Circle
	}
	isPath() {
		return this.tagName() === SvgTagName.Path
	}
	getID() {
		// return this.getAttr("id")
		return this.el_.id
	}
	setID(id: string) {
		this.el_.id = id
	}
	setAttr(prop: string | IProps, val?: string) {
		const _setAttr = (key: string, val: string) => {
			this.el_.setAttribute(key, val)
		}

		if (typeof prop === 'string') {
			_setAttr(prop, val)
		}
		if (typeof prop === 'object') {
			for (let key in prop) {
				const val = prop[key]
				_setAttr(key, '' + val)
			}
		}
		return this
	}
	getAttr(prop: string) {
		return this.el_.getAttribute(prop)
	}
	removeAttr(prop: string) {
		this.el_.removeAttribute(prop)
	}
	setNonScalingStroke() {
		this.setAttr('vector-effect', 'non-scaling-stroke')
	}
	getBBox() {
		return (this.el_ as SVGGraphicsElement).getBBox()
	}
	remove() {
		return this.el_.remove()
	}
	equal(el: FElement | SVGElement) {
		if ((el as FElement).el) {
			el = (el as FElement).el()
		}
		return this.el_ === el
	}

	/** DOM methods */
	parent() {
		const p = this.el_.parentElement
		if (p instanceof SVGElement) {
			return FSVG.create(p)
		}
		throw new Error('parent is not SVGElement')
	}
	nextSibling() {
		const nextOne = this.el_.nextElementSibling
		if (nextOne === null) return null
		return FSVG.create(nextOne as SVGElement)
	}
	previousSibling() {
		const n = this.el_.previousElementSibling
		if (n === null) return null
		return FSVG.create(n as SVGElement)
	}
	append(el: FElement) {
		this.children.push(el)
		this.el_.appendChild(el.el())
	}
	front() {
		const parent = this.el_.parentElement
		parent.appendChild(this.el_)
	}
	back() {
		const parent = this.el_.parentElement
		const firstChild = parent.firstElementChild
		if (firstChild) {
			parent.insertBefore(this.el_, firstChild)
		}
	}
	forward() {
		const parent = this.el_.parentElement
		const nextSibling = this.el_.nextSibling
		if (nextSibling) {
			parent.insertBefore(nextSibling, this.el_)
		}
	}
	backward() {
		const previousSibling = this.el_.previousElementSibling
		if (previousSibling) {
			this.before(previousSibling as SVGElement)
		}
	}
	before(referElement: FElement | SVGElement) {
		if ((referElement as FElement).el) {
			referElement = (referElement as FElement).el()
		}
		const parent = (referElement as SVGElement).parentElement
		parent.insertBefore(this.el_, referElement as SVGElement)
	}
	after(referElement: FElement | SVGElement) {
		if ((referElement as FElement).el) {
			referElement = (referElement as FElement).el()
		}
		const parent = (referElement as SVGElement).parentElement
		const nextSibling = referElement.nextSibling
		if (nextSibling) {
			parent.insertBefore(this.el_, nextSibling as SVGElement)
		} else {
			parent.appendChild(this.el_)
		}
	}

	getPos() {
		const x = parseFloat(this.getAttr('x')) || 0
		const y = parseFloat(this.getAttr('y')) || 0
		return { x, y }
	}
	dmove(dx: number, dy: number) {
		const pos = this.getPos()
		this.setAttr('x', pos.x + dx + '')
		this.setAttr('y', pos.y + dy + '')
	}

	hide() {
		this.el_.style.display = 'none'
	}
	visible() {
		this.el_.style.display = ''
	}

	/** transform */
	scale(scaleX: number, scaleY: number, cx?: number, cy?: number) {
		if (cx === undefined || cy === undefined) {
			// TODO: get center pos
		}
		const scale = [`scale(${scaleX} ${scaleY})`]
		const transfrom = this.getAttr('transform') || ''
		this.setAttr('tranform', transfrom + ' ' + scale)
	}
	translate(x: number, y: number) {
		this.setAttr('transform', `translate(${x} ${y})`)
	}

	/**
	 * meta data store
	 * 元数据保存
	 */
	setMetaData(key: string, value: any) {
		const el = this.el_ as hashMap
		if (!el.metaData) el.metaData = {} as hashMap
		el.metaData[key] = value
	}
	getMetaData(key: string): any {
		const el = this.el_ as hashMap
		if (!el.metaData) el.metaData = {} as hashMap
		return (el.metaData as hashMap)[key]
	}

	hover(color: string, on?: () => void, off?: () => void) {
		this.el_.onmouseover = () => {
			this.setAttr('fill', color)
			on && on()
		}
		this.el_.onmouseout = () => {
			this.setAttr('fill', '#FFF')
			off && off()
		}
	}
}
