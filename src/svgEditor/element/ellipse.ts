/**
 * 对 rect 元素的简单封装
 */

import { IPoint } from '../types'
import { createSvgEllipse } from '../util/svgHelper'
import { FElement } from './baseElement'

export class Ellipse extends FElement {
	el_: SVGEllipseElement

	constructor(x: number, y: number, rx: number, ry: number)
	constructor(el: SVGElement)
	constructor(x: number | SVGElement, y?: number, rx?: number, ry?: number) {
		super()
		if (typeof x === 'object') {
			this.el_ = x as SVGEllipseElement
		} else {
			this.el_ = createSvgEllipse()
			this.setAttr('cx', x + '')
			this.setAttr('cy', y + '')
			this.setAttr('rx', rx + '')
			this.setAttr('ry', ry + '')
		}
	}
	setPos(x: number, y: number) {
		this.setAttr('cx', String(x))
		this.setAttr('cy', String(y))
	}
	//   setCenterPos(cx: number, cy: number) {
	//     const w = this.getWidth()
	//     const h = this.getHeight()
	//     this.setPos(cx - w / 2, cy - h / 2)
	//   }
	//   getCenterPos(): IPoint {
	//     const { x, y } = this.getPos()
	//     const w = this.getWidth()
	//     const h = this.getHeight()
	//     return {
	//       x: x + w / 2,
	//       y: y + h / 2,
	//     }
	//   }
	getPos(): IPoint {
		const x = parseFloat(this.getAttr('cx')) || 0
		const y = parseFloat(this.getAttr('cy')) || 0
		return { x, y }
	}
	// getRadius(): number {
	//     return parseFloat(this.getAttr('r'))
	// }
	getWidth(): number {
		return parseFloat(this.getAttr('rx'))
	}
	getHeight(): number {
		return parseFloat(this.getAttr('ry'))
	}
	// dmove(dx: number, dy: number) {
	//   const pos = this.getPos()
	//   this.setAttr('x', pos.x + dx + '')
	//   this.setAttr('y', pos.y + dy + '')
	// }
}
