import { Box } from './box'
import { Group } from './group'
import { Path } from './path'
import { FElement } from './baseElement'
import { Line } from './line'
import { Rect } from './rect'
import { Circle } from './circle'
import { Ellipse } from './ellipse'

export enum SVGTagName {
	Rect = 'rect',
	Path = 'path',
	G = 'g',
	Line = 'line',
	Circle = 'circle',
	Ellipse = 'ellipse',
}

/**
 * FSVG
 *
 * simple SVGElement encapsulation
 */
function create(el: SVGElement): FElement {
	const tagName = el.tagName
	if (tagName === SVGTagName.Rect) {
		return new FSVG.Rect(el)
	} else if (tagName === SVGTagName.Circle) {
		return new FSVG.Circle(el)
	} else if (tagName === SVGTagName.Ellipse) {
		return new FSVG.Ellipse(el)
	} else if (tagName === SVGTagName.G) {
		return new FSVG.Group(el)
	} else if (tagName === SVGTagName.Path) {
		return new FSVG.Path(el)
	} else {
		throw new Error(`Can not creat ${tagName} instance, no match class.`)
	}
}

export const FSVG = {
	create,
	Rect,
	Box,
	Group,
	Path,
	Line,
	Text,
	Circle,
	Ellipse,
	//   Div,
}

export interface IFSVG {
	Rect: Rect
	Box: Box
	Group: Group
	Path: Path
	Line: Line
	Text: Text
	Circle: Circle
	Ellipse: Ellipse
}
