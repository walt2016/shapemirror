/**
 * 对 rect 元素的简单封装
 */

import { IPoint } from '../types'
import { createSvgText } from '../util/svgHelper'
import { FElement } from './baseElement'

export class Text extends FElement {
	el_: SVGElement

	constructor(x: number, y: number, w: number, h: number)
	constructor(el: SVGElement)
	constructor(x: number | SVGElement, y?: number, w?: number, h?: number) {
		super()
		if (typeof x === 'object') {
			this.el_ = x
		} else {
			this.el_ = createSvgText()
			this.setAttr('x', x + '')
			this.setAttr('y', y + '')
			this.setAttr('width', w + '')
			this.setAttr('height', h + '')
		}
	}
	setPos(x: number, y: number) {
		this.setAttr('x', String(x))
		this.setAttr('y', String(y))
	}
	setCenterPos(cx: number, cy: number) {
		const w = this.getWidth()
		const h = this.getHeight()
		this.setPos(cx - w / 2, cy - h / 2)
	}
	getCenterPos(): IPoint {
		const { x, y } = this.getPos()
		const w = this.getWidth()
		const h = this.getHeight()
		return {
			x: x + w / 2,
			y: y + h / 2,
		}
	}
	getPos(): IPoint {
		const x = parseFloat(this.getAttr('x')) || 0
		const y = parseFloat(this.getAttr('y')) || 0
		return { x, y }
	}
	getWidth(): number {
		return parseFloat(this.getAttr('width'))
	}
	getHeight(): number {
		return parseFloat(this.getAttr('height'))
	}
	// dmove(dx: number, dy: number) {
	//   const pos = this.getPos()
	//   this.setAttr('x', pos.x + dx + '')
	//   this.setAttr('y', pos.y + dy + '')
	// }
}
