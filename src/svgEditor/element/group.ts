/**
 * group
 *
 * Encapsulation of <g>
 */

import { createSvgGroup } from '../util/svgHelper'
import { FElement } from './baseElement'

export class Group extends FElement {
	el_: SVGGElement

	constructor(el?: SVGElement) {
		super()
		if (el) {
			this.el_ = el as SVGGElement
		} else {
			this.el_ = createSvgGroup()
		}
	}
	el(): SVGGElement {
		return this.el_
	}
	clear() {
		this.el_.innerHTML = ''
	}
}
