export interface IPoint {
	x: number
	y: number
}

export interface ISegment {
	x: number
	y: number
	handleIn: IPoint
	handleOut: IPoint
}

export interface IBounds {
	x: number
	y: number
	width: number
	height: number
}
