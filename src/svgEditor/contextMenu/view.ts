import { _query, dom } from '@/ui/core/dom'
import { Point } from '../drawer/type'
import { ItemGroupType, ItemType } from './type'

interface ICreateContextMenuOptions {
	pos: Point
	items: ItemGroupType[]
}
export const createContextMenu = ({ pos, items }: ICreateContextMenuOptions) => {
	const createItem = (t: ItemType) => {
		return dom.div(
			t.name,
			{
				class: 'item',
			},
			{
				click: t.command,
			}
		)
	}

	const list = dom.div(
		items.map(t => {
			const { items } = t
			return items.map(t => createItem(t))
		}),
		{ class: 'list' }
	)

	const [x, y] = pos
	const content = dom.div(list, {
		class: 'contextmenu-content',
		style: { left: x, top: y },
	})
	const contextmenu = dom.div(content, {
		class: 'contextmenu-mask',
	})

	document.body.appendChild(contextmenu)

	return contextmenu
}

export const removeContentMenu = () => {
	const mask = _query('.contextmenu-mask')
	mask.remove()
}
