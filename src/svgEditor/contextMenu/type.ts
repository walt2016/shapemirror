export type ItemType = {
	name: string
	disable: boolean
	// shortcut: string,
	command: () => void
}

export type ItemGroupType = {
	group: string
	items: ItemType[]
}

export type ShowEventOptions = {
	x: number
	y: number
	items: ItemGroupType[]
}

export type EventName = 'show' | 'hide'
