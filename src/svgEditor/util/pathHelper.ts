const PATH_COMMANDS = {
	M: ['x', 'y'],
	m: ['dx', 'dy'],
	H: ['x'],
	h: ['dx'],
	V: ['y'],
	v: ['dy'],
	L: ['x', 'y'],
	l: ['dx', 'dy'],
	Z: [],
	C: ['x1', 'y1', 'x2', 'y2', 'x', 'y'],
	c: ['dx1', 'dy1', 'dx2', 'dy2', 'dx', 'dy'],
	S: ['x2', 'y2', 'x', 'y'],
	s: ['dx2', 'dy2', 'dx', 'dy'],
	Q: ['x1', 'y1', 'x', 'y'],
	q: ['dx1', 'dy1', 'dx', 'dy'],
	T: ['x', 'y'],
	t: ['dx', 'dy'],
	A: ['rx', 'ry', 'rotation', 'large-arc', 'sweep', 'x', 'y'],
	a: ['rx', 'ry', 'rotation', 'large-arc', 'sweep', 'dx', 'dy'],
}

interface ISegment {
	type: string
	x: number
	y: number
	x1?: number
	x2?: number
	y1?: number
	y2?: number
}

export function parsePath(path: string): ISegment[] {
	const items = path
		.replace(/[\n\r]/g, '')
		.replace(/-/g, ' -')
		.replace(/(\d*\.)(\d+)(?=\.)/g, '$1$2 ')
		.trim()
		.split(/\s*,|\s+/)
	const segments = []
	let currentCommand = ''
	let currentElement = {}
	while (items.length > 0) {
		let it = items.shift()
		if (PATH_COMMANDS.hasOwnProperty(it)) {
			currentCommand = it
		} else {
			items.unshift(it)
		}
		currentElement = { type: currentCommand }
		PATH_COMMANDS[currentCommand].forEach(prop => {
			it = items.shift() // TODO sanity check
			currentElement[prop] = it
		})
		if (currentCommand === 'M') {
			currentCommand = 'L'
		} else if (currentCommand === 'm') {
			currentCommand = 'l'
		}
		segments.push(currentElement)
	}
	return segments
}

export function rect2path(x: number, y: number, width: number, height: number, rx?: number, ry?: number) {
	/*
	 * rx 和 ry 的规则是：
	 * 1. 如果其中一个设置为 0 则圆角不生效
	 * 2. 如果有一个没有设置则取值为另一个
	 */
	rx = rx || ry || 0
	ry = ry || rx || 0
	//非数值单位计算，如当宽度像100%则移除
	if (isNaN(x - y + width - height + rx - ry)) return
	rx = rx > width / 2 ? width / 2 : rx
	ry = ry > height / 2 ? height / 2 : ry
	//如果其中一个设置为 0 则圆角不生效
	if (0 == rx || 0 == ry) {
		// var path =
		// 'M' + x + ' ' + y +
		// 'H' + (x + width) + 不推荐用绝对路径，相对路径节省代码量
		// 'V' + (y + height) +
		// 'H' + x +
		// 'z';
		var path = 'M' + x + ' ' + y + 'h' + width + 'v' + height + 'h' + -width + 'z'
	} else {
		var path = 'M' + x + ' ' + (y + ry) + 'a' + rx + ' ' + ry + ' 0 0 1 ' + rx + ' ' + -ry + 'h' + (width - rx - rx) + 'a' + rx + ' ' + ry + ' 0 0 1 ' + rx + ' ' + ry + 'v' + (height - ry - ry) + 'a' + rx + ' ' + ry + ' 0 0 1 ' + -rx + ' ' + ry + 'h' + (rx + rx - width) + 'a' + rx + ' ' + ry + ' 0 0 1 ' + -rx + ' ' + -ry + 'z'
	}
	return path
}

export function ellipse2path(cx: number, cy: number, rx: number, ry: number) {
	//非数值单位计算，如当宽度像100%则移除
	if (isNaN(cx - cy + rx - ry)) return
	var path = 'M' + (cx - rx) + ' ' + cy + 'a' + rx + ' ' + ry + ' 0 1 0 ' + 2 * rx + ' 0' + 'a' + rx + ' ' + ry + ' 0 1 0 ' + -2 * rx + ' 0' + 'z'
	return path
}

export function line2path(x1: number, y1: number, x2: number, y2: number) {
	//非数值单位计算，如当宽度像100%则移除
	if (isNaN(x1 - y1 + x2 - y2)) return
	x1 = x1 || 0
	y1 = y1 || 0
	x2 = x2 || 0
	y2 = y2 || 0
	var path = 'M' + x1 + ' ' + y1 + 'L' + x2 + ' ' + y2
	return path
}

// polygon折线转换
//points = [x1, y1, x2, y2, x3, y3 ...];
export function polyline2path(points: number[]) {
	var path = 'M' + points.slice(0, 2).join(' ') + 'L' + points.slice(2).join(' ')
	return path
}
// polygon多边形转换
//points = [x1, y1, x2, y2, x3, y3 ...];
export function polygon2path(points: number[]) {
	var path = 'M' + points.slice(0, 2).join(' ') + 'L' + points.slice(2).join(' ') + 'z'
	return path
}
