import { Namespace } from '../namespace'

export const createSvgElement = (tagName: string): SVGElement => {
	return document.createElementNS(Namespace.SVG, tagName) as SVGElement
}

export const createSvgGroup = () => {
	return createSvgElement('g') as SVGGElement
}

export const createSvgPath = () => {
	return createSvgElement('path') as SVGPathElement
}

export const createSvgLine = () => {
	return createSvgElement('line') as SVGLineElement
}

export const createSvgRect = () => {
	return createSvgElement('rect') as SVGRectElement
}

export const createSvgCircle = () => {
	return createSvgElement('circle') as SVGCircleElement
}

export const createSvgEllipse = () => {
	return createSvgElement('ellipse') as SVGEllipseElement
}

export const createSvgText = () => {
	return createSvgElement('text') as SVGTextElement
}
