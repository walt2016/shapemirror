import { IPoint } from '@/types/point'
import { FElement } from '../element/baseElement'
import { getCentre, keepTwoDecimal } from './math'
import { parsePath } from './pathHelper'
import { Circle } from '../element/circle'
import { IScale } from '@/types/base'

const _scale = (x1: number, x: number, scaleX: number) => {
	return keepTwoDecimal((x1 - x) * scaleX + x)
}

const resizePoint = (p: IPoint, scale: IScale, basePoint: IPoint) => {
	const { x: bx, y: by } = basePoint
	const { scaleX, scaleY } = scale
	const { x, y } = p

	const newX = _scale(x, bx, scaleX)
	const newY = _scale(y, by, scaleY)
	return {
		x: newX,
		y: newY,
	}
}

export const resizeRect = (t: FElement, scale: IScale, basePoint: IPoint) => {
	const { scaleX, scaleY } = scale
	if (t.isRect()) {
		const bbox = t.getBBox()
		const p = resizePoint(bbox as IPoint, scale, basePoint)
		const newWidth = bbox.width * scaleX
		const newHeight = bbox.height * scaleY
		return {
			...p,
			width: newWidth,
			height: newHeight,
		}
	}
}

export const resizeCircle = (t: FElement, scale: IScale, basePoint: IPoint) => {
	// const { x, y, width, height } = bbox
	// const { cx, cy, r } = getCircleProps(x, y, x + width * scaleX, y + height * scaleY)
	if (t.isCircle()) {
		const bbox = t.getBBox()
		const { x, y } = resizePoint(bbox as IPoint, scale, basePoint)
		const { scaleX, scaleY } = scale
		const { x: cx, y: cy } = getCentre({
			x,
			y,
			width: bbox.width * scaleX,
			height: bbox.height * scaleY,
		})
		return {
			cx,
			cy,
			r: (t as Circle).getRadius() * Math.min(scaleX, scaleY),
		}
	}
}

/**
 * 路径缩放
 * @param d
 * @param scaleX
 * @param scaleY
 * @param basePoint
 * @returns
 */
export const resizePath = (d: string, scale: IScale, basePoint?: IPoint): string => {
	const { scaleX, scaleY } = scale
	const segments = parsePath(d)
	const { x, y } = basePoint ?? segments[0]
	return segments
		.map(t => {
			const { type } = t
			if (type === 'C') {
				return {
					type,
					x: keepTwoDecimal((t.x - x) * scaleX + x),
					x1: keepTwoDecimal((t.x1 - x) * scaleX + x),
					x2: keepTwoDecimal((t.x2 - x) * scaleX + x),
					y: keepTwoDecimal((t.y - y) * scaleY + y),
					y1: keepTwoDecimal((t.y1 - y) * scaleY + y),
					y2: keepTwoDecimal((t.y2 - y) * scaleY + y),
				}
			}

			return {
				type,
				x: _scale(t.x, x, scaleX),
				y: _scale(t.y, y, scaleY),
			}
		})
		.map(t => {
			if (t.type === 'C') {
				return `${t.type} ${t.x1} ${t.y1} ${t.x2} ${t.y2} ${t.x} ${t.y}`
			}
			return `${t.type} ${t.x} ${t.y}`
		})
		.join(' ')
}
