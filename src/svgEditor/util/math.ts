import { IBounds, IPoint } from '../types'

export function getBoxBy2points(x1: number, y1: number, x2: number, y2: number) {
	const w = Math.abs(x2 - x1)
	const h = Math.abs(y2 - y1)
	const x = Math.min(x2, x1)
	const y = Math.min(y2, y1)
	return { x, y, w, h }
}

export const getCircleIn2Points = (p1: IPoint, p2: IPoint) => {
	const { x: x1, y: y1 } = p1
	const { x: x2, y: y2 } = p2
	const w = Math.abs(x2 - x1)
	const h = Math.abs(y2 - y1)
	const x = Math.min(x2, x1)
	const y = Math.min(y2, y1)

	return {
		cx: x + w / 2,
		cy: y + h / 2,
		r: Math.min(w / 2, h / 2),
	}
}

export const getCentre = (bounds: IBounds) => {
	const { x, y, width, height } = bounds
	return {
		x: x + width / 2,
		y: y + height / 2,
	}
}

export const getCircleProps = (x1: number, y1: number, x2: number, y2: number) => {
	const w = Math.abs(x2 - x1)
	const h = Math.abs(y2 - y1)
	const x = Math.min(x2, x1)
	const y = Math.min(y2, y1)

	return {
		cx: x + w / 2,
		cy: y + h / 2,
		r: Math.min(w / 2, h / 2),
	}
}

export const getEllipseProps = (x1: number, y1: number, x2: number, y2: number) => {
	const w = Math.abs(x2 - x1)
	const h = Math.abs(y2 - y1)
	const x = Math.min(x2, x1)
	const y = Math.min(y2, y1)

	return {
		x: x + w / 2,
		y: y + h / 2,
		rx: w / 2,
		ry: h / 2,
	}
}

export function getSymmetryPoint(pt: IPoint, cx: number, cy: number): IPoint {
	return {
		x: cx * 2 - pt.x,
		y: cy * 2 - pt.y,
	}
}

export const keepTwoDecimal = (num: number, decimal: number = 2): number => {
	const g = Math.pow(10, decimal)
	return Math.round(num * g) / g
}
