/**
 * 激活元素管理类
 */

import { Editor } from './Editor'
import { FElement } from './element/baseElement'
import { IBox, getElementsInBox, getElementsIntersectWithBox } from './element/box'
import { FSVG } from './element/factory'

export class ActiveElementsManager {
	els: Array<FElement>

	constructor(private editor: Editor) {
		this.els = []
	}
	setEls(els: Array<FElement> | FElement) {
		if (!Array.isArray(els)) els = [els]
		this.els = els
		// TODO: highlight outline, according to current tool
		this.heighligthEls()

		this.setSetting('fill')
		this.setSetting('stroke')
		this.setSetting('stroke-width')
	}
	getEls() {
		return this.els
	}
	setElsInBox(box: IBox) {
		if (box.width === 0 || box.height === 0) {
			this.clear()
			return
		}

		const elsInBox = getElementsInBox(box, this.editor.svgContent)
		if (elsInBox.length === 0) {
			this.clear()
		} else {
			this.setEls(elsInBox)
		}
	}
	setElsIntersectWithBox(box: IBox) {
		if (box.width === 0 || box.height === 0) {
			this.clear()
			return
		}

		const elsInBox = getElementsIntersectWithBox(box, this.editor.svgContent)
		if (elsInBox.length === 0) {
			this.clear()
		} else {
			this.setEls(elsInBox)
		}
	}
	isEmpty() {
		return this.els.length === 0
	}
	isNotEmpty() {
		return this.els.length > 0
	}
	clear() {
		this.els = []
		// clear outline
		this.editor.huds.clear()
		// huds.outlineBoxHud.clear()
		// huds.elementOutlinesHub.clear()
	}
	contains(el: HTMLOrSVGElement) {
		for (let i = 0; i < this.els.length; i++) {
			if (this.els[i].el() === el) {
				return true
			}
		}
		return false
	}
	getMergeBBox() {
		// TODO:
		/* let x, y, w, h
    this.els.forEach(el => {
      const bbox = el.el().getbbox()
    }) */
	}
	// heightlight the elements
	heighligthEls() {
		const els = this.els
		if (els.length === 0) {
			console.warn("Can't heightlight Empty Elements")
			return
		}
		const huds = this.editor.huds

		const firstBox = new FSVG.Box(els[0].getBBox())
		const mergedBox = els.reduce((pre, curEl) => {
			const curBox = curEl.getBBox()
			return pre.merge(new FSVG.Box(curBox))
		}, firstBox)

		huds.outlineBoxHud.drawRect(mergedBox.x, mergedBox.y, mergedBox.width, mergedBox.height) // 绘制元素的包围盒子
		huds.elementOutlinesHub.draw(els)
	}
	setSetting(name: string) {
		const els = this.els

		const vals = els.map(el => {
			return el.getAttr(name)
		})

		this.editor.setting.set(name, vals[0]) // FIXME:
	}
	setElsAttr(name: string, val: string) {
		if (this.isNotEmpty()) {
			this.editor.executeCommand('setAttr', this.els, { [name]: val })
		}
	}
}
