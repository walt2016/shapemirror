import { _navTree } from '@/ui/petal/navTree'
import CommandManager from './command'
import editorDefaultConfig from './config'
import { Huds } from './hubs'
import { LayerManager } from './layer'
import { Namespace } from './namespace'
import { EditorSetting } from './setting/EditorSetting'
import { Shortcut } from './shortcut'
import { ToolManager } from './tools'
import { ToolType } from './tools/ToolAbstract'
import { Viewport } from './viewport'
import { ActiveElementsManager } from './activeElementsManager'
import { createSvgGroup, createSvgRect } from './util/svgHelper'
import ContextMenu from './contextMenu'

const createEditorStage = () => {
	const { svgRootW, svgRootH, svgStageW, svgStageH } = editorDefaultConfig
	const svgContainer = document.createElement('div')
	svgContainer.id = 'svg-container'
	svgContainer.style.backgroundColor = '#999'
	svgContainer.style.width = svgRootW + 'px'
	svgContainer.style.height = svgRootH + 'px'

	const svgRoot = document.createElementNS(Namespace.SVG, 'svg') as SVGSVGElement
	svgRoot.id = 'svg-root'
	svgRoot.setAttribute('width', svgRootW + '')
	svgRoot.setAttribute('height', svgRootH + '')
	svgRoot.setAttribute('viewBox', `0 0 ${svgRootW} ${svgRootH}`)

	const svgStage = document.createElementNS(Namespace.SVG, 'svg') as SVGSVGElement
	svgStage.id = 'svg-stage'
	svgStage.setAttribute('width', String(svgStageW))
	svgStage.setAttribute('height', String(svgStageH))
	svgStage.setAttribute('x', String(Math.floor((svgRootW - svgStageW) / 2)))
	svgStage.setAttribute('y', String(Math.floor((svgRootH - svgStageH) / 2)))
	// svgStage.style.overflow = 'visible'
	svgStage.setAttribute('style', 'overflow:visible')

	const svgBg = createSvgGroup()
	svgBg.id = 'background'
	// svgBg.setAttribute('width', 400)
	// svgBg.setAttribute('height', 300)
	svgBg.setAttribute('x', '0')
	svgBg.setAttribute('y', '0')

	const bgRect = createSvgRect()
	bgRect.setAttribute('width', '100%')
	bgRect.setAttribute('height', '100%')
	bgRect.setAttribute('fill', '#fff')

	const svgContent = createSvgGroup()
	svgContent.id = 'content'
	// svgContent.setAttribute('width', 400)
	// svgContent.setAttribute('height', 300)
	svgContent.setAttribute('x', '0')
	svgContent.setAttribute('y', '0')

	svgContainer.appendChild(svgRoot)
	svgRoot.appendChild(svgStage)

	svgStage.appendChild(svgBg)
	svgBg.appendChild(bgRect)
	svgStage.appendChild(svgContent)

	return { svgContainer, svgRoot, svgStage, svgContent }
}

export class Editor {
	setting: EditorSetting
	commandManager: CommandManager
	activeElementsManager: ActiveElementsManager
	shortcut: Shortcut
	viewport: Viewport
	huds: Huds
	toolManager: ToolManager
	layerManager: LayerManager
	// elements
	viewportElement: HTMLElement
	svgContainer: HTMLElement
	svgRoot: SVGSVGElement
	svgStage: SVGSVGElement
	svgContent: SVGGElement
	contextMenu: ContextMenu
	constructor() {
		this.setting = null
		this.contextMenu = new ContextMenu(this)
		this.commandManager = null
		this.activeElementsManager = new ActiveElementsManager(this)
		this.shortcut = new Shortcut(this)
		this.viewport = new Viewport(this)

		this.layerManager = new LayerManager(this)
		this.huds = new Huds(this)

		const { svgContainer, svgRoot, svgStage, svgContent } = createEditorStage()
		this.svgContainer = svgContainer
		this.svgRoot = svgRoot
		this.svgStage = svgStage
		this.svgContent = svgContent
		this.layerManager.createInitLayerAndMount()
		this.huds.mount()
	}

	mount(selector: string) {
		const viewportElement = document.querySelector(selector) as HTMLDivElement
		viewportElement.style.overflow = 'scroll'
		this.viewportElement = viewportElement
		viewportElement.appendChild(this.svgContainer)
	}
	getCurrentLayer() {
		return this.layerManager.getCurrent()
	}
	setCursor(val: string) {
		this.svgRoot.style.cursor = val
	}
	setSetting(setting: EditorSetting) {
		this.setting = setting
	}
	setToolManager(toolManager: ToolManager) {
		this.toolManager = toolManager
	}

	// 命令相关
	setCommandManager(commandManager: CommandManager) {
		this.commandManager = commandManager
	}
	executeCommand(name: string, ...params: any[]) {
		if (name === 'undo') {
			this.commandManager.undo()
			return
		}
		if (name === 'redo') {
			this.commandManager.redo()
			return
		}
		this.commandManager.execute(name, ...params)
	}

	setCurrentTool(name: ToolType) {
		this.toolManager.setCurrentTool(name)
	}
	getToolNavTree() {
		return this.toolManager.getToolNavTree()
	}
	buildNavTree() {
		const tools = this.getToolNavTree()
		_navTree(tools)
	}

	// TODO: set any type temporarily
	isContentElement(el: Element) {
		while (el) {
			const parent = el.parentElement
			if (parent instanceof SVGGElement && parent === this.svgContent) {
				return true
			}
			if (parent instanceof SVGSVGElement && parent === this.svgRoot) {
				return false
			}
			el = el.parentElement
		}
		return false
	}

	isElementOutlinesHub(el: Element) {
		const parent = el.parentElement
		if (parent instanceof SVGSVGElement && parent === this.huds.elementOutlinesHub.container.el()) {
			return true
		}
		return false
	}
}
