import { Editor } from '../Editor'
import EditorEventContext from '../EditorEventContext'
import { CommandName } from '../command/baseCommand'
import { getBoxBy2points, getEllipseProps } from '../util/math'
import { CursorType, ToolAbstract, ToolType } from './ToolAbstract'

export class Ellipse extends ToolAbstract {
	constructor(editor: Editor) {
		super(editor)
	}
	name() {
		return ToolType.Ellipse
	}
	cursorNormal() {
		return CursorType.Crosshair
	}
	cursorPress() {
		return CursorType.Crosshair
	}
	start() {
		/** do nothing */
	}
	move(ctx: EditorEventContext) {
		const { x: endX, y: endY } = ctx.getPos()
		const { x: startX, y: startY } = ctx.getStartPos()
		const { x, y, w, h } = getBoxBy2points(startX, startY, endX, endY)
		this.editor.huds.outlineBoxHud.drawRect(x, y, w, h)
		// const {x,y,r}=getCircleProps(startX, startY, endX, endY)
		// this.editor.executeCommand(CommandName.AddCircle, x, y, r)
	}
	end(ctx: EditorEventContext) {
		this.editor.huds.clear()
		const { x: endX, y: endY } = ctx.getPos()
		const { x: startX, y: startY } = ctx.getStartPos()
		const { x, y, rx, ry } = getEllipseProps(startX, startY, endX, endY)
		this.editor.executeCommand(CommandName.AddEllipse, x, y, rx, ry)
	}
	// mousedown outside viewport
	endOutside() {
		this.editor.huds.clear()
	}
}
