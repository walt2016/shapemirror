import EditorEventContext from '../EditorEventContext'
import { CommandName } from '../command/baseCommand'
import { CursorType, ToolAbstract, ToolType } from './ToolAbstract'

export class Pencil extends ToolAbstract {
	name() {
		return ToolType.Pencil
	}
	cursorNormal() {
		return CursorType.Default
	}
	cursorPress() {
		return CursorType.Default
	}
	start(ctx: EditorEventContext) {
		const { x, y } = ctx.getPos()
		this.editor.huds.pencilDraw.addPoint(x, y)
	}
	move(ctx: EditorEventContext) {
		const { x, y } = ctx.getPos()
		this.editor.huds.pencilDraw.addPoint(x, y)
	}
	private doWhenEndOrEndOutside() {
		const d = this.editor.huds.pencilDraw.getD()
		this.editor.huds.pencilDraw.clear()

		this.editor.setting.set('fill', 'none')
		this.editor.executeCommand(CommandName.AddPath, { d })
	}
	end() {
		this.doWhenEndOrEndOutside()
	}
	endOutside() {
		this.doWhenEndOrEndOutside()
	}
}

export default Pencil
