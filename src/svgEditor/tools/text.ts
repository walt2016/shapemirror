import { Editor } from '../Editor'
import EditorEventContext from '../EditorEventContext'
import { CommandName } from '../command/baseCommand'
import { getBoxBy2points } from '../util/math'
import { CursorType, ToolAbstract, ToolType } from './ToolAbstract'

export class Text extends ToolAbstract {
	constructor(editor: Editor) {
		super(editor)
	}
	name() {
		return ToolType.Text
	}
	cursorNormal() {
		return CursorType.Crosshair
	}
	cursorPress() {
		return CursorType.Crosshair
	}
	start() {
		/** do nothing */
	}
	move(ctx: EditorEventContext) {
		const { x: endX, y: endY } = ctx.getPos()
		const { x: startX, y: startY } = ctx.getStartPos()
		const { x, y, w, h } = getBoxBy2points(startX, startY, endX, endY)
		this.editor.huds.outlineBoxHud.drawRect(x, y, w, h)
	}
	end(ctx: EditorEventContext) {
		this.editor.huds.outlineBoxHud.clear()
		// this.editor.huds.elsOutlinesHub.clear()

		const { x: endX, y: endY } = ctx.getPos()
		const { x: startX, y: startY } = ctx.getStartPos()
		const { x, y, w, h } = getBoxBy2points(startX, startY, endX, endY)
		if (w < 2 && h < 2) {
			// TODO: open a dialog to input width and height
			console.log('width and height both less equal to 2，drawing nothing')
			return
		}
		this.editor.executeCommand(CommandName.AddRect, x, y, w, h)
	}
	// mousedown outside viewport
	endOutside() {
		// this.editor.huds.outlineBoxHud.clear()
		// this.editor.huds.elsOutlinesHub.clear()
	}
}
