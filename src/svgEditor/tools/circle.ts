import { Editor } from '../Editor'
import EditorEventContext from '../EditorEventContext'
import { CommandName } from '../command/baseCommand'
import { getBoxBy2points, getCircleProps } from '../util/math'
import { CursorType, ToolAbstract, ToolType } from './ToolAbstract'

export class Circle extends ToolAbstract {
	constructor(editor: Editor) {
		super(editor)
	}
	name() {
		return ToolType.Circle
	}
	cursorNormal() {
		return CursorType.Crosshair
	}
	cursorPress() {
		return CursorType.Crosshair
	}
	start() {
		/** do nothing */
	}
	move(ctx: EditorEventContext) {
		const { x: endX, y: endY } = ctx.getPos()
		const { x: startX, y: startY } = ctx.getStartPos()
		const { x, y, w, h } = getBoxBy2points(startX, startY, endX, endY)
		this.editor.huds.outlineBoxHud.drawRect(x, y, w, h)
		// const {x,y,r}=getCircleProps(startX, startY, endX, endY)
		// this.editor.executeCommand(CommandName.AddCircle, x, y, r)
	}
	end(ctx: EditorEventContext) {
		this.editor.huds.clear()
		const { x: endX, y: endY } = ctx.getPos()
		const { x: startX, y: startY } = ctx.getStartPos()
		const { cx, cy, r } = getCircleProps(startX, startY, endX, endY)
		this.editor.executeCommand(CommandName.AddCircle, cx, cy, r)
	}
	// mousedown outside viewport
	endOutside() {
		this.editor.huds.clear()
	}
}
