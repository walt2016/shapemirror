import { Editor } from '../Editor'
import EditorEventContext from '../EditorEventContext'
export enum ToolType {
	Pen = 'pen',
	Pencil = 'pencil',
	Select = 'select',
	Rect = 'rect',
	Text = 'text',
	Circle = 'circle',
	Ellipse = 'ellipse',
}

export enum CursorType {
	Default = 'default',
	Crosshair = 'crosshair',
	nwResize = 'nw-resize',
	nResize = 'n-resize',
	neResize = 'ne-resize',
	swResize = 'sw-resize',
	sResize = 's-resize',
	seResize = 'se-resize',
	eResize = 'e-resize',
	wResize = 'w-resize',
	Move = 'move',
	Text = 'text',
}
export abstract class ToolAbstract {
	constructor(protected editor: Editor) {}
	mounted() {
		/** Do Nothing */
	}
	willUnmount() {
		/** Do Nothing */
	}
	abstract name(): ToolType
	abstract cursorNormal(): CursorType
	abstract cursorPress(): string
	abstract start(ctx: EditorEventContext): void
	abstract move(ctx: EditorEventContext): void
	moveNoDrag(ctx: EditorEventContext) {
		/* nope */
	}
	abstract end(ctx: EditorEventContext): void
	abstract endOutside(ctx: EditorEventContext): void
	/*  beforeActive() {
    // do something before switch to current tool
  } */
}
