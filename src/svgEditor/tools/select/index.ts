import { FSVG } from '@/svgEditor/element/factory'
import { Editor } from '../../Editor'
import EditorEventContext from '../../EditorEventContext'
import { FElement } from '../../element/baseElement'
import { CursorType, ToolAbstract, ToolType } from '../ToolAbstract'
import { ModeFactory } from './factory'
import MouseMode, { ModeType } from './mouseMode'

/**
 * select
 *
 * 此模块非常复杂
 *
 * 1. 鼠标按下时，选中单个元素
 * 2. 鼠标按下为空，拖拽时产生选中框，可以选择多个元素
 * 3. 选中单个（或选区选中多个） 缩放 等控制点，拖拽改变宽高
 * 3. 切断到这个工具时，激活的元素进入被选中状态（轮廓线+控制点）。
 * 4. 选区和元素相交的判定
 * 5. 激活元素如何保存，保存到哪里
 */
export class Select extends ToolAbstract {
	private mode: MouseMode
	private modeFactory: ModeFactory
	private selectedEls: Array<FElement> = []

	constructor(editor: Editor) {
		super(editor)
		this.modeFactory = new ModeFactory(editor)
	}
	name() {
		return ToolType.Select
	}
	cursorNormal() {
		return CursorType.Default
	}
	cursorPress() {
		return CursorType.Default
	}
	start(ctx: EditorEventContext) {
		const target = ctx.nativeEvent.target as Element
		const outlineBoxHud = this.editor.huds.outlineBoxHud
		console.log(target, outlineBoxHud, this.editor.activeElementsManager)

		const grip = outlineBoxHud.findGrip(target as SVGElement)
		if (grip) {
			console.log(grip)

			if (grip.getID() === 'center-grip') {
				this.mode = this.modeFactory.getMode(ModeType.Move)
			} else {
				this.mode = this.modeFactory.getMode(ModeType.Resize)
			}
		} else if (this.editor.isElementOutlinesHub(target)) {
			this.mode = this.modeFactory.getMode(ModeType.Move)
		} else if (target === outlineBoxHud.outline.el()) {
			this.mode = this.modeFactory.getMode(ModeType.Move)
		} else if (this.editor.isContentElement(target)) {
			this.mode = this.modeFactory.getMode(ModeType.Move)
		} else {
			this.mode = this.modeFactory.getMode(ModeType.FrameSelect)
		}
		console.log(this.mode.moveType)

		this.mode.start(ctx)
	}
	move(ctx: EditorEventContext) {
		console.log('move')
		this.mode.move(ctx)
	}
	end(ctx: EditorEventContext) {
		this.mode.end(ctx)
	}
	// mousedown outside viewport
	endOutside(ctx: EditorEventContext) {
		this.mode.endOutside(ctx)
	}

	mounted() {
		this.editor.huds.outlineBoxHud.enableResizeGrip()
		if (this.editor.activeElementsManager.isNotEmpty()) {
			this.editor.activeElementsManager.heighligthEls()
		}
	}
	willUnmount() {
		this.editor.huds.outlineBoxHud.disableResizeGrip(true)
	}
	moveNoDrag(ctx: EditorEventContext) {
		const target = ctx.nativeEvent.target as Element
		// console.log(target)

		if (this.editor.isContentElement(target)) {
			const targetFElement = FSVG.create(target as SVGElement)
			this.editor.activeElementsManager.setEls(targetFElement)
			this.editor.activeElementsManager.heighligthEls()
		}
		// console.log(ctx, 'select moveNoDrag')
		// this.mode.moveNoDrag(ctx)
	}
}
