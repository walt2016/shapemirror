import { Editor } from '@/svgEditor/Editor'
import EditorEventContext from '@/svgEditor/EditorEventContext'

export enum ModeType {
	FrameSelect = 'frameSelect',
	Move = 'move',
	Resize = 'resize',
}
abstract class MouseMode {
	abstract moveType: ModeType
	constructor(protected editor: Editor) {}
	abstract start(ctx: EditorEventContext): void
	abstract move(ctx: EditorEventContext): void
	abstract end(ctx: EditorEventContext): void
	abstract endOutside(ctx: EditorEventContext): void
}

export default MouseMode
