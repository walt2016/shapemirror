import EditorEventContext from '@/svgEditor/EditorEventContext'
import MouseMode, { ModeType } from './mouseMode'
import { ResizeDirection } from '@/svgEditor/hubs/resizeGrips'
import { Path } from '@/svgEditor/element/path'
import { resizeCircle, resizeRect } from '@/svgEditor/util/resizeHelper'

export class Resize extends MouseMode {
	moveType: ModeType = ModeType.Resize
	private cx: number
	private cy: number
	private originWidth: number
	private originHeight: number
	private direction: ResizeDirection

	start(ctx: EditorEventContext) {
		/**
		 * 1. record match position
		 * 记录相匹配位置
		 */
		const target = ctx.nativeEvent.target
		const outlineBoxHud = this.editor.huds.outlineBoxHud
		// 根据 target 获取对应缩放点（比如是左上还是右下）
		const grip = outlineBoxHud.findGrip(target as SVGElement)

		this.originHeight = outlineBoxHud.getHeight()
		this.originWidth = outlineBoxHud.getWidth()

		// 方向
		this.direction = outlineBoxHud.resizeGrips.getDirection(grip)

		const centerGrip = outlineBoxHud.resizeGrips.getOppositeGrip(grip) // 获取缩放中心点
		const pos = centerGrip.getCentre()
		this.cx = pos.x
		this.cy = pos.y
	}
	move(ctx: EditorEventContext) {
		/** 2. get current pos */
		const { x, y } = ctx.getPos()
		/** calc size */
		const x1 = Math.min(x, this.cx)
		const y1 = Math.min(y, this.cy)
		const x2 = Math.max(x, this.cx)
		const y2 = Math.max(y, this.cy)
		let width = x2 - x1
		let height = y2 - y1

		if ([ResizeDirection.Right, ResizeDirection.Left].includes(this.direction)) {
			height = this.editor.huds.outlineBoxHud.getHeight()
		}
		if ([ResizeDirection.Bottom, ResizeDirection.Top].includes(this.direction)) {
			width = this.editor.huds.outlineBoxHud.getWidth()
		}

		this.editor.huds.outlineBoxHud.drawRect(x1, y1, width, height)
	}
	end(ctx: EditorEventContext) {
		const { x: dx, y: dy } = ctx.getDiffPos()
		if (dx === 0 && dy === 0) return

		const { x, y, width, height } = this.editor.huds.outlineBoxHud.getBox()
		const scaleX = width / this.originWidth
		const scaleY = height / this.originHeight
		const scale = {
			scaleX,
			scaleY,
		}
		const basePoint = {
			x,
			y,
		}
		const elements = this.editor.activeElementsManager.getEls()

		elements.forEach(t => {
			const bbox = t.getBBox()
			if (t.isRect()) {
				const bounds = resizeRect(t, scale, basePoint)
				this.editor.executeCommand('setAttr', t, bounds)
			} else if (t.isCircle()) {
				const cp = resizeCircle(t, scale, basePoint)
				this.editor.executeCommand('setAttr', t, cp)
			} else if (t.isPath()) {
				let basePoint = {
					x: bbox.x,
					y: bbox.y,
				}

				if ([ResizeDirection.Left, ResizeDirection.BottomLeft].includes(this.direction)) {
					basePoint = {
						x: bbox.x + bbox.width,
						y: bbox.y,
					}
				}

				if (ResizeDirection.TopLeft === this.direction) {
					basePoint = {
						x: bbox.x + bbox.width,
						y: bbox.y + bbox.height,
					}
				}

				if ([ResizeDirection.Top, ResizeDirection.TopRight].includes(this.direction)) {
					basePoint = {
						x: bbox.x,
						y: bbox.y + bbox.height,
					}
				}

				;(t as Path).resize({ scaleX, scaleY }, basePoint)

				// const d = (t as Path).resize(width / bbox.width, height / bbox.height, basePoint)

				// this.editor.executeCommand('setAttr', t, { d })
			}
		})

		// 计算新的图形位置
		this.editor.activeElementsManager.heighligthEls()
	}
	endOutside() {
		this.editor.huds.outlineBoxHud.clear()
		this.editor.huds.elementOutlinesHub.clear()
	}
}
