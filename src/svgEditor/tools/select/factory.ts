import { Editor } from '@/svgEditor/Editor'
import MouseMode, { ModeType } from './mouseMode'
import { FrameSelect } from './frameSelect'
import { Move } from './move'
import { Resize } from './resize'

class ModeFactory {
	private strategies: { [K in ModeType]: MouseMode }

	constructor(editor: Editor) {
		this.strategies = {
			frameSelect: new FrameSelect(editor),
			move: new Move(editor),
			resize: new Resize(editor),
		}
	}
	getMode(type: ModeType) {
		return this.strategies[type]
	}
}

export { MouseMode, ModeFactory }
