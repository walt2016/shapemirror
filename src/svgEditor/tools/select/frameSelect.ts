import EditorEventContext from '../../EditorEventContext'
import { getBoxBy2points } from '../../util/math'
import MouseMode, { ModeType } from './mouseMode'

/**
 * 框选
 */
export class FrameSelect extends MouseMode {
	moveType: ModeType = ModeType.FrameSelect

	start(ctx: EditorEventContext): void {
		// throw new Error("Method not implemented.");
		console.log(this.moveType)
	}
	move(ctx: EditorEventContext): void {
		const { x: endX, y: endY } = ctx.getPos()
		const { x: startX, y: startY } = ctx.getStartPos()
		const { x, y, w, h } = getBoxBy2points(startX, startY, endX, endY)
		this.editor.huds.frameSelect.drawRect(x, y, w, h)
	}
	end(ctx: EditorEventContext): void {
		const box = this.editor.huds.frameSelect.getBox()
		this.editor.huds.frameSelect.clear()
		// 相交
		this.editor.activeElementsManager.setElsIntersectWithBox(box)
	}
	endOutside(ctx: EditorEventContext): void {
		this.editor.huds.frameSelect.clear()
		this.editor.activeElementsManager.clear()
	}
	moveNoDrag(ctx: EditorEventContext) {
		console.log(ctx, 'moveNoDrag')
	}
}
