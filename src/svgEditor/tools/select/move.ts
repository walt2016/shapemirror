import { FElement } from '@/svgEditor/element/baseElement'
import MouseMode, { ModeType } from './mouseMode'
import EditorEventContext from '@/svgEditor/EditorEventContext'
import { FSVG } from '@/svgEditor/element/factory'
import { CursorType } from '../ToolAbstract'

export class Move extends MouseMode {
	moveType: ModeType = ModeType.Move
	private selectedEls: Array<FElement> = []

	start(ctx: EditorEventContext) {
		// encapsulate svg element
		const target = ctx.nativeEvent.target
		console.log(target)
		const activedElsManager = this.editor.activeElementsManager

		const targetFElement = FSVG.create(target as SVGElement)
		// check whether target element is part of activedEls.
		if (activedElsManager.contains(target as SVGElement)) {
			activedElsManager.heighligthEls()
		} else {
			activedElsManager.setEls(targetFElement)
		}
		this.selectedEls = activedElsManager.getEls()
		console.log(this.selectedEls)

		this.editor.setCursor(CursorType.Move)
	}
	move(ctx: EditorEventContext) {
		const { x: dx, y: dy } = ctx.getDiffPos()
		const zoom = this.editor.viewport.getZoom()
		this.editor.huds.elementOutlinesHub.translate(dx / zoom, dy / zoom)
	}
	end(ctx: EditorEventContext) {
		this.editor.huds.outlineBoxHud.clear()
		this.editor.huds.elementOutlinesHub.clear()

		const { x: dx, y: dy } = ctx.getDiffPos()
		if (dx !== 0 || dy !== 0) {
			const zoom = this.editor.viewport.getZoom()
			console.log(this.selectedEls)
			this.editor.executeCommand('dmove', this.selectedEls, dx / zoom, dy / zoom)
		}

		this.editor.activeElementsManager.setEls(this.selectedEls) // set global actived elements
		this.selectedEls = []
		this.editor.setCursor(CursorType.Default)
	}
	endOutside() {
		this.selectedEls = []
	}
}
