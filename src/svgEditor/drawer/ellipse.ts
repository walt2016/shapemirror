import { curveTo, putStyle } from './base'

export const ellipse = (x: number, y: number, rx: number, ry: number, style: string) => {
	const lx = (4 / 3) * (Math.SQRT2 - 1) * rx,
		ly = (4 / 3) * (Math.SQRT2 - 1) * ry

	moveTo(x + rx, y)
	curveTo([x + rx, y - ly], [x + lx, y - ry], [x, y - ry])
	curveTo([x - lx, y - ry], [x - rx, y - ly], [x - rx, y])
	curveTo([x - rx, y + ly], [x - lx, y + ry], [x, y + ry])
	curveTo([x + lx, y + ry], [x + rx, y + ly], [x + rx, y])

	putStyle(style)
	return this
}
