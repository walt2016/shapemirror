export const isValidStyle = (style: string) => {
	const validStyleVariants = [undefined, null, 'S', 'D', 'F', 'DF', 'FD', 'f', 'f*', 'B', 'B*', 'n']
	let result = false
	if (validStyleVariants.indexOf(style) !== -1) {
		result = true
	}
	return result
}
