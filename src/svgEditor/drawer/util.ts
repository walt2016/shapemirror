import { Point } from './type'

const precision = 2
const roundToPrecision = (number: number, parmPrecision: number): string => {
	const tmpPrecision = precision || parmPrecision
	return number.toFixed(tmpPrecision).replace(/0+$/, '')
}

// const floatPrecision: number = 16;
// // high precision float
// let hpf: (n: number) => string;
// if (typeof floatPrecision === "number") {
//   hpf = (n: number) => {
//     return roundToPrecision(n, floatPrecision);
//   };
// } else if (floatPrecision === "smart") {
//   hpf = (n: number) => {
//     if (n > -1 && n < 1) {
//       return roundToPrecision(n, 16);
//     } else {
//       return roundToPrecision(n, 5);
//     }
//   };
// } else {
//   hpf = (n: number) => {
//     return roundToPrecision(n, 16);
//   };
// }

export const hpf = (n: number) => {
	return roundToPrecision(n, 16)
}

export const f2 = (n: number) => {
	return roundToPrecision(n, 2)
}

export const f3 = (n: number) => {
	return roundToPrecision(n, 3)
}

export const toVector = (p1: Point, p2: Point): Point => {
	const [x1, y1] = p1
	const [x2, y2] = p2
	return [x2 - x1, y2 - y1]
}
