import { Mode, Point } from './type'
import { hpf } from './util'
const scaleFactor = 96 / 72 // Scale factor
const unit = 'px'
// switch (unit) {
//   case "pt":
//     scaleFactor = 1;
//     break;
//   case "mm":
//     scaleFactor = 72 / 25.4;
//     break;
//   case "cm":
//     scaleFactor = 72 / 2.54;
//     break;
//   case "in":
//     scaleFactor = 72;
//     break;
//   case "px":
//     if (hasHotfix("px_scaling") == true) {
//       scaleFactor = 72 / 96;
//     } else {
//       scaleFactor = 96 / 72;
//     }
//     break;
//   case "pc":
//     scaleFactor = 12;
//     break;
//   case "em":
//     scaleFactor = 12;
//     break;
//   case "ex":
//     scaleFactor = 6;
//     break;
//   default:
//     if (typeof unit === "number") {
//       scaleFactor = unit;
//     } else {
//       throw new Error("Invalid unit: " + unit);
//     }
// }
const content = []
let contentLength = 0
const outputDestination = content

export const out = (str: string) => {
	contentLength += str.length + 1
	outputDestination.push(str)

	return outputDestination
}

export var putStyle = function (style) {
	switch (style) {
		case 'stroke':
			out('S')
			break
		case 'fill':
			out('f')
			break
	}
}

export const scale = (n: number, mode?: Mode) => {
	if (mode === Mode.ADVANCED) {
		return n
	} else {
		return n * scaleFactor
	}
}
const a4 = [595.28, 841.89]
const format = a4
const mediaBox = {
	bottomLeftX: 0,
	bottomLeftY: 0,
	topRightX: Number(format[0]),
	topRightY: Number(format[1]),
}

const currentPage = 0
const getPageHeight = (pageNumber?: number): number => {
	// pageNumber = pageNumber || currentPage;
	return (mediaBox.topRightY - mediaBox.bottomLeftY) / scaleFactor
}

export const transformY = function (y: number, mode?: Mode): number {
	if (mode === Mode.ADVANCED) {
		return y
	} else {
		return getPageHeight() - y
	}
}

export const transformScaleY = function (y) {
	return scale(transformY(y))
}

export const moveTo = (p: Point) => {
	const [x, y] = p
	out(hpf(scale(x)) + ' ' + hpf(transformScaleY(y)) + ' m')
}

export const curveTo = (p1: Point, p2: Point, p3: Point) => {
	const [x1, y1] = p1
	const [x2, y2] = p2
	const [x3, y3] = p3
	out([hpf(scale(x1)), hpf(transformScaleY(y1)), hpf(scale(x2)), hpf(transformScaleY(y2)), hpf(scale(x3)), hpf(transformScaleY(y3)), 'c'].join(' '))
}

export const lineTo = (p: Point) => {
	const [x, y] = p
	out(hpf(scale(x)) + ' ' + hpf(transformScaleY(y)) + ' l')
}

export const close = () => {
	out('h')
}

export const stroke = () => {
	out('S')
}

const defaultPathOperation = 'S'

export const getStyle = (style: string) => {
	// see path-painting operators in PDF spec
	var op = defaultPathOperation // stroke

	switch (style) {
		case 'D':
		case 'S':
			op = 'S' // stroke
			break
		case 'F':
			op = 'f' // fill
			break
		case 'FD':
		case 'DF':
			op = 'B'
			break
		case 'f':
		case 'f*':
		case 'B':
		case 'B*':
			/*
               Allow direct use of these PDF path-painting operators:
               - f    fill using nonzero winding number rule
               - f*    fill using even-odd rule
               - B    fill then stroke with fill using non-zero winding number rule
               - B*    fill then stroke with fill using even-odd rule
               */
			op = style
			break
	}
	return op
}

export const clipRuleFromStyle = function (style: string) {
	switch (style) {
		case 'f':
		case 'F':
			return 'W n'
		case 'f*':
			return 'W* n'
		case 'B':
			return 'W S'
		case 'B*':
			return 'W* S'

		// these two are for compatibility reasons (in the past, calling any primitive method with a shading pattern
		// and "n"/"S" as style would still fill/fill and stroke the path)
		case 'S':
			return 'W S'
		case 'n':
			return 'W n'
	}
}
