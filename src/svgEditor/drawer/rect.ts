import { out, putStyle, scale, transformScaleY } from './base'
import { lines } from './lines'
import { Mode } from './type'
import { hpf } from './util'

export const rect = (x: number, y: number, w: number, h: number, style: string, mode?: Mode) => {
	if (mode === Mode.COMPAT) {
		h = -h
	}

	out([hpf(scale(x)), hpf(transformScaleY(y)), hpf(scale(w)), hpf(scale(h)), 're'].join(' '))

	putStyle(style)
}

export const roundedRect = (x: number, y: number, w: number, h: number, rx: number, ry: number, style: string) => {
	var MyArc = (4 / 3) * (Math.SQRT2 - 1)

	rx = Math.min(rx, w * 0.5)
	ry = Math.min(ry, h * 0.5)

	lines(
		[
			[w - 2 * rx, 0],
			[rx * MyArc, 0, rx, ry - ry * MyArc, rx, ry],
			[0, h - 2 * ry],
			[0, ry * MyArc, -(rx * MyArc), ry, -rx, ry],
			[-w + 2 * rx, 0],
			[-(rx * MyArc), 0, -rx, -(ry * MyArc), -rx, -ry],
			[0, -h + 2 * ry],
			[0, -(ry * MyArc), rx * MyArc, -ry, rx, -ry],
		],
		[x + rx, y], // start of path
		[1, 1],
		style,
		true
	)
}
