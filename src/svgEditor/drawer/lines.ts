import { curveTo, lineTo, putStyle, moveTo } from './base'
import { Point } from './type'

export const lines = function (lines: Point[], startPoint: Point, scale: [number, number], style?: string, closed?: boolean) {
	let i, leg, x2, y2, x3, y3, tmp

	// Pre-August-2012 the order of arguments was function(x, y, lines, scale, style)
	// in effort to make all calls have similar signature like
	//   function(content, coordinateX, coordinateY , miscellaneous)
	// this method had its args flipped.
	// code below allows backward compatibility with old arg order.
	//   if (typeof lines === "number") {
	//     tmp = y;
	//     y = x;
	//     x = lines;
	//     lines = tmp;
	//   }

	//   scale = scale || [1, 1];
	//   closed = closed || false;

	//   if (
	//     isNaN(x) ||
	//     isNaN(y) ||
	//     !Array.isArray(lines) ||
	//     !Array.isArray(scale) ||
	//     !isValidStyle(style) ||
	//     typeof closed !== "boolean"
	//   ) {
	//     throw new Error("Invalid arguments passed to jsPDF.lines");
	//   }

	// starting point
	moveTo(startPoint)

	let [scalex, scaley] = scale
	const l = lines.length
	//, x2, y2 // bezier only. In page default measurement "units", *after* scaling
	//, x3, y3 // bezier only. In page default measurement "units", *after* scaling
	// ending point for all, lines and bezier. . In page default measurement "units", *after* scaling
	const [x, y] = startPoint
	let x4 = x // last / ending point = starting point for first item.
	let y4 = y // last / ending point = starting point for first item.

	for (i = 0; i < l; i++) {
		leg = lines[i]
		if (leg.length === 2) {
			// simple line
			x4 = leg[0] * scalex + x4 // here last x4 was prior ending point
			y4 = leg[1] * scaley + y4 // here last y4 was prior ending point
			lineTo([x4, y4])
		} else {
			// bezier curve
			x2 = leg[0] * scalex + x4 // here last x4 is prior ending point
			y2 = leg[1] * scaley + y4 // here last y4 is prior ending point
			x3 = leg[2] * scalex + x4 // here last x4 is prior ending point
			y3 = leg[3] * scaley + y4 // here last y4 is prior ending point
			x4 = leg[4] * scalex + x4 // here last x4 was prior ending point
			y4 = leg[5] * scaley + y4 // here last y4 was prior ending point
			curveTo([x2, y2], [x3, y3], [x4, y4])
		}
	}

	if (closed) {
		close()
	}

	putStyle(style)
}
