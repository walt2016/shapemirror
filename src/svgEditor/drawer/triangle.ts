import { lines } from './lines'
import { Point } from './type'
import { toVector } from './util'

export const triangle = (p1: Point, p2: Point, p3: Point, style: string) => {
	lines([toVector(p1, p2), toVector(p2, p3), toVector(p3, p1)], p1, [1, 1], style, true)
}
