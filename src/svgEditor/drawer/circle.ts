import { ellipse } from './ellipse'

export const circle = (x: number, y: number, r: number, style: string) => {
	return ellipse(x, y, r, r, style)
}
