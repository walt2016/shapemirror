export enum Mode {
	COMPAT = 'compat',
	ADVANCED = 'advanced',
}

export interface IPoint {
	x: number
	y: number
}

export type Point = number[]

export const CapJoinStyles = {
	0: 0,
	butt: 0,
	but: 0,
	miter: 0,
	1: 1,
	round: 1,
	rounded: 1,
	circle: 1,
	2: 2,
	projecting: 2,
	project: 2,
	square: 2,
	bevel: 2,
}

export type TCapJoin = keyof typeof CapJoinStyles
