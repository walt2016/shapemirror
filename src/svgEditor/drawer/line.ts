import { stroke } from './base'
import { lines } from './lines'
import { Mode, Point } from './type'
import { toVector } from './util'

export const line = (p1: Point, p2: Point, style: string, apiMode: Mode) => {
	if (apiMode === Mode.COMPAT) {
		return lines([toVector(p1, p2)], p1, [1, 1], style || 'S')
	} else {
		return lines([toVector(p1, p2)], p1, [1, 1])
		// stroke();
	}
}
