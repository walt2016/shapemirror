import { Editor } from '@/svgEditor/Editor'
import { IScreen } from '@/types/base'
import { IMenuItem } from '@/types/menu'
import { IFeild } from '@/types/field'
import { NavTree } from '@/ui/petal/navTree'

// 屏幕大小
export const _screen = (width?: string | number, height?: string | number): IScreen => {
	let w: number
	let h: number
	if (width == null || height == null || width === '100%' || height === '100%') {
		let viewer = document.querySelector('.main .screen-viewer')
		if (viewer) {
			let rect = viewer.getBoundingClientRect()
			w = h = Math.min(rect.height, rect.width)
		}
	}

	w = w ?? 800
	h = h ?? 600

	return {
		width: w,
		height: h,
		o: [w / 2, h / 2],
	}
}
const isH5 = (): boolean => {
	return window.innerWidth < 640
	// var sUserAgent = navigator.userAgent.toLowerCase();
	// return (/ipad|iphone|midp|rv:1.2.3.4|ucweb|android|windows ce|windows mobile/.test(sUserAgent))
}

class Store {
	allPages: IMenuItem[] = []
	openList: IMenuItem[] = []
	currentPage: IMenuItem = {}
	currentFields: IFeild[] = []
	screenViewer: HTMLElement = document.body
	root: HTMLElement = document.body
	screenWidth: number = window.innerWidth
	isH5: boolean = isH5()
	screen: IScreen = _screen()
	editor: Editor = null
	navTree: NavTree = null

	setFields(fields: IFeild[]) {
		this.currentFields = fields
	}
	getFields() {
		return this.currentFields
	}
	setPage(page: IMenuItem) {
		const openList = this.openList
		if (!openList.some(t => t.name === page.name)) {
			openList.unshift(page)
		}
		this.currentPage = page
	}
	getPage() {
		return this.currentPage
	}
	getAllPages() {
		return this.allPages
	}
	setAllPages(pages: IMenuItem[]) {
		this.allPages = pages
	}
	getOpenList() {
		return this.openList
	}
	removePage(name: string) {
		const index = this.openList.findIndex(t => t.name === name)
		this.openList.splice(index, 1)
	}
	setEditor(editor: Editor) {
		this.editor = editor
	}
	getEditor() {
		return this.editor
	}
	setNavTree(navTree: NavTree) {
		this.navTree = navTree
	}
	getNavTree() {
		return this.navTree
	}
	getPageByName(name: string): IMenuItem {
		const list = this.getOpenList()
		const page = list.find(t => t.name == name)

		if (page) {
			return page
		}
		return this.allPages.find(t => t.name == name)
	}
	getNextPage(): IMenuItem {
		const list = this.allPages
		const len = list.length
		const current = this.getPage()
		const index = list.findIndex(t => t.name === current.name)
		const t = index + 1 >= len ? list[0] : list[index + 1]
		return t
	}
	getPrevPage(): IMenuItem {
		const list = this.allPages
		const len = list.length
		const current = this.getPage()
		const index = list.findIndex(t => t.name === current.name)
		const t = index - 1 < 0 ? list[len - 1] : list[index - 1]
		return t
	}
}

// 全局变量
export const store = new Store()
