import { IColorProps } from './color'
import { Point } from './point'

export enum Fractal {
	None = 'none',
	EdgeFractal = 'edgeFractal',
	VertexFractal = 'vertexFractal',
	VertexMirror = 'vertexMirror',
	EdgeMirror = 'edgeMirror',
	EdgeMid = 'edgeMid',
	Yanghui = 'yanghui',
	Gougu = 'gougu',
}

// 分形参数
export interface IFractalOptions {
	o?: Point
	r?: number
	a?: number
	n?: number
	k?: number
	depth?: number
	sweep?: boolean
	startIndex?: number
	wriggle?: number
	transform?: string
	pathMode?: string
	vertex?: any
	fractal?: any
	direct?: number
	points?: Point[]
	color?: string | IColorProps
	closed?: boolean
	broken?: boolean
	index?: number
}
