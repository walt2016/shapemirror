export interface ITreeOptions {
	o?: number[]
	n?: number
	r?: number
	a?: number
	depth?: number
	attenuation?: number
	wriggle?: number
	pathMode?: any
	treeCurve?: string
	color?: string
	alpha?: number
}
