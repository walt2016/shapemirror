import { TSweepFlag } from '.'
import { IMirrorProps } from './mirror'
import { IEdgeProps } from './path'
import { Point } from './point'
import { IBaseShape } from './shape'

// 极坐标点参数
export interface IPolarOptions extends IBaseShape {
	k?: number
	w?: number
	v?: number
	increasedAgnle?: number
	sweepFlag?: TSweepFlag
	a2?: number
	direction?: number
	to?: Point
	num?: number
	transform?: string
	mirror?: IMirrorProps
	pathMode?: string
	interiorPolygon?: number
	edge?: IEdgeProps
	color?: string
	end?: Point
	curveShape?: string
}
