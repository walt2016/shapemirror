export type Field = string

export enum FieldType {
	String = 'string',
	Number = 'number',
}

export interface IOption {
	label?: string
	value?: string
	name?: string
	click?: {
		(arg: MouseEvent, scope: any): void
	}
}

/**
 * 表单字段
 */
export interface IFeild {
	label?: string
	field?: string
	type?: string
	value?: string | number
	disabled?: boolean
	name?: string
	options?: IOption[]
	group?: string
	class?: string

	placeholder?: string
	clearable?: boolean
}

export interface IRow {
	index?: number
}
