import { TSweepFlag } from '.'
import { IFillBaseProps } from './fill'
import { Point } from './point'
import { IStrokeProps } from './stroke'

export interface IAngleInfo {
	points: Point[]
	a: number
	ia: number
	sweepFlag: TSweepFlag
	o: Point
	r: number
	labelPos: Point
	label: string | number
}

export interface ILineInfo {
	points: Point[]
	labelPos: Point
	label: string | number
}

export interface ILabelInfo {
	labelPos: Point
	label: string | number
}
export interface IBoundsInfo {
	points: Point[]
	labelPos: Point
	label: string | number
}

export interface IMarkInfo {
	angleInfo?: IAngleInfo[]
	edgeInfo?: ILineInfo[]
	radiusInfo?: ILineInfo[]
	labelsInfo?: ILabelInfo[]
	boundingSizeInfo?: IBoundsInfo[]
	boundingRectPoints?: Point[]
}
export interface IMarkInfoOptions {
	angle?: boolean
	edge?: boolean
	r?: number
	radius?: boolean
	labels?: boolean
	boundingRect?: boolean
	boundingSize?: boolean
}

export interface IMarkOptions {
	points: Point[]
	mark: IMarkInfoOptions
}

export interface IMarkProps extends IStrokeProps, IFillBaseProps {
	visible: boolean
	r: number
	fontSize: number
	edge: true
	angle: true
	radius: boolean
	labels: boolean
	boundingSize: boolean
	boundingRect: boolean
}
