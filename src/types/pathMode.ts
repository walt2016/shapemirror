import { ICentreOptions } from '.'
import { ICurve } from './curve'
import { IFillOptions } from './fill'
import { Point } from './point'

//  参数名参考webgl.type
export enum PathMode {
	POINTS = 'POINTS',
	LINES = 'LINES',
	LINE_STRIP = 'LINE_STRIP',
	LINE_LOOP = 'LINE_LOOP',
	LINE_FAN = 'LINE_FAN',
	// POLYGON= 'POLYGON',
	TRIANGLES = 'TRIANGLES',
	TRIANGLE_STRIP = 'TRIANGLE_STRIP',
	TRIANGLE_STRIP_LOOP = 'TRIANGLE_STRIP_LOOP',
	TRIANGLE_FAN = 'TRIANGLE_FAN',
	TRIANGLE_SERIAL = 'TRIANGLE_SERIAL',
	TRIANGLE_SERIAL_LOOP = 'TRIANGLE_SERIAL_LOOP',

	SQUARES = 'SQUARES',
	SQUARE_STRIP = 'SQUARE_STRIP',
	SQUARE_STRIP_LOOP = 'SQUARE_STRIP_LOOP',
	SQUARE_SERIAL = 'SQUARE_SERIAL',
	SQUARE_SERIAL_LOOP = 'SQUARE_SERIAL_LOOP',
	RAY = 'RAY',
	RAY_FAN = 'RAY_FAN',
	LINK = 'LINK',
	CURVE_STRIP = 'CURVE_STRIP',
	CURVE_LOOP = 'CURVE_LOOP',
	BEZIER_STRIP = 'BEZIER_STRIP',
	BEZIER_LOOP = 'BEZIER_LOOP',
	DISCRETE_BEZIER = 'DISCRETE_BEZIER',
}

export const pathModeTypes = Object.values(PathMode)

export interface IPathModeOptions {
	pathMode?: string
	points: Point[]
	curve?: string | ICurve
	closed?: boolean
	fill?: IFillOptions
	centre?: ICentreOptions
}
