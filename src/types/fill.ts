import { Point } from './point'
import { IStrokeProps } from './stroke'

export enum FillRule {
	Nonzero = 'nonzero',
	Evenodd = 'evenodd',
}
export interface IFillBaseProps {
	fill?: string
	fillRule?: FillRule
}

export interface IFillOptions {
	points?: Point[]
	gridDensity?: number
	rayDensity?: number
	offset?: number
	inheritCurve?: boolean
	pattern?: string
}

export interface IFillProps extends IStrokeProps, IFillBaseProps {
	pattern: string
	gridDensity: number
	rayDensity: number
	offset: number
	fontSize: number
	inheritCurve: false
	// color:'none'
}
