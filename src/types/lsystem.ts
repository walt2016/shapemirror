import { Point } from './point'

export interface ILSystemOptions {
	o?: Point
	r?: number
	angle?: number
	wriggle?: number
	reduction?: number
}

export interface IRules {
	F: string
	[k: string]: string
}
export interface IGrammerOptions {
	axiom?: string
	rules?: IRules
	depth?: number
	angle?: number
}
