import { ICurve } from './curve'

export type Point = number[]
export type Points = Point[]
export type PointMatrix = Point[][]
export type PointSegment = [Point, Point]
export type PointRect = [Point, Point, Point, Point]

export interface IPoint {
	x: number
	y: number
}

export interface IPointOptions {
	r: number
}

export interface IPointsOptions {
	points: Point[]
	curve?: string | ICurve
	loop?: boolean
	o?: Point
	step?: number
	discrete?: boolean
	skew?: number | boolean
	amplitude?: number
	depth?: number
	increasedAgnle?: number
	bottomControlGear?: string
	closed?: boolean
	broken?: boolean
	endToEnd?: boolean
}
