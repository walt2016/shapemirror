import { Point } from './point'

export interface IAxisProps {
	type: string
	crossPoints: boolean
	labels: boolean
	cellPoints: boolean
}

export interface IAxisOptions {
	gridSpace?: number
	gridLineColor?: string
	lineDashStyle?: number

	id?: string
	width?: number
	height?: number
	padding?: number
	o?: Point
	r?: number

	// svg,
	sticks?: any
	// marker = {},
	grid?: any
	props?: any
}
