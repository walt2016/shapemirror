export interface IVertexOptions {
	r?: number
	one?: boolean

	draggable?: boolean
	color?: string
}
