// export enum Motion {}
export enum MotionType {
	Rotate = 'rotate',
	Dashoffset = 'dashoffset',
}

export interface IMotion {
	type: MotionType
	speed: number
}
