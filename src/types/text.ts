import { Point } from './point'

export enum TextAnchor {
	Start = 'start',
	Middle = 'middle',
	End = 'end',
}

// export type Text = string | number

export interface ITextOptions {
	x?: number
	y?: number
	text?: string | number
	type?: string
	shape?: string
}

export interface ILabelOptions {
	points: Point[]
	render?: (t: string | number) => string
	label?: string
}
