// export type StrokeMode = 'normal' | 'innerStroke' | 'outerStroke'

export enum StrokeMode {
	InnerStroke = 'innerStroke',
	OuterStroke = 'outerStroke',
	Normal = 'normal',
}
/**
 * 描边属性
 */
export interface IStrokeProps {
	stroke?: string
	strokeWidth?: number | string
	strokeDasharray?: number
	strokeDashoffset?: number | string
	strokeLinecap?: string
	strokeLinejoin?: string
	strokeMiterlimit?: number | string
	'stroke-dasharray'?: number | string
	strokeMode?: StrokeMode
}
