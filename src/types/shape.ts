import { IMotion } from './motion'
import { Point } from './point'

export enum Shape {
	None = 'none',
	Polygon = 'polygon',
	Axis = 'axis',
	Path = 'path',
	Circle = 'circle',
	Text = 'text',
	Rect = 'rect',
	Star = 'star',
	Grid = 'grid',
	Polar = 'polar',
	Radial = 'radial',
	Dot = 'dot',
	Sin = 'sin',
	Wave = 'wave',
	Fibonacci = 'fibonacci',
	FibonacciCircle = 'fibonacciCircle',

	Heart = 'heart',
	Koch = 'koch',
	LSystem = 'lsystem',
	LSystemPlant = 'lsystemPlant',
	Maze = 'maze',
	Oneline = 'oneline',
	Peano = 'peano',
	Triangle = 'triangle',
	Line = 'line',
	Ray = 'ray',
	Curve = 'curve',
	Rose = 'rose',
	Sawtooth = 'sawtooth',
	Sierpinski = 'sierpinski',
	SierpinskiBezier = 'sierpinskiBezier',
	SierpinskiTriangle = 'sierpinskiTriangle',
	CircleMaze = 'circleMaze',
	WaveMaze = 'waveMaze',
	Spiral = 'spiral',
	Stripe = 'stripe',
	Chequer = 'chequer',
	Isometric = 'isometric',
	PolarShape = 'polarShape',
	SpiralShape = 'spiralShape',
	RingShape = 'ringShape',
	RayShape = 'rayShape',
	PolygonShape = 'polygonShape',
	GridShape = 'gridShape',
	IsometricShape = 'isometricShape',
	Gougu = 'gougu',
	Plant = 'plant',
	Tree = 'tree',
	TreeMode = 'treeMode',
	TreeCircle = 'treeCircle',
	Mitsubishi = 'mitsubishi',
	Hexaflake = 'hexaflake',
	GridRect = 'gridRect',
	GridPolygon = 'gridPolygon',
	Wanhuachi = 'wanhuachi',
	Leaves = 'leaves',
	TriangleLeaf = 'triangleLeaf',
	Dragon = 'dragon',
	SquareFractal = 'squareFractal',
	Leaf = 'leaf',
	Semicircle = 'semicircle',
	Elliptical = 'elliptical',
	Gradient = 'gradient',
	DiagonalStripe = 'diagonalStripe',
	Square = 'square',
	Pentagon = 'pentagon',
	Hexagon = 'hexagon',
	Heptagon = 'heptagon',
}

export interface IBaseShape {
	o?: Point
	r?: number //| ((n: number) => number);
	a?: number
	n?: number
	m?: number
	shape?: string
}

export interface IShape extends IBaseShape {
	motion?: IMotion
	height?: number
	width?: number
	[x: string]: string | number | any
}

export interface IPolarShape extends IBaseShape {
	sweepFlag: boolean
	transform: string
	pathMode: string
}
