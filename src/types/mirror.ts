import { IFillBaseProps } from './fill'

export enum Mirror {
	None = 'none',
	Edge = 'edge',
	Vertex = 'vertex',
}

export interface IMotionProps {
	type: string
	speed: number
	origin: string
}

export interface IMirrorProps extends IFillBaseProps {
	type?: string
	scale?: number
	refraction?: number //| string;
	departure?: number
	borderIndex?: number
	oneline?: boolean
	onelineOffset?: number
	color?: string
	alpha?: number
	ratio?: number
}
