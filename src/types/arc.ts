import { TSweepFlag } from '.'
import { Point } from './point'

// 圆弧
export interface IArcOptions {
	sweepFlag?: TSweepFlag
	radiusRatio?: number
	xAxisRotation?: number
	largeArcFlag?: boolean
	r?: number
	to?: Point
}
