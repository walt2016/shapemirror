import { ContentType, IContent } from "./ui";

// 菜单项
export interface IMenuItem {
    title?: string;
    name?: string;
    url?: string;
    content?: IContent;
    textContent?: string;
    contentType?: ContentType;
    render?: IRenderMenuItem
}

// 树形菜单
export interface IMenu extends IMenuItem {
    children?: IMenu[];
    render?: IRenderMenu
}

export interface IRenderMenuItem {
    (item: IMenuItem, level?: number): HTMLElement |string
}

export interface IRenderMenu {
    (item: IMenu, level?: number): HTMLElement |string
}


export interface IMenuConfig {
    target?: string | HTMLElement;
    root?: HTMLElement;
    title?: string;
    name?: string;
    children?: IMenuItem[]
}


export interface ITabsConfig {
    tabs: IMenu[];
    activeName: string
}
