import { Layout } from './layout'
import { IAxisOptions } from './axis'
import { Point } from './point'
import { IShape } from './shape'

export interface ICanvasOptions {
	width?: string | number
	height?: string | number
	shapes?: IShape[]
	axis?: IAxisOptions
	o?: Point
	layout?: Layout
	[x: string]: any
}
