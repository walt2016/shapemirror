import { Point } from './point'

export interface IGridOptions {
	width?: number
	height?: number
	o?: Point
	padding?: number
	r?: number
	start?: Point
}
