import { Point } from './point'

export interface ISize {
	width: number
	height: number
}

export interface IScale {
	scaleX: number
	scaleY: number
}

export interface IAngle {
	a: number
}

export interface IBounds {
	bottom: number
	left: number
	top: number
	right: number
	width: number
	height: number
}
export interface IScreen {
	width: number
	height: number
	o: Point
}
