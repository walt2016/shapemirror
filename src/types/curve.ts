import { Point } from './point'

export interface ICurveKey {
	shape: string
	type: string
}
export type CurveKey = string | ICurveKey

export enum Curve {
	None = 'none',
	LeftAngle = 'leftAngle',
	RightAngle = 'rightAngle',
	Centripetal = 'centripetal',
	FibonacciCurve = 'fibonacciCurve',
	LeftSemicircle = 'leftSemicircle',
	KochCurve = 'kochCurve',
	DoubleBezier = 'doubleBezier',
	DoubleCentripetal = 'doubleCentripetal',
	Rough = 'rough',
}

export interface ICurve {
	type: string
	amplitude?: number
	skew?: number
	depth?: number
	increasedAgnle?: number
	controlPointsVisible?: boolean
}

export interface ICurveOptions {
	ctx?: CanvasRenderingContext2D
	points: Point[]
	o?: Point
	curve?: string | ICurve
	loop?: boolean
	step?: number
	discrete?: boolean
	n?: number
	skew?: number
	amplitude?: number
	increasedAgnle?: number
	depth?: number
	controlPointsVisible?: boolean
	closed?: boolean
	pathMode?: string
}
