export enum Transform {
	None = 'none',
	Misplaced = 'misplaced',
	Normal = 'normal',
	Diagonal = 'diagonal',
	ParitySort = 'paritySort',
	IntervalSort = 'intervalSort',
	NeighborSwap = 'neighborSwap',
	Shuffle = 'shuffle',
}
