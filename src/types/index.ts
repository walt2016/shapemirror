import { ICurve } from './curve'
import { Point, PointSegment } from './point'

export type TSweepFlag = 0 | 1 | boolean // number |boolean

type TIter = (options: { ctx?: CanvasRenderingContext2D; points: Point[]; index?: number }) => void
type TInit = (options: { ctx?: CanvasRenderingContext2D; point?: Point }) => void
export interface ITraversalOptions {
	ctx?: CanvasRenderingContext2D
	points: Point[]
	n?: number
	loop?: boolean
	iter?: TIter
	init?: TInit
	discrete?: boolean
}
export interface ITraversalPolarOptions {
	o: Point
	r: number | ((n: number) => number)
	a?: number
	n?: number
	m?: number
	_x?: (n: number) => number
	_y?: (n: number) => number
	sweepFlag?: TSweepFlag
}

export interface IPolylineOptions {
	points: Point[]
	loop?: boolean
	step?: number
}

export interface IBoundsPoints {
	bottom: Point
	left: Point
	top: Point
	right: Point
	width: number
	height: number
}

export interface ICentreOptions {
	r: number
	draggable?: boolean
}

export enum ESvgProps {
	fill = 'fill',
	alpha = 'alpha',
	fillRule = 'fillRule',
	stroke = 'stroke',
	strokeDasharray = 'strokeDasharray',
	strokeWidth = 'strokeWidth',
	strokeDashoffset = 'strokeDashoffset',
	strokeLinecap = 'strokeLinecap',
	strokeLinejoin = 'strokeLinejoin',
	strokeMiterlimit = 'strokeMiterlimit',
	fontSize = 'fontSize',
	textAnchor = 'textAnchor',
	dominantBaseline = 'dominantBaseline',
	x = 'x',
	y = 'y',
	textContent = 'textContent',
	patternUnits = 'patternUnits',
	d = 'd',
	'xlink:href' = 'xlink:href',
	id = 'id',
	cx = 'cx',
	cy = 'cy',
	x1 = 'x1',
	y1 = 'y1',
	x2 = 'x2',
	y2 = 'y2',
	points = 'points',
	width = 'width',
	height = 'height',
}

export type TDominatBaseline = 'auto' | 'use-script' | 'no-change' | 'reset-size' | 'ideographic' | 'alphabetic' | 'hanging' | 'mathematical' | 'central' | 'middle' | 'text-after-edge' | 'text-before-edge' | 'inherit'

export type TPatternUnits = 'userSpaceOnUse' | 'objectBoundingBox'

export interface IStripeOptions {
	name: string
	id: string
	size: number
	color1?: string
	offset?: number
	r?: number
	color2?: string
	radio?: number
	skewX?: number
}

export type TSelector = string | string[]

export interface IPloygonOptions {
	points: Point[]
	n?: number
	curve?: string | ICurve
}

export interface IDragCallback {
	(arg0: any): string | void
}

export interface ILoopIterator {
	(seg0: PointSegment, seg1?: PointSegment, n?: number): string
}

export interface IDragOptions {
	color?: string
	name?: string
	curve?: string | ICurve
	pathMode?: string
}

export interface IControlOptions {
	radiusRatio: number
	angleOffset: number
	orient: boolean //向心
}

export interface IDataItem {
	name: string
	city: string
	random: number
	number: number
	checkbox: string
	index?: number
}
