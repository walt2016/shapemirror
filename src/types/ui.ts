import { IShape } from '@/types/shape'
import { IFeild, IOption } from './field'
import { Point } from './point'

export interface INumberOptions {
	min?: number
	max?: number
	range?: number[]
	width?: number | string
	class?: string
}

export interface IElementProps {
	checked?: boolean
	name?: string
	options?: IOption[]
}

export interface IFormOptions {
	name?: string
	title?: string
	fields?: IFeild[]
	op?: any[]
	[key: string]: any
}
export interface IFormItemOptions {
	labelWidth?: string
	inputWidth?: string
	name?: string
	width?: string
}

export enum ContentType {
	SVG = 'svg',
	Canvas = 'Canvas',
	Webgl = 'webgl',
	Lottie = 'lottie',
	Editor = 'editor',
	Html = 'html',
}

export interface IContent {
	shapes?: IShape[]
	name?: string
	data?: any[]

	fields?: IFeild[]
	statis?: IStatis
	props?: any
	op?: any
	width?: string | number
	height?: string | number
	pattern?: any
	axis?: any
	layout?: any
	o?: Point
	[x: string]: any
}

export type TContent = string | IContent

export type TNavTreeConfig = {
	target: HTMLElement | string
	root: HTMLElement
	menu?: any[]
	activeName?: string
}

export interface IStatis {
	fields: string[]
	label: string
}

export interface IStyleOptions {
	[x: string]: string | number
}

export interface IElementOptions {
	isHtml?: any
	[x: string]: any
}

export interface IEventsHandler {
	(e?: any, options?: any): void
}

export interface IEventsOptions {
	click?: IEventsHandler
	change?: IEventsHandler
	mousedown?: IEventsHandler
	mouseup?: IEventsHandler
	mousemove?: IEventsHandler
	[x: string]: IEventsHandler //EventListenerOrEventListenerObject;
}

export type TEvents = IEventsOptions //|IEventsHandler

export interface IRender {
	(arg0?: any, arg1?: any): any
}

export type TElement = HTMLElement | SVGElement

export type TChildren = any[] | number | string | Text | HTMLElement | HTMLElement[] | SVGElement | SVGElement[]

export type TDomHandler = (str: TChildren, options?: IElementOptions, events?: TEvents) => HTMLElement
export type TInputHandler = (str: TChildren, options?: IElementOptions, events?: TEvents) => HTMLInputElement

export interface IDom {
	input?: TInputHandler
	[key: string]: TDomHandler
}

export interface IOperation {
	name?: string
	text?: string
	className?: string
	popover?: IElementOptions
	click?: (e: MouseEvent, scope: any) => void
}

export interface IModel {
	axis?: string
	layout?: string
	[x: string]: string | number | boolean
}

export interface IScope {
	model: IModel
	fields: IFeild[]
	form: HTMLFormElement
	emit?: (el: HTMLElement, key: string, detail: any) => void
}
