import { FillRule } from './fill'

export enum Color {
	None = 'none',
	ColorCircle = 'colorCircle',
}

export interface IColor {
	r: number
	g: number
	b: number
	a: number
}

export interface IColorProps {
	type: string
	fill: string | boolean
	alpha?: number
	fillRule?: FillRule
}
