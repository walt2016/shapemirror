import { Point } from './point'

export interface ILayoutProps {
	type: string
	angle: string
}

export type Layout = string | ILayoutProps
export interface IlayoutOptions {
	layout?: Layout
	width: number
	height: number
	o: Point
}
