import { TDominatBaseline, TPatternUnits, TSweepFlag } from '.'
import { IAxisOptions } from './axis'
import { ICurve } from './curve'
import { FillRule, IFillBaseProps } from './fill'
import { Point } from './point'
import { IBaseShape } from './shape'
import { IStrokeProps } from './stroke'
import { TextAnchor } from './text'
import { IVertexOptions } from './vertex'

export interface IPathOptions extends IBaseShape {
	points?: Point[]
	closed?: boolean
	pathMode?: any
	mirror?: any
	color?: any
	k?: number
	w?: number
	v?: number
	x?: number
	y?: number
	increasedAgnle?: number
	sweepFlag?: TSweepFlag
	a2?: number
	direction?: number
	to?: Point
	num?: number
	transform?: string
	depth?: number
	attenuation?: number
	wriggle?: number
	treeCurve?: string
	sweep?: boolean
	startIndex?: number
	centre?: any
	vertex?: IVertexOptions
	labels?: any
	radius?: any
	excircle?: ICircleOptions
	incircle?: ICircleOptions
	links?: any
	edge?: any
	edgeExtension?: any
	mark?: any
	fill?: any
	mirrorPoints?: Point[]
	curve?: string | ICurve
	name?: string
	linkCross?: any
	motion?: any
	shape?: string
	pattern?: string
	orient?: boolean
	props?: IPathProps
	text?: string | number
	width?: number
	height?: number
	fractal?: any
	axis?: IAxisOptions
	pathPoints?: {
		orient?: boolean
		m?: number
		props?: IPathProps
	}
	crossPoints?: boolean
	broken?: boolean
	curveShape?: string
	alpha?: number
	id?: string

	color1?: string
	color2?: string
	borderRadius1?: number
	borderRadius2?: number
	origin?: string
	speed?: number
	type?: string
	begin?: string

	dur?: string
	from?: string
	path?: string
	repeatCount?: string
	noseToTail?: boolean
	oneLine?: boolean
}

export interface IPathProps extends IStrokeProps, IFillBaseProps {
	points?: string
	fontSize?: number | string
	opacity?: number | string
	type?: string
	name?: string

	transform?: string
	textAnchor?: TextAnchor
	dominantBaseline?: TDominatBaseline
	x?: number | string
	y?: number | string
	textContent?: string
	d?: string

	id?: string
	cx?: number | string
	cy?: number | string
	x1?: number | string
	y1?: number | string
	x2?: number | string
	y2?: number | string

	width?: number | string
	height?: number | string
	patternUnits?: TPatternUnits
	offset?: string
	'xlink:href'?: string
	'stop-color'?: string
	'font-size'?: number | string
	'text-anchor'?: TextAnchor
	'dominant-baseline'?: TDominatBaseline
	rx?: number
	ry?: number
	r?: number

	one?: boolean
	crossPoints?: boolean
	attributeType?: string
	attributeName?: string
	begin?: string
	dur?: string
	from?: string
	to?: string
	path?: string
	repeatCount?: string

	viewBox?: string
	markerUnits?: string
	markerWidth?: number
	markerHeight?: number
	refX?: number
	refY?: number
	orient?: string

	xmlns?: string
	'xmlns:xlink'?: string
	fillRule?: FillRule
	// [key: string]: string | number | boolean | Point[]
}

export interface IEdgeProps extends IStrokeProps, IFillBaseProps {
	visible: boolean
	opacity: number
}

export interface IEdgeExtensionProps extends IStrokeProps, IFillBaseProps {
	visible: boolean
	r: number
	opacity: number
	iterNum: number
	crossPoints: boolean
	labels: boolean
}

export interface IEdgeExtension {
	crossPoints: boolean
	labels: boolean
	iterNum: number
}

export interface ICentreProps extends IStrokeProps, IFillBaseProps {
	visible: boolean
	r: number
	opacity: number
	draggable: boolean
}
export interface IRadiusProps extends IStrokeProps {
	visible: boolean
	one?: boolean
}

export interface ILinksProps extends IStrokeProps, IFillBaseProps {
	visible: boolean
	crossPoints: boolean
	opacity?: number
}

export interface ICircleOptions {
	o: Point
	r?: number
	name?: string
	id?: string
	centre?: boolean
}

export interface IPathStringOptions {
	closed: boolean
	broken: number | boolean
}
