import { createPdf, createPdfByDataUri, createPdfBySvg, IImageForPdf } from '@/pdf';
import { ISize } from '@/types/base';
import { saveAs } from 'file-saver';
import JSZip from 'jszip';

interface ISaveSvgOptions {
    svg: string;
    name?: string;
    size?: ISize
}

interface ISaveCanvasOptions {
    canvas: HTMLCanvasElement;
    name?: string;
    size?: ISize
}

export enum GraphicDataType {
    svg = 'svg',
    canvas = 'canvas'
}

export interface ISvgData {
    data: string;
    type: GraphicDataType.svg;
    size: ISize,
    name?: string;
}

export interface ICanvasData {
    data: HTMLCanvasElement;
    type: GraphicDataType.canvas;
    size: ISize;
    name?: string;
}

interface ISaveAsOptions {
    data: HTMLCanvasElement | string;
    name?: string;
    size?: ISize,
    type?: GraphicDataType
}

export const saveCanvasOrSvgAsPng = (options: ISaveAsOptions) => {
    const { data, type, size, name } = options
    if (type === GraphicDataType.svg) {
        saveSvgAsPng({
            svg: data as string,
            size,
            name
        })
    } else if (type === GraphicDataType.canvas) {
        saveCanvasAsPng({ canvas: data as HTMLCanvasElement, name })
    }
}

export const saveAsPdf = (options: (ISvgData | ICanvasData)[]): void => {
    let list: ISaveAsOptions[]
    if (!Array.isArray(options)) {
        list = [options]
    } else {
        list = options
    }
    let filename
    const imagelist: IImageForPdf[] = list.map(t => {
        const { data, name, size, type } = t
        filename = name
        if (type === GraphicDataType.svg) {
            return {
                data: data as string,
                size,
                type: 'svg'
            }
        }
        return {
            data: (data as HTMLCanvasElement).toDataURL(),
            size,
            type: 'uri'
        }

    })
    const pdf = createPdf(imagelist)
    pdf.getBlob((b) => {
        saveAs(b, `${filename}.pdf`)
    })
}

export const saveCanvasOrSvgAsPdf = (options: ISaveAsOptions): void => {
    const { data, type, size, name } = options
    if (type === GraphicDataType.svg) {
        saveSvgAsPdf({
            svg: data as string,
            size,
            name
        })
    } else if (type === GraphicDataType.canvas) {
        saveCanvasAsPdf({ canvas: data as HTMLCanvasElement, name })
    }
}

export const saveSvg = ({ svg, name }: ISaveSvgOptions) => {
    saveAs(toDataUri(svg), `${name}.svg`)
}

const makeSvg = (canvas: HTMLCanvasElement) => {
    const uri = canvas.toDataURL('image/png');
    const svg = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><image xlink:href="${uri}" /></svg>`
    return svg
}

export const saveCanvaAsSvg = ({ canvas, name }: ISaveCanvasOptions) => {
    const svg = makeSvg(canvas)
    saveAs(toDataUri(svg), `${name}.svg`)
}


export const saveSvgFiles = (options: (ISvgData | ICanvasData)[]): void => {
    const len = options.length
    if (len > 1) {
        const zip = new JSZip();
        options.forEach(t => {
            const { data, type, name } = t
            if (type === GraphicDataType.svg) {
                zip.file(`${name}.svg`, data)
            } else if (type === GraphicDataType.canvas) {
                zip.file(`${name}.svg`, makeSvg(data))

            }
        })

        zip.generateAsync({ type: "blob" }).then(b => {
            saveAs(b, 'shapemirror.zip')
        })
    } else if (len === 1) {
        const { data, type, name } = options[0]

        if (type === GraphicDataType.svg) {
            saveSvg({
                svg: data,
                name: name
            })
        } else if (type === GraphicDataType.canvas) {
            saveCanvaAsSvg({
                canvas: data,
                name
            })
        }

    }

}


const getBlob = (canvas: HTMLCanvasElement): Promise<Blob> => {
    return new Promise((resolve) => {
        canvas.toBlob(b => {
            resolve(b)
        })
    })

}

export const savePngFiles = (options: (ISvgData | ICanvasData)[]): void => {

    const len = options.length
    if (len > 1) {
        const zip = new JSZip();
        options.forEach(async t => {
            const { data, type, name, size } = t
            if (type === GraphicDataType.svg) {
             

                const canvas = await draw({ svg: data, size })
                // const blob = await getBlob(canvas)

                const uri = canvas.toDataURL() //'image/png'
                //  `data:${formatInfo.mime};base64,${btoa(str)}` : 

                // const imgSrc = `data:image/png;base64,${btoa(uri)}`  //btoa(uri);
                // 
                // const img =uri.slice(22)
                // console.log(uri)
                zip.file(`${name}.png`, uri.split(',')[1], {base64: true})


            } else if (type === GraphicDataType.canvas) {
                // zip.file(`${name}.svg`, makeSvg(data))

                // const blob = await getBlob(data)
                // zip.file(`${name}.png`, blob)

                const uri = data.toDataURL()
                zip.file(`${name}.png`, uri.split(',')[1], {base64: true})


                // zip.file(`${name}.png`, data.toDataURL().split(',')[1], {base64: true})

                // data.toBlob(b => {
                //     zip.file(`${name}.png`, b)
                // })

            }
        })
        console.log(zip)

        setTimeout(()=>{
            zip.generateAsync({ type: "blob" }).then(b => {
                saveAs(b, 'shapemirror.zip')
            })
        },100)

    } else if (len === 1) {
        const { data, type, name, size } = options[0]

        if (type === GraphicDataType.svg) {
            saveSvgAsPng({
                svg: data,
                name: name,
                size
            })
        } else if (type === GraphicDataType.canvas) {
            saveCanvasAsPng({
                canvas: data,
                name
            })

        }

    }
}

export const saveSvgAsPdf = ({ svg, name, size }: ISaveSvgOptions) => {
    const pdf = createPdfBySvg([{
        svg: svg,
        size: size ?? {
            width: 100,
            height: 100
        }
    }])
    pdf.getBlob((b) => {
        saveAs(b, `${name}.pdf`)
    })
}


export const saveCanvasAsPng = ({ canvas, name }: ISaveCanvasOptions) => {
    canvas.toBlob(b => {
        saveAs(b, `${name}.png`)
    })
}

export const saveCanvasAsPdf = ({ canvas, name }: ISaveCanvasOptions) => {
    const uri = canvas.toDataURL('image/png')
    const pdf = createPdfByDataUri(uri)
    pdf.getBlob((b) => {
        saveAs(b, `${name}.pdf`)
    })
}


function toDataUri(svg: string): string {
    return 'data:image/svg+xml;charset=utf-8,' + svg;
}


export const draw = ({ svg, size }: ISaveSvgOptions): Promise<HTMLCanvasElement> => {
    const uri = toDataUri(svg)
    return makeImage(uri).then((image) => {
        const canvas: HTMLCanvasElement = newCanvas(size);
        const ctx = canvas.getContext('2d');
        if (ctx) {
            ctx.drawImage(image as CanvasImageSource, 0, 0);
        }
        return canvas;
    })
}

export const saveSvgAsPng = ({ svg, name, size }: ISaveSvgOptions): void => {
    draw({ svg, size }).then(canvas =>
        canvas.toBlob(b => {
            saveAs(b, `${name}.png`)
        })
    )
}


function makeImage(uri: string): Promise<HTMLImageElement> {
    return new Promise((resolve, reject) => {
        const image: HTMLImageElement = new Image();
        image.onload = function () {
            resolve(image);
        };
        image.onerror = reject;
        image.src = uri;
    });
}


function newCanvas({ width, height }: ISize): HTMLCanvasElement {
    const canvas: HTMLCanvasElement = document.createElement('canvas');
    canvas.width = width
    canvas.height = height
    return canvas;
}