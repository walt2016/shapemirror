// 平方
export function square(x) {
	return x * x
}
// 立方
export function cube(x) {
	return x * x * x
}
