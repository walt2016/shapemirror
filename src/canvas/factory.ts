import { lsystem } from './pental/lsystem'
import { _plant } from './pental/plant'
import { _polygon, _rect } from './core/polygon'
import { _circle } from './core/circle'
import { _star } from './core/star'
import { _gougu } from './pental/gougu'
import { polarShape, spiralShape, ringShape, rayShape } from './core/polarShape'
import { IPolarOptions } from '@/types/polar'
import { Shape } from '@/types/shape'
// import { _rougth } from './rougth.js'
export const canvasConfig = {
	[Shape.Circle]: _circle,
	[Shape.Rect]: _rect,
	[Shape.Polygon]: _polygon,
	[Shape.Plant]: _plant,
	[Shape.LSystem]: lsystem,
	[Shape.LSystemPlant]: lsystem,
	[Shape.PolarShape]: polarShape,
	[Shape.SpiralShape]: spiralShape,
	[Shape.RingShape]: ringShape,
	[Shape.RayShape]: rayShape,
	[Shape.Star]: _star,
	[Shape.Gougu]: _gougu,
	// rougth: _rougth
}

export const factory = (shape: string, ctx: CanvasRenderingContext2D, options: IPolarOptions, props) => {
	return canvasConfig[shape] && canvasConfig[shape](ctx, options, props)
}
