import { _axis } from './core/axis'
import { _screen } from '@/store'
import { translateY } from '@/math'
import { factory } from './factory'
import { isVisible } from '@/common'
import { _layout } from '@/common/layout'
import { MotionType } from '@/types/motion'
import { ICanvasOptions } from '@/types/canvas'

let timer: number
export const _canvas = (options: ICanvasOptions, props = {}) => {
	let { width = 800, height = 600, shapes, axis, o, layout } = options

	const screen = _screen(width, height)
	const h = screen.height
	const w = screen.width
	if (!o) {
		o = screen.o
	}

	const canvas = document.createElement('canvas')
	const ctx = canvas.getContext('2d')
	canvas.width = w
	canvas.height = h

	timer && cancelAnimationFrame(timer)

	const _main = () => {
		const fn = t => {
			const { shape } = t
			if (shape === 'lsystemPlant' && (layout == null || layout === '0')) {
				// 植物lsystem的原点下移
				o = translateY(o, h / 2 - 100)
			}

			factory(
				shape,
				ctx,
				{
					o,
					...t,
				},
				{
					stroke: 'blue',
					fill: 'none',
					...t.props,
					...props,
				}
			)
		}

		if (isVisible(axis)) {
			_axis(ctx, axis)
		}

		_layout({ layout, width: w, height: h, o }, shapes, fn)

		// 旋转动画
		const rotateShapes = shapes.filter(t => t.motion && t.motion.type === MotionType.Rotate)
		if (rotateShapes.length) {
			timer = requestAnimationFrame(() => {
				ctx.fillStyle = 'rgba(0,0,0,0.02)'
				ctx.fillRect(0, 0, w, h)
				rotateShapes.forEach(t => {
					const speed = t.motion.speed || 1
					t.a += speed
				})
				_main()
			})
		}
	}

	_main()

	return canvas
}
