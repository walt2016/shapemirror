import { _path } from '../core/path'
import { gouguPoints } from '@/algorithm/gougu'
import { _props } from '../core/canvas'
import { _transform } from '@/math/transform'
import { IPathProps } from '@/types/path'
import { renderMatrix } from '../helper'
import { IFractalOptions } from '@/types/fractal'
export const _gougu = (ctx: CanvasRenderingContext2D, options: IFractalOptions, props: IPathProps) => {
	const matrix = gouguPoints(options)
	renderMatrix({
		ctx,
		matrix,
		options,
		props,
	})
}
