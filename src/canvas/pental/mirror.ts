import { _mid } from '@/math/index'
import { _polygon } from '../core/polygon'
import { _colors } from '@/color/index'
import { _mirrorPath, _colorProps, _merge } from '@/common'
import { _path } from '../core/path'

export const _mirrorPolygon = (ctx, options, props) => {
	return _mirrorPath(
		(options, props) => {
			_polygon(ctx, options, props)
		},
		options,
		props
	)
}

export const _mirror = (ctx, options, props) => {
	return _mirrorPath(
		(options, props) => {
			_path(ctx, options, props)
		},
		options,
		props
	)
}
