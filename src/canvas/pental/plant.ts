import { treePoints } from '@/algorithm/tree'
import { _path } from '../core/path'
import { _props } from '../core/canvas'
import { ITreeOptions } from '@/types/tree'
import { renderMatrix } from '../helper'

export const _plant = (ctx: CanvasRenderingContext2D, options: ITreeOptions, props: {}): void => {
	const matrix = treePoints(options)

	renderMatrix({
		matrix,
		ctx,
		options,
		props,
	})
}
