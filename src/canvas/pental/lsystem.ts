import { lSystemPoints } from '@/algorithm/lsystem'
import { _path } from '../core/path'
import { _props } from '../core/canvas'
import { IPolarOptions } from '@/types/polar'
import { renderMatrix } from '../helper'
import { IPathProps } from '@/types/path'

export const lsystem = (ctx: CanvasRenderingContext2D, options: IPolarOptions, props: IPathProps) => {
	let matrix = lSystemPoints(options)
	renderMatrix({
		ctx,
		matrix,
		options,
		props,
	})
}
