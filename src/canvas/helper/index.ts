import { isVisible, _colorProps, _mirrorColor } from '@/common'
import { PathMode } from '@/types/pathMode'
import { _transform } from '@/math/transform'
import { IPathOptions, IPathProps } from '@/types/path'
import { _path } from '../core/path'
import { _props } from '../core/canvas'
import { _mirrorPoints } from '@/algorithm/mirror'
import { Point } from '@/types/point'

interface IRenderMatrixOptions {
	ctx: CanvasRenderingContext2D
	matrix: Point[][]
	options: IPathOptions
	props: IPathProps
}
export const renderMatrix = (renderMatrixoptions: IRenderMatrixOptions): void => {
	const { ctx, matrix, options, props } = renderMatrixoptions
	const { edge, mirror, transform, pathMode = PathMode.LINE_LOOP } = options
	const n = matrix.length
	const color = isVisible(mirror) && mirror && mirror.color ? _mirrorColor(mirror) : options.color
	const makeProps = _colorProps(color, n)

	ctx.save()
	matrix.forEach((t, i) => {
		const ps = transform ? _transform(t, transform) : t
		const newEdge = {
			...props,
			...edge,
			...makeProps(i),
		}
		if (isVisible(mirror)) {
			const mps = _mirrorPoints(ps, mirror)

			renderMatrix({
				ctx,
				matrix: mps,
				options: {
					...options,
					pathMode,
					// points: mps,
					mirror: null,
					edge: newEdge,
				},
				props,
			})
			// _path(ctx, {
			//     ...options,
			//     pathMode,
			//     points: mps,
			//     mirror: null,
			//     edge: newEdge
			// }, props)
		} else {
			_path(
				ctx,
				{
					...options,
					pathMode,
					points: ps,
					edge: newEdge,
				},
				props
			)
		}
	})
	ctx.restore()
}
