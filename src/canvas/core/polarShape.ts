import { spiralPoints, ringPoints, rayPoints } from '@/algorithm/polarMatrix'
import { _path } from './path'
import { _props } from './canvas'
import { plainMatrix } from '@/math'
import { _transform } from '@/math/transform'
import { _polygon } from './polygon'
import { _borderPoints, _mirrorColor } from '@/common'
import { IPolarOptions } from '@/types/polar'
import { renderMatrix } from '../helper'
import { polarPoints } from '@/algorithm/polar'
import { toMatrix } from '@/math/array'
import { IPathProps } from '@/types/path'

/**
 * 多边形
 * @param ctx
 * @param options
 * @param props
 */
export const polarShape = (ctx: CanvasRenderingContext2D, options: IPolarOptions, props) => {
	// _polygon(ctx, options, props)
	const matrix = polarPoints(options)
	renderMatrix({
		matrix,
		ctx,
		options,
		props,
	})
}

/**
 * 螺旋
 * @param ctx
 * @param options
 * @param props
 */
export const spiralShape = (ctx: CanvasRenderingContext2D, options: IPolarOptions, props: IPathProps) => {
	let { transform, n } = options
	let matrix = spiralPoints(options).map(t => _transform(t, transform))
	let points = plainMatrix(matrix)
	matrix = toMatrix(points, n, true)

	renderMatrix({
		ctx,
		matrix,
		options,
		props,
	})
}

/**
 * 环形
 * @param ctx
 * @param options
 * @param props
 */
export const ringShape = (ctx: CanvasRenderingContext2D, options: IPolarOptions, props) => {
	const matrix = ringPoints(options)
	renderMatrix({
		matrix,
		ctx,
		options,
		props,
	})
}

/**
 * 射线
 * @param ctx
 * @param options
 * @param props
 */
export const rayShape = (ctx: CanvasRenderingContext2D, options: IPolarOptions, props) => {
	const matrix = rayPoints(options)
	renderMatrix({
		matrix,
		ctx,
		options,
		props,
	})
}
