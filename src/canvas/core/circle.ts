import { ICircleOptions, IPathProps } from '@/types/path'
import { _props } from './canvas'

export const _circle = (ctx: CanvasRenderingContext2D, options: ICircleOptions, props: IPathProps) => {
	const { o, r, centre } = options
	ctx.save()
	ctx.lineWidth = 1
	_props(ctx, props)
	ctx.beginPath()
	ctx.arc(o[0], o[1], r, 0, Math.PI * 2, false)
	ctx.stroke()
	ctx.fill()
	ctx.restore()
	if (centre) {
		_circle(
			ctx,
			{
				o,
				r: 3,
			},
			{
				fill: 'red',
				stroke: 'none',
			}
		)
	}
}
