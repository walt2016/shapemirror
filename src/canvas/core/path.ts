import { _o } from '@/math'
import { _colors } from '@/color'
import { _props, _centre, _texts, _vertex, _radius, _incircle, _excircle, _edge, _links, _edgeExtension } from './canvas'

import { isVisible, _borderPoints, _mergeProps, _colorProps, _merge, _mirrorColor } from '@/common'
import { _mirror } from '../pental/mirror'
import { _mark } from './mark'
import { _fill } from './fill'
import { IPathOptions, IPathProps } from '@/types/path'

export const _path = (ctx: CanvasRenderingContext2D, options: IPathOptions, props?: IPathProps) => {
	ctx.save()
	ctx.beginPath()
	props && _props(ctx, props)
	const { o = [400, 300], r = 100, points, mirror, centre, vertex, labels, radius, excircle, incircle, links, edge, edgeExtension, mark, fill } = options

	if (edge) {
		if (isVisible(edge)) {
			_edge(ctx, options, { ...props, ...edge })
		}
	} else {
		_edge(ctx, options, props)
	}

	// 边延长线
	if (isVisible(edgeExtension)) {
		_edgeExtension(
			ctx,
			{
				points,
				edgeExtension,
			},
			{ ...props, ...edgeExtension }
		)
	}

	// 顶点
	if (isVisible(vertex)) {
		_vertex(ctx, points, { ...props, ...vertex })
	}

	// 中心点
	if (isVisible(centre)) {
		let o = _o(points)
		_centre(ctx, o, { ...props, ...centre })
	}
	// 半径
	if (isVisible(radius)) {
		_radius(ctx, { points }, { ...props, ...radius })
	}
	// 外切圆
	if (isVisible(excircle)) {
		_excircle(ctx, { o, r }, { ...props, ...excircle })
	}
	// 内切圆
	if (isVisible(incircle)) {
		_incircle(ctx, { o, points }, { ...props, ...incircle })
	}

	// 链接线
	if (isVisible(links)) {
		_links(ctx, points, { ...props, ...links })
	}

	// 文字
	if (isVisible(labels)) {
		_texts(ctx, points, { ...props, ...labels })
	}

	// 标注
	if (isVisible(mark)) {
		_mark(ctx, { points, mark }, { ...props, ...mark })
	}

	// 镜像，被mirrorPoints代替
	if (isVisible(mirror)) {
		_mirror(ctx, options, props)
	}

	if (isVisible(fill)) {
		_fill(ctx, { points, fill }, { ...props, ...fill })
	}

	// }
	ctx.restore()
}
