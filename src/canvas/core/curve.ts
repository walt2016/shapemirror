import { rotatePoints, _o, _dis, _angleRad, _mid, _disToSeg, _lerp, plainMatrix } from '@/math'
import { _mirror } from '@/math/mirror'
import { sinPoints } from '@/algorithm/sin'
import { isObject } from '@/utils'
import { controlPoints } from '@/algorithm/controlPoints'
import { _rough } from '../pental/rough'
import { _props } from './canvas'
import { kochPoints } from '@/algorithm/koch'
import { sierpinskiPoints } from '@/algorithm/sierpinski'
import { _traversal } from '@/algorithm/traversal'
import { IPolylineOptions } from '@/types'
import { Point, PointSegment } from '@/types/point'
import { ICurveOptions } from '@/types/curve'

export const _moveTo = (ctx: CanvasRenderingContext2D, p: Point) => {
	ctx.moveTo(p[0], p[1])
}
export const _lineTo = (ctx: CanvasRenderingContext2D, p: Point) => {
	ctx.lineTo(p[0], p[1])
}
const _polylineTo = (ctx: CanvasRenderingContext2D, points: Point[]): void => {
	points.forEach(t => _lineTo(ctx, t))
}

export const _bezierCurveTo = (ctx: CanvasRenderingContext2D, [c1, c2, t]: [Point, Point, Point]) => {
	ctx.bezierCurveTo(c1[0], c1[1], c2[0], c2[1], t[0], t[1])
}
const _quadraticCurveTo = (ctx: CanvasRenderingContext2D, [c, to]: PointSegment) => {
	ctx.quadraticCurveTo(c[0], c[1], to[0], to[1])
}
const _curveTo = (ctx: CanvasRenderingContext2D, [from, to]: PointSegment, [c1, c2]: PointSegment) => {
	_bezierCurveTo(ctx, [c2, c1, to])
}

const _leftCurveTo = (ctx: CanvasRenderingContext2D, [from, to]: PointSegment, [c1, c2]: PointSegment) => {
	_bezierCurveTo(ctx, [c1, c2, to])
}

const _doubleCurveTo = (ctx: CanvasRenderingContext2D, [from, to]: PointSegment, [c1, c2]: PointSegment) => {
	_moveTo(ctx, from)
	_bezierCurveTo(ctx, [c1, c2, to])
	_moveTo(ctx, from)
	_bezierCurveTo(ctx, [c2, c1, to])
}

const _sawtoothTo = (ctx: CanvasRenderingContext2D, [from, to]: PointSegment, [c1, c2]: PointSegment) => {
	_polylineTo(ctx, [c2, c1, to])
}

const _leftSawtoothTo = (ctx: CanvasRenderingContext2D, [from, to]: PointSegment, [c1, c2]: PointSegment): void => {
	_polylineTo(ctx, [c1, c2, to])
}

const _doubleSawtoothTo = (ctx: CanvasRenderingContext2D, [from, to]: PointSegment, [c1, c2]: PointSegment): void => {
	_moveTo(ctx, from)
	_polylineTo(ctx, [c1, c2, to])
	_moveTo(ctx, from)
	_polylineTo(ctx, [c2, c1, to])
}

const _rightAngleTo = (ctx: CanvasRenderingContext2D, [from, to]: PointSegment, [c1, c2]: PointSegment): void => {
	_polylineTo(ctx, [c2, to])
}
const _leftAngleTo = (ctx: CanvasRenderingContext2D, [from, to]: PointSegment, [c1, c2]: PointSegment) => {
	_polylineTo(ctx, [c1, to])
}
const _diamondTo = (ctx: CanvasRenderingContext2D, [from, to]: PointSegment, [c1, c2]: PointSegment) => {
	_moveTo(ctx, from)
	_polylineTo(ctx, [c1, to, c2, from])
}
const _crossTo = (ctx: CanvasRenderingContext2D, [from, to]: PointSegment, [c1, c2]: PointSegment): void => {
	_moveTo(ctx, from)
	_lineTo(ctx, to)
	_moveTo(ctx, c1)
	_lineTo(ctx, c2)
}
const _triangleTo = (ctx: CanvasRenderingContext2D, [from, to]: PointSegment, [c1, c2]: PointSegment): void => {
	_moveTo(ctx, from)
	_polylineTo(ctx, [c2, to, from])
}
const _leftTriangleTo = (ctx: CanvasRenderingContext2D, [from, to]: PointSegment, [c1, c2]: PointSegment): void => {
	_moveTo(ctx, from)
	_polylineTo(ctx, [c1, to, from])
}

const _circleTo = (ctx: CanvasRenderingContext2D, [from, to]: PointSegment): void => {
	const o = _o([from, to])
	const r = _dis(o, to)
	ctx.beginPath()
	ctx.arc(o[0], o[1], r, 0, 2 * Math.PI)
	ctx.stroke()
}

const _semicircleTo = (ctx: CanvasRenderingContext2D, [from, to]: PointSegment): void => {
	const o = _o([from, to])
	const r = _dis(o, to)
	const a = _angleRad(from, to)
	ctx.beginPath()
	ctx.arc(o[0], o[1], r, a, Math.PI + a)
	ctx.stroke()
}

const _leftSemicircleTo = (ctx: CanvasRenderingContext2D, [from, to]: PointSegment): void => {
	const o = _o([from, to])
	const r = _dis(o, to)
	const a = _angleRad(from, to)
	ctx.beginPath()
	ctx.arc(o[0], o[1], r, Math.PI + a, 2 * Math.PI + a)
	ctx.stroke()
}

const _controls = (ctx: CanvasRenderingContext2D, points: Point[]): void => {
	const r = 3
	ctx.save()
	_props(ctx, { stroke: 'red', fill: 'gray' })
	points.forEach(t => {
		ctx.arc(t[0], t[1], r, 0, 2 * Math.PI)
	})
	// ctx.stroke()
	// ctx.fill()
	ctx.restore()
}

// 循环
export const _loop = (ctx: CanvasRenderingContext2D, options: ICurveOptions, fn) => {
	const { points, loop, step = 1, skew, amplitude, depth = 1, controlPointsVisible = false } = options
	const len = points.length
	let prev
	let index = 0
	let callFn = (i, t, next) => {
		if (step > 1) {
			if (i % step === 0) {
				_moveTo(ctx, t)
				prev = t
			} else {
				let [c1, c2] = controlPoints([prev, t], skew, amplitude)
				// if (controlPointsVisible) {
				//     _controls(ctx, [c1, c2])
				// }
				fn(ctx, [prev, t], [c1, c2], index++)
			}
		} else {
			let [c1, c2] = controlPoints([t, next], skew, amplitude)
			// if (controlPointsVisible) {
			//     _controls(ctx, [c1, c2])
			// }
			fn(ctx, [t, next], [c1, c2], index++)
		}
	}

	let total = loop ? len : len - 1
	for (let i = 0; i < total; i++) {
		let t = points[i]
		let next = points[(i + 1) % len]
		if (depth === 1) {
			callFn(i, t, next)
		} else {
			let curr = t
			for (let j = 1; j <= depth; j++) {
				let p = _lerp(t, next, j / depth)
				callFn(i, curr, p)
				curr = p
			}
		}
	}
}

// 折线
export const _polyline = (ctx: CanvasRenderingContext2D, { points = [], loop, step = 1 }: IPolylineOptions) => {
	_moveTo(ctx, points[0])
	if (step === 1) {
		// 性能优化
		const len = points.length
		for (let i = 1; i < len; i++) {
			const t = points[i]
			_lineTo(ctx, t)
		}
		if (loop) {
			ctx.closePath()
		}
	} else {
		if (step > 1) {
			loop = true
		}
		// for lines
		_loop(ctx, { points, step, loop }, (ctx: CanvasRenderingContext2D, ps: Point[]) => {
			_polylineTo(ctx, ps)
		})
	}
}

// 曲线
export const _bezier = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	const { points } = options
	_moveTo(ctx, points[0])
	_loop(ctx, options, _curveTo)
}

export const _leftBezier = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	const { points } = options
	_moveTo(ctx, points[0])
	_loop(ctx, options, _leftCurveTo)
}
// 双曲线
export const _doubleBezier = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	_loop(ctx, options, _doubleCurveTo)
}
const _symmetry = (ctx: CanvasRenderingContext2D, options: ICurveOptions, fn1, fn2) => {
	_loop(ctx, options, (ctx: CanvasRenderingContext2D, [t, next]: PointSegment, [c1, c2]: PointSegment, index: number) => {
		_moveTo(ctx, t)
		if (index % 2 === 0) {
			fn1(ctx, [t, next], [c1, c2])
		} else {
			fn2(ctx, [t, next], [c1, c2])
		}
	})
}
const _symmetryBezier = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	_symmetry(ctx, options, _curveTo, _leftCurveTo)
}

// 锯齿
export const _sawtooth = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	const { points } = options
	_moveTo(ctx, points[0])
	_loop(ctx, options, _sawtoothTo)
}

export const _leftSawtooth = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	const { points } = options
	_moveTo(ctx, points[0])
	_loop(ctx, options, _leftSawtoothTo)
}

// 双锯齿
export const _doubleSawtooth = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	_loop(ctx, options, _doubleSawtoothTo)
}

const _symmetrySawtooth = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	_symmetry(ctx, options, _sawtoothTo, _leftSawtoothTo)
}

// 直角拐线
const _rightAngle = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	const { points } = options
	_moveTo(ctx, points[0])
	_loop(ctx, options, _rightAngleTo)
}

const _leftAngle = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	const { points } = options
	_moveTo(ctx, points[0])
	_loop(ctx, options, _leftAngleTo)
}
const _symmetryAngle = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	_symmetry(ctx, options, _rightAngleTo, _leftAngleTo)
}

const _circle = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	_loop(ctx, options, _circleTo)
}

// 菱形
const _diamond = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	_loop(ctx, options, _diamondTo)
}
// 十字
const _cross = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	_loop(ctx, options, _crossTo)
}
// 三角形
const _triangle = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	_loop(ctx, options, _triangleTo)
}

const _leftTriangle = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	_loop(ctx, options, _leftTriangleTo)
}

// 半圆
const _semicircle = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	_loop(ctx, options, _semicircleTo)
}
const _leftSemicircle = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	_loop(ctx, options, _leftSemicircleTo)
}

// 向心弧线
const _centripetal = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	const { points } = options
	const o = _o(points)
	_moveTo(ctx, points[0])
	_loop(ctx, options, (ctx: CanvasRenderingContext2D, ps: Point[]) => {
		const [t, next] = ps
		_quadraticCurveTo(ctx, [o, next])
	})
}

// 向心弧线
const _leftCentripetal = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	const { points } = options
	const o = _o(points)
	_moveTo(ctx, points[0])
	_loop(ctx, options, (ctx: CanvasRenderingContext2D, ps: Point[]) => {
		const [t, next] = ps
		const c = _mid(t, next)
		const oo = _mirror(o, c)
		return _quadraticCurveTo(ctx, [oo, next])
	})
}
const _doubleCentripetal = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	const { points } = options
	const o = _o(points)
	_loop(ctx, options, (ctx: CanvasRenderingContext2D, ps: Point[]) => {
		const [t, next] = ps
		_moveTo(ctx, t)
		_quadraticCurveTo(ctx, [o, next])
		_moveTo(ctx, t)
		const c = _mid(t, next)
		const oo = _mirror(o, c)
		return _quadraticCurveTo(ctx, [oo, next])
	})
}

// 正玄
const _sin = (ctx: CanvasRenderingContext2D, { points, loop }: ICurveOptions) => {
	const ps = sinPoints(points, {})
	_polyline(ctx, { points: ps, loop })
}

// 互切圆
const _tangentCircle = (ctx: CanvasRenderingContext2D, options: ICurveOptions) => {
	const { points } = options
	const o = _o(points)
	_moveTo(ctx, points[0])
	_loop(ctx, options, (ctx: CanvasRenderingContext2D, ps: Point[]) => {
		const [t, next] = ps
		const c = _mid(t, next)
		const r = _disToSeg(c, [o, next])
		ctx.beginPath()
		ctx.arc(c[0], c[1], r, 0, 2 * Math.PI)
		ctx.stroke()
	})
}

// 杨辉三角
const _sierpinski = (ctx: CanvasRenderingContext2D, { points, depth, amplitude, loop, discrete }: ICurveOptions) => {
	const matrix = sierpinskiPoints({ points, depth, amplitude, loop, discrete })
	_traversal({
		ctx,
		points: plainMatrix(matrix),
		iter: ({ points }) => {
			_polylineTo(ctx, points)
		},
		init: ({ point }) => {
			_moveTo(ctx, point)
		},
		loop,
	})
}
// 杨辉三角贝塞尔
const _sierpinskiBezier = (ctx: CanvasRenderingContext2D, { points, depth, amplitude, loop, discrete }: ICurveOptions) => {
	const matrix = sierpinskiPoints({ points, depth, amplitude, loop, discrete })
	const n = 4

	_traversal({
		ctx,
		points: plainMatrix(matrix),
		n,
		iter: ({ points: [p1, p2, p3, p4] }) => {
			_bezierCurveTo(ctx, [p2, p3, p4])
		},
		init: ({ point }) => {
			_moveTo(ctx, point)
		},
		loop,
	}).join(' ')
}

const _sierpinskiDiscreteBezier = (ctx: CanvasRenderingContext2D, { points, depth, amplitude, loop }: ICurveOptions) => {
	const matrix = sierpinskiPoints({ points, depth, amplitude, loop })
	const n = 4
	const discrete = true

	const ps = plainMatrix(matrix)

	_traversal({
		ctx,
		points: ps,
		n,
		iter: ({ points: [p1, p2, p3, p4] }) => {
			_moveTo(ctx, p1)
			_bezierCurveTo(ctx, [p2, p3, p4])
		},
		loop,
		discrete,
	})
}

const _koch = (ctx: CanvasRenderingContext2D, { points, depth, skew, amplitude, loop, discrete }: ICurveOptions) => {
	const ps = kochPoints({ points, depth, skew, amplitude, loop, discrete })
	_traversal({
		ctx,
		points: ps,
		iter: ({ points }) => _polylineTo(ctx, points),
		init: ({ point }) => _moveTo(ctx, point),
		loop,
	})
}

const _kochCurve = (ctx: CanvasRenderingContext2D, { points, depth, skew, amplitude, loop, discrete }: ICurveOptions) => {
	const ps = kochPoints({ points, depth, skew, amplitude, loop, discrete })
	const n = 3
	const iter = ({ points: [p1, p2, p3] }) => {
		_quadraticCurveTo(ctx, [p2, p3])
	}
	_traversal({
		ctx,
		points: ps,
		n,
		iter,
		init: ({ point }) => _moveTo(ctx, point),
		loop,
	})
}

const _kochDiscreteCurve = (ctx: CanvasRenderingContext2D, { points, depth, skew, amplitude, loop }: ICurveOptions) => {
	const ps = kochPoints({ points, depth, skew, amplitude, loop })
	const n = 5
	const iter = ({ points: [p1, p2, p3, p4, p5] }) => {
		_moveTo(ctx, p1)
		_quadraticCurveTo(ctx, [p2, p3])
		_quadraticCurveTo(ctx, [p4, p5])
	}
	const discrete = true
	_traversal({
		ctx,
		points: ps,
		n,
		iter,
		loop,
		discrete,
	})
}

const curveMapFn = {
	bezier: _bezier,
	leftBezier: _leftBezier,
	doubleBezier: _doubleBezier,
	symmetryBezier: _symmetryBezier,
	sawtooth: _sawtooth,
	leftSawtooth: _leftSawtooth,
	doubleSawtooth: _doubleSawtooth,
	symmetrySawtooth: _symmetrySawtooth,
	rightAngle: _rightAngle,
	leftAngle: _leftAngle,
	symmetryAngle: _symmetryAngle,
	circle: _circle,
	diamond: _diamond,
	cross: _cross,
	triangle: _triangle,
	leftTriangle: _leftTriangle,
	semicircle: _semicircle,
	leftSemicircle: _leftSemicircle,
	centripetal: _centripetal,
	leftCentripetal: _leftCentripetal,
	doubleCentripetal: _doubleCentripetal,
	none: _polyline,
	sin: _sin,
	tangentCircle: _tangentCircle,
	rough: _rough,
	sierpinski: _sierpinski,
	sierpinskiBezier: _sierpinskiBezier,
	sierpinskiDiscreteBezier: _sierpinskiDiscreteBezier,
	koch: _koch,
	kochCurve: _kochCurve,
	kochDiscreteCurve: _kochDiscreteCurve,
}

// 曲线
export const _curve = ({ ctx, points, curve, loop = false, step = 1, discrete }: ICurveOptions): void => {
	let type,
		amplitude = 1,
		skew = 0,
		depth = 1,
		controlPointsVisible = false

	if (typeof curve === 'string') {
		type = curve
	} else if (isObject(curve)) {
		type = curve.type
		amplitude = curve.amplitude
		skew = curve.skew
		depth = curve.depth
		controlPointsVisible = curve.controlPointsVisible
	}
	let fn = curveMapFn[type] || _polyline
	if (step > 1) {
		loop = true
	}

	fn(ctx, { points, loop, step, skew, amplitude, depth, controlPointsVisible, discrete })
}
