import { _circle, _props, _text } from './canvas'
import { _polyline } from './curve'
import { markInfo } from '@/algorithm/mark'
import { IMarkProps } from '@/types/mark'

export const _mark = (ctx: CanvasRenderingContext2D, { points, mark }, props: IMarkProps) => {
	const mi = markInfo(points, mark)
	const { angle = true, edge = true, radius = false, labels = false, boundingRect = false, boundingSize } = mark

	// 夹角
	if (angle) {
		const { angleInfo } = mi
		ctx.save()
		ctx.beginPath()
		_props(ctx, { ...props, strokeDasharray: 0 })
		angleInfo.forEach(({ points, a, ia, sweepFlag, o, r, label, labelPos }) => {
			_polyline(ctx, { points })

			if (sweepFlag === 0) {
				// 逆时针  凹点
				ctx.arc(o[0], o[1], r, a - ia, a)
			} else {
				ctx.arc(o[0], o[1], r, a, a + ia)
			}

			// if (Math.abs(ia - Math.PI / 2) < 0.0001) { //Number.EPSILON
			//     // 直角
			// } else {
			//     ctx.arc(o[0], o[1], r, a, a + ia);
			// }

			_text(ctx, label, labelPos, { fontSize: 10, ...props })
		})
		ctx.stroke()
		ctx.fill()
		ctx.restore()
	}

	// 线段长度
	if (edge) {
		const { edgeInfo } = mi
		ctx.save()
		ctx.beginPath()
		_props(ctx, { strokeDasharray: 4, ...props })
		edgeInfo.forEach(({ points, label, labelPos }) => {
			_polyline(ctx, { points })
			_text(ctx, label, labelPos, { fontSize: 10, ...props })
		})
		ctx.stroke()
		ctx.restore()
	}

	if (radius) {
		const { radiusInfo } = mi
		ctx.save()
		ctx.beginPath()
		_props(ctx, { strokeDasharray: 4, ...props })
		radiusInfo.forEach(({ points, label, labelPos }) => {
			_polyline(ctx, { points })
			_text(ctx, label, labelPos, { fontSize: 10, ...props })
		})
		ctx.stroke()
		ctx.restore()
	}

	if (labels) {
		const { labelsInfo } = mi
		ctx.save()
		ctx.beginPath()
		labelsInfo.forEach(({ label, labelPos }) => {
			_text(ctx, label, labelPos, { fontSize: 10, ...props, strokeDasharray: 0 })
		})
		ctx.stroke()
		ctx.restore()
	}

	if (boundingSize) {
		const { boundingSizeInfo } = mi
		ctx.save()
		ctx.beginPath()
		_props(ctx, { strokeDasharray: 4, ...props })
		boundingSizeInfo.forEach(({ points, label, labelPos }) => {
			_polyline(ctx, { points })
			_text(ctx, label, labelPos, { fontSize: 10, ...props })

			// g[g.length] = _path(_polyline({ points }), { strokeDasharray: 4, ...props, fill: 'none' })

			// g[g.length] = _text({
			//     x: labelPos[0],
			//     y: labelPos[1],
			//     text: label
			// }, { fontSize: 10, ...props, strokeDasharray: 0, textAnchor: 'middle' })
		})

		ctx.stroke()
		ctx.restore()
	}

	if (boundingRect) {
		const { boundingRectPoints } = mi
		ctx.save()
		ctx.beginPath()
		_props(ctx, { ...props, strokeDasharray: 0, stroke: 'gray' })
		_polyline(ctx, { points: boundingRectPoints, loop: true })
		ctx.stroke()
		ctx.beginPath()
		boundingRectPoints.forEach(t => {
			_circle(ctx, t, 3, { fill: 'gray', stroke: 'gray' })
		})

		ctx.stroke()
		ctx.restore()
	}
}
