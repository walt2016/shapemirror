import { PathMode } from '@/types/pathMode'
import { _curve } from './curve'
import { _circle, _circles } from './canvas'
import { rayMix } from '@/math/arrayMix'
import { _o } from '@/math'
import { toMatrix } from '@/math/array'
import { linkPoints, stripPoints, stripLoopPoints, stripFanPoints } from '@/algorithm/link'
import { _traversal } from '@/algorithm/traversal'
import { IPathProps } from '@/types/path'
import { Point } from '@/types/point'
import { ICurveOptions } from '@/types/curve'

// 点
export const _points = ({ ctx, points }: ICurveOptions, props?: IPathProps) => {
	const r = 3
	points.forEach((t, i) => {
		_circle(ctx, t, r, {
			stroke: 'none',
			fill: 'red',
			...props,
		})
	})
}

export const _lineStrip = ({ ctx, points, curve }: ICurveOptions): void => {
	_curve({ ctx, points, curve })
}

export const _lineLoop = ({ ctx, points, curve }: ICurveOptions): void => {
	_curve({ ctx, points, curve, loop: true })
}
// 线段
export const _lines = ({ ctx, points, curve }: ICurveOptions): void => {
	_curve({ ctx, points, curve, step: 2, discrete: true })
}
// 多边形
export const _ploygon = ({ ctx, points, n, curve }: ICurveOptions): void => {
	const matrix = toMatrix(points, n)
	matrix.forEach(ps => {
		_curve({ ctx, points: ps, curve, loop: true })
	})
}
// 三角形
export const _triangles = ({ ctx, points, curve }: ICurveOptions): void => {
	_ploygon({ ctx, points, n: 3, curve })
}

const _serial = ({ ctx, points, n, loop, curve }: ICurveOptions): Point[] => {
	const iter = ({ points }) => {
		return _curve({ ctx, points, curve, loop: true })
	}
	return _traversal({
		points,
		n,
		iter,
		loop,
	})
}

const _triangleSerial = ({ ctx, points, curve }: ICurveOptions): Point[] => {
	const n = 3
	const loop = false
	return _serial({ ctx, points, n, loop, curve })
}
const _squareSerial = ({ ctx, points, curve }: ICurveOptions): Point[] => {
	const n = 4
	const loop = false
	return _serial({ ctx, points, n, loop, curve })
}

const _triangleSerialLoop = ({ ctx, points, curve }: ICurveOptions): Point[] => {
	const n = 3
	const loop = true
	return _serial({ ctx, points, n, loop, curve })
}
const _squareSerialLoop = ({ ctx, points, curve }: ICurveOptions): Point[] => {
	const n = 4
	const loop = true
	return _serial({ ctx, points, n, loop, curve })
}

const _triangleStrip = ({ ctx, points, curve }: ICurveOptions): void => {
	const matrix = stripPoints(points, 3)
	matrix.forEach(ps => {
		_curve({ ctx, points: ps, curve, loop: true })
	})
}

const _triangleStripLoop = ({ ctx, points, curve }: ICurveOptions): void => {
	const matrix = stripLoopPoints(points, 3)
	matrix.forEach(ps => {
		_curve({ ctx, points: ps, curve, loop: true })
	})
}
const _triangleFan = ({ ctx, points, curve }: ICurveOptions): void => {
	const matrix = stripFanPoints(points, 2)
	const last = matrix.pop()
	matrix.forEach(ps => {
		_curve({ ctx, points: ps, curve })
	})
	_curve({ ctx, points: last, curve, loop: true })
}

// 四边形
export const _squares = ({ ctx, points, curve }: ICurveOptions): void => {
	_ploygon({ ctx, points, n: 4, curve })
}

const _squareStrip = ({ ctx, points, curve }: ICurveOptions): void => {
	const matrix = stripPoints(points, 4)
	matrix.forEach(ps => {
		_curve({ ctx, points: ps, curve, loop: true })
	})
}

const _squareStripLoop = ({ ctx, points, curve }: ICurveOptions): void => {
	const matrix = stripLoopPoints(points, 4)
	matrix.forEach(ps => {
		_curve({ ctx, points: ps, curve, loop: true })
	})
}
// 扇线
const _lineFan = ({ ctx, points, curve }: ICurveOptions): void => {
	const [p, ...rest] = points
	const ps = rayMix(p, rest)
	_curve({ ctx, points: ps, curve })
}

export const _ray = ({ ctx, points, curve }: ICurveOptions): void => {
	const o = _o(points)
	const ps = rayMix(o, points)
	_curve({ ctx, points: ps, curve, step: 2 })
}

const _rayOne = ({ ctx, points, curve }: ICurveOptions): void => {
	const o = _o(points)
	_curve({ ctx, points: [o, points[0]], curve })
}

const _rayFan = ({ ctx, points, curve }: ICurveOptions): void => {
	_ray({ ctx, points, curve })
	_lineLoop({ ctx, points, curve })
}
const _link = ({ ctx, points, curve }: ICurveOptions): void => {
	const lps = linkPoints(points)
	_curve({ ctx, points: lps, curve, step: 2 })
}
const pathModeMapFn = {
	[PathMode.LINE_STRIP]: _lineStrip,
	[PathMode.LINE_LOOP]: _lineLoop,
	[PathMode.POINTS]: _points,
	[PathMode.LINES]: _lines,
	[PathMode.TRIANGLES]: _triangles,
	[PathMode.TRIANGLE_SERIAL]: _triangleSerial,
	[PathMode.TRIANGLE_SERIAL_LOOP]: _triangleSerialLoop,
	[PathMode.SQUARES]: _squares,
	[PathMode.LINE_FAN]: _lineFan,
	[PathMode.RAY]: _ray,
	[PathMode.RAY_FAN]: _rayFan,
	[PathMode.LINK]: _link,
	[PathMode.TRIANGLE_STRIP]: _triangleStrip,
	[PathMode.SQUARE_STRIP]: _squareStrip,
	[PathMode.TRIANGLE_STRIP_LOOP]: _triangleStripLoop,
	[PathMode.SQUARE_STRIP_LOOP]: _squareStripLoop,
	[PathMode.SQUARE_SERIAL]: _squareSerial,
	[PathMode.SQUARE_SERIAL_LOOP]: _squareSerialLoop,
	[PathMode.TRIANGLE_FAN]: _triangleFan,
}

export const _pathMode = ({ ctx, pathMode, points, curve, closed }: ICurveOptions, props: IPathProps) => {
	if (closed && !pathMode) {
		pathMode = PathMode.LINE_LOOP
	}
	let fn = pathModeMapFn[pathMode] || _lineStrip
	fn({ ctx, points, curve })
	let { stroke, fill, fillRule } = props
	if (fill !== 'none') {
		if (fillRule === 'evenodd') {
			ctx.fill('evenodd')
		} else {
			ctx.fill()
		}
	}
	if (stroke == null || stroke !== 'none') {
		ctx.stroke()
	}
}
