// 填充
import { fillPatternPointsMap } from '@/algorithm/fill'
import { PathMode } from '@/types/pathMode'
import { _props } from './canvas'
import { _path } from './path'

export const _fill = (ctx: CanvasRenderingContext2D, options, props) => {
	const { points = [], fill = {} } = options
	const { pattern, r, a } = fill

	const fn = fillPatternPointsMap[pattern]
	const ps = fn({ points, r, a })

	ctx.save()
	_props(ctx, props)
	_path(
		ctx,
		{
			pathMode: PathMode.LINES,
			points: ps,
		},
		props
	)
	ctx.stroke()
	ctx.restore()
}
