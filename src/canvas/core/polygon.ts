import { polarPoints } from '@/algorithm/polar'
import { _transform } from '@/math/transform'
import { _path } from './path'
import { _props } from './canvas'
import { _mirrorColor } from '@/common'
import { IPolarOptions } from '@/types/polar'
import { renderMatrix } from '../helper'
import { Point } from '@/types/point'
import { IPathProps } from '@/types/path'

export const _polygon = (ctx: CanvasRenderingContext2D, options: IPolarOptions, props: IPathProps) => {
	let matrix: Point[][] = polarPoints(options)
	// let { transform, mirror, pathMode = 'LINE_LOOP' } = options
	// // // 变形函数
	// matrix = _transform(matrix, transform)
	// matrix = _mirrorPoints(matrix, mirror)
	// const color = _mirrorColor(mirror)
	renderMatrix({
		matrix,
		ctx,
		options,
		// options: {
		//     ...options,
		//     pathMode,
		//     mirror: null,
		//     transform:null
		//     // color
		// },
		props,
	})
	//

	// ctx.save();
	// _props(ctx, props)

	// matrix.forEach(points => {
	//     _path(ctx, {
	//         ...options,
	//         points,
	//         pathMode,
	//         mirror: null,
	//         color
	//     }, props)
	// })

	// ctx.restore();
}

export const _rect = (ctx: CanvasRenderingContext2D, options: IPolarOptions, props) => {
	options.n = 4
	_polygon(ctx, options, props)
}
