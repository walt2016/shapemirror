import { rayMix } from '@/math/arrayMix'
import { _dis, _mid, rotatePoints, _o, _moveDis } from '@/math'
import { _path } from './path'
import { linkPoints, linkCrossPoints, edgeExtensionPoints, edgeExtensionCrossPoints } from '@/algorithm/link'
import { _polyline } from './curve'
import { _merge } from '@/common'
import { _pathMode } from './pathMode'
import { genLabel } from '@/common/labels'
import { IEdgeExtension, IPathOptions, IPathProps } from '@/types/path'
import { Point } from '@/types/point'

const propsMap = {
	stroke: 'strokeStyle',
	fill: 'fillStyle',
	strokeWidth: 'lineWidth',
	fillAlpha: 'globalAlpha',
	fillOpacity: 'globalAlpha',
	opacity: 'globalAlpha',
	alpha: 'globalAlpha',
}
const valMap = {
	none: 'transparent',
}
// 虚线
const _dashLine = (ctx: CanvasRenderingContext2D, key: string, value: number): boolean => {
	if ('strokeDasharray' === key && value > 0) {
		ctx.setLineDash([value, value])
		return true
	}
	return false
}

// 属性
export const _props = (ctx: CanvasRenderingContext2D, props = {}) => {
	Object.keys(props).forEach(t => {
		const value = props[t]
		if (!_dashLine(ctx, t, value)) {
			const prop = propsMap[t] || t
			ctx[prop] = valMap[value] || value
		}
	})
}
// 文本
export const _text = (ctx: CanvasRenderingContext2D, text: string | number, pos: Point, props: IPathProps) => {
	ctx.save()
	const { fontSize = 12, stroke = 'red' } = props
	ctx.font = `${fontSize}px 微软雅黑`
	ctx.textAlign = 'center'
	ctx.textBaseline = 'middle'
	ctx.fillStyle = stroke
	ctx.fillText(String(text), pos[0], pos[1])
	ctx.restore()
}
// 多文本
export const _texts = (ctx: CanvasRenderingContext2D, points: Point[], labels: IPathProps) => {
	const { type } = labels
	const o = _o(points)
	points.forEach((t, i) => {
		const pos = _moveDis(t, o, 10)
		const text = genLabel(type, i)
		_text(ctx, text, pos, labels)
	})
}
// 圆
export const _circle = (ctx: CanvasRenderingContext2D, o: Point, r: number, props: IPathProps) => {
	ctx.save()
	const [x, y] = o
	const { stroke, fill } = props
	ctx.beginPath()
	_props(ctx, props)
	ctx.arc(x, y, r, 0, 2 * Math.PI)
	if (stroke && stroke !== 'none') {
		ctx.stroke()
	}
	if (fill && fill !== 'none') {
		ctx.fill()
	}
	ctx.closePath()
	ctx.restore()
}

// 多个圆
export const _circles = (ctx: CanvasRenderingContext2D, points: Point[], props: IPathProps) => {
	const { r = 5 } = props
	points.forEach((t, i) => {
		_circle(ctx, t, r, {
			stroke: 'red',
			...props,
		})
	})
}

// 顶点
export const _vertex = (ctx: CanvasRenderingContext2D, points: Point[], props?: IPathProps) => {
	const { r = 5 } = props
	const { one } = props
	if (one) {
		_circle(ctx, points[0], r, {
			stroke: 'red',
			...props,
		})
	} else {
		_circles(ctx, points, {
			r,
			...props,
		})
	}
}
// 中心点
export const _centre = (ctx: CanvasRenderingContext2D, o: Point, props?: IPathProps) => {
	const { r = 5 } = props
	_circle(ctx, o, r, {
		fill: 'none',
		stroke: 'red',
		...props,
	})
}

// 半径
export const _radius = (ctx: CanvasRenderingContext2D, { points }, props?: IPathProps) => {
	const { one } = props
	const o = _o(points)
	if (one) {
		_path(ctx, { points: [o, points[0]], closed: false }, props)
	} else {
		const rps = rayMix(o, points)
		_path(ctx, { points: rps, closed: false }, props)
	}
}

// 内切圆
export const _incircle = (ctx: CanvasRenderingContext2D, { o, points }, props?: IPathProps) => {
	const r = _dis(o, _mid.apply(null, points.slice(0, 2)))
	_circle(ctx, o, r, {
		fill: 'none',
		stroke: 'red',
		...props,
	})
}
// 外切圆
export const _excircle = (ctx: CanvasRenderingContext2D, { o, r }, props: IPathProps) => {
	_circle(ctx, o, r, {
		fill: 'none',
		stroke: 'red',
		...props,
	})
}

// 边
export const _edge = (ctx: CanvasRenderingContext2D, options?: IPathOptions, props?: IPathProps) => {
	const { pathMode, points, curve, closed } = options
	ctx.save()
	ctx.beginPath()
	_props(ctx, props)
	_pathMode({ ctx, pathMode, points, curve, closed }, props)
	ctx.restore()
}

// 链接线
export const _links = (ctx: CanvasRenderingContext2D, points, props?: IPathProps) => {
	const lps = linkPoints(points)

	_path(ctx, { points: lps, closed: false }, props)

	if (props.crossPoints) {
		const lcps = linkCrossPoints(points)
		_circles(ctx, lcps, {
			fill: 'red',
			...props,
		})
	}
}

// const _dashLines = (ctx,points, props) => {
//     return _path(_lines(points), props)
// }

// 边延长线
export const _edgeExtension = (ctx: CanvasRenderingContext2D, { points, edgeExtension }: { points: Point[]; edgeExtension: IEdgeExtension }, props) => {
	const { crossPoints, labels } = edgeExtension
	const ps: Point[] = edgeExtensionPoints(points, edgeExtension.iterNum)

	ctx.save()
	ctx.beginPath()
	_props(ctx, props)
	_polyline(ctx, { points: ps, step: 2 })
	ctx.stroke()

	if (crossPoints || labels) {
		const cps = edgeExtensionCrossPoints(points)
		// 交点
		if (crossPoints) {
			_circles(ctx, cps, {
				stroke: 'gray',
				fill: 'white',
				..._merge(props, edgeExtension),
			})
		}

		// 标注
		if (labels) {
			_texts(ctx, cps, {
				stroke: 'blue',
			})
		}
	}
	ctx.restore()
}
