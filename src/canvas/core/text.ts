import { IPathProps } from '@/types/path'
import { ITextOptions } from '@/types/text'

export const _text = (ctx: CanvasRenderingContext2D, options: ITextOptions, props?: IPathProps) => {
	let { x, y, text } = options
	ctx.fillStyle = 'green'
	ctx.font = '14px'
	Object.assign(ctx, props)
	ctx.fillText(String(text), x, y)
}
// ctx.fillText("test",100,100);
