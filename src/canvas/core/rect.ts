import { polarPoints } from '@/algorithm/polar'
import { IPathProps } from '@/types/path'
import { _props } from './canvas'
import { _circle } from './circle'
import { _path } from './path'
import { _text } from './text'
import { Point } from '@/types/point'

// 被polygon._rect代替
export const _rect = (ctx: CanvasRenderingContext2D, options, props: IPathProps) => {
	let {
		// p1, p2,
		o = [400, 300],
		r = 100,
		a = 0,
		centre,
		vertex,
		labels,
	} = options
	let points: Point[] = polarPoints({
		o,
		r,
		a,
		n: 4,
	})[0]
	ctx.save()
	if (vertex) {
		points.forEach(t => {
			_circle(
				ctx,
				{
					o: t,
					r: 3,
				},
				{
					fill: 'red',
					stroke: 'none',
				}
			)
		})
	}
	if (labels) {
		points.forEach((t, i) => {
			_text(ctx, {
				x: t[0],
				y: t[1],
				text: i,
			})
		})
	}
	if (centre) {
		_circle(
			ctx,
			{
				o,
				r: 3,
			},
			{
				fill: 'red',
				stroke: 'none',
			}
		)
	}

	// let p1 = points[0],
	//     p2 = points[2]

	// ctx.lineWidth = 1;
	_props(ctx, props)
	_path(ctx, { points })

	// ctx.beginPath();
	// // rect(x,y,width,height);
	// ctx.rect(p1[0], p1[1], p2[0] - p1[0], p2[1] - p1[1]);
	// // this.isInPath(ctx, o, 9999)
	// ctx.stroke();
	// ctx.fill();

	ctx.restore()
}
