import { IAxisOptions } from '@/types/axis'

export const _axis = (ctx: CanvasRenderingContext2D, options: IAxisOptions): void => {
	let { gridSpace = 50, gridLineColor = '#ccc', lineDashStyle = 5 } = options
	let canvas = ctx.canvas
	let xLines = Math.floor(canvas.width / gridSpace)
	ctx.save()
	// 画横线
	for (let i = 0; i <= xLines; i++) {
		ctx.beginPath()
		ctx.moveTo(0, 0 + i * gridSpace)
		ctx.lineTo(canvas.width, 0 + i * gridSpace)
		ctx.lineWidth = 1
		ctx.setLineDash([lineDashStyle])
		ctx.strokeStyle = gridLineColor
		ctx.stroke()
	}
	// 画竖线
	for (let i = 0; i <= xLines; i++) {
		ctx.beginPath()
		ctx.moveTo(0 + i * gridSpace, 0)
		ctx.lineTo(0 + i * gridSpace, canvas.height)
		ctx.lineWidth = 1
		ctx.setLineDash([lineDashStyle])
		ctx.strokeStyle = gridLineColor
		ctx.stroke()
	}
	ctx.restore()
}
