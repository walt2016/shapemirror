// 星星
import { _polygon } from './polygon'
import { starPoints } from '@/algorithm/star'
import { polarPoints } from '@/algorithm/polar'
import { _path } from './path'
import { PathMode } from '@/types/pathMode'
import { IPolarOptions } from '@/types/polar'
import { Point } from '@/types/point'
export const _star = (ctx: CanvasRenderingContext2D, options: IPolarOptions, props) => {
	let { o, r, n, a, interiorPolygon = true } = options

	if (n > 4) {
		let points: Point[] = polarPoints({ o, r, n, a })[0]
		let ps = starPoints(points)

		if (interiorPolygon) {
			// ploygon
			_path(
				ctx,
				{
					pathMode: PathMode.LINE_LOOP,
					// ...options,
					points,
					// labels: null
				},
				{
					...props,
					strokeDasharray: 4,
				}
			)
		}

		// let star =
		_path(
			ctx,
			{
				...options,
				points: ps,
			},
			props
		)
	} else {
		// let star =
		_polygon(
			ctx,
			{
				...options,
				transform: 'paritySort',
			},
			props
		)
	}
}
