import { ISize } from '@/types/base';
import * as pdfMake from 'pdfmake/build/pdfmake.js';
import { CanvasElement, ContentImage, ContentSvg, PageOrientation } from 'pdfmake/interfaces';


interface ISvgImage {
    svg: string;
    size: ISize;
}
export interface IImageForPdf {
    data: string,
    size: ISize,
    type: 'svg' | 'uri'

}

const getMaxSize = (images: { size: ISize }[]) => {
    return images.reduce(
        (a, b) => {
            const width = Math.max(a.width, b.size.width);
            const height = Math.max(a.height, b.size.height);
            return {
                width,
                height,
            };
        },
        {
            width: 0,
            height: 0,
        },
    );
}

export function createPdf(images: IImageForPdf[]): pdfMake.TCreatedPdf {
    const { width, height } = getMaxSize(images)
    const docDefinition = {
        pageMargins: 0,
        content: images.map(({ data, size: { width, height }, type }: IImageForPdf) => {
            const fit: [number, number] = [width, height];
            if (type === 'uri') {
                return {
                    image: data,
                    width,
                    height,
                    fit,
                } as ContentImage;
            }
            return {
                svg: data,
                width,
                height,
                fit,
            } as ContentSvg;


        }),
        pageBreak: 'before',
        pageSize: { width, height },
        pageOrientation: (width >= height ? 'landscape' : 'portrait') as PageOrientation,
    };

    const pdf = pdfMake.createPdf(docDefinition);
    return pdf;
}


export function createPdfBySvg(images: ISvgImage[]): pdfMake.TCreatedPdf {
    const { width, height } = getMaxSize(images)
    const docDefinition = {
        pageMargins: 0,
        content: images.map(({ svg, size: { width, height } }: ISvgImage) => {
            const fit: [number, number] = [width, height];
            return {
                svg,
                width,
                height,
                fit,
            };
        }),
        pageBreak: 'before',
        pageSize: { width, height },
        pageOrientation: (width >= height ? 'landscape' : 'portrait') as PageOrientation,
    };
    const pdf = pdfMake.createPdf(docDefinition);
    return pdf;
}

export function createPdfByDataUri(uri: string): pdfMake.TCreatedPdf {
    const docDefinition = {
        pageMargins: 0,
        content: { image: uri },
        pageBreak: 'before',
    };
    const pdf = pdfMake.createPdf(docDefinition);
    return pdf;
}