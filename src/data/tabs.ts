import { svgRect, svgGrid, svgPolar } from './svg'
import { form } from './form'
import { grid } from './grid'
import { html } from './html'
import { canvas } from './canvas'
import { list, renderlistItem } from './list'
import { btns } from './btns'
import { IMenu } from '@/types/menu'
import { ContentType } from '@/types/ui'


const tabs :IMenu[] = [{
  title: 'form',
  name: 'first',
  content: form,
  contentType: ContentType.Html
}, {
  title: 'table',
  name: 'second',
  content: grid,
  contentType: ContentType.Html

}, {
  title: 'list',
  name: 'list',
  content: list,
  contentType: ContentType.Html,
  render: renderlistItem

},

{
  title: 'html',
  name: 'html',
  textContent: html,
  contentType: ContentType.Html
}, {
  title: 'text',
  name: 'text',
  textContent: html
}, {
  //   title: 'svg',
  //   name: 'svg',
  //   content: svg,
  //   contentType: 'svg'
  // }, {
  title: 'svgRect',
  name: 'svgRect',
  content: svgRect,
  contentType: ContentType.SVG
}, {
  //   title: 'svg.pattern',
  //   name: 'svg_pattern',
  //   content: svgPattern,
  //   contentType: 'svg'
  // }, {
  title: 'svg.grid',
  name: 'svg_grid',
  content: svgGrid,
  contentType:  ContentType.SVG
}, {
  title: 'svg.polar',
  name: 'svg_polar',
  content: svgPolar,
  contentType:  ContentType.SVG
}, {
  title: 'canvas',
  name: 'canvas',
  content: canvas,
  contentType:  ContentType.Canvas
},
{
  title: 'btns',
  name: 'btns',
  content: btns,
  contentType: ContentType.Html
}
]

export default {
  tabs,
  activeName: 'first'
}