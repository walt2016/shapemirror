
let menu = [
    // {

    //     title: 'html',
    //     name: 'html',
    //     children: [{
    //         title: 'form',
    //         name: 'first',
    //         url: 'form',
    //         contentType: 'Form'
    //     }, {
    //         title: 'table',
    //         name: 'second',
    //         url: 'grid',
    //         contentType: 'Grid'

    //     }, {
    //         title: 'list',
    //         name: 'list',
    //         url: 'list',
    //         contentType: "List",
    //         render: 'renderlistItem'

    //     },
    //     {
    //         title: 'html',
    //         name: 'html',
    //         url: 'html',
    //         contentType: 'html'
    //     }, {
    //         title: 'text',
    //         name: 'text',
    //         url: 'html'
    //     },
    //     {
    //         title: 'btns',
    //         name: 'btns',
    //         url: 'btns',
    //         contentType: 'btns'
    //     }]

    // },
    {
        title: 'svg',
        name: 'svg',
        children: [
            {
                title: 'axis',
                name: 'axis',
                children: [
                    {
                        title: 'axis',
                        name: 'axis',
                        url: 'svgAxis',
                        contentType: 'svg'
                    },
                    {
                        title: 'grid',
                        name: 'grid',
                        url: 'svgGrid',
                        contentType: 'svg'
                    }, {
                        title: 'polar',
                        name: 'polar',
                        url: 'svgPolar',
                        contentType: 'svg'
                    }, {
                        title: 'gridPoints',
                        name: 'gridPoints',
                        url: 'svgGridPoints',
                        contentType: 'svg'
                    }

                ]
            },
            {
                title: 'shape',
                name: 'shape',
                children: [{
                    title: 'circle',
                    name: 'circle',
                    url: 'svgCircle',
                    contentType: 'svg',
                },
                {
                    title: 'rect',
                    name: 'rect',
                    url: 'svgRect',
                    contentType: 'svg'
                }, {
                    title: 'lines',
                    name: 'lines',
                    url: 'svgLines',
                    contentType: 'svg'
                }, {
                    title: 'curves',
                    name: 'curves',
                    url: 'svgCurves',
                    contentType: 'svg'
                },
                {
                    title: 'sawtooths',
                    name: 'sawtooths',
                    url: 'svgSawtooths',
                    contentType: 'svg'
                },

                {
                    title: 'circlesPattern',
                    name: 'circlesPattern',
                    url: 'svgCirclesPattern',
                    contentType: 'svg'
                }, {
                    title: 'rectsPattern',
                    name: 'rectsPattern',
                    url: 'svgRectsPattern',
                    contentType: 'svg'
                }, {
                    title: 'gridRect',
                    name: 'gridRect',
                    url: 'svgGridRect',
                    contentType: 'svg'
                }, {
                    title: 'gridPolygon',
                    name: 'gridPolygon',
                    url: 'svgGridPolygon',
                    contentType: 'svg'
                },

                ]
            },
            {
                title: 'polygon',
                name: 'polygon',
                children: [{
                    title: 'triangle',
                    name: 'triangle',
                    url: 'svgtriangle',
                    contentType: 'svg'
                },
                {
                    title: 'polygon',
                    name: 'polygon',
                    url: 'svgPolygon',
                    contentType: 'svg'
                },



                {
                    title: 'polygonColor',
                    name: 'polygonColor',
                    url: 'svgPolygonColor',
                    contentType: 'svg'
                }, {
                    title: 'polygonLink',
                    name: 'polygonLink',
                    url: 'svgPolygonLink',
                    contentType: 'svg'
                },
                {
                    title: 'ferrisWheel',
                    name: 'ferrisWheel',
                    url: 'svgFerrisWheel',
                    contentType: 'svg'
                }, {
                    title: 'complex',
                    name: 'complex',
                    url: 'svgComplex',
                    contentType: 'svg'
                },

                {
                    title: 'ray',
                    name: 'ray',
                    url: 'svgRay',
                    contentType: 'svg'
                },
                {
                    title: 'rayCurve',
                    name: 'rayCurve',
                    url: 'svgRayCurve',
                    contentType: 'svg'
                }, {
                    title: 'isometric',
                    name: 'isometric',
                    url: 'svgIsometric',
                    contentType: 'svg'
                },
                {
                    title: 'polarShape',
                    name: 'polarShape',
                    url: 'svgPolarShape',
                    contentType: 'svg'
                },
                {
                    title: 'spiralShape',
                    name: 'spiralShape',
                    url: 'svgSpiralShape',
                    contentType: 'svg'
                },
                {
                    title: 'ringShape',
                    name: 'ringShape',
                    url: 'svgRingShape',
                    contentType: 'svg'
                },
                {
                    title: 'rayShape',
                    name: 'rayShape',
                    url: 'svgRayShape',
                    contentType: 'svg'
                },
                {
                    title: 'gridShape',
                    name: 'gridShape',
                    url: 'svgGridShape',
                    contentType: 'svg'
                },
                {
                    title: 'isometricShape',
                    name: 'isometricShape',
                    url: 'svgIsometricShape',
                    contentType: 'svg'
                },
                {
                    title: 'polygonShape',
                    name: 'polygonShape',
                    url: 'svgPolygonShape',
                    contentType: 'svg'
                },
                ]
            },
            {
                title: 'fractal',
                name: 'fractal',
                children: [{
                    title: 'edgemirror',
                    name: 'edgemirror',
                    url: 'svgEdgeMirror',
                    contentType: 'svg'
                },
                {
                    title: 'vertexmirror',
                    name: 'vertexmirror',
                    url: 'svgVertexMirror',
                    contentType: 'svg'
                },
                {
                    title: 'edgeFractal',
                    name: 'edgeFractal',
                    url: 'svgEdgeFractal',
                    contentType: 'svg'
                },
                {
                    title: 'vertexFractal',
                    name: 'vertexFractal',
                    url: 'svgVertexFractal',
                    contentType: 'svg'
                },

                {
                    title: 'edgeMirrorLink',
                    name: 'edgeMirrorLink',
                    url: 'svgEdgeMirrorLink',
                    contentType: 'svg'
                },
                {
                    title: 'fractalGroup',
                    name: 'fractalGroup',
                    url: 'svgFractalGroup',
                    contentType: 'svg'
                },
                {
                    title: 'fractalFull',
                    name: 'fractalFull',
                    url: 'svgFractalFull',
                    contentType: 'svg'
                },
                {
                    title: 'fractalRadio',
                    name: 'fractalRadio',
                    url: 'svgFractalRadio',
                    contentType: 'svg'
                },
                {
                    title: 'edgeMid',
                    name: 'edgeMid',
                    url: 'svgEdgeMid',
                    contentType: 'svg'
                },
                {
                    title: 'yanghui',
                    name: 'yanghui',
                    url: 'svgYangehui',
                    contentType: 'svg'
                }, {
                    title: 'leaves',
                    name: 'leaves',
                    url: 'svgLeaves',
                    contentType: 'svg'

                }, {
                    title: 'triangleLeaf',
                    name: 'triangleLeaf',
                    url: 'svgTriangleLeaf',
                    contentType: 'svg'

                }, {
                    title: 'gougu',
                    name: 'gougu',
                    url: 'svgGougu',
                    contentType: 'svg'

                }, {
                    title: 'squareFractal',
                    name: 'squareFractal',
                    url: 'svgSquareFractal',
                    contentType: 'svg'

                }, {
                    title: 'sierpinskiTriangle',
                    name: 'sierpinskiTriangle',
                    url: 'svgSierpinskiTriangle',
                    contentType: 'svg'
                }, {
                    title: 'koch',
                    name: 'koch',
                    url: 'svgKoch',
                    contentType: 'svg'
                }, {

                    title: 'mitsubishi',
                    name: 'mitsubishi',
                    url: 'svgMitsubishi',
                    contentType: 'svg'
                }, {
                    title: 'hexaflake',
                    name: 'hexaflake',
                    url: 'svgHexaflake',
                    contentType: 'svg'

                }, {
                    title: 'tree',
                    name: 'tree',
                    url: 'svgTree',
                    contentType: 'svg'

                }, {
                    title: 'dragon',
                    name: 'dragon',
                    url: 'svgDragon',
                    contentType: 'svg'

                }, {
                    title: 'peano',
                    name: 'peano',
                    url: 'svgPeano',
                    contentType: 'svg'

                }

                ]
            },
            {
                title: 'curve',
                name: 'curve',
                children: [
                    {
                        title: 'curve',
                        name: 'curve',
                        url: 'svgCurve',
                        contentType: 'svg'
                    },
                    {
                        title: 'wave',
                        name: 'wave',
                        url: 'svgWave',
                        contentType: 'svg'
                    },
                    {
                        title: 'sawtooth',
                        name: 'sawtooth',
                        url: 'svgSawtooth',
                        contentType: 'svg'
                    },
                    {
                        title: 'semicircle',
                        name: 'semicircle',
                        url: 'svgSemicircle',
                        contentType: 'svg'
                    },
                    {
                        title: 'elliptical',
                        name: 'elliptical',
                        url: 'svgElliptical',
                        contentType: 'svg'
                    },
                    {
                        title: 'ellipticalLink',
                        name: 'ellipticalLink',
                        url: 'svgEllipticalLink',
                        contentType: 'svg'
                    },
                    {
                        title: 'sin',
                        name: 'sin',
                        url: 'svgSin',
                        contentType: 'svg'
                    },
                    {
                        title: 'wanhuachi',
                        name: 'wanhuachi',
                        url: 'svgWanhuachi',
                        contentType: 'svg'
                    },
                ]
            },
            {
                title: 'star',
                name: 'transform',
                children: [

                    {
                        title: 'Star5',
                        name: 'Star5',
                        url: 'svgStar5',
                        contentType: 'svg'
                    },
                    {
                        title: 'Star6',
                        name: 'Star6',
                        url: 'svgStar6',
                        contentType: 'svg'
                    },
                    {
                        title: 'Star7',
                        name: 'Star7',
                        url: 'svgStar7',
                        contentType: 'svg'
                    },
                    {
                        title: 'Star8',
                        name: 'Star8',
                        url: 'svgStar8',
                        contentType: 'svg'
                    },
                    {
                        title: 'Star12',
                        name: 'Star12',
                        url: 'svgStar12',
                        contentType: 'svg'
                    },
                ]
            }, {
                title: 'transform',
                name: 'transform',
                children: [{
                    title: 'misplaced5',
                    name: 'misplaced5',
                    url: 'svgMisplaced5',
                    contentType: 'svg'
                }, {
                    title: 'misplaced6',
                    name: 'misplaced6',
                    url: 'svgMisplaced6',
                    contentType: 'svg'
                }, {
                    title: 'misplaced7',
                    name: 'misplaced7',
                    url: 'svgMisplaced7',
                    contentType: 'svg'
                }, {
                    title: 'misplaced8',
                    name: 'misplaced8',
                    url: 'svgMisplaced8',
                    contentType: 'svg'
                }, {
                    title: 'misplaced9',
                    name: 'misplaced9',
                    url: 'svgMisplaced9',
                    contentType: 'svg'
                },]
            }, {
                title: 'pathpoints',
                name: 'pathpoints',
                children: [{
                    title: 'polygonPath',
                    name: 'polygonPath',
                    url: 'svgPolygonPath',
                    contentType: 'svg'
                }, {
                    title: 'polygonPath2',
                    name: 'polygonPath2',
                    url: 'svgPolygonPath2',
                    contentType: 'svg'
                }, {
                    title: 'polygonPath3',
                    name: 'polygonPath3',
                    url: 'svgPolygonPath3',
                    contentType: 'svg'
                }, {
                    title: 'polygonPath4',
                    name: 'polygonPath4',
                    url: 'svgPolygonPath4',
                    contentType: 'svg'
                }, {
                    title: 'polygonPath5',
                    name: 'polygonPath5',
                    url: 'svgPolygonPath5',
                    contentType: 'svg'
                }]
            }, {
                title: 'spiral',
                name: 'spiral',
                children: [
                    {
                        title: 'spiral',
                        name: 'spiral',
                        url: 'svgSpiral',
                        contentType: 'svg'
                    },
                    {
                        title: 'spiraltriangle',
                        name: 'spiraltriangle',
                        url: 'svgSpiraltriangle',
                        contentType: 'svg'
                    },
                    // {
                    //     title: 'twoSpiral',
                    //     name: 'twoSpiral',
                    //     url: 'svgTwoSpiral',
                    //     contentType: 'svg'
                    // },


                ]

            }, {
                title: 'game',
                name: 'game',
                children: [{
                    title: 'maze',
                    name: 'maze',
                    url: 'svgMaze',
                    contentType: 'svg'
                }, {
                    title: 'maze2',
                    name: 'maze2',
                    url: 'svgMaze2',
                    contentType: 'svg'
                }, {
                    title: 'motion',
                    name: 'motion',
                    url: 'svgMotion',
                    contentType: 'svg'
                }, {
                    title: 'layout',
                    name: 'layout',
                    url: 'svgLayout',
                    contentType: 'svg'
                }]
            }


        ]
    }, {
        title: 'canvas',
        name: 'canvas',
        children: [
            {
                title: 'canvasShape',
                name: 'canvasShape',
                children: [
                    {
                        title: 'canvasCircle',
                        name: 'canvasCircle',
                        url: 'canvasCircle',
                        contentType: 'canvas'
                    },
                    {
                        title: 'canvasRect',
                        name: 'canvasRect',
                        url: 'canvasRect',
                        contentType: 'canvas'
                    },

                    {
                        title: 'canvasPolygon',
                        name: 'canvasPolygon',
                        url: 'canvasPolygon',
                        contentType: 'canvas'
                    }, {
                        title: 'canvasPolygonMirror',
                        name: 'canvasPolygonMirror',
                        url: 'canvasPolygonMirror',
                        contentType: 'canvas'
                    },
                    {
                        title: 'canvasPolygonEdgeMirror',
                        name: 'canvasPolygonEdgeMirror',
                        url: 'canvasPolygonEdgeMirror',
                        contentType: 'canvas'
                    },




                ]

            }
        ]

    }, {
        title: 'webgl',
        name: 'webgl',
        children: [{
            title: 'webgl',
            name: 'webgl',
            children: [{
                title: 'webgl',
                name: 'webgl',
                url: 'webgl',
                contentType: 'webgl'
            }, {
                title: 'glTriangle',
                name: 'glTriangle',
                url: 'glTriangle',
                contentType: 'webgl'
            }, {
                title: 'glSquare',
                name: 'glSquare',
                url: 'glSquare',
                contentType: 'webgl'
            }, {
                title: 'glCube',
                name: 'glCube',
                url: 'glCube',
                contentType: 'webgl'
            }]
        }]

    }


]

export default {
    menu,
    activeName: 'first',
    target: '.main .screen-viewer'
}