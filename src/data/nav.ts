import {
    svgAxis, svgGridPoints,
    svgCircle, svgRect, svgLines, svgCurves, svgSawtooths,
    svgGrid, svgPolar, svgRay, svgRayCurve,
    svgtriangle,
    svgPolygon, svgPolygonColor, svgPolygonLink, svgFerrisWheel,
    svgComplex,
    svgEdgeMirror, svgVertexMirror, svgEdgeFractal, svgVertexFractal,
    svgFractalFull, svgEdgeMirrorLink, svgFractalGroup, svgFractalRadio, svgEdgeMid,
    svgYangehui, svgCurve, svgWave, svgSawtooth, svgSemicircle, svgElliptical,
    svgEllipticalLink, svgSin,
    svgStar5, svgStar12, svgStar6, svgStar7, svgStar8,
    svgMisplaced5, svgMisplaced6, svgMisplaced7, svgMisplaced8, svgMisplaced9,
    svgPolygonPath, svgPolygonPath2, svgPolygonPath3, svgPolygonPath4, svgPolygonPath5,
    svgSpiral,  svgMaze, svgCircleMaze, svgMotion,
    // svgGouguTree
} from './svg'
import {
    form
} from './form'
import {
    grid
} from './grid'
import {
    html
} from './html'
import {
    canvas
} from './canvas'
import {
    list,
    renderlistItem
} from './list'
import { btns } from './btns'


let menu = [
    {

        title: 'html',
        name: 'html',
        children: [{
            title: 'form',
            name: 'first',
            content: form,
            contentType: 'Form'
        }, {
            title: 'table',
            name: 'second',
            content: grid,
            contentType: 'Grid'

        }, {
            title: 'list',
            name: 'list',
            content: list,
            contentType: "List",
            render: renderlistItem

        },
        {
            title: 'html',
            name: 'html',
            content: html,
            contentType: 'html'
        }, {
            title: 'text',
            name: 'text',
            content: html
        },
        {
            title: 'btns',
            name: 'btns',
            content: btns,
            contentType: 'btns'
        }]

    },
    {
        title: 'svg',
        name: 'svg',
        children: [
            {
                title: 'axis',
                name: 'axis',
                children: [
                    {
                        title: 'axis',
                        name: 'axis',
                        content: svgAxis,
                        contentType: 'svg'
                    },
                    {
                        title: 'grid',
                        name: 'grid',
                        content: svgGrid,
                        contentType: 'svg'
                    }, {
                        title: 'polar',
                        name: 'polar',
                        content: svgPolar,
                        contentType: 'svg'
                    }, {
                        title: 'gridPoints',
                        name: 'gridPoints',
                        content: svgGridPoints,
                        contentType: 'svg'
                    }

                ]
            },
            {
                title: 'shape',
                name: 'shape',
                children: [{
                    title: 'circle',
                    name: 'circle',
                    content: svgCircle,
                    contentType: 'svg',
                },
                {
                    title: 'rect',
                    name: 'rect',
                    content: svgRect,
                    contentType: 'svg'
                }, {
                    title: 'lines',
                    name: 'lines',
                    content: svgLines,
                    contentType: 'svg'
                }, {
                    title: 'curves',
                    name: 'curves',
                    content: svgCurves,
                    contentType: 'svg'
                },
                {
                    title: 'sawtooths',
                    name: 'sawtooths',
                    content: svgSawtooths,
                    contentType: 'svg'
                },

                // {
                //     title: 'pattern',
                //     name: 'pattern',
                //     content: svgPattern,
                //     contentType: 'svg'
                // }
            ]
            },
            {
                title: 'polygon',
                name: 'polygon',
                children: [{
                    title: 'triangle',
                    name: 'triangle',
                    content: svgtriangle,
                    contentType: 'svg'
                },
                {
                    title: 'polygon',
                    name: 'polygon',
                    content: svgPolygon,
                    contentType: 'svg'
                },



                {
                    title: 'polygonColor',
                    name: 'polygonColor',
                    content: svgPolygonColor,
                    contentType: 'svg'
                }, {
                    title: 'polygonLink',
                    name: 'polygonLink',
                    content: svgPolygonLink,
                    contentType: 'svg'
                },
                {
                    title: 'ferrisWheel',
                    name: 'ferrisWheel',
                    content: svgFerrisWheel,
                    contentType: 'svg'
                }, {
                    title: 'complex',
                    name: 'complex',
                    content: svgComplex,
                    contentType: 'svg'
                },

                {
                    title: 'ray',
                    name: 'ray',
                    content: svgRay,
                    contentType: 'svg'
                },
                {
                    title: 'rayCurve',
                    name: 'rayCurve',
                    content: svgRayCurve,
                    contentType: 'svg'
                },
                ]
            },
            {
                title: 'fractal',
                name: 'fractal',
                children: [{
                    title: 'edgemirror',
                    name: 'edgemirror',
                    content: svgEdgeMirror,
                    contentType: 'svg'
                },
                {
                    title: 'vertexmirror',
                    name: 'vertexmirror',
                    content: svgVertexMirror,
                    contentType: 'svg'
                },
                {
                    title: 'edgeFractal',
                    name: 'edgeFractal',
                    content: svgEdgeFractal,
                    contentType: 'svg'
                },
                {
                    title: 'vertexFractal',
                    name: 'vertexFractal',
                    content: svgVertexFractal,
                    contentType: 'svg'
                },

                {
                    title: 'edgeMirrorLink',
                    name: 'edgeMirrorLink',
                    content: svgEdgeMirrorLink,
                    contentType: 'svg'
                },
                {
                    title: 'fractalGroup',
                    name: 'fractalGroup',
                    content: svgFractalGroup,
                    contentType: 'svg'
                },
                {
                    title: 'fractalFull',
                    name: 'fractalFull',
                    content: svgFractalFull,
                    contentType: 'svg'
                },
                {
                    title: 'fractalRadio',
                    name: 'fractalRadio',
                    content: svgFractalRadio,
                    contentType: 'svg'
                },
                {
                    title: 'edgeMid',
                    name: 'edgeMid',
                    content: svgEdgeMid,
                    contentType: 'svg'
                },
                {
                    title: 'yanghui',
                    name: 'yanghui',
                    content: svgYangehui,
                    contentType: 'svg'
                }, 
                // {
                //     title: 'gougu',
                //     name: 'gougu',
                //     content: svgGouguTree,
                //     contentType: 'svg'

                // }
            ]
            },
            {
                title: 'curve',
                name: 'curve',
                children: [
                    {
                        title: 'curve',
                        name: 'curve',
                        content: svgCurve,
                        contentType: 'svg'
                    },
                    {
                        title: 'wave',
                        name: 'wave',
                        content: svgWave,
                        contentType: 'svg'
                    },
                    {
                        title: 'sawtooth',
                        name: 'sawtooth',
                        content: svgSawtooth,
                        contentType: 'svg'
                    },
                    {
                        title: 'semicircle',
                        name: 'semicircle',
                        content: svgSemicircle,
                        contentType: 'svg'
                    },
                    {
                        title: 'elliptical',
                        name: 'elliptical',
                        content: svgElliptical,
                        contentType: 'svg'
                    },
                    {
                        title: 'ellipticalLink',
                        name: 'ellipticalLink',
                        content: svgEllipticalLink,
                        contentType: 'svg'
                    },
                    {
                        title: 'sin',
                        name: 'sin',
                        content: svgSin,
                        contentType: 'svg'
                    },
                ]
            },
            {
                title: 'star',
                name: 'transform',
                children: [

                    {
                        title: 'Star5',
                        name: 'Star5',
                        content: svgStar5,
                        contentType: 'svg'
                    },
                    {
                        title: 'Star6',
                        name: 'Star6',
                        content: svgStar6,
                        contentType: 'svg'
                    },
                    {
                        title: 'Star7',
                        name: 'Star7',
                        content: svgStar7,
                        contentType: 'svg'
                    },
                    {
                        title: 'Star8',
                        name: 'Star8',
                        content: svgStar8,
                        contentType: 'svg'
                    },
                    {
                        title: 'Star12',
                        name: 'Star12',
                        content: svgStar12,
                        contentType: 'svg'
                    },
                ]
            }, {
                title: 'transform',
                name: 'transform',
                children: [{
                    title: 'misplaced5',
                    name: 'misplaced5',
                    content: svgMisplaced5,
                    contentType: 'svg'
                }, {
                    title: 'misplaced6',
                    name: 'misplaced6',
                    content: svgMisplaced6,
                    contentType: 'svg'
                }, {
                    title: 'misplaced7',
                    name: 'misplaced7',
                    content: svgMisplaced7,
                    contentType: 'svg'
                }, {
                    title: 'misplaced8',
                    name: 'misplaced8',
                    content: svgMisplaced8,
                    contentType: 'svg'
                }, {
                    title: 'misplaced9',
                    name: 'misplaced9',
                    content: svgMisplaced9,
                    contentType: 'svg'
                },]
            }, {
                title: 'pathpoints',
                name: 'pathpoints',
                children: [{
                    title: 'polygonPath',
                    name: 'polygonPath',
                    content: svgPolygonPath,
                    contentType: 'svg'
                }, {
                    title: 'polygonPath2',
                    name: 'polygonPath2',
                    content: svgPolygonPath2,
                    contentType: 'svg'
                }, {
                    title: 'polygonPath3',
                    name: 'polygonPath3',
                    content: svgPolygonPath3,
                    contentType: 'svg'
                }, {
                    title: 'polygonPath4',
                    name: 'polygonPath4',
                    content: svgPolygonPath4,
                    contentType: 'svg'
                }, {
                    title: 'polygonPath5',
                    name: 'polygonPath5',
                    content: svgPolygonPath5,
                    contentType: 'svg'
                }]
            }, {
                title: 'spiral',
                name: 'spiral',
                children: [
                    {
                        title: 'spiral',
                        name: 'spiral',
                        content: svgSpiral,
                        contentType: 'svg'
                    },
                    // {
                    //     title: 'spiraltriangle',
                    //     name: 'spiraltriangle',
                    //     content: svgSpiraltriangle,
                    //     contentType: 'svg'
                    // },
                    // {
                    //     title: 'spiral2',
                    //     name: 'spiral2',
                    //     content: svgSpiral2,
                    //     contentType: 'svg'
                    // },


                ]

            }, {
                title: 'game',
                name: 'game',
                children: [{
                    title: 'maze',
                    name: 'maze',
                    content: svgMaze,
                    contentType: 'svg'
                }, {
                    title: 'circleMaze',
                    name: 'circleMaze',
                    content: svgCircleMaze,
                    contentType: 'svg'
                }, {
                    title: 'motion',
                    name: 'motion',
                    content: svgMotion,
                    contentType: 'svg'
                }]
            }


        ]
    }, {
        title: 'canvas',
        name: 'canvas',
        content: canvas,
        contentType: 'canvas'
    }

]

export default {
    menu,
    activeName: 'first',
    target: '.main .screen-viewer'
}