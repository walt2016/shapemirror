import { Curve } from '@/types/curve'
import { PathMode } from '@/types/pathMode'
import { Shape } from '@/types/shape'

export const LsystemTree = {
	shape: Shape.LSystemPlant,
	axiom: 'F',
	rules: { F: 'F[+F-F]-F+F' },
	angle: 22,
	r: 20,
	depth: 5,
	wriggle: 0,
	reduction: 1.0,
	color: 'circleColor',
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
}

export const Lsystem_2 = {
	shape: Shape.LSystemPlant,
	r: 30,
	angle: 22,
	depth: 5,
	wriggle: 0,
	reduction: 1.0,
	color: 'circleColor',
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	axiom: 'F',
	rules: { F: 'F[+F-F][-F]' },
}

export const dragonCurve = {
	shape: Shape.LSystem,
	axiom: 'X',
	rules: { X: 'X+YF+', Y: '-FX-Y' },
	angle: 90,
	r: 20,
	depth: 10,
	wriggle: 0,
	reduction: 1.0,
	color: 'circleColor',
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
}

// `// William McWorters Terdragon
export const terdragon = {
	shape: Shape.LSystem,
	axiom: 'F',
	rules: { F: 'F+F-F' },
	angle: 120,
	r: 20,
	depth: 8,
	wriggle: 0,
	reduction: 1.0,
	color: 'circleColor',
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
}

// `// aquatic plant
// axiom: F
// rules:
//   F -> FFc[-F++F]d[+F--F]e++F--F

// color: brown
// direction: [0, 1, 0.5]
// angle: 27
// depth:4
// actions:
//   c => setColor('green')
//   d => setColor('lime')
//   e => setColor('goldenrod')`,

export const aquaticPlant = {
	shape: Shape.LSystemPlant,
	axiom: 'F',
	rules: { F: 'FFc[-F++F]d[+F--F]e++F--F' },
	angle: 27,
	r: 10,
	depth: 4,
	wriggle: 0,
	reduction: 1.0,
	color: 'circleColor',
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
}

// `// Color Mosaic
// axiom: F+F+F+F
// rules:
//   F => dFF+F+cF+F+FF

// color: green
// depth: 3
// angle: 90
// actions:
//   c => setColor('lime')
//   d => setColor('green')`

export const mosaic = {
	shape: Shape.LSystem,
	r: 20,
	angle: 90,
	depth: 3,
	wriggle: 0,
	reduction: 1.0,
	color: 'circleColor',
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	axiom: 'F',
	rules: { F: 'dFF+F+cF+F+FF' },
}

//   `// 3 Blocks
// axiom: F^^F^^F
// rules:
//  F => F-fff^F^^F^^F&&fff-FFF
//  f => fff

// depth: 3
// actions:
//  - => rotate(-90)
//  ^ => rotate(60)
//  & => rotate(-60) `

export const blocks = {
	shape: Shape.LSystem,
	r: 20,
	angle: 90,
	depth: 3,
	wriggle: 0,
	reduction: 1.0,
	color: 'circleColor',
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	axiom: 'F^^F^^F',
	rules: { F: 'F-fff^F^^F^^F&&fff-FFF', f: 'fff' },
}

// `// Leaf
// axiom: Y---Y
// rules:
//  X => F-FF-F--[--X]F-FF-F--F-FF-F--
//  Y => f-F+X+F-fY

// depth: 8
// angle: 60`, `// esum
// axiom: X+X+X+X+X+X+X+X
// rules:
//  X => [F[-X++Y]]
//  Y => [F[-Y--X]]
//  F => F

export const Leaf = {
	shape: Shape.LSystem,
	r: 20,
	angle: 60,
	depth: 8,
	wriggle: 0,
	reduction: 1.0,
	color: 'circleColor',
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	axiom: 'Y---Y',
	rules: { X: 'F-FF-F--[--X]F-FF-F--F-FF-F--', Y: 'f-F+X+F-fY' },
}

// `// Penrose tiling
// axiom: [N]++[N]++[N]++[N]++[N]
// rules:
//   M => OF++PF----NF[-OF----MF]++
//   N => +OF--PF[---MF--NF]+
//   O => -MF++NF[+++OF++PF]-
//   P => --OF++++MF[+PF++++NF]--NF
//   F =>

// depth: 4
// angle: 36

export const penroseTiling = {
	shape: Shape.LSystem,
	r: 20,
	angle: 36,
	depth: 4,
	wriggle: 0,
	reduction: 1.0,
	color: 'circleColor',
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	axiom: '[N]++[N]++[N]++[N]++[N]',
	rules: {
		M: 'OF++PF----NF[-OF----MF]++',
		N: '+OF--PF[---MF--NF]+',
		O: '-MF++NF[+++OF++PF]-',
		P: '--OF++++MF[+PF++++NF]--NF',
	},
}

// poetasters shrub
// axiom: F
// rules:
//  F => Fe[+cFF]Fd[-FF]cF

// color:brown
// direction: [0, 1, 0]
// angle: 322
// depth: 4
// actions:
//   c => setColor('green')
//   d => setColor('lightgreen')
//   e => setColor('brown')

export const shrub = {
	shape: Shape.LSystemPlant,
	r: 5,
	angle: 322,
	depth: 4,
	wriggle: 10,
	reduction: 0.8,
	color: 'circleColor',
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	axiom: 'F',
	rules: {
		F: 'F+-e[+cFF]Fd[-FF]cF',
	},
}

// P. Bourke after Saupe
// axiom: VZFFF
// rules:
//   V -> [+++W][---W]YV
//   W -> +X[-W]Z
//   X -> -W[+X]Z
//   Y -> YZ
//   Z -> [-FcFF][+FdFF]F

// color: green
// depth:8
// direction: [0, 1, 0]
// angle: 20
// actions:
//   c => setColor('lightgreen')
//   d => setColor('lime')

export const saupe = {
	shape: Shape.LSystemPlant,
	r: 10,
	angle: 20,
	depth: 4,
	wriggle: 0,
	reduction: 1.0,
	color: 'circleColor',
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	axiom: 'VZFFF',
	rules: {
		V: '[+++W][---W]YV',
		W: '+X[-W]Z',
		X: '-W[+X]Z',
		Y: 'YZ',
		Z: 'F+-F[-FcFF][+FdFF]F',
	},
}

// another aquatic
// axiom: F
// rules:
//   F => FMNOMBxPNMyO
//   M => e[-F++F++]
//   N => d[+F--F--]
//   O => c++F--F
//   P => d--F++F

// color: brown
// direction: [0, 1, -1]
// angle: 27
// depth:5
// actions:
//   c => setColor('green')
//   d => setColor('lime')
//   e => setColor('goldenrod')
//   x => rotateX(2)
//   y => rotateY(-3)

export const aquatic = {
	shape: Shape.LSystemPlant,
	r: 10,
	angle: 27,
	depth: 4,
	wriggle: 0,
	reduction: 1.0,
	color: 'circleColor',
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	axiom: 'F',
	rules: {
		F: 'FMNOMBxPNMyO',
		M: 'e[-F++F++]',
		N: 'd[+F--F--]',
		O: 'c++F--F',
		P: 'd--F++F',
	},
}

// Bush, P. Bourke
// axiom: F
// rules:
//   F => FF+[c+F-F-F]-[-F+F+dF]

// color: green
// direction: [0, 1, 0]
// angle: 21
// depth:4
// actions:
//   c => setColor('green')
//   d => setColor('lime')

export const bush = {
	shape: Shape.LSystemPlant,
	r: 10,
	angle: 21,
	depth: 4,
	wriggle: 5,
	reduction: 0.9,
	color: 'circleColor',
	oneLine: false,
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	labels: {
		visible: false,
		stroke: 'gray',
		fontSize: 14,
	},
	axiom: 'F',
	rules: {
		F: 'F+-F+[c+F-F-F]-[-F+F+dF]',
	},
	props: {
		stroke: 'red',
		strokeWidth: 1,
		strokeDasharray: 0,
	},
}
