import { Shape } from '@/types/shape'
import { defaultProps } from '../defaultProps'
import { PathMode } from '@/types/pathMode'
export const rose = {
	shape: Shape.Rose,
	n: 200,
	r: 200,
	a: 0,
	w: 4,
	m: 3,
	pathMode: PathMode.LINE_LOOP,
	...defaultProps,
}
