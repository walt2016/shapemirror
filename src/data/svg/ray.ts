import { Fractal } from '@/types/fractal'
import { Shape } from '@/types/shape'

export const ray = {
	shape: Shape.Ray,
	r: 100,
	n: 36,
	fractal: Fractal.VertexMirror,
	axis: { grid: {} },
}

export const rayCurve = {
	shape: Shape.Ray,
	r: 100,
	n: 36,
	fractal: Fractal.VertexMirror,
	axis: { grid: {} },
	radius: {
		shape: Shape.Curve,
	},
	// a:15,
	// orient:true
}
