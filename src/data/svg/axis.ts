import { Shape } from '@/types/shape'

export const axis = {
	shape: Shape.Axis,
	// o: [400, 300],
	r: 50,
	padding: 30,
	sticks: {},
	grid: {
		crossPoints: {},
		cellPoints: {},
	},
}

export const grid = {
	shape: Shape.Grid,
	width: 800,
	height: 600,
	r: 50,
}

export const polar = {
	shape: Shape.Polar,
	width: 800,
	height: 600,
	r: 100,
	n: 12,
}

export const gridPoints = {
	shape: Shape.Grid,
	// o: [400, 300],
	r: 100,
	crossPoints: {
		label: {},
	},
	cellPoints: { label: {} },
}
