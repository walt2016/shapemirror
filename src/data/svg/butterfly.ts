import { Shape } from '@/types/shape'
import { defaultProps } from '../defaultProps'
import { PathMode } from '@/types/pathMode'
import { Curve } from '@/types/curve'
import { Transform } from '@/types/transform'
export const butterfly = {
	shape: Shape.RingShape,
	n: 7,
	r: 3,
	a: -38,
	m: 50,
	transform: Transform.None,
	pathMode: PathMode.TRIANGLES,
	...defaultProps,
	curve: {
		type: Curve.LeftSemicircle,
		skew: 0,
		amplitude: 1,
		depth: 1,
		controlPointsVisible: false,
	},
}

export const butterfly2 = {
	shape: Shape.RingShape,
	n: 7,
	r: 3,
	a: -38,
	m: 50,
	transform: Transform.None,
	pathMode: PathMode.TRIANGLES,
	...defaultProps,
	curve: {
		type: Curve.RightAngle,
		skew: 0,
		amplitude: 1,
		depth: 1,
		controlPointsVisible: false,
	},
}

export const butterfly3 = {
	shape: Shape.RingShape,
	n: 7,
	r: 3,
	a: -38,
	m: 50,
	transform: Transform.None,
	pathMode: PathMode.TRIANGLES,
	...defaultProps,
	curve: Curve.Centripetal,
}

export const butterfly4 = {
	shape: Shape.RingShape,
	n: 7,
	r: 3,
	a: -38,
	m: 50,
	transform: Transform.None,
	pathMode: PathMode.TRIANGLES,
	...defaultProps,
	curve: {
		type: Curve.KochCurve,
		skew: 0,
		amplitude: 1,
		depth: 1,
		controlPointsVisible: false,
	},
}

export const butterfly5 = {
	shape: Shape.RingShape,
	n: 7,
	r: 3,
	a: -38,
	m: 50,
	transform: Transform.None,
	pathMode: PathMode.TRIANGLES,
	...defaultProps,
	curve: Curve.LeftAngle,
}
