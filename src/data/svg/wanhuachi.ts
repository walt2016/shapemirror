import { Shape } from '@/types/shape'

export const wanhuachi = {
	shape: Shape.Wanhuachi,
	r: 200,
	r2: 99,
	n: 10,
	m: 100,
}
