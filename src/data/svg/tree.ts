import { Shape } from '@/types/shape'
import { defaultProps } from '../defaultProps'
import { PathMode } from '@/types/pathMode'
export const tree = {
	shape: Shape.Tree,
	r: 100,
	n: 3,
	a: -90,
	depth: 9,
	attenuation: 0.618,
	wriggle: 10,
	pathMode: PathMode.LINES,
	...defaultProps,
}

export const treeMode = {
	shape: Shape.TreeMode,
	r: 50,
	n: 3,
	a: -90,
	depth: 9,
	attenuation: 0.618,
	wriggle: 10,
	pathMode: PathMode.TRIANGLES,
	...defaultProps,
}
