import { Shape } from '@/types/shape'
import { defaultProps } from '../defaultProps'
import { Curve } from '@/types/curve'
import { Color } from '@/types/color'
export const fibonacci = {
	shape: Shape.Fibonacci,
	r: 1,
	a: 0,
	n: 15,
	m: 2,
	increasedAgnle: 90,
	color: Color.ColorCircle,
	...defaultProps,
	curve: {
		type: Curve.FibonacciCurve,
		skew: 0,
		amplitude: 1,
		depth: 1,
		increasedAgnle: 90,
	},
}

export const fibonacciCircle = {
	shape: Shape.FibonacciCircle,
	r: 1,
	a: 0,
	n: 4,
	m: 15,
	// n: 4,
	color: Color.ColorCircle,
	...defaultProps,
}
