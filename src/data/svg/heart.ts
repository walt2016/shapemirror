import { Shape } from '@/types/shape'
import { defaultProps } from '../defaultProps'
import { PathMode } from '@/types/pathMode'
export const heart = {
	shape: Shape.Heart,
	curveShape: 'peachHeart',
	n: 200,
	r: 15,
	a: 0,
	m: 4,
	pathMode: PathMode.LINE_LOOP,
	...defaultProps,
}
