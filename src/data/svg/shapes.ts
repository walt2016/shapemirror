import { translate, plainMatrix } from '@/math'
import { gridCellPoints } from '@/algorithm/grid'
import { colorCircle } from '@/color'
import { deepClone } from '@/utils'
import { pattern } from './pattern'
import { Shape } from '@/types/shape'

let r = 25
let gps = plainMatrix(gridCellPoints())
let len = gps.length
let colors = colorCircle(len, 0.5)
let colorsLen = colors.length
let arr = gps.map((t, i) => {
	let o = t
	let points = [translate(t, [r, r]), translate(t, [-r, -r])]
	return {
		shape: Shape.Circle,
		r,
		o,
		points,
		props: {
			fill: colors[i % colorsLen],
		},
	}
})
export const shapes = deepClone(arr)
export const circles = deepClone(arr)
export const rects = deepClone(arr).map(t => {
	return {
		...t,
		shape: Shape.Rect,
	}
})

export const lines = deepClone(arr).map(t => {
	return {
		...t,
		shape: Shape.Line,
		props: {
			stroke: t.props.fill,
			strokeWidth: 2,
		},
	}
})

export const curves = deepClone(arr).map(t => {
	return {
		...t,
		shape: Shape.Curve,
		props: {
			stroke: t.props.fill,
			strokeWidth: 2,
		},
	}
})

export const sawtooths = deepClone(arr).map(t => {
	return {
		...t,
		shape: Shape.Sawtooth,
		props: {
			// stroke: t.props.fill,
			// strokeWidth: 2
		},
	}
})

let pt = pattern[0]
export const circlesPattern = deepClone(circles).map(t => {
	return {
		...t,
		pattern: pt.id,
	}
})

export const rectsPattern = deepClone(rects).map(t => {
	return {
		...t,
		pattern: pt.id,
	}
})
