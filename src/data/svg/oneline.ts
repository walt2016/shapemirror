import { Shape } from '@/types/shape'
import { defaultProps } from '../defaultProps'
import { PathMode } from '@/types/pathMode'
import { Mirror } from '@/types/mirror'
import { Curve } from '@/types/curve'
import { Transform } from '@/types/transform'
import { Color } from '@/types/color'

export const oneline = {
	shape: Shape.Oneline,
	n: 12,
	r: 100,
	a: 0,
	pathMode: PathMode.LINE_LOOP,
	curve: Curve.None,
	props: {
		stroke: 'red',
		fill: 'none',
		strokeWidth: 1,
	},
	transform: Transform.None,
	...defaultProps,
	mirror: {
		type: Mirror.Edge,
		scale: 0.3,
		oneline: true,
		onelineOffset: 1,
		color: Color.ColorCircle,
	},
}
