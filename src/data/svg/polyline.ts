import { Shape } from '@/types/shape'

export const isometric = {
	shape: Shape.Isometric,
	r: 200,
	n: 66,
	a: 101,
	vertex: {},
}
