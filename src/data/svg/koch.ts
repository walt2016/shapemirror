import { Shape } from '@/types/shape'
import { defaultProps } from '../defaultProps'
import { PathMode } from '@/types/pathMode'
export const koch = {
	shape: Shape.Koch,
	r: 200,
	n: 3,
	a: 0,
	depth: 4,
	skew: 0,
	amplitude: 1,
	m: 3,
	pathMode: PathMode.LINE_LOOP,

	...defaultProps,
}

export const koch2 = {
	shape: Shape.Koch,
	r: 200,
	n: 4,
	a: 0,
	depth: 4,
	skew: 0,
	amplitude: 1,
	m: 3,
	pathMode: PathMode.LINE_LOOP,
	...defaultProps,
}

export const koch3 = {
	shape: Shape.Koch,
	r: 200,
	n: 5,
	a: 0,
	depth: 4,
	skew: 0,
	amplitude: 1,
	m: 3,
	pathMode: PathMode.LINE_LOOP,
	...defaultProps,
}

export const koch4 = {
	shape: Shape.Koch,
	r: 200,
	n: 6,
	a: 0,
	depth: 4,
	skew: 0,
	amplitude: 1,
	m: 3,
	pathMode: PathMode.LINE_LOOP,
	...defaultProps,
}

export const koch5 = {
	shape: Shape.Koch,
	r: 200,
	n: 7,
	a: 0,
	depth: 4,
	skew: 0,
	amplitude: 1,
	m: 3,
	pathMode: PathMode.LINE_LOOP,
	...defaultProps,
}

export const koch6 = {
	shape: Shape.Koch,
	r: 200,
	n: 12,
	a: 0,
	depth: 4,
	skew: 0,
	amplitude: 1,
	m: 3,
	pathMode: PathMode.LINE_LOOP,
	...defaultProps,
}

export const koch7 = {
	shape: Shape.Koch,
	r: 200,
	n: 6,
	a: 0,
	depth: 4,
	skew: 180,
	amplitude: 1,
	m: 3,
	pathMode: PathMode.LINE_LOOP,
	...defaultProps,
}

export const koch8 = {
	shape: Shape.Koch,
	r: 200,
	n: 5,
	a: 0,
	depth: 4,
	skew: 180,
	amplitude: 1,
	m: 3,
	pathMode: PathMode.LINE_LOOP,
	...defaultProps,
}

export const koch9 = {
	shape: Shape.Koch,
	r: 200,
	n: 4,
	a: 0,
	depth: 4,
	skew: 180,
	amplitude: 1,
	m: 3,
	pathMode: PathMode.LINE_LOOP,
	...defaultProps,
}

export const koch10 = {
	shape: Shape.Koch,
	r: 200,
	n: 3,
	a: 0,
	depth: 4,
	skew: 180,
	amplitude: 1,
	m: 3,
	pathMode: PathMode.LINE_LOOP,
	...defaultProps,
}
