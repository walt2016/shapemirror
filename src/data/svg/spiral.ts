import { Shape } from '@/types/shape'
import { defaultProps } from '../defaultProps'
import { Color } from '@/types/color'
export const spiral = {
	shape: Shape.Spiral,
	n: 200,
	r: 50,
	a: 0,
	v: 1,
	w: 5,
	m: 2,
	color: Color.ColorCircle,
	...defaultProps,
}

// export const spiraltriangle = {
//     shape: Shape.Spiral,
//     r: 50,
//     n: 36,
//     m: 5,
//     // vertex: {
//     //     shape: 'triangle',
//     //     r: 30
//     // },
//     // radius: {}

//     ...defaultProps
// }

// export const spiral2 = {
//     shape: Shape.Spiral,
//     r: 50,
//     n: 36,
//     m: 5,
//     a: 180,
//     // vertex: {},
//     // radius: {}
//     ...defaultProps
// }

// export const twoSpiral = {
//     shape: 'twoSpiral',
//     r: 50,
//     n: 36,
//     m: 5,
//     a: 0,
//     // vertex: {},
//     // radius: {}
//     ...defaultProps
// }
