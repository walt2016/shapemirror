import { PathMode } from '@/types/pathMode'
import { defaultProps } from '../defaultProps'
import { Shape } from '@/types/shape'
let props = {
	bottomControlGear: 'middle',
	// skew: 0,
	amplitude: 1,
	pathMode: PathMode.LINE_STRIP,
	...defaultProps,
}
export const sierpinski = {
	shape: Shape.Sierpinski,
	r: 200,
	n: 3,
	a: 0,
	depth: 4,
	...props,
}
export const sierpinski2 = {
	shape: Shape.Sierpinski,
	r: 200,
	n: 4,
	a: 0,
	depth: 4,
	...props,
}

export const sierpinski3 = {
	shape: Shape.Sierpinski,
	r: 200,
	n: 5,
	a: 0,
	depth: 4,
	...props,
}

export const sierpinski4 = {
	shape: Shape.Sierpinski,
	r: 200,
	n: 6,
	a: 0,
	depth: 4,
	...props,
}

export const sierpinski5 = {
	shape: Shape.Sierpinski,
	r: 200,
	n: 12,
	a: 0,
	depth: 4,
	...props,
}

export const sierpinski6 = {
	shape: Shape.SierpinskiBezier,
	r: 200,
	n: 12,
	a: 0,
	depth: 4,
	...props,
}

export const sierpinski7 = {
	shape: Shape.SierpinskiBezier,
	r: 200,
	n: 12,
	a: 0,
	depth: 3,
	...props,
}

export const sierpinski8 = {
	shape: Shape.SierpinskiBezier,
	r: 200,
	n: 12,
	a: 0,
	depth: 2,
	...props,
}

export const sierpinski9 = {
	shape: Shape.SierpinskiBezier,
	r: 200,
	n: 12,
	a: 0,
	depth: 1,
	...props,
}
