import { Shape } from '@/types/shape'

export const leaves = {
	shape: Shape.Leaves,
	n: 6,
	r: 100,
	a: 0,
	assemble: false,
}

export const triangleLeaf = {
	shape: Shape.TriangleLeaf,
	n: 6,
	r: 100,
	a: 0,
	assemble: false,
	vertex: {},
}
