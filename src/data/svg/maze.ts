import { Shape } from '@/types/shape'
import { defaultProps } from '../defaultProps'
import { PathMode } from '@/types/pathMode'
import { Color } from '@/types/color'
export const maze = {
	shape: Shape.Maze,
	m: 10,
	n: 10,
	// r: 50,
	// height: 600,
	// width: 800,
	// o: [400, 300],
	padding: 30,
	pathMode: PathMode.LINE_STRIP,
	...defaultProps,
	color: {
		type: Color.ColorCircle,
		fill: 'none',
		alpha: 1,
	},
}

export const circleMaze = {
	shape: Shape.CircleMaze,
	m: 10,
	n: 10,
	padding: 30,
	pathMode: PathMode.LINE_STRIP,
	...defaultProps,
	color: {
		type: Color.ColorCircle,
		fill: 'none',
		alpha: 1,
	},
}

export const waveMaze = {
	shape: Shape.WaveMaze,
	m: 10,
	n: 10,

	padding: 30,
	pathMode: PathMode.LINE_STRIP,
	...defaultProps,
	color: {
		type: Color.ColorCircle,
		fill: 'none',
		alpha: 1,
	},
}
