import { Fractal } from '@/types/fractal'
import { Shape } from '@/types/shape'
import { Transform } from '@/types/transform'

export const misplaced5 = {
	shape: Shape.Polygon,
	r: 100,
	n: 5,
	radius: {},
	centre: {},
	vertex: {},
	transform: Transform.Misplaced,
	fractal: Fractal.VertexMirror,
	orient: true,
}

export const misplaced6 = {
	shape: Shape.Polygon,
	r: 100,
	n: 6,
	radius: {},
	centre: {},
	vertex: {},
	transform: Transform.Misplaced,
	fractal: Fractal.VertexMirror,
}

export const misplaced7 = {
	shape: Shape.Polygon,
	r: 100,
	n: 7,
	radius: {},
	centre: {},
	vertex: {},
	transform: Transform.Misplaced,
	fractal: Fractal.VertexMirror,
}

export const misplaced8 = {
	shape: Shape.Polygon,
	r: 100,
	n: 8,
	radius: {},
	centre: {},
	vertex: {},
	transform: Transform.Misplaced,
	fractal: Fractal.VertexMirror,
}

export const misplaced9 = {
	shape: Shape.Polygon,
	r: 100,
	n: 9,
	radius: {},
	centre: {},
	vertex: {},
	transform: Transform.Misplaced,
	fractal: Fractal.VertexMirror,
}
