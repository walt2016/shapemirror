import { Shape } from '@/types/shape'

export const pattern = [
	{
		shape: Shape.Chequer,
		id: 'chequer',
		r: 10,
	},
	{
		shape: Shape.Chequer,
		id: 'chequer2',
		r: 20,
	},

	{
		shape: Shape.Stripe,
		id: 'stripe',
		r: 10,
	},
	{
		shape: Shape.Stripe,
		id: 'stripe2',
		r: 5,
	},
	{
		shape: Shape.DiagonalStripe,
		id: 'diagonalStripe',
		r: 10,
	},
	{
		shape: Shape.Gradient,
		id: 'gradient',
	},
	{
		shape: Shape.Radial,
		id: 'radial',
	},
	{
		shape: Shape.Dot,
		id: 'dot',
		r: 5,
	},
	{
		shape: Shape.Dot,
		id: 'dot2',
		r: 10,
	},
]
