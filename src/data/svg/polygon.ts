import { Shape } from '@/types/shape'

export const triangle = {
	shape: Shape.Triangle,
	r: 200,
	centre: {
		shape: Shape.Circle,
		r: 10,
	},
	vertex: {
		shape: Shape.Triangle,
		r: 20,
	},
}

export const polygon = {
	shape: Shape.Polygon,
	r: 200,
	n: 6,
	centre: {
		shape: Shape.Circle,
		r: 10,
	},
	vertex: {
		shape: Shape.Triangle,
		r: 20,
	},
	radius: {},
	excircle: {},
	incircle: {},
	axis: {
		r: 50,
		sticks: {},
		grid: {},
	},
	props: {
		fill: 'none',
		stroke: 'black',
	},
}

export const polygonColor = {
	shape: Shape.Polygon,
	r: 200,
	n: 6,
	centre: {
		shape: Shape.Circle,
		r: 10,
		props: {
			fill: 'red',
		},
	},
	vertex: {
		shape: Shape.Polygon,
		r: 20,
		n: 6,
		props: {
			fill: 'green',
		},
	},
	radius: {
		shape: Shape.Line,

		props: {
			stroke: 'purple',
			strokeWidth: 2,
		},
	},
	excircle: {
		props: {
			stroke: 'red',
		},
	},
	incircle: {},
}

export const polygonLink = {
	shape: Shape.Polygon,
	r: 200,
	n: 12,
	links: {
		visible: true,
		stroke: 'red',
		strokeWidth: 1,
		strokeDasharray: 0,
		crossPoints: false,
	},
	props: {
		stroke: 'blue',
		strokeWidth: 3,
	},
	vertex: {},
}

export const ferrisWheel = {
	shape: Shape.Polygon,
	r: 200,
	n: 24,
	centre: {
		shape: Shape.Circle,
		r: 10,
		props: {
			fill: 'red',
		},
	},
	vertex: {
		shape: Shape.Polygon,
		r: 20,
		n: 6,
		props: {
			fill: 'green',
		},
	},
	radius: {
		shape: Shape.Line,

		props: {
			stroke: 'purple',
			strokeWidth: 2,
		},
	},
	excircle: {},
	incircle: {},
}
