import { Shape } from '@/types/shape'

export const polygonPath = {
	shape: Shape.Polygon,
	r: 100,
	n: 6,
	pathPoints: {},
}

export const polygonPath2 = {
	shape: Shape.Polygon,
	r: 100,
	n: 3,
	pathPoints: {
		shape: 'ray',
		n: 3,
		r: 50,
	},
	axis: { sticks: {}, grid: {} },
}

export const polygonPath3 = {
	shape: Shape.Polygon,
	r: 100,
	n: 160,
	pathPoints: {
		shape: Shape.Polygon,
		n: 4,
		r: 50,
		m: 3,
		orient: false,
	},
	axis: { sticks: {} },
}

export const polygonPath4 = {
	shape: Shape.Polygon,
	r: 100,
	n: 160,
	pathPoints: {
		shape: Shape.Polygon,
		n: 4,
		r: 50,
		m: 3,
		orient: true,
	},
	axis: { sticks: {} },
}

export const polygonPath5 = {
	shape: Shape.Polygon,
	r: 100,
	n: 160,
	pathPoints: {
		shape: Shape.Polygon,
		n: 4,
		r: 100,
		m: 3,
		orient: true,
	},
	axis: { sticks: {} },
}
