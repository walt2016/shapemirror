import { Color } from '@/types/color'
import { Shape } from '@/types/shape'
import { Transform } from '@/types/transform'

export const gridRect = {
	shape: Shape.GridRect,
	r: 25,
}

export const gridPolygon = {
	shape: Shape.GridPolygon,
	r: 25,
	n: 6,
	transform: Transform.Normal,
	color: Color.ColorCircle,
}

// export const gridPolygonTransform = {
//     shape: 'gridPolygon',
//     r: 25,
//     n: 6,
//     transform: 'normal'
// }
