import { MotionType } from '@/types/motion'
import { Shape } from '@/types/shape'

export const motionPath = {
	shape: Shape.Polygon,
	// o: [400, 300],
	r: 100,
	n: 2,
	dur: '1s',
	props: {
		fill: 'none',
		stroke: 'blue',
	},
}
export const motion = {
	shape: Shape.Star,
	// o: [400, 300],
	r: 100,
	n: 5,
	vertex: {
		props: {
			fill: 'blue',
			stroke: 'none',
		},
	},
	motion: {
		// path: motionPath,
		dur: '4s',
		type: MotionType.Dashoffset,
		'stroke-dasharray': '10',
	},
	props: {
		fill: 'none',
		stroke: 'red',
		// 'stroke-dasharray':'50,50',
		// 'stroke-dashoffset':'50'
	},
}
