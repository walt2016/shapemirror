import { Shape } from '@/types/shape'
import { defaultProps } from '../defaultProps'
import { PathMode } from '@/types/pathMode'
export const peano = {
	shape: Shape.Peano,
	r: 100,
	n: 4,
	depth: 4,
	a: 0,
	pathMode: PathMode.LINE_STRIP,
	noseToTail: true,
	...defaultProps,
}

export const peano2 = {
	shape: Shape.Peano,
	r: 100,
	n: 4,
	depth: 4,
	a: 0,
	pathMode: PathMode.CURVE_STRIP,
	noseToTail: true,
	...defaultProps,
}

export const peano3 = {
	shape: Shape.Peano,
	r: 100,
	n: 4,
	depth: 4,
	a: 0,
	pathMode: PathMode.BEZIER_STRIP,
	noseToTail: true,
	...defaultProps,
}
