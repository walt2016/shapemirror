import { IPolarShape, Shape } from '@/types/shape'
import { defaultProps } from '../defaultProps'
import { PathMode } from '@/types/pathMode'
import { Curve } from '@/types/curve'
import { Transform } from '@/types/transform'
import { Color } from '@/types/color'

export const polarShape: IPolarShape = {
	shape: Shape.PolarShape,
	n: 12,
	r: 200,
	a: 0,
	m: 4,
	sweepFlag: true,
	transform: Transform.None,
	pathMode: PathMode.LINE_LOOP,
	// curve: Curve.None,
	// props: {
	//     stroke: 'red',
	//     fill: 'none',
	//     strokeWidth: 1
	// },
	// layout:{
	//     type:'0',
	//     angle:'a'
	// },
	...defaultProps,
}

export const spiralShape = {
	shape: Shape.SpiralShape,
	n: 12,
	r: 50,
	a: 0,
	m: 5,
	transform: Transform.None,
	pathMode: PathMode.LINE_LOOP,
	curve: Curve.None,
	props: {
		stroke: 'red',
		fill: 'none',
		strokeWidth: 1,
	},
	...defaultProps,
}

export const ringShape = {
	shape: Shape.RingShape,
	n: 12,
	r: 50,
	a: 0,
	m: 5,
	transform: Transform.None,
	pathMode: PathMode.LINE_LOOP,
	curve: Curve.None,
	props: {
		stroke: 'red',
		fill: 'none',
		strokeWidth: 1,
	},
	color: {
		type: Color.ColorCircle,
		alpha: 0.5,
		fill: true,
	},
	...defaultProps,
}

export const rayShape = {
	shape: Shape.RayShape,
	n: 12,
	r: 50,
	a: 0,
	m: 5,
	transform: Transform.None,
	pathMode: PathMode.LINE_LOOP,
	curve: Curve.None,
	props: {
		stroke: 'red',
		fill: 'none',
		strokeWidth: 1,
	},
	...defaultProps,
}

export const isometricShape = {
	shape: Shape.IsometricShape,
	n: 12,
	r: 200,
	a: 101,
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	props: {
		stroke: 'red',
		fill: 'none',
		strokeWidth: 1,
	},
	// mirror: 'none',
	// vertex: {},
	// centre: {},
	// excircle: false,
	// incircle: false,
	// labels: false,
	// linkCross: false,
	transform: Transform.None,
	...defaultProps,
	mirror: null,
}

export const gridShape = {
	shape: Shape.GridShape,
	r: 50,
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	props: {
		stroke: 'red',
		fill: 'none',
		strokeWidth: 1,
	},
	// mirror: 'none',
	// vertex: {},
	// centre: {},
	// excircle: false,
	// incircle: false,
	// labels: false,
	// linkCross: false,
	transform: Transform.None,
	...defaultProps,
	mirror: null,
}

export const polygonShape = {
	shape: Shape.PolygonShape,
	n: 6,
	r: 100,
	a: 0,
	sweepFlag: true,
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	props: {
		stroke: 'red',
		fill: 'none',
		strokeWidth: 1,
	},
	// mirror: 'none',
	// vertex: {},
	// centre: {},
	// excircle: false,
	// incircle: false,
	// labels: false,
	// linkCross: false,
	transform: Transform.None,
	...defaultProps,
	mirror: null,
}
