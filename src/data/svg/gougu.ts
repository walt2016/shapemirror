import { PathMode } from '@/types/pathMode'
import { defaultProps } from '../defaultProps'
import { Shape } from '@/types/shape'
import { Transform } from '@/types/transform'
import { Curve } from '@/types/curve'
import { Mirror } from '@/types/mirror'
import { Color } from '@/types/color'
export const gougu = {
	shape: Shape.Gougu,
	r: 50,
	n: 4,
	depth: 8,
	a: 180,
	wriggle: 30,
	color: {
		type: Color.ColorCircle,
		fill: 'gray',
		alpha: 0.5,
	},
	transform: Transform.None,
	pathMode: PathMode.LINE_LOOP,
	...defaultProps,
	mirror: {
		type: Mirror.None,
		scale: 1,
		// borderIndex: 0,
		oneline: false,
		onelineOffset: 1,
		color: Color.ColorCircle,
		alpha: 0.5,
		fill: 'gray',
	},
}

export const gougu2 = {
	shape: Shape.Gougu,
	r: 50,
	n: 6,
	depth: 8,
	a: 180,
	wriggle: 30,
	transform: Transform.None,
	pathMode: PathMode.LINE_LOOP,
	color: {
		type: Color.ColorCircle,
		fill: 'gray',
		alpha: 0.5,
	},
	...defaultProps,
	curve: {
		type: Curve.Centripetal,
		angle: 90,
		radio: 2,
	},
	mirror: {
		type: Mirror.None,
		scale: 1,
		// borderIndex: 0,
		oneline: false,
		onelineOffset: 1,
		color: Color.ColorCircle,
		alpha: 0.5,
		fill: 'gray',
	},
}

export const gougu3 = {
	shape: Shape.Gougu,
	r: 50,
	n: 5,
	depth: 8,
	a: 180,
	wriggle: 30,
	transform: Transform.None,
	color: {
		type: Color.ColorCircle,
		fill: 'gray',
		alpha: 0.5,
	},
	pathMode: PathMode.LINE_LOOP,
	...defaultProps,
	curve: {
		type: Curve.LeftAngle,
		angle: 90,
		radio: 2,
	},
	mirror: {
		type: 'none',
		scale: 1,
		// borderIndex: 0,
		oneline: false,
		onelineOffset: 1,
		color: Color.ColorCircle,
		alpha: 0.5,
		fill: 'gray',
	},
}

export const gougu4 = {
	shape: Shape.Gougu,
	r: 50,
	n: 4,
	depth: 8,
	a: 180,
	wriggle: 60,
	transform: Transform.None,
	color: {
		type: Color.ColorCircle,
		fill: 'gray',
		alpha: 0.5,
	},
	pathMode: PathMode.LINE_LOOP,
	...defaultProps,
	curve: {
		type: Curve.DoubleCentripetal,
		angle: 90,
		radio: 1,
	},
	mirror: {
		type: Mirror.None,
		scale: 1,
		// borderIndex: 0,
		oneline: false,
		onelineOffset: 1,
		color: Color.ColorCircle,
		alpha: 0.5,
		fill: 'gray',
	},
}
