import { Color } from '@/types/color'
import { Curve } from '@/types/curve'
import { Fractal } from '@/types/fractal'
import { Mirror } from '@/types/mirror'
import { PathMode } from '@/types/pathMode'
import { Shape } from '@/types/shape'

export const edgeFractal = {
	shape: Shape.Polygon,
	r: 100,
	n: 6,
	a: 0,
	fractal: Fractal.EdgeFractal,
	centre: {
		shape: Shape.Circle,
		r: 3,
		props: {
			fill: 'red',
			stroke: 'none',
		},
	},
	props: {
		stroke: 'blue',
	},
	axis: { sticks: {} },
}

export const vertexFractal = {
	shape: Shape.Polygon,
	r: 100,
	n: 6,
	a: 0,
	fractal: Fractal.VertexFractal,
	centre: {
		shape: Shape.Circle,
		r: 3,
		props: {
			fill: 'red',
			stroke: 'none',
		},
	},
	props: {
		stroke: 'green',
	},
	axis: { sticks: {} },
}

export const edgeMirror = {
	shape: Shape.Polygon,
	r: 100,
	n: 6,
	a: 0,
	fractal: Fractal.EdgeMirror,
	centre: {
		shape: Shape.Circle,
		r: 3,
		props: {
			fill: 'red',
			stroke: 'none',
		},
	},
	axis: {
		r: 50,
		sticks: {},
	},
}

export const vertexMirror = {
	shape: Shape.Polygon,
	r: 100,
	n: 6,
	a: 0,
	fractal: Fractal.VertexMirror,
	centre: {
		shape: Shape.Circle,
		r: 3,
		props: {
			fill: 'red',
			stroke: 'none',
		},
	},
	props: {
		stroke: 'purple',
	},
	axis: {
		r: 50,
		sticks: {},
	},
}

export const edgeMirrorLink = {
	shape: Shape.Polygon,
	r: 100,
	n: 10,
	a: 0,
	links: {},
	fractal: Fractal.EdgeMirror,
	props: {
		stroke: 'red',
	},
}

export const fractalGroup = {
	shape: Shape.Polygon,
	r: 100,
	n: 12,
	a: 0,
	props: {
		stroke: 'purple',
	},
	fractal: ['vertexFractal', 'edgeFractal'],
}

export const fractalFull = {
	shape: Shape.Polygon,
	r: 100,
	n: 36,
	a: 0,
	vertex: {
		shape: Shape.Circle,
		r: 1,
	},
	fractal: ['vertexMirror', 'edgeMirror', 'vertexFractal', 'edgeFractal'], //'vertexFractal',
}

export const fractalRadio = {
	shape: Shape.Polygon,
	r: 100,
	n: 100,
	// props: {
	//     stroke: 'purple'
	// },
	fractal: [
		{
			type: Fractal.VertexFractal,
			radio: 0.5,
		},
		{
			type: Fractal.VertexMirror,
			radio: 0.5,
		},
	],
}

export const edgeMid = {
	shape: Shape.Polygon,
	r: 200,
	n: 6,
	fractal: {
		type: Fractal.EdgeMid,
		level: 20,
	},
	radius: {},
	// radius:{
	//     curve:{
	//      type:   'curve',
	//      orient:true
	//     }
	// }
	// label: {
	// }
}

export const yanghui = {
	shape: Shape.Polygon,
	r: 200,
	n: 4,
	fractal: [
		{
			type: Fractal.Yanghui,
			level: 5,
		},
		{
			type: Fractal.EdgeMid,
			level: 10,
		},
	],
	vertex: {
		r: 2,
	},
	props: {
		stroke: 'red',
		// fill:'rgba(255,0,0,10)'
	},
}

export const gougu = {
	shape: Shape.Gougu,
	r: 100,
	n: 2,
	fractal: {
		fractalLevel: 6,
	},
}

export const squareFractal = {
	shape: Shape.SquareFractal,
	r: 100,
	// n: 2,
	pathMode: PathMode.LINE_LOOP,
	curve: Curve.None,
	fractalLevel: 5,
	assemble: false,
	color: Color.ColorCircle,
}

export const sierpinskiTriangle = {
	shape: Shape.SierpinskiTriangle,
	r: 200,
	depth: 6,
	a: 0,
	k: 1,
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	props: {
		stroke: 'red',
		fill: 'none',
		strokeWidth: 1,
	},
}

export const koch = {
	shape: Shape.Koch,
	r: 200,
	n: 3,
	depth: 4,
	a: 0,
	// k: 1,
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	props: {
		stroke: 'red',
		fill: 'none',
	},
}

export const mitsubishi = {
	shape: Shape.Mitsubishi,
	r: 200,
	n: 3,
	depth: 0,
	a: 0,
	// k: 1,
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	props: {
		stroke: 'red',
		fill: 'none',
	},
}
export const hexaflake = {
	shape: Shape.Hexaflake,
	r: 100,
	n: 3,
	depth: 4,
	a: 0,
	// pathMode: PathMode.LINE_STRIP,
	// curve: Curve.None,
	// k: 1,
	props: {
		stroke: 'red',
		fill: 'none',
	},
}

export const tree = {
	shape: Shape.Tree,
	r: 50,
	n: 3,
	depth: 9,
	a: -90,
	k: 0.618,
	wriggle: 10,
	treeCurve: 'QQ',
	// pathMode: PathMode.LINE_STRIP,
	// curve: Curve.None,
	color: Color.ColorCircle,
	alpha: 0.9,
	// props: {
	//     stroke: 'red',
	//     fill: 'none'
	// }
}

export const plant = {
	shape: Shape.Plant,
	r: 50,
	n: 3,
	depth: 9,
	a: -90,
	k: 0.618,
	wriggle: 100,
	treeCurve: 'QQ',
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	color: Color.ColorCircle,
	alpha: 0.9,
	// labels:{}
	// fill:{}
	props: {
		// stroke: 'red',
		// fill: 'none',
		strokeWidth: 1,
		'stroke-dasharray': 0,
		'stroke-dashoffset': 0,
	},
	motion: {
		type: Mirror.None,
		speed: 1,
	},
}

export const treeCircle = {
	shape: Shape.TreeCircle,
	r: 100,
	n: 3,
	depth: 9,
	a: -90,
	k: 0.5,
	random: 10,
	color: Color.ColorCircle,
	alpha: 0.9,
	// props: {
	//     stroke: 'red',
	//     fill: 'none'
	// }
}

export const dragon = {
	shape: Shape.Dragon,
	r: 200,
	n: 2,
	depth: 6,
	a: 135,
	k: 0.5,
	random: 10,
	// treeCurve: 'QQ',
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	// mirror: 'none',
	// color: Color.ColorCircle,
	// alpha: 0.9,
	labels: false,
	props: {
		stroke: 'red',
		fill: 'none',
	},
}

export const peano = {
	shape: Shape.Peano,
	r: 200,
	n: 2,
	depth: 1,
	a: 135,
	k: 0.5,
	random: 10,
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	labels: false,
	props: {
		stroke: 'red',
		fill: 'none',
	},
}
