import { Fractal } from '@/types/fractal'
import { Shape } from '@/types/shape'

export const curve = {
	shape: Shape.Polygon,
	r: 200,
	n: 36,
	a: 0,
	edge: {
		// curve: {
		//     shape: Shape.Curve,
		//     orient: true
		// }
		shape: Shape.Curve,
		orient: true,
	},
	radius: {
		shape: Shape.Curve,
		orient: true,
	},
	centre: {},
	// label: {}
}

export const wave = {
	shape: Shape.Polygon,
	r: 200,
	n: 100,
	a: 0,
	edge: {
		shape: Shape.Wave,
		origin: true,
	},
	radius: {
		curve: Shape.Wave,
	},
	centre: {},
	// label: {}
}

export const sawtooth = {
	shape: Shape.Polygon,
	r: 200,
	n: 100,
	a: 0,
	edge: {
		shape: Shape.Sawtooth,
		origin: true,
	},
	radius: {
		shape: Shape.Sawtooth,
	},
	centre: {},
	// label: {}
}

export const semicircle = {
	shape: Shape.Polygon,
	r: 100,
	n: 6,
	a: 0,
	edge: {
		// shape: 'semicircle'
	},
	// radius:{
	//     shape: 'semicircle'
	// },
	centre: {},
	radius: {},
	// label: {

	// },
	fractal: Fractal.VertexMirror,
	props: {
		stroke: 'red',
		strokeWidth: 1,
		strokeDasharray: 0,
	},
}

export const elliptical = {
	shape: Shape.Polygon,
	r: 100,
	n: 4,
	a: 0,
	edge: {
		// shape: 'elliptical'
	},
	radius: {
		// shape: 'elliptical'
	},
	centre: {},
	fractal: Fractal.VertexMirror,
	links: {},
	props: {
		stroke: 'red',
		strokeWidth: 1,
		strokeDasharray: 0,
		// stroke: 'red'
	},
}

export const ellipticalLink = {
	shape: Shape.Polygon,
	r: 80,
	n: 10,
	a: 0,
	// edge: {
	//     // shape: 'elliptical'
	// },
	// radius: {
	//     shape: 'elliptical'
	// },
	links: {
		visible: true,
		crossPoints: true,
	},
	// centre: {

	// },
	fractal: Fractal.EdgeMirror,
	props: {
		fill: 'none',
		stroke: 'purple',
		strokeWidth: 1,
		strokeDasharray: 0,
	},
	labels: {
		visible: false,
		stroke: 'gray',
		fontSize: 14,
	},
}

export const sin = {
	shape: Shape.Polygon,
	r: 100,
	n: 6,
	edge: {
		shape: Shape.Sin,
	},
	label: {},
}
