const iconeProps = {
    edge: 20,
    props: {
        fill: 'red',
        stroke: 'none',
        strokeWidth: 1
    }
}

export const iconPlay = {
    shape: 'iconPlay',
    ...iconeProps

}
export const iconPause = {
    shape: 'iconPause',
    ...iconeProps

}

export const iconStop = {
    shape: 'iconStop',
    ...iconeProps

}

export const iconNext = {
    shape: 'iconNext',
    ...iconeProps

}

export const iconPrev = {
    shape: 'iconPrev',
    ...iconeProps

}