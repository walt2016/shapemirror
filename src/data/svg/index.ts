
import { pattern } from "./pattern"
import { circles, rects, lines, curves, sawtooths, circlesPattern, rectsPattern } from "./shapes"
import { triangle, polygon, polygonColor, polygonLink, ferrisWheel } from "./polygon"
import {
  edgeFractal, vertexFractal, edgeMirror, vertexMirror,
  fractalFull, edgeMirrorLink, fractalGroup, fractalRadio, edgeMid,
  yanghui, squareFractal, sierpinskiTriangle, mitsubishi, hexaflake,
  dragon, plant //koch, peano gougu,   tree,
} from "./fractal"
import { koch, koch2, koch3, koch4, koch5, koch6, koch7, koch8, koch9, koch10 } from './koch'
import { peano, peano2, peano3 } from './peano'
import {
  sierpinski, sierpinski2, sierpinski3, sierpinski4, sierpinski5, sierpinski6
  , sierpinski7, sierpinski8, sierpinski9
} from './sierpinski'
import { ray, rayCurve } from './ray'
import { curve, wave, sawtooth, semicircle, elliptical, ellipticalLink, sin } from './curve'
import { misplaced5, misplaced6, misplaced7, misplaced8, misplaced9 } from './transform'
import { star5, star6, star7, star8, star12 } from './star'
import { axis, grid, polar, gridPoints } from './axis'
import { polygonPath, polygonPath2, polygonPath3, polygonPath4, polygonPath5 } from './pathPoints'
import { spiral } from './spiral'
import {
  maze, circleMaze, waveMaze
} from './maze'
import { motion, motionPath } from "./motion"
import { leaves, triangleLeaf } from './leaves'
import { gridRect, gridPolygon } from "./grid"
import { wanhuachi } from './wanhuachi'
import { isometric } from "./polyline"
import { polarShape, spiralShape, ringShape, rayShape, gridShape, isometricShape, polygonShape } from "./polarShape"
import { iconPause, iconPlay, iconStop, iconPrev } from "./icon"
import {
  LsystemTree, Lsystem_2, dragonCurve, terdragon, aquaticPlant, mosaic,
  blocks, Leaf, penroseTiling, shrub, saupe, aquatic, bush
} from "./lsystem"
import { tree, treeMode } from './tree'

import { oneline } from "./oneline"
import { butterfly, butterfly2, butterfly3, butterfly4, butterfly5 } from './butterfly'
import { gougu, gougu2, gougu3, gougu4 } from './gougu'
import { heart } from './heart'
import { fibonacci } from './fibonacci'
import { rose } from './rose'

export const svgCircle = {
  width: '100%',
  height: '100%',
  shapes: circles //: JSON.parse(JSON.stringify(shapes))
}

export const svgRect = {
  width: '100%',
  height: '100%',
  shapes: rects
}

export const svgLines = {
  width: '100%',
  height: '100%',
  shapes: lines
}

export const svgCurves = {
  width: '100%',
  height: '100%',
  shapes: curves
}
export const svgSawtooths = {
  width: '100%',
  height: '100%',
  shapes: sawtooths
}


export const svgCirclesPattern = {
  width: '100%',
  height: '100%',
  pattern,
  shapes: circlesPattern,

}

export const svgRectsPattern = {
  width: '100%',
  height: '100%',
  pattern,
  shapes: rectsPattern
}


export const svgAxis = {
  width: '100%',
  height: '100%',
  shapes: [axis]
}

export const svgGrid = {
  width: '100%',
  height: '100%',
  shapes: [grid]
}

export const svgPolar = {
  width: '100%',
  height: '100%',
  shapes: [polar]
}



export const svgGridPoints = {
  width: '100%',
  height: '100%',
  shapes: [gridPoints]
}

export const svgRay = {
  width: '100%',
  height: '100%',
  shapes: [ray]
}

export const svgRayCurve = {
  width: '100%',
  height: '100%',
  shapes: [rayCurve]
}


export const svgtriangle = {
  width: '100%',
  height: '100%',
  shapes: [triangle],
  // axis: { grid: {} }
}

export const svgPolygon = {
  width: '100%',
  height: '100%',
  shapes: [polygon]
}

export const svgPolygonColor = {
  width: '100%',
  height: '100%',
  shapes: [polygonColor]
}
export const svgPolygonLink = {
  width: '100%',
  height: '100%',
  shapes: [polygonLink]
}

export const svgFerrisWheel = {
  width: '100%',
  height: '100%',
  shapes: [ferrisWheel]
}


export const svgComplex = {
  width: '100%',
  height: '100%',
  // pattern,
  shapes: [grid, polar, polygon]
  // .map((t, index) => {
  //   let pt = pattern[index % patternLen]
  //   return {
  //     ...t,
  //     pattern: pt.id
  //   }
  // })
}

export const svgEdgeMirror = {
  width: '100%',
  height: '100%',
  shapes: [edgeMirror]
}

export const svgVertexMirror = {
  width: '100%',
  height: '100%',
  shapes: [vertexMirror]
}

export const svgVertexFractal = {
  width: '100%',
  height: '100%',
  shapes: [vertexFractal]
}

export const svgEdgeFractal = {
  width: '100%',
  height: '100%',
  shapes: [edgeFractal]
}


export const svgFractalFull = {
  width: '100%',
  height: '100%',
  shapes: [fractalFull]
}

export const svgEdgeMirrorLink = {
  width: '100%',
  height: '100%',
  shapes: [edgeMirrorLink]
}

export const svgFractalGroup = {
  width: '100%',
  height: '100%',
  shapes: [fractalGroup]
}

export const svgFractalRadio = {
  width: '100%',
  height: '100%',
  shapes: [fractalRadio]
}

export const svgEdgeMid = {
  width: '100%',
  height: '100%',
  shapes: [edgeMid]
}

export const svgYangehui = {
  width: '100%',
  height: '100%',
  shapes: [yanghui]
}

export const svgCurve = {
  width: '100%',
  height: '100%',
  shapes: [curve]
}

export const svgWave = {
  width: '100%',
  height: '100%',
  shapes: [wave]
}

export const svgSawtooth = {
  width: '100%',
  height: '100%',
  shapes: [sawtooth]
}

export const svgSemicircle = {
  width: '100%',
  height: '100%',
  shapes: [semicircle]
}

export const svgElliptical = {
  width: '100%',
  height: '100%',
  shapes: [elliptical]
}

export const svgEllipticalLink = {
  width: '100%',
  height: '100%',
  shapes: [ellipticalLink]
}

export const svgSin = {
  width: '100%',
  height: '100%',
  shapes: [sin]
}
export const svgMisplaced5 = {
  width: '100%',
  height: '100%',
  shapes: [misplaced5]
}
export const svgMisplaced6 = {
  width: '100%',
  height: '100%',
  shapes: [misplaced6]
}
export const svgMisplaced7 = {
  width: '100%',
  height: '100%',
  shapes: [misplaced7]
}
export const svgMisplaced8 = {
  width: '100%',
  height: '100%',
  shapes: [misplaced8]
}

export const svgMisplaced9 = {
  width: '100%',
  height: '100%',
  shapes: [misplaced9]
}


export const svgStar5 = {
  width: '100%',
  height: '100%',
  shapes: [star5]
}

export const svgStar6 = {
  width: '100%',
  height: '100%',
  shapes: [star6]
}

export const svgStar7 = {
  width: '100%',
  height: '100%',
  shapes: [star7]
}

export const svgStar8 = {
  width: '100%',
  height: '100%',
  shapes: [star8]
}

export const svgStar12 = {
  width: '100%',
  height: '100%',
  shapes: [star12]
}
export const svgPolygonPath = {
  width: '100%',
  height: '100%',
  shapes: [polygonPath]

}

export const svgPolygonPath2 = {
  width: '100%',
  height: '100%',
  shapes: [polygonPath2]

}

export const svgPolygonPath3 = {
  width: '100%',
  height: '100%',
  shapes: [polygonPath3]
}

export const svgPolygonPath4 = {
  width: '100%',
  height: '100%',
  shapes: [polygonPath4]
}

export const svgPolygonPath5 = {
  width: '100%',
  height: '100%',
  shapes: [polygonPath5]
}

export const svgSpiral = {
  width: '100%',
  height: '100%',
  shapes: [spiral],
  // axis: {}
}

// export const svgSpiraltriangle = {
//   width: '100%',
//   height: '100%',
//   shapes: [spiraltriangle],
//   // axis: {}
// }


// export const svgTwoSpiral = {
//   width: '100%',
//   height: '100%',
//   shapes: [twoSpiral],
//   // axis: {}
// }

export const svgMaze = {
  width: '100%',
  height: '100%',
  shapes: [maze],
  // grid: {}
}

export const svgCircleMaze = {
  width: '100%',
  height: '100%',
  shapes: [circleMaze],
  // grid: {}
}

export const svgWaveMaze = {
  width: '100%',
  height: '100%',
  shapes: [waveMaze],
  // grid: {}
}


export const svgMotion = {
  width: '100%',
  height: '100%',
  shapes: [motion, motionPath]
}

// export const svgGougu = {
//   width: '100%',
//   height: '100%',
//   shapes: [gougu],
//   axis: {
//     shape: 'polar'
//   }
// }

export const svgSquareFractal = {
  width: '100%',
  height: '100%',
  shapes: [squareFractal],
  // axis: {
  //   shape: 'polar'
  // }
}


export const svgLeaves = {
  width: '100%',
  height: '100%',
  shapes: [leaves],
  // axis: {
  //   shape: 'polar'
  // }
}

export const svgTriangleLeaf = {
  width: '100%',
  height: '100%',
  shapes: [triangleLeaf],
  // axis: {
  //   shape: 'polar'
  // }
}



export const svgGridRect = {
  width: '100%',
  height: '100%',
  shapes: [gridRect],
  // axis: {
  //   shape: 'grid'
  // }
}

export const svgGridPolygon = {
  width: '100%',
  height: '100%',
  shapes: [gridPolygon],
  axis: {
    shape: 'grid'
  }
}

export const svgWanhuachi = {
  width: '100%',
  height: '100%',
  shapes: [wanhuachi],
  // axis: {
  //   shape: 'polar'
  // }
}


export const svgLayout = {
  width: '100%',
  height: '100%',
  shapes: [{
    shape: 'polygon',
    r: 50,
    props: {
      fill: 'red'
    }
  }],
  // axis: {
  //   grid: {}
  // },
  layout: 8
}

export const svgIsometric = {
  width: '100%',
  height: '100%',
  shapes: [isometric]
}

export const svgSierpinskiTriangle = {
  width: '100%',
  height: '100%',
  shapes: [sierpinskiTriangle]
}



export const svgMitsubishi = {
  width: '100%',
  height: '100%',
  shapes: [mitsubishi]
}

export const svgHexaflake = {
  width: '100%',
  height: '100%',
  shapes: [hexaflake]
}



export const svgPolarShape = {
  width: '100%',
  height: '100%',
  shapes: [polarShape]
}


export const svgSpiralShape = {
  width: '100%',
  height: '100%',
  shapes: [spiralShape]

}

export const svgRingShape = {
  width: '100%',
  height: '100%',
  shapes: [ringShape]

}

export const svgRayShape = {
  width: '100%',
  height: '100%',
  shapes: [rayShape]

}

export const svgGridShape = {
  width: '100%',
  height: '100%',
  shapes: [gridShape]

}

export const svgIsometricShape = {
  width: '100%',
  height: '100%',
  shapes: [isometricShape]

}

export const svgPolygonShape = {
  width: '100%',
  height: '100%',
  shapes: [polygonShape]

}

export const svgDragon = {
  width: '100%',
  height: '100%',
  shapes: [dragon]

}


export const svgIconPlay = {
  width: '36',
  height: '36',
  shapes: [iconPlay]

}

export const svgIconPause = {
  width: '36',
  height: '36',
  shapes: [iconPause]

}

export const svgIconStop = {
  width: '36',
  height: '36',
  shapes: [iconStop]

}

export const svgIconPrev = {
  width: '36',
  height: '36',
  shapes: [iconPrev]
}

export const svgPlant = {
  width: '100%',
  height: '100%',
  shapes: [plant]
}

export const svgLsystemTree = {
  width: '100%',
  height: '100%',
  shapes: [LsystemTree]
}

export const svgLsystem_2 = {
  width: '100%',
  height: '100%',
  shapes: [Lsystem_2]
}



export const svgDragonCurve = {
  width: '100%',
  height: '100%',
  shapes: [dragonCurve]
}

export const svgTerdragon = {
  width: '100%',
  height: '100%',
  shapes: [terdragon]
}

export const svgAquaticPlant = {
  width: '100%',
  height: '100%',
  shapes: [aquaticPlant]
}

export const svgMosaic = {
  width: '100%',
  height: '100%',
  shapes: [mosaic]

}

export const svgBlocks = {

  width: '100%',
  height: '100%',
  shapes: [blocks]
}

export const svgLeaf = {
  width: '100%',
  height: '100%',
  shapes: [Leaf]
}



export const svgPenroseTiling = {
  width: '100%',
  height: '100%',
  shapes: [penroseTiling]
}



export const svgShrub = {
  width: '100%',
  height: '100%',
  shapes: [shrub]
}

export const svgSaupe = {
  width: '100%',
  height: '100%',
  shapes: [saupe]
}

export const svgAquatic = {
  width: '100%',
  height: '100%',
  shapes: [aquatic]
}

export const svgBush = {

  width: '100%',
  height: '100%',
  shapes: [bush]
}

export const svgOneline = {
  width: '100%',
  height: '100%',
  shapes: [oneline]
}


export const svgKoch = {
  width: '100%',
  height: '100%',
  shapes: [koch]
}


export const svgKoch2 = {
  width: '100%',
  height: '100%',
  shapes: [koch2]
}


export const svgKoch3 = {
  width: '100%',
  height: '100%',
  shapes: [koch3]
}

export const svgKoch4 = {
  width: '100%',
  height: '100%',
  shapes: [koch4]
}

export const svgKoch5 = {
  width: '100%',
  height: '100%',
  shapes: [koch5]
}

export const svgKoch6 = {
  width: '100%',
  height: '100%',
  shapes: [koch6]
}

export const svgKoch7 = {
  width: '100%',
  height: '100%',
  shapes: [koch7]
}

export const svgKoch8 = {
  width: '100%',
  height: '100%',
  shapes: [koch8]
}

export const svgKoch9 = {
  width: '100%',
  height: '100%',
  shapes: [koch9]
}

export const svgKoch10 = {
  width: '100%',
  height: '100%',
  shapes: [koch10]
}


export const svgSierpinski = {
  width: '100%',
  height: '100%',
  shapes: [sierpinski]
}

export const svgSierpinski2 = {
  width: '100%',
  height: '100%',
  shapes: [sierpinski2]
}

export const svgSierpinski3 = {
  width: '100%',
  height: '100%',
  shapes: [sierpinski3]
}

export const svgSierpinski4 = {
  width: '100%',
  height: '100%',
  shapes: [sierpinski4]
}


export const svgSierpinski5 = {
  width: '100%',
  height: '100%',
  shapes: [sierpinski5]
}

export const svgSierpinski6 = {
  width: '100%',
  height: '100%',
  shapes: [sierpinski6]
}



export const svgSierpinski7 = {
  width: '100%',
  height: '100%',
  shapes: [sierpinski7]
}

export const svgSierpinski8 = {
  width: '100%',
  height: '100%',
  shapes: [sierpinski8]
}

export const svgSierpinski9 = {
  width: '100%',
  height: '100%',
  shapes: [sierpinski9]
}


export const svgPeano = {
  width: '100%',
  height: '100%',
  shapes: [peano]

}

export const svgPeano2 = {
  width: '100%',
  height: '100%',
  shapes: [peano2]

}

export const svgPeano3 = {
  width: '100%',
  height: '100%',
  shapes: [peano3]

}



export const svgButterfly = {
  width: '800',
  height: '600',
  shapes: [butterfly]
}

export const svgButterfly2 = {
  width: '800',
  height: '600',
  shapes: [butterfly2]
}
export const svgButterfly3 = {
  width: '800',
  height: '600',
  shapes: [butterfly3]
}
export const svgButterfly4 = {
  width: '800',
  height: '600',
  shapes: [butterfly4]
}



export const svgButterfly5 = {
  width: '800',
  height: '600',
  shapes: [butterfly5]
}



export const svgGougu = {
  width: '100%',
  height: '100%',
  shapes: [gougu]
}

export const svgGougu2 = {
  width: '100%',
  height: '100%',
  shapes: [gougu2]
}

export const svgGougu3 = {
  width: '100%',
  height: '100%',
  shapes: [gougu3]
}

export const svgGougu4 = {
  width: '100%',
  height: '100%',
  shapes: [gougu4]
}

export const svgHeart = {
  width: '100%',
  height: '100%',
  shapes: [heart]
}

export const svgFibonacci = {
  width: '100%',
  height: '100%',
  shapes: [fibonacci]

}

export const svgRose = {
  width: '100%',
  height: '100%',
  shapes: [rose]

}


export const svgTree = {
  width: '100%',
  height: '100%',
  shapes: [tree]
}

export const svgTreeMode = {
  width: '100%',
  height: '100%',
  shapes: [treeMode]
}

