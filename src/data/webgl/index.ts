export const webgl = {


    width: '800',
    height: '600',
    axis: { grid: {} },
    shapes: [{
        shape: 'webgl',
        r: 100

    }]
}


export const glTriangle = {


    width: '800',
    height: '600',
    axis: { grid: {} },
    shapes: [{
        shape: 'glTriangle',
        r: 100

    }]
}

export const glSquare = {


    width: '800',
    height: '600',
    axis: { grid: {} },
    shapes: [{
        shape: 'glSquare',
        r: 100

    }]
}

export const glCube = {


    width: '800',
    height: '600',
    axis: { grid: {} },
    shapes: [{
        shape: 'glCube',
        r: 100

    }]
}


export const glBall = {
    width: '800',
    height: '600',
    axis: { grid: {} },
    shapes: [{
        shape: 'glBall',
        r: 100

    }]
}


export const glLine = {
    width: '800',
    height: '600',
    // axis: { grid: {} },
    shapes: [{
        shape: 'glLine',
        r: 100

    }]
}