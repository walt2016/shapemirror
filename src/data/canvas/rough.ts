import { Shape } from '@/types/shape'
import { defaultProps } from '../defaultProps'
import { PathMode } from '@/types/pathMode'
import { Color } from '@/types/color'
import { Curve } from '@/types/curve'
import { Mirror } from '@/types/mirror'
export const rough = {
	shape: Shape.Polygon,
	r: 100,
	n: 4,
	a: 0,
	pathMode: PathMode.LINE_LOOP,
	// curve: Curve.Rough,
	...defaultProps,
	curve: {
		type: Curve.Rough,
		angle: 90,
		radio: 1,
	},
}

export const rough2 = {
	shape: Shape.Polygon,
	r: 100,
	n: 4,
	a: 0,
	pathMode: PathMode.LINE_LOOP,
	// curve: 'rough',
	...defaultProps,
	curve: {
		type: Curve.Rough,
		angle: 90,
		radio: 1,
	},
	mirror: {
		type: Mirror.Vertex,
		scale: 1,
		// borderIndex: 0,
		oneline: false,
		onelineOffset: 1,
		color: Color.ColorCircle,
		alpha: 0.5,
		fill: true,
	},
}
