import { Shape } from '@/types/shape'
import { defaultProps } from '../defaultProps'
import { PathMode } from '@/types/pathMode'
import { Color } from '@/types/color'
import { Curve } from '@/types/curve'
import { Transform } from '@/types/transform'
import { Mirror } from '@/types/mirror'
export const circle = {
	shape: Shape.Circle,
	r: 100,
	centre: {},
}
export const rect = {
	shape: Shape.Rect,
	r: 100,
	a: 0,
	transform: Transform.Normal,
	pathMode: PathMode.LINE_LOOP,
	curve: Curve.None,
	...defaultProps,
	// centre: {
	// },
	// vertex: {},
	// labels: {}
}

export const polygon = {
	shape: Shape.Polygon,
	r: 100,
	n: 6,
	a: 0,
	transform: Transform.Normal,
	pathMode: PathMode.LINE_LOOP,
	curve: Curve.None,
	...defaultProps,
	// centre: {
	// },
	// vertex: {},
	// labels: {}
}

export const polygonMirror = {
	shape: Shape.Polygon,
	r: 100,
	n: 6,
	a: 0,

	transform: Transform.Normal,
	pathMode: PathMode.LINE_LOOP,
	curve: Curve.None,
	...defaultProps,
	mirror: {
		type: Mirror.Vertex,
		scale: 0.75,
		oneline: false,
		onelineOffset: 1,
		color: Color.ColorCircle,
	},
}

export const polygonEdgeMirror = {
	shape: Shape.Polygon,
	r: 100,
	n: 6,
	a: 0,

	transform: Transform.Normal,
	pathMode: PathMode.LINE_LOOP,
	curve: Curve.None,
	...defaultProps,
	mirror: {
		type: Mirror.Edge,
		scale: 0.75,
		oneline: false,
		onelineOffset: 1,
		color: Color.ColorCircle,
	},
}
