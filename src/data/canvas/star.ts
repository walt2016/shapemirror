import { Shape } from '@/types/shape'
import { defaultProps } from '../defaultProps'
import { PathMode } from '@/types/pathMode'
import { Curve } from '@/types/curve'
export const star5 = {
	shape: Shape.Star,
	r: 100,
	n: 5,
	interiorPolygon: true,
	pathMode: PathMode.LINE_LOOP,
	curve: Curve.None,
	...defaultProps,
}

export const star6 = {
	shape: Shape.Star,
	r: 100,
	n: 6,
	interiorPolygon: true,
	pathMode: PathMode.LINE_LOOP,
	curve: Curve.None,
	...defaultProps,
}

export const star7 = {
	shape: Shape.Star,
	r: 100,
	n: 7,
	interiorPolygon: true,
	pathMode: PathMode.LINE_LOOP,
	curve: Curve.None,
	...defaultProps,
}

export const star8 = {
	shape: Shape.Star,
	r: 100,
	n: 9,
	interiorPolygon: true,
	pathMode: PathMode.LINE_LOOP,
	curve: Curve.None,
	...defaultProps,
}

export const star12 = {
	shape: Shape.Star,
	r: 100,
	n: 13,
	interiorPolygon: true,
	pathMode: PathMode.LINE_LOOP,
	curve: Curve.None,
	...defaultProps,
}
