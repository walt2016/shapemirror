import { Color } from '@/types/color'
import { Curve } from '@/types/curve'
import { PathMode } from '@/types/pathMode'
import { Shape } from '@/types/shape'

export const plant = {
	shape: Shape.Plant,
	r: 100,
	n: 3,
	depth: 9,
	a: -90,
	k: 0.618,
	wriggle: 100,
	treeCurve: 'V',
	pathMode: PathMode.LINES,
	curve: Curve.None,
	color: {
		type: Color.ColorCircle,
		alpha: 0.5,
		fill: true,
	},
	alpha: 0.9,
	// labels:{}
	// fill:{}
	props: {
		// stroke: 'red',
		// fill: 'none',
		strokeWidth: 1,
		strokeDasharray: 0,
		strokeDashoffset: 0,
	},
}

export const bush = {
	shape: Shape.LSystemPlant,
	r: 10,
	angle: 21,
	depth: 4,
	wriggle: 5,
	reduction: 0.9,
	color: {
		type: Color.ColorCircle,
		alpha: 0.5,
		fill: true,
	},
	// oneLine: false,
	pathMode: PathMode.LINE_STRIP,
	// curve: Curve.None,
	labels: {
		visible: false,
		stroke: 'gray',
		fontSize: 14,
	},
	axiom: 'F',
	rules: {
		F: 'F+-F+[c+F-F-F]-[-F+F+dF]',
	},
	props: {
		// fill:'none',
		stroke: 'red',
		strokeWidth: 1,
		strokeDasharray: 0,
	},
}

export const aquatic = {
	shape: Shape.LSystemPlant,
	r: 10,
	angle: 27,
	depth: 4,
	wriggle: 0,
	reduction: 1.0,
	color: {
		type: Color.ColorCircle,
		alpha: 0.5,
		fill: true,
	},
	labels: {
		visible: false,
		stroke: 'gray',
		fontSize: 14,
	},
	edge: {
		visible: true,
		fill: 'none',
		stroke: 'red',
	},
	// pathMode: PathMode.LINE_STRIP,
	// curve: Curve.None,
	axiom: 'F',
	rules: {
		F: 'FMNOMBxPNMyO',
		M: 'e[-F++F++]',
		N: 'd[+F--F--]',
		O: 'c++F--F',
		P: 'd--F++F',
	},
}

export const saupe = {
	shape: Shape.LSystemPlant,
	r: 10,
	angle: 20,
	depth: 7,
	wriggle: 2,
	reduction: 1.0,
	color: {
		type: Color.ColorCircle,
		alpha: 0.5,
		fill: true,
	},
	labels: {
		visible: false,
		stroke: 'gray',
		fontSize: 14,
	},
	// pathMode: PathMode.LINE_STRIP,
	// curve: Curve.None,
	axiom: 'VZFFF',
	rules: {
		V: '[+++W][---W]YV',
		W: '+X[-W]Z',
		X: '-W[+X]Z',
		Y: 'YZ',
		Z: 'F+-F[-FcFF][+FdFF]F',
	},
}

export const shrub = {
	shape: Shape.LSystemPlant,
	r: 5,
	angle: 322,
	depth: 4,
	wriggle: 10,
	reduction: 0.8,
	color: {
		type: Color.ColorCircle,
		alpha: 0.5,
		fill: true,
	},
	labels: {
		visible: false,
		stroke: 'gray',
		fontSize: 14,
	},
	// pathMode: PathMode.LINE_STRIP,
	// curve: Curve.None,
	axiom: 'F',
	rules: {
		F: 'F+-e[+cFF]Fd[-FF]cF',
	},
}

export const aquaticPlant = {
	shape: Shape.LSystemPlant,
	axiom: 'F',
	rules: { F: 'F+-Fc[-F++F]d[+F--F]e++F--F' },
	angle: 27,
	r: 10,
	depth: 3,
	wriggle: 10,
	reduction: 0.9,
	color: {
		type: Color.ColorCircle,
		alpha: 0.5,
		fill: true,
	},
	labels: {
		visible: false,
		stroke: 'gray',
		fontSize: 14,
	},
	// pathMode: PathMode.LINE_STRIP,
	// curve: 'none'
}

export const LsystemTree = {
	shape: Shape.LSystemPlant,
	axiom: 'F',
	rules: { F: 'F[+F-F]-F+F' },
	angle: 22,
	r: 10,
	depth: 4,
	wriggle: 10,
	reduction: 1.0,
	color: {
		type: Color.ColorCircle,
		alpha: 0.5,
		fill: true,
	},
	labels: {
		visible: false,
		stroke: 'gray',
		fontSize: 14,
	},
	pathMode: PathMode.LINE_STRIP,
	// curve: Curve.None,
}
