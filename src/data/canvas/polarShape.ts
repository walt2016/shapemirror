import { Color } from '@/types/color'
import { defaultProps } from '../defaultProps'
import { Shape } from '@/types/shape'
import { Transform } from '@/types/transform'
import { PathMode } from '@/types/pathMode'
import { Curve } from '@/types/curve'

export const polarShape = {
	shape: Shape.PolarShape,
	n: 12,
	r: 200,
	a: 0,
	m: 5,
	transform: Transform.None,
	pathMode: PathMode.LINE_LOOP,
	// curve: Curve.None,
	...defaultProps,
}

export const spiralShape = {
	shape: Shape.SpiralShape,
	n: 12,
	r: 50,
	a: 0,
	m: 5,
	transform: Transform.None,
	pathMode: PathMode.LINE_STRIP,
	curve: Curve.None,
	...defaultProps,
}

export const ringShape = {
	shape: Shape.RingShape,
	n: 12,
	r: 20,
	a: 0,
	m: 5,
	transform: Transform.None,
	pathMode: PathMode.LINE_LOOP,
	curve: Curve.None,
	...defaultProps,
	// mirror: {
	//     type: 'vertex',
	//     scale: 0.618,
	//     borderIndex: 0,
	//     oneline: false,
	//     onelineOffset: 1,
	//     color: Color.ColorCircle
	// },
	color: {
		type: Color.ColorCircle,
		alpha: 0.5,
		fill: true,
	},
}

export const rayShape = {
	shape: Shape.RayShape,
	n: 12,
	r: 50,
	a: 0,
	m: 5,
	transform: Transform.None,
	pathMode: PathMode.LINE_STRIP,

	...defaultProps,
	curve: {
		type: Curve.DoubleBezier,
		skew: 0,
		amplitude: 1,
		depth: 1,
		increasedAgnle: 90,
	},
}
