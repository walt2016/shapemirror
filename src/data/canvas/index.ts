import { plant, bush, aquatic, saupe, shrub, aquaticPlant, LsystemTree } from './plant'
import {
    polarShape,
    spiralShape,
    ringShape,
    rayShape,
} from './polarShape'
import { circle, rect, polygon, polygonMirror, polygonEdgeMirror } from './shapes'
import { butterfly, butterfly2, butterfly3, butterfly4, butterfly5 } from './butterfly'
import { star5, star6, star7, star8, star12 } from './star'
import { gougu, gougu2, gougu3, gougu4 } from './gougu'
import { rough, rough2 } from './rough'
export const canvasCircle = {
    width: '800',
    height: '600',
    axis: { grid: {} },
    shapes: [circle]
}

export const canvasRect = {
    width: '800',
    height: '600',
    axis: { grid: {} },
    shapes: [rect]
}


export const canvasPolygon = {
    width: '800',
    height: '600',
    axis: { grid: {} },
    shapes: [polygon, rect, circle]
}


export const canvasPolygonMirror = {
    width: '800',
    height: '600',
    shapes: [polygonMirror],
    axis: {}
}


export const canvasPolygonEdgeMirror = {
    width: '800',
    height: '600',
    shapes: [polygonEdgeMirror],
    axis: {}
}


export const canvasPlant = {
    width: '800',
    height: '600',
    shapes: [plant],
}

export const canvasBush = {
    width: '800',
    height: '600',
    shapes: [bush],
}

export const canvasAquatic = {
    width: '800',
    height: '600',
    shapes: [aquatic],
}



export const canvasSaupe = {
    width: '800',
    height: '600',
    shapes: [saupe],
}




export const canvasShrub = {
    width: '800',
    height: '600',
    shapes: [shrub],
}




export const canvasAquaticPlant = {
    width: '800',
    height: '600',
    shapes: [aquaticPlant],
}



export const canvasLsystemTree = {
    width: '800',
    height: '600',
    shapes: [LsystemTree],
}


export const canvasPolarShape = {
    width: '800',
    height: '600',
    shapes: [polarShape],
}


export const canvasSpiralShape = {
    width: '800',
    height: '600',
    shapes: [spiralShape],
}

export const canvasRingShape = {
    width: '800',
    height: '600',
    shapes: [ringShape],
}

export const canvasRayShape = {
    width: '800',
    height: '600',
    shapes: [rayShape],
}

export const canvasButterfly = {
    width: '800',
    height: '600',
    shapes: [butterfly]
}

export const canvasButterfly2 = {
    width: '800',
    height: '600',
    shapes: [butterfly2]
}
export const canvasButterfly3 = {
    width: '800',
    height: '600',
    shapes: [butterfly3]
}
export const canvasButterfly4 = {
    width: '800',
    height: '600',
    shapes: [butterfly4]
}



export const canvasButterfly5 = {
    width: '800',
    height: '600',
    shapes: [butterfly5]
}



export const canvasStar5 = {
    width: '100%',
    height: '100%',
    shapes: [star5]
}

export const canvasStar6 = {
    width: '100%',
    height: '100%',
    shapes: [star6]
}

export const canvasStar7 = {
    width: '100%',
    height: '100%',
    shapes: [star7]
}

export const canvasStar8 = {
    width: '100%',
    height: '100%',
    shapes: [star8]
}

export const canvasStar12 = {
    width: '100%',
    height: '100%',
    shapes: [star12]
}

export const canvasGougu = {
    width: '100%',
    height: '100%',
    shapes: [gougu]
}

export const canvasGougu2 = {
    width: '100%',
    height: '100%',
    shapes: [gougu2]
}

export const canvasGougu3 = {
    width: '100%',
    height: '100%',
    shapes: [gougu3]
}

export const canvasGougu4 = {
    width: '100%',
    height: '100%',
    shapes: [gougu4]
}

export const canvasRough = {
    width: '100%',
    height: '100%',
    shapes: [rough]
}

export const canvasRough2 = {
    width: '100%',
    height: '100%',
    shapes: [rough2]
}