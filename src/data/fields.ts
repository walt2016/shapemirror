 const fields = [{
  field: 'name',
  label: 'name',
  placeholder:'请输入名称',
  clearable:true
}, {
  field: 'city',
  label: 'city',
  type: 'select',
  options: [{
    value: 'bj',
    label: 'bj'
  }, {
    label: 'sh',
    value: 'sh'
  }]
}, {
  field: 'number',
  label: 'number',
  type: 'number',
  value: 0,
  // range: [0, 10]
  min:0,
  max:10
},{
  field:'checkbox',
  label:'checkbox',
  type:'checkbox',
  value:'haha',
  options:[{
    label:'haha',
    value:'haha'
  },{
    label:'hehe',
    value:'hehe'
  }]

}]

export default fields