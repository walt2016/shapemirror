const _mapOptions = (arr) => {
  return arr.map(t => {
    return {
      label: t,
      value: t
    }
  })
}
const data = {
  title: 'form',
  // 字段
  fields: [{
      label: '选择',
      type: 'selection'
    },

    {
      label: '#',
      type: 'index'

    },
    {
      field: 'name',
      label: 'name',
      value: 'hi',
      type: 'text',
      class: 'red'
    }, {
      field: 'city',
      label: 'city',
      type: 'select',
      value: 'shenzhen',
      options: _mapOptions(['beijing', 'shenzhen'])
    },
    {
      field: 'number',
      type: 'number',
      value: 0
    },
    {
      field: 'random',
      type: 'random',
      rang: [-10, 10]
    },
    {
      label: '操作',
      type: 'operation',
      options: [{
        label: 'edit',
        name: 'edit',
        click: () => {

        }
      }, {
        label: 'del',
        name: 'del',
        click: (e, {
          row,
          rowIndex,
          table,
          emit
        }) => {
          emit(table, "del", {
            row,
            rowIndex
          })

        }
      }]
    }

  ],
  // 操作按钮
  op: [{
      type: 'btn',
      text: 'search',
      className: 'success',
      click: function () {
      }
    },
    {
      type: 'btn',
      text: 'add',
      click: (e, {
        model,
        form,
        emit
      }) => {
        emit(form, "add", model)
      }
    },
    {
      type: 'btn',
      text: 'reset',
      click: (e, {
        model,
        form,
        emit
      }) => {
        emit(form, "reset", model)
      }
    }
  ],

  // 统计列
  statis: {
    label: '合计',
    type: 'sum',
    fields: ['number', 'random']
  },
  config: {
    labelWidth: 100,
    width: 300,
    // 统计
    sum: true
  }

}
const _mapFormFields = (fields, config = {}) => {
  return fields.map(t => {
    return {
      name: t.field,
      label: t.field,
      ...t,
      ...config
    }
  })
}


export default {
  ...data,
  fields: _mapFormFields(data.fields, data.config)
}