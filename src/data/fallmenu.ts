import {
    svgAxis, svgGridPoints,
    svgCircle, svgRect, svgLines, svgCurves, svgSawtooths,
    svgGrid, svgPolar, svgRay, svgRayCurve,
    svgtriangle,
    svgPolygon, svgPolygonColor, svgPolygonLink, svgFerrisWheel,
    svgComplex,
    svgEdgeMirror, svgVertexMirror, svgEdgeFractal, svgVertexFractal,
    svgFractalFull, svgEdgeMirrorLink, svgFractalGroup, svgFractalRadio, svgEdgeMid,
    svgYangehui, svgCurve, svgWave, svgSawtooth, svgSemicircle, svgElliptical,
    svgEllipticalLink, svgSin,
    svgStar5, svgStar12, svgStar6, svgStar7, svgStar8,
    svgMisplaced5, svgMisplaced6, svgMisplaced7, svgMisplaced8, svgMisplaced9
} from './svg'
import {
    form
} from './form'
import {
    grid
} from './grid'
import {
    html
} from './html'
import {
    canvas
} from './canvas'
import {
    list,
    renderlistItem
} from './list'
import { btns } from './btns'
import { ContentType } from '@/types/ui'


let menu = [
    {
        title: 'html',
        name: 'html',
        children: [{
            title: 'html',
            name: 'html',
            children: [{
                title: 'form',
                name: 'first',
                content: form,
                contentType: ContentType.Html
            }, {
                title: 'table',
                name: 'second',
                content: grid,
                contentType: ContentType.Html

            }, {
                title: 'list',
                name: 'list',
                content: list,
                contentType: ContentType.Html,
                render: renderlistItem

            },
            {
                title: 'html',
                name: 'html',
                textContent:html,
                contentType: ContentType.Html
            }, {
                title: 'text',
                name: 'text',
                textContent:html,
            },
            {
                title: 'btns',
                name: 'btns',
                content: btns,
                contentType: ContentType.Html
            }]
        }]

    },
    {
        title: 'svg',
        name: 'svg',
        children: [
            {
                title: 'axis',
                name: 'axis',
                children: [
                    {
                        title: 'axis',
                        name: 'axis',
                        content: svgAxis,
                        contentType: ContentType.SVG
                    },
                    {
                        title: 'grid',
                        name: 'grid',
                        content: svgGrid,
                        contentType: ContentType.SVG
                    }, {
                        title: 'polar',
                        name: 'polar',
                        content: svgPolar,
                        contentType: ContentType.SVG
                    }, {
                        title: 'gridPoints',
                        name: 'gridPoints',
                        content: svgGridPoints,
                        contentType: ContentType.SVG
                    }

                ]
            },
            {
                title: 'shape',
                name: 'shape',
                children: [{
                    title: 'circle',
                    name: 'circle',
                    content: svgCircle,
                    contentType: ContentType.SVG,
                },
                {
                    title: 'rect',
                    name: 'rect',
                    content: svgRect,
                    contentType: ContentType.SVG
                }, {
                    title: 'lines',
                    name: 'lines',
                    content: svgLines,
                    contentType: ContentType.SVG
                }, {
                    title: 'curves',
                    name: 'curves',
                    content: svgCurves,
                    contentType: ContentType.SVG
                },
                {
                    title: 'sawtooths',
                    name: 'sawtooths',
                    content: svgSawtooths,
                    contentType: ContentType.SVG
                },

                    // {
                    //     title: 'pattern',
                    //     name: 'pattern',
                    //     content: svgPattern,
                    //     contentType: ContentType.SVG
                    // }
                ]
            },
            {
                title: 'polygon',
                name: 'polygon',
                children: [{
                    title: 'triangle',
                    name: 'triangle',
                    content: svgtriangle,
                    contentType: ContentType.SVG
                },
                {
                    title: 'polygon',
                    name: 'polygon',
                    content: svgPolygon,
                    contentType: ContentType.SVG
                },



                {
                    title: 'polygonColor',
                    name: 'polygonColor',
                    content: svgPolygonColor,
                    contentType: ContentType.SVG
                }, {
                    title: 'polygonLink',
                    name: 'polygonLink',
                    content: svgPolygonLink,
                    contentType: ContentType.SVG
                },
                {
                    title: 'ferrisWheel',
                    name: 'ferrisWheel',
                    content: svgFerrisWheel,
                    contentType: ContentType.SVG
                }, {
                    title: 'complex',
                    name: 'complex',
                    content: svgComplex,
                    contentType: ContentType.SVG
                },

                {
                    title: 'ray',
                    name: 'ray',
                    content: svgRay,
                    contentType: ContentType.SVG
                },
                {
                    title: 'rayCurve',
                    name: 'rayCurve',
                    content: svgRayCurve,
                    contentType: ContentType.SVG
                },
                ]
            },
            {
                title: 'fractal',
                name: 'fractal',
                children: [{
                    title: 'edgemirror',
                    name: 'edgemirror',
                    content: svgEdgeMirror,
                    contentType: ContentType.SVG
                },
                {
                    title: 'vertexmirror',
                    name: 'vertexmirror',
                    content: svgVertexMirror,
                    contentType: ContentType.SVG
                },
                {
                    title: 'edgeFractal',
                    name: 'edgeFractal',
                    content: svgEdgeFractal,
                    contentType: ContentType.SVG
                },
                {
                    title: 'vertexFractal',
                    name: 'vertexFractal',
                    content: svgVertexFractal,
                    contentType: ContentType.SVG
                },

                {
                    title: 'edgeMirrorLink',
                    name: 'edgeMirrorLink',
                    content: svgEdgeMirrorLink,
                    contentType: ContentType.SVG
                },
                {
                    title: 'fractalGroup',
                    name: 'fractalGroup',
                    content: svgFractalGroup,
                    contentType: ContentType.SVG
                },
                {
                    title: 'fractalFull',
                    name: 'fractalFull',
                    content: svgFractalFull,
                    contentType: ContentType.SVG
                },
                {
                    title: 'fractalRadio',
                    name: 'fractalRadio',
                    content: svgFractalRadio,
                    contentType: ContentType.SVG
                },
                {
                    title: 'edgeMid',
                    name: 'edgeMid',
                    content: svgEdgeMid,
                    contentType: ContentType.SVG
                },
                {
                    title: 'yanghui',
                    name: 'yanghui',
                    content: svgYangehui,
                    contentType: ContentType.SVG
                },]
            },
            {
                title: 'curve',
                name: 'curve',
                children: [
                    {
                        title: 'curve',
                        name: 'curve',
                        content: svgCurve,
                        contentType: ContentType.SVG
                    },
                    {
                        title: 'wave',
                        name: 'wave',
                        content: svgWave,
                        contentType: ContentType.SVG
                    },
                    {
                        title: 'sawtooth',
                        name: 'sawtooth',
                        content: svgSawtooth,
                        contentType: ContentType.SVG
                    },
                    {
                        title: 'semicircle',
                        name: 'semicircle',
                        content: svgSemicircle,
                        contentType: ContentType.SVG
                    },
                    {
                        title: 'elliptical',
                        name: 'elliptical',
                        content: svgElliptical,
                        contentType: ContentType.SVG
                    },
                    {
                        title: 'ellipticalLink',
                        name: 'ellipticalLink',
                        content: svgEllipticalLink,
                        contentType: ContentType.SVG
                    },
                    {
                        title: 'sin',
                        name: 'sin',
                        content: svgSin,
                        contentType: ContentType.SVG
                    },
                ]
            },
            {
                title: 'star',
                name: 'transform',
                children: [

                    {
                        title: 'Star5',
                        name: 'Star5',
                        content: svgStar5,
                        contentType: ContentType.SVG
                    },
                    {
                        title: 'Star6',
                        name: 'Star6',
                        content: svgStar6,
                        contentType: ContentType.SVG
                    },
                    {
                        title: 'Star7',
                        name: 'Star7',
                        content: svgStar7,
                        contentType: ContentType.SVG
                    },
                    {
                        title: 'Star8',
                        name: 'Star8',
                        content: svgStar8,
                        contentType: ContentType.SVG
                    },
                    {
                        title: 'Star12',
                        name: 'Star12',
                        content: svgStar12,
                        contentType: ContentType.SVG
                    },
                ]
            }, {
                title: 'transform',
                name: 'transform',
                children: [{
                    title: 'misplaced5',
                    name: 'misplaced5',
                    content: svgMisplaced5,
                    contentType: ContentType.SVG
                }, {
                    title: 'misplaced6',
                    name: 'misplaced6',
                    content: svgMisplaced6,
                    contentType: ContentType.SVG
                }, {
                    title: 'misplaced7',
                    name: 'misplaced7',
                    content: svgMisplaced7,
                    contentType: ContentType.SVG
                }, {
                    title: 'misplaced8',
                    name: 'misplaced8',
                    content: svgMisplaced8,
                    contentType: ContentType.SVG
                }, {
                    title: 'misplaced9',
                    name: 'misplaced9',
                    content: svgMisplaced9,
                    contentType: ContentType.SVG
                },]
            }


        ]
    }, {
        title: 'canvas',
        name: 'canvas',
        content: canvas,
        contentType: ContentType.Canvas
    }
]

export default {
    menu,
    activeName: 'first',
    target: '#main'
}