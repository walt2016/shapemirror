export const canvas = {

  width: 200,
  height: 200,
  shapes: [{

    shape: 'circle',
    o: [50, 50],
    r: 50
  }, {

    shape: 'rect',
    p1: [50, 50],
    p2: [100, 100]
  }]
}