import fields from './fields'
import data from './data'
export const grid = {
  fields: [{
    label: '选择',
    type: 'selection'
  }, {
    type: 'index',
    label: '#'
  },
  ...fields,
  {
    label: '操作',
    type: 'operation',
    options: [{
      label: 'edit',
      name: 'edit',
      click: () => {

      }
    }, {
      label: 'del',
      name: 'del',
      click: (e, {
        row,
        rowIndex,
        table,
        emit
      }) => {
        emit(table, "del", {
          row,
          rowIndex
        })

      }
    }]
  }],
  // 统计列
  statis: {
    label: '合计',
    type: 'sum',
    fields: ['number']
  },
  props: {
    style: {
      width: '100%'
    }
  },
  data
}