export const html = '<span style="color:red">test</span>'


import { form } from './form'
import { grid } from './grid'
import { canvas } from './canvas'
import { list, renderlistItem } from './list'
import { btns } from './btns'


export {
    form,
    grid,
    canvas,
    list,
    btns,
    renderlistItem
}