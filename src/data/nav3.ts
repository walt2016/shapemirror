import { IMenu } from "@/types/menu"
import { ContentType } from "@/types/ui"



const axis = ['svgAxis', 'svgGrid', 'svgPolar', 'svgGridPoints']
const svgPattern = [
    'svgCirclesPattern', 'svgRectsPattern', 'svgGridRect', 'svgGridPolygon']
// 'svgCircle', 'svgRect', 'svgLines',
// 'svgtriangle', 'svgPolygon', 'svgPolygonColor', 'svgPolygonLink','svgCurves',  'svgSawtooths',
// 'svgFerrisWheel', 'svgComplex',
const polygon = ['svgRay', 'svgRayCurve', 'svgIsometric', 'svgPolarShape',
    'svgSpiralShape', 'svgRingShape', 'svgRayShape', 'svgGridShape', 'svgIsometricShape', 'svgPolygonShape']
const fractal = ['svgEdgeMirror', 'svgVertexMirror', 'svgEdgeFractal', 'svgVertexFractal', 'svgEdgeMirrorLink'
    , 'svgFractalGroup', 'svgFractalFull', 'svgFractalRadio', 'svgEdgeMid', 'svgYangehui', 'svgLeaves',
    'svgTriangleLeaf', 'svgSquareFractal', 'svgSierpinskiTriangle', 'svgMitsubishi',
    'svgHexaflake', 'svgTree', 'svgPlant', 'svgDragon', 'svgOneline']

// const curve = ['svgCurve', 'svgWave', 'svgSawtooth', 'svgSemicircle', 'svgElliptical', 'svgEllipticalLink',
//     'svgSin']
const star = ['svgStar5', 'svgStar6', 'svgStar7', 'svgStar8', 'svgStar12']
const icon = ['svgIconPlay', 'svgIconPrev', 'svgIconPause', 'svgIconStop']
const transform = ['svgMisplaced5', 'svgMisplaced6', 'svgMisplaced7', 'svgMisplaced8', 'svgMisplaced9']
const pathpoints = ['svgPolygonPath', 'svgPolygonPath2', 'svgPolygonPath3', 'svgPolygonPath4', 'svgPolygonPath5']

// const game = [ 'svgMaze2']
// , 'svgMotion', 'svgLayout'
const canvasShape = ['canvasCircle', 'canvasRect', 'canvasPolygon', 'canvasPolygonMirror',
    'canvasPolygonEdgeMirror',
    'canvasPolarShape', 'canvasSpiralShape', 'canvasRingShape', 'canvasRayShape']
const webglShape = ['glTriangle', 'glSquare', 'glCube'] //'webgl',
const canvasPlant = ['canvasPlant', 'canvasBush', 'canvasAquatic', 'canvasSaupe',
    'canvasShrub', 'canvasAquaticPlant', 'canvasLsystemTree']
const canvasButterfly = ['canvasButterfly', 'canvasButterfly2', 'canvasButterfly3', 'canvasButterfly4', 'canvasButterfly5']
const sygPlant = ['svgLsystemTree', 'svgLsystem_2', 'svgAquaticPlant'
    , 'svgShrub', 'svgSaupe', 'svgAquatic', 'svgBush', 'svgTree', 'svgTreeMode']
// 'svgDragonCurve','svgTerdragon','svgMosaic','svgBlocks','svgLeaf', 'svgPenroseTiling',
const canvasStar = ['canvasStar5', 'canvasStar6', 'canvasStar7', 'canvasStar8', 'canvasStar12']
const canvasGougu = ['canvasGougu', 'canvasGougu2', 'canvasGougu3', 'canvasGougu4']
const canvasRough = ['canvasRough', 'canvasRough2']

const svgKoch = ['svgKoch', 'svgKoch2', 'svgKoch3', 'svgKoch4', 'svgKoch5', 'svgKoch6', 'svgKoch7', 'svgKoch8', 'svgKoch9', 'svgKoch10']
const svgSierpinski = ['svgSierpinski', 'svgSierpinski2', 'svgSierpinski3', 'svgSierpinski4',
    'svgSierpinski5', 'svgSierpinski6', 'svgSierpinski7', 'svgSierpinski8', 'svgSierpinski9']
const svgPeano = ['svgPeano', 'svgPeano2', 'svgPeano3']
const svgButterfly = ['svgButterfly', 'svgButterfly2', 'svgButterfly3', 'svgButterfly4', 'svgButterfly5']
const svgGougu = ['svgGougu', 'svgGougu2', 'svgGougu3', 'svgGougu4']
const svgCurve = ['svgPolarShape', 'svgHeart', 'svgRose', 'svgFibonacci', 'svgSpiral', 'svgWanhuachi', 'svgMaze', 'svgCircleMaze', 'svgWaveMaze']
const lottieGas = ['lottieGas', 'lottieEarphone']
const items: {
    [key: string]: string[]
} = {
    axis,
    svgPattern,
    polygon,
    fractal,
    star,
    icon,
    transform,
    pathpoints,
    // game,
    canvasShape,
    webglShape,
    canvasPlant,
    canvasButterfly,
    sygPlant,
    canvasStar,
    canvasGougu,
    canvasRough,
    svgKoch,
    svgSierpinski,
    svgPeano,
    svgButterfly,
    svgGougu,
    svgCurve,
    lottieGas
}

const genTitle = (name: string, type: ContentType) => {
    const reg = new RegExp(`^${type === 'webgl' ? 'gl' : type}(.+)`)
    const str = name.replace(reg, "$1")
    return str.slice(0, 1).toLowerCase() + str.slice(1)
}
const _itemMap = (name: string, type: ContentType) => {
    const children = items[name].map(t => {
        return {
            title: genTitle(t, type),
            name: t,
            url: t,
            contentType: type
        }

    })
    return {
        name,
        title: name,
        children
    }
}
const svg = {
    title: 'svg',
    name: 'svg',
    children: ['svgCurve', 'sygPlant', 'svgPattern',
        'svgKoch', 'svgSierpinski', 'svgPeano', 'svgButterfly', 'svgGougu'].map(t => _itemMap(t, ContentType.SVG))
}

// 'game', 'polygon',
// if (location.href.indexOf('localhost') >= 0) {
//     svg.children = svg.children.concat(['axis', 'polygon', 'fractal', 'star', 'icon',
//         'transform', 'pathpoints'].map(t => _itemMap(t, 'svg')))
// }

const canvas = {
    title: 'canvas',
    name: 'canvas',
    children: ['canvasShape', 'canvasPlant', 'canvasButterfly',
        'canvasStar', 'canvasGougu', 'canvasRough'].map(t => _itemMap(t, ContentType.Canvas))
}
const webgl = {
    title: 'webgl',
    name: 'webgl',
    children: ['webglShape'].map(t => _itemMap(t, ContentType.Webgl))
}

const lottie = {
    title: 'lottie',
    name: 'lottie',
    children: ['lottieGas'].map(t => _itemMap(t, ContentType.Lottie)),
    contentType: ContentType.Lottie
}

const editor = {
    title: 'editor',
    name: 'editor',
    contentType: ContentType.Editor
    // children:[]
}

const menu: IMenu[] = [svg, canvas, webgl, lottie, editor]

console.log(menu)
export default {
    menu,
    activeName: '',
    target: '.main .screen-viewer'
}