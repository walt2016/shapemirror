import { colorTypes } from '@/color'
import { treeCurveTypes } from '@/algorithm/tree'
import { fractalTypes } from '@/svg/fractal/fractal'
import { transformTypes } from '@/math/transform'
import { layoutTypes } from '@/algorithm/layout'
import { patternTypes } from '@/svg/factory'
import { labelsTypes } from './labels'
import { layoutAngles } from './layout'
import { fillPatterns } from '@/algorithm/fill'
import { mirrorTypes, mirrorRefractions } from '@/algorithm/mirror'
import { curveTypes } from '@/svg/core/curve'
import { pathModeTypes } from '../types/pathMode'
import { bottomControlGearTypes } from '@/algorithm/sierpinski'
import { _type, snake2camel } from '@/utils'
import { curveShapeTypes } from '@/algorithm/curve'

export const axisTypes = ['none', 'axis', 'grid', 'polar']

// 默认right 向内 left向外
// export const curveTypes = ['none', 'bezier', 'leftBezier', 'doubleBezier',
//     'sawtooth', 'leftSawtooth', 'doubleSawtooth',
//     'semicircle', 'leftSemicircle', 'circle', 'diamond', 'cross',
//     'triangle', 'leftTriangle', 'rightAngle', 'leftAngle', 'centripetal',
//     'leftCentripetal', 'doubleCentripetal', 'tangentCircle',
//     'sin', 'rough']
// export const mirrorTypes = ['none', 'vertex', 'edge', 'edgeMid', 'radiusMid', 'leftVertex','topVertex']
export const animationTypes = ['none', 'rotate', 'scale', 'skewX', 'skewY', 'translate', 'path']
export const motionTypes = animationTypes
export const motionOrigins = ['centre', 'vertex']

const monocolors = ['none', 'black', 'gray', 'red', 'blue', 'purple', 'green'] //,'yellow'
const strokeTypes = monocolors
const fillTypes = monocolors

const fillRuleTypes = ['none', 'nonzero', 'evenodd']
const strokeLinecapTypes = ['none', 'butt', 'round', 'square']
const strokeLinejoinTypes = ['none', 'miter', 'round', 'bevel']
export const strokeModeTypes = ['normal', 'outerStroke', 'innerStroke']
export const typesMap = {
	fractalTypes,
	transformTypes,
	patternTypes,
	colorTypes,
	strokeTypes,
	fillTypes,
	axisTypes,
	layoutTypes,
	treeCurveTypes,
	pathModeTypes,
	curveTypes,
	mirrorTypes,
	animationTypes,
	motionTypes,
	motionOrigins,
	labelsTypes,
	layoutAngles,
	fillPatterns,
	mirrorRefractions,
	bottomControlGearTypes,
	fillRuleTypes,
	strokeLinecapTypes,
	strokeLinejoinTypes,
	curveShapeTypes,
	strokeModeTypes,
}

// 参数顺序
export const keysOrder = ['shape', 'n', 'r', 'a', 'm', 'transform', 'pathModel', 'curve', 'edge', 'vertex', 'centre', 'excircle', 'incircle', 'labels', 'linkCross', 'radius', 'links', 'pattern', 'color', 'colorful', 'alpha', 'fill', 'props_.*?', 'axis', 'layout', 'axiom', 'rules_.*?', 'props', 'mirror', 'fractal', 'animation', 'motion']
// 默认参数
export const defaultProps = [
	{
		field: 'shape',
		type: 'string',
		disabled: true,
	},
	{
		field: 'n',
		type: 'number',
		min: 1,
		max: 999,
	},
	{
		field: 'r',
		type: 'number',
		min: 1,
		max: 999999,
	},
	{
		field: 'a',
		type: 'number',
	},
	{
		field: 'm',
		min: 1,
		max: 999,
	},
]
// 多层级属性
export const multiLevelProps = ['color', 'curve', 'edge', 'edgeExtension', 'vertex', 'centre', 'excircle', 'incircle', 'labels', 'rules', 'links', 'radius', 'props', 'mirror', 'fractal', 'animation', 'motion', 'layout', 'axis', 'mark', 'fill']

// 分割符
export const propsSplitChar = '_'
// 属于多层级属性
const getMulitLevelPropRoot = (field: string) => {
	let flag = multiLevelProps.some(t => {
		let pref = `${t}${propsSplitChar}`
		return field.indexOf(pref) === 0
	})
	if (flag) {
		return field.split(propsSplitChar)[0]
	}
	return ''
}

export const _groupField = (field: string, item) => {
	let group = getMulitLevelPropRoot(field)
	if (group) {
		Object.assign(item, {
			group,
			label: field.replace(new RegExp(`${group}_`), ''),
		})
	}
}

// 下拉选项字段
export const selectFields = ['fractal', 'transform', 'pattern', 'color', 'stroke', 'fill', 'axis', 'layout', 'treeCurve', 'pathMode', 'curve', 'mirror', 'mirror_type', 'mirror_refraction', 'animation_type', 'motion_type', 'motion_origin', 'curve_type', 'labels_type', 'color_type', 'layout_type', 'layout_angle', 'axis_type', 'fill_pattern', 'bottomControlGear', 'fillRule', 'strokeLinecap', 'strokeLinejoin', 'heartType', 'curveShape', 'strokeMode']

export const _selectOptions = (field: string, item) => {
	// 下拉选项
	selectFields.forEach(t => {
		const reg = new RegExp(`([a-z]+_)?${t}$`)
		if (reg.test(field)) {
			//field === t
			let types
			if (typesMap[`${t}Types`]) {
				types = typesMap[`${t}Types`] || []
			} else if (field.indexOf('_') >= 0) {
				types = typesMap[`${snake2camel(field)}s`] || []
			}

			if (Array.isArray(types)) {
				item.options = types.map(t => {
					return {
						label: t,
						value: t,
					}
				})
				item.type = 'select'
			}
		}
	})
}

// 能旋转的图形
// 会自动加角度a参数
export const rotatableShape = ['polygon', 'ray', 'triangle', 'square', 'pentagon', 'hexagon', 'heptagon', 'spiral', 'polar', 'star', 'leaf', 'gridRect', 'gridPolygon']

// 需要layout和axis的图形
export const layoutableShape = ['polygon', 'ray', 'triangle', 'square', 'pentagon', 'hexagon', 'heptagon', 'spiral', 'polar', 'star', 'leaf', 'wanhuachi', 'curve', 'wave', 'elliptical', 'sin', 'tree', 'plant', 'dragon', 'isometric', 'koch', 'mitsubishi', 'peano', 'polarShape', 'spiralShape', 'ringShape', 'rayShape', 'isometricShape', 'polygonShape', 'lsystem', 'lsystemPlant']
