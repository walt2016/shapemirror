const heavenlyStems = ['甲', '乙', '丙', '丁', '戊', '己', '庚', '辛', '壬', '癸']
const earthlyBranches = ['子', '丑', '寅', '卯', '辰', '巳', '午', '未', '申', '酉', '戌', '亥']
// 天干就是Heavenly Stems，地支就是Earthly Branches，他们两辆配对组成的“六十干支”，这里被译作sexagenary cycle。
export enum LabelType {
	Index = 'index',
	Alphabet = 'alphabet',
	HeavenlyStems = 'heavenlyStems',
	EarthlyBranches = 'earthlyBranches',
	SexagenaryCycle = 'sexagenaryCycle',
}
export const labelsTypes = Object.values(LabelType)

export const genLabel = (type: string, index: number): string => {
	switch (type) {
		case LabelType.Alphabet:
			return String.fromCharCode((index % 25) + 65)
		case LabelType.HeavenlyStems:
			return heavenlyStems[index % heavenlyStems.length]
		case LabelType.EarthlyBranches:
			return earthlyBranches[index % earthlyBranches.length]
		case LabelType.SexagenaryCycle:
			//  阳干配阳支，阴干配阴支：
			let x = Math.floor(index / 6)
			let y = x % 2 === 0 ? (2 * index) % 12 : ((2 * index) % 12) + 1
			return heavenlyStems[x] + earthlyBranches[y]
		default:
			return String(index)
	}
}

export const _alphabet = (index: number) => {
	return genLabel(LabelType.Alphabet, index)
}
