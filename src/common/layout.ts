import { layoutPoints } from '@/algorithm/layout'
import { angleMapFn } from '@/algorithm/angle'
import { IlayoutOptions } from '@/types/layout'
import { IShape } from '@/types/shape'

export const layoutAngles = Object.keys(angleMapFn)
export const _layout = (options: IlayoutOptions, shapes: IShape[], fn) => {
	let len = shapes.length
	let { layout, width, height, o } = options
	if (layout) {
		let type, angle
		if (typeof layout === 'object') {
			type = layout.type
			angle = layout.angle
		} else {
			type = layout
		}
		const lps = layoutPoints(type, { width, height, o })
		lps.forEach((t, i) => {
			const shapeObj = shapes[len === 1 ? 0 : i % len]
			const { n, a } = shapeObj

			const incFn = angleMapFn[angle]
			const inc = incFn ? incFn(a, i, n) : {}
			fn({
				shape: 'circle',
				...shapeObj,
				o: t,
				...inc,
			})
		})
	} else {
		shapes.forEach(t => fn(t))
	}
}
