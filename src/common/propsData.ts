import { keysOrder, defaultProps, multiLevelProps, rotatableShape, _groupField, _selectOptions, propsSplitChar } from './types'
import { sortByOrder } from '../math/arraySort'
import { _type, isObject } from '../utils'
import { _query, _attr } from '../ui'
import { emit } from '../ui/eventBus'
import { IContent, IModel, IScope } from '@/types/ui'
import { Color } from '@/types/color'
import { IFeild } from '@/types/field'

const getValue = (obj = {}, prop = '') => {
	const index = prop.indexOf(propsSplitChar)
	if (index === -1) {
		return obj ? obj[prop] : ''
	} else {
		return getValue(obj[prop.slice(0, index)], prop.slice(index + propsSplitChar.length))
	}
}
const setValue = (obj, prop, value) => {
	const index = prop.indexOf(propsSplitChar)
	if (index === -1) {
		obj[prop] = value
	} else {
		if (!obj[prop.slice(0, index)]) {
			obj[prop.slice(0, index)] = {}
		}
		setValue(obj[prop.slice(0, index)], prop.slice(index + propsSplitChar.length), value)
	}
}

const _push = (list, key) => {
	if (Array.isArray(key)) {
		key.forEach(t => _push(list, t))
	} else {
		!list.includes(key) && list.push(key)
	}
}
// 保留属性
const keepProps = (obj, t) => {
	return ['number', 'string', 'boolean'].includes(_type(obj[t]))
}

export const mapPropsData = shapeObj => {
	// 默认参数 旋转角度
	if (rotatableShape.includes(shapeObj.shape)) {
		if (shapeObj.a === undefined) {
			shapeObj.a = 0
		}
	}
	// 加颜色参数
	if (!shapeObj.color && !shapeObj.props && ['polygon', 'star', 'spiral', 'triangleLeaf'].includes(shapeObj.shape) && ['fractal', 'pathPoints', 'vertex'].some(t => shapeObj[t])) {
		shapeObj.color = Color.ColorCircle
	}

	let keys = Object.keys(shapeObj).filter(t => keepProps(shapeObj, t))

	// 多层级属性
	multiLevelProps.forEach(t => {
		const obj = shapeObj[t]
		if (isObject(obj)) {
			const props = Object.keys(obj)
			if (props.length) {
				props
					.filter(u => keepProps(obj, u))
					.forEach(u => {
						_push(keys, `${t}${propsSplitChar}${u}`)
					})
			} else {
				// 布尔型
				_push(keys, t)
			}
		} else if (obj) {
			// 字符
			_push(keys, t)
		}
	})

	// if (layoutable) {
	//     // 坐标轴
	//     _push(keys, ['axis', 'layout'])
	// }

	keys = sortByOrder(keys, keysOrder)
	return keys
}

export const mapFormFields = (content: IContent) => {
	const { shapes, axis } = content
	const shapeObj = shapes[0] || {}

	const keys = mapPropsData(shapeObj)

	return keys.map(field => {
		let value = field === 'axis' ? getValue(axis, field) : getValue(shapeObj, field)
		let type = _type(value)

		// 空对象转布尔
		if (type === 'object' && Object.keys(value).length === 0) {
			type = 'checkbox'
			value = true
		}

		if (type === 'number' && /(alpha|opacity)/.test(field)) {
			type = 'progress'
		}

		const item = {
			...defaultProps.find(t => t.field === field),
			field,
			label: field,
			value,
			type,
			disabled: /([a-z]+_)?shape$/.test(field),
		}

		// 字段分组
		_groupField(field, item)

		// 下拉选项
		_selectOptions(field, item)

		return item
	})
}

// 深度覆盖
const deepAssign = (obj, obj2) => {
	Object.keys(obj).forEach(t => {
		if (obj2.hasOwnProperty(t)) {
			if (_type(obj[t]) === 'object' && _type(obj2[t]) === 'object') {
				Object.assign(obj[t], obj2[t])
			} else {
				obj[t] = obj2[t]
			}
		}
	})
	return obj
}
export const contentAssignModel = (content, model) => {
	const newModel: IModel = {}
	Object.keys(model).forEach(t => setValue(newModel, t, model[t]))
	let shapes = content.shapes.filter(t => model.shape === t.shape)
	shapes.forEach(t => deepAssign(t, newModel))

	// 坐标轴
	content.axis = newModel.axis
	// 排版
	content.layout = newModel.layout

	// 画图
	return content
}

export const makeModel = (fields: IFeild[], form: HTMLFormElement): IScope => {
	const model: IModel = {}
	fields
		.filter(t => t.field)
		.forEach(t => {
			//['text','number','select'].includes(t.type)
			const key = t.field
			const item = _query(`[name=${key}]`, form) as HTMLInputElement
			if (item) {
				const type = _attr(item, 'type')
				switch (type) {
					case 'number':
						model[key] = Number(item.value)
						break
					case 'checkbox':
						model[key] = item.checked
						break
					default:
						model[key] = item.value
				}
			}
			// model[key] = type === 'number' ? Number(item.value) : item.value
		})
	const scope = {
		model,
		fields,
		form,
		emit, //事件
	}
	return scope
}
