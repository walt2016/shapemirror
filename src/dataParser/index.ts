import { IMenuItem } from '@/types/menu'

// 解析目录item
export const parseTreeItem = (t: IMenuItem): Promise<IMenuItem> => {
	return new Promise(resolve => {
		if (t.content) {
			return resolve(t)
		} else if (t.url) {
			if (/^canvas.*?/.test(t.url)) {
				import(`../data/canvas/index`).then(res => {
					return resolve({
						...t,
						content: res[t.url],
					})
				})
			} else if (/^svg.*?/.test(t.url)) {
				import(`../data/svg/index`).then(res => {
					return resolve({
						...t,
						content: res[t.url],
					})
				})
			} else if (/^(webgl|gl).*?/.test(t.url)) {
				import(`../data/webgl/index`).then(res => {
					return resolve({
						...t,
						content: res[t.url],
					})
				})
			} else if (/^(lottie).*?/.test(t.url)) {
				import(`../data/lottie/index`).then(res => {
					return resolve({
						...t,
						content: res[t.url],
					})
				})
			} else {
				import(`../data/html`).then(res => {
					return resolve({
						...t,
						content: res[t.url],
					})
				})
			}
		}
	})
}
