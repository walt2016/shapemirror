import './assets/css/index.less'

// import './page'
// import './page/hello'
// import './page/form'
// import './page/grid'
// import './page/crud'
// import './page/tabs'
// import './page/menu'
// import './page/fallmenu'
import './page/nav'
import * as config from '../package.json'

console.info(`%cversion: ${config.version}`, 'color: #009dff; font-size: 24px')
