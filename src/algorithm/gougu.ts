import { segToPolygonPoints } from './polar'
import { controlPoints } from './controlPoints'
import { _k, _polar, _rotate, _random } from '@/math'
import { IFractalOptions } from '@/types/fractal'
import { Point } from '@/types/point'

/**
 * 勾股树
 * @param options
 * @returns
 */
export const gouguPoints = (options: IFractalOptions): Point[][] => {
	const { o, n = 4, a = 180, r = 100, depth = 3, wriggle = 10 } = options
	const from = o
	const to = _polar(o, r, a)
	const _ra = () => {
		return wriggle ? _random(-wriggle, wriggle) : 0
	}

	const points = segToPolygonPoints([from, to], n, true)
	const result = [points]

	const _gougu = (points: Point[], i: number, depth: number): void => {
		const t = points[i]
		const next = points[(i + 1) % n]
		const [c1, c2] = controlPoints([t, next], -180 + _ra())

		const ps1 = segToPolygonPoints([c1, t], n)
		const ps2 = segToPolygonPoints([next, c1], n)

		result.push(ps1)
		result.push(ps2)

		if (depth > 0) {
			_gougu(ps1, Math.floor(n / 2), depth - 1)
			_gougu(ps2, Math.ceil(n / 2), depth - 1)
		}
	}
	_gougu(points, 0, depth)
	return result
}
