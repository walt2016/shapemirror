// axiom: F-F-F-F-F-F-F-F
// rules:
//  F => -F++F-

// depth: 5
// stepsPerFrame: 10
// angle: 45

// actions:
//   F => draw()

import { IGrammerOptions, ILSystemOptions, IRules } from '@/types/lsystem'
import { _sin, _cos, _polar, _rotate, _random } from '../math'
import { Point } from '@/types/point'

const _iter = (axiom = '', rules: IRules): string => {
	let tmp: string,
		result = ''
	axiom.split('').forEach(t => {
		tmp = rules[t]
		if (tmp) {
			result += tmp
		} else {
			result += t
		}
	})
	return result
}

const _grammar = (options: IGrammerOptions): string => {
	const { axiom = 'F', rules = { F: 'F[+F]' }, depth = 5, angle = 60 } = options
	let result = axiom
	for (let i = 0; i < depth; i++) {
		result = _iter(result, rules)
	}
	return result
}

export const lSystemPoints = (options: ILSystemOptions): Point[][] => {
	const { o = [0, 0], r = 20, angle = 60, wriggle = 10, reduction = 1.0 } = options
	const _ra = () => {
		return wriggle ? _random(-wriggle, wriggle) : 0
	}
	let a = 0
	const stack = []
	let point = o
	let radius = r
	const result = [[point]]
	const _branch = () => {
		result[result.length] = [point]
	}
	const _grow = () => {
		result[result.length - 1].push([...point])
	}
	const _forword = () => {
		point = _polar(point, radius, a - 90)
	}
	const _turnLeft = () => {
		a -= angle + _ra()
	}
	const _turnRight = () => {
		a += angle + _ra()
	}
	const _move = () => {
		_forword()
		_grow()
	}
	const _move2 = () => {
		_forword()
		_branch()
	}
	const _push = () => {
		stack.push({ point: [...point], a: a })
		radius *= reduction
	}
	const _pop = () => {
		;({ point: point, a: a } = stack.pop())
		_branch()
		radius /= reduction
	}
	const actions = {
		F: _move,
		f: _move2,
		'+': _turnLeft,
		'-': _turnRight,
		'[': _push,
		']': _pop,
	}
	const grammer = _grammar(options)

	const grammers = grammer.replace(/[^F+-\[\]]/g, '').split('')

	grammers.forEach(t => {
		var act = actions[t]
		if (act) {
			act()
		}
	})

	return result
}
