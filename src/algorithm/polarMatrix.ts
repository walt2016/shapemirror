import { _polar } from '@/math'
import { IPolarOptions } from '@/types/polar'
import { Point } from '@/types/point'

// 螺旋线点
export const spiralPoints = (options: IPolarOptions): Point[][] => {
	const { o = [0, 0], r = 10, n = 6, a = 0, m = 10 } = options
	const a1 = a
	const a2 = 360 + a1
	return Array.from({ length: m }, (_, j) => {
		return Array.from(
			{
				length: n,
			},
			(_, i) => {
				const a = a1 + (i * (a2 - a1)) / n
				return _polar(o, r * (j + 1) + (r * i) / n, a)
			}
		)
	})
}

// 环形
export const ringPoints = (options: IPolarOptions): Point[][] => {
	const { o = [0, 0], r = 10, n = 6, a = 0, m = 10 } = options
	const a1 = a
	const a2 = 360 + a1
	return Array.from({ length: m }, (_, j) => {
		const r2 = r * (j + 1)
		return Array.from(
			{
				length: n,
			},
			(_, i) => {
				const a = a1 + (i * (a2 - a1)) / n
				return _polar(o, r2, a)
			}
		)
	}).reverse()
}

// 射线
export const rayPoints = (options: IPolarOptions): Point[][] => {
	const { o = [0, 0], r = 10, n = 6, a = 0, m = 10 } = options
	const a1 = a
	const a2 = 360 + a1
	return Array.from({ length: n }, (_, j) => {
		const a = a1 + (j * (a2 - a1)) / n
		return Array.from(
			{
				length: m,
			},
			(_, i) => {
				const r2 = r * (i + 1)
				return _polar(o, r2, a)
			}
		)
	})
}
