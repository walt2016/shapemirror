import { _angleRad, _angleToRad, _deg, _dis, _mid, _o, _polarRad, _rotate, _moveDis } from '../math'
import { checkSweepFlag, checkPolygonSweepFlag } from '../math/vec'
import { _alphabet } from '../common/labels'
import { getBoundingClientRectPoints, getBounds } from './shapeRect'
import { IAngleInfo, IBoundsInfo, ILabelInfo, ILineInfo, IMarkInfo, IMarkInfoOptions } from '@/types/mark'
import { Point, PointSegment } from '@/types/point'

export const markInfo = (points: Point[], options: IMarkInfoOptions): IMarkInfo => {
	const { angle = true, edge = true, r = 20, radius = false, labels = false, boundingRect = false, boundingSize = false } = options
	const len = points.length
	const edgeInfo: ILineInfo[] = []
	const es1 = []
	const es2 = []
	const angleInfo: IAngleInfo[] = []
	const polygonSweepFlag = checkPolygonSweepFlag(points)
	const radiusInfo: ILineInfo[] = []
	const o = _o(points)
	const labelsInfo: ILabelInfo[] = []
	const boundingSizeInfo: IBoundsInfo[] = []
	let boundingRectPoints: Point[] = []

	points.forEach((t, i) => {
		// 逆时针 ===1
		const sweepFlag = checkSweepFlag(points, i)
		const prev = points[i - 1 < 0 ? len - 1 : i - 1]
		const next = points[(i + 1) % len]

		const a = _angleRad(t, next)
		const ap = _angleRad(t, prev)
		const ia = _angleToRad(prev, t, next)

		// 夹角
		if (angle) {
			const pn = _polarRad(t, r, a)
			const PP = _polarRad(t, r, ap)

			angleInfo.push({
				points: [PP, t, pn],
				a,
				ia,
				sweepFlag,
				o: t,
				r,
				labelPos: _polarRad(t, r * 2, sweepFlag === 0 ? a - ia / 2 : a + ia / 2),
				label: _deg(ia) + '°',
			})
		}
		// 半径
		if (radius) {
			radiusInfo.push({
				points: [o, t],
				labelPos: _mid(o, t),
				label: _dis(o, t),
			})
		}

		if (labels) {
			labelsInfo.push({
				labelPos: _moveDis(t, o, r),
				label: _alphabet(i),
			})
		}

		// 逆时针
		const k = polygonSweepFlag === 1 ? -1 : 1

		const pn = _polarRad(t, r * 2, a - (k * Math.PI) / 2)
		es1.push([t, pn])
		const PP = _polarRad(t, r * 2, ap + (k * Math.PI) / 2)
		es2.push([t, PP])
	})

	// 边长度
	if (edge) {
		const _es = (es1: PointSegment[], es2: PointSegment[]) => {
			es1.forEach((t, i) => {
				const next = es2[(i + 1) % len]
				const m = _mid(t[1], next[1])
				const d = _dis(t[1], next[1])
				edgeInfo.push({
					points: [...t, next[1], next[0]],
					label: d,
					labelPos: m,
				})
			})
		}
		_es(es1, es2)
	}

	if (boundingSize) {
		const rect = getBoundingClientRectPoints(points)
		const { left, right, top, bottom } = rect

		const by = bottom[1] + r * 3
		boundingSizeInfo.push({
			points: [left, [left[0], by], [right[0], by], right],
			label: right[0] - left[0],
			labelPos: [(right[0] + left[0]) / 2, by],
		})

		let lx = left[0] - r * 3
		boundingSizeInfo.push({
			points: [bottom, [lx, bottom[1]], [lx, top[1]], top],
			label: bottom[1] - top[1],
			labelPos: [lx, (bottom[1] + top[1]) / 2],
		})
	}

	if (boundingRect) {
		let rect = getBounds(points)
		let { left, right, top, bottom } = rect
		let midx = (left + right) / 2
		let midy = (top + bottom) / 2
		boundingRectPoints = [
			[left, top],
			[midx, top],
			[right, top],
			[right, midy],
			[right, bottom],
			[midx, bottom],
			[left, bottom],
			[left, midy],
		]
	}

	return {
		angleInfo,
		edgeInfo,
		radiusInfo,
		labelsInfo,
		boundingSizeInfo,
		boundingRectPoints,
	}
}
