import { IPolarOptions } from '@/types/polar'
import { _traversalPolar, _multiplePolar } from './traversal'
import { Point } from '@/types/point'

// 桃心型线
// x=16 * (sin(t)) ^ 3
// y=13 * cos(t) - 5 * cos(2 * t) - 2 * cos(3 * t) - cos(4 * t)
export const peachHeartPoints = ({ r = 15, o, a, n = 200, m = 1 }: IPolarOptions): Point[][] => {
	const _x = (t: number) => {
		return 16 * Math.pow(Math.sin(t), 3)
	}
	const _y = (t: number) => {
		return -1 * (13 * Math.cos(t) - 5 * Math.cos(2 * t) - 2 * Math.cos(3 * t) - Math.cos(4 * t))
	}
	const _r = (i: number) => {
		return (r * (m - i)) / m
	}

	return _multiplePolar({
		o,
		r: _r,
		a,
		n,
		m,
		_x,
		_y,
	})
}

// i = np.linspace(0,2*math.pi,500)
// x=np.cos(i)
// y=np.sin(i)+np.power(np.power(np.cos(i),2),1/3)
export const heartPoints = ({ r = 15, o, a, n = 200, m = 1 }: IPolarOptions): Point[][] => {
	let _x = (t: number) => {
		return 10 * Math.cos(t) // Math.pow(Math.sin(t), 3);
	}
	let _y = (t: number) => {
		return -10 * (Math.sin(t) + Math.pow(Math.pow(Math.cos(t), 2), 1 / 3)) // -1 * (13 * Math.cos(t) - 5 * Math.cos(2 * t) - 2 * Math.cos(3 * t) - Math.cos(4 * t));
	}
	let _r = (i: number) => {
		return (r * (m - i)) / m
	}
	return _multiplePolar({
		o,
		r: _r,
		a,
		n,
		m,
		_x,
		_y,
	})
}

// 笛卡尔心形函数
// x=size*(2*sin(t)+sin(2*t))
// y=size*(2*cos(t)+cos(2*t))
// Cartesian cardioid

export const cartesianCardioidPoints = ({ r = 100, o, a, n = 200, m = 1 }: IPolarOptions): Point[][] => {
	let _x = (t: number): number => {
		return 5 * (2 * Math.sin(t) + Math.sin(2 * t))
	}
	let _y = (t: number): number => {
		return 5 * (2 * Math.cos(t) + Math.cos(2 * t))
	}
	let _r = (i: number): number => {
		return (r * (m - i)) / m
	}
	return _multiplePolar({
		o,
		r: _r,
		a,
		n,
		m,
		_x,
		_y,
	})
}
