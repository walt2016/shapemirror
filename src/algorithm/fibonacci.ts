import { _polar, _k, _lerp } from '@/math'
import { IPolarOptions } from '@/types/polar'
import { _traversalPolar } from './traversal'
import { Point } from '@/types/point'

/**
 * // 斐波那契螺旋线 黄金螺旋线
斐波那契螺旋线，也称“黄金螺旋”，是斐波那契数列画出来螺旋曲线，
自然界中存在许多斐波那契螺旋线的图案，是自然界最完美的经典黄金比例。
作图规则是在以斐波那契数为边的正方形拼成的长方形中画一个90度的扇形，连起来的弧线就是斐波那契螺旋线。
它来源于斐波那契数列（FibonacciSequence），又称黄金螺旋分割。
注意斐波那契数列是【1，1，2，3，5，8，13，21，34，55，89，144，233，377，610，987】
,F表示数）
 */
//优化的斐波那契数列计算，把数列结果用数组保存
//用传统递归太慢了太费内存了

export const fibonacciSequence = (n: number): number[] => {
	const fib_val = [0, 1]
	const fib = (n: number) => {
		const len = fib_val.length
		for (let i = len; i <= n; i++) {
			fib_val.push(fib_val[i - 1] + fib_val[i - 2])
		}
		return fib_val[n]
	}
	fib(n)
	return fib_val
}

export const fibonacciPoints = (options: IPolarOptions): Point[][] => {
	const _fn = (options: IPolarOptions): Point[] => {
		const { o, r, a, n, increasedAgnle = 90 } = options
		const fs = fibonacciSequence(n)
		const p0 = _polar(o, r, a)
		const points = [p0]
		for (let i = 0; i < n; i++) {
			const p = _polar(o, r * fs[i + 1], -1 * increasedAgnle * (i + 1) + a)
			points.push(p)
		}

		return points
	}

	const { m } = options

	return Array.from({ length: m }).map((t, i) => {
		const a = (i * 360) / m
		return _fn({ ...options, a })
	})
}
