import { Point } from '@/types/point'
import { _random } from '../../math'

// type TDirect = 0 | 1 | 2 | 3
// 用0123表示 方向[up, right, bottom, left]
enum Direct {
	up,
	right,
	bottom,
	left,
}
const DIRECTS = [0, 1, 2, 3]
const INLET_DIRECTS = [2, 3, 0, 1]

// 每个方向上的通过状态，0表示不能通过，1表示可以通过
export type TMoveState = 0 | 1

// 节点状态, 4个方向
type TNodeState = [TMoveState, TMoveState, TMoveState, TMoveState]
type TMazeState = TNodeState[][]

// 下个节点
interface INextNode {
	status?: TMoveState
	direct: Direct
	pos: Point
}

export interface IMazeOptions {
	m?: number
	n?: number
	width?: number
	height?: number
	start?: Point
	padding?: number
}

interface BaseMaze {
	initMazeStatus: () => void
	initMazePoints: () => void
}
export class Maze implements BaseMaze {
	protected m: number
	protected n: number
	protected width: number
	protected height: number
	protected padding: number
	protected mazeStatus: TMazeState
	protected start: Point
	protected unitMaze: Point[][]
	protected mazePoints: Point[][]

	// public output: Point[][]

	constructor(options: IMazeOptions) {
		const { m = 3, n = 3, width = 100, height = 100, start = [0, 0], padding = 0 } = options
		this.n = n
		this.m = m
		this.width = width
		this.height = height
		this.start = start
		this.padding = padding
	}

	public setup = (): Point[][] => {
		this.initUnitMaze()
		this.initMazeStatus()
		this.initMazePoints()
		const tree = [[]]
		const stack = []
		this.move(this.start, tree, stack)
		return tree.map(t => t.map(([x, y]) => this.mazePoints[y][x]))
	}

	/**
	 * 单位点阵
	 * @returns
	 */
	private initUnitMaze = () => {
		const n = this.n
		const m = this.m
		const result = []
		for (let j = 0; j < m; j++) {
			const row = []
			for (let i = 0; i < n; i++) {
				row.push([i, j])
			}
			result.push(row)
		}
		this.unitMaze = result
	}

	/**
	 * 点阵坐标
	 * @param unitMaze
	 * @param options
	 * @returns
	 */
	initMazePoints = () => {
		const { m, n, width, height, padding } = this
		const [left, top] = [padding, padding]
		const r = Math.floor(Math.min((width - padding * 2) / n, (height - padding * 2) / m))

		this.mazePoints = this.unitMaze.map(row => {
			return row.map(([x, y]) => {
				return [left + x * r, top + y * r]
			})
		})
	}

	// 当前状态
	private getCurrNodeStatus = (currNode: Point): TNodeState => {
		const [x, y] = currNode
		if (x < this.n && y < this.m) {
			return this.mazeStatus[y][x]
		}
	}

	// 用0123表示 方向[up, right, bottom, left]
	private getNextNodeByDirect = (currNode: Point, direct: Direct): Point => {
		let [x, y] = currNode
		switch (direct) {
			case 0:
				y -= 1
				break
			case 1:
				x += 1
				break
			case 2:
				y += 1
				break
			case 3:
				x -= 1
				break
		}
		return [x, y]
	}

	// 反方向 进口方向
	// [0,1,2,3]
	// [2,3,0,1]
	private getInletDirect = (direct: Direct): Direct => {
		return INLET_DIRECTS[direct]
	}

	// 邻居
	private getNeighbours = (currNode: Point): INextNode[] => {
		const n = this.n
		const m = this.m
		return DIRECTS.map(t => {
			return {
				direct: t,
				pos: this.getNextNodeByDirect(currNode, t),
			}
		}).filter(t => {
			const [x, y] = t.pos
			return x >= 0 && y >= 0 && x < n && y < m
		})
	}

	// 4个方向上，哪个能走
	// 只找 TMoveState =1的
	private getNextNodes = ({ pos, status }: { pos: Point; status: TNodeState }): INextNode[] => {
		return status
			.map((t, i) => {
				return {
					status: t,
					direct: i,
					pos: t ? this.getNextNodeByDirect(pos, i) : null,
				}
			})
			.filter(t => t.status)
	}

	// 用0123表示 方向[up, right, bottom, left]
	private setMazeStatus = (currNode: Point, direct: Direct): void => {
		const [x, y] = currNode
		if (x < 0 || y < 0) return
		if (!this.mazeStatus[y]) return
		const currNodeStatus: TNodeState = this.getCurrNodeStatus(currNode)

		if (!currNodeStatus) return
		// 指定方向置0，其他保持不变
		currNodeStatus[direct] = 0
	}

	// 初始化状态
	// 约定四边状态
	initMazeStatus = (): void => {
		const m = this.m - 1 //unitMaze.length - 1
		const n = this.n - 1 //unitMaze[0].length - 1

		this.mazeStatus = this.unitMaze.map(row => {
			return row.map(([x, y]) => {
				let up: TMoveState = 1
				let right: TMoveState = 1
				let bottom: TMoveState = 1
				let left: TMoveState = 1
				if (x === 0) {
					left = 0
				}
				if (x === n) {
					right = 0
				}
				if (y === 0) {
					up = 0
				}
				if (y === m) {
					bottom = 0
				}
				return [up, right, bottom, left]
			})
		})
	}

	// 移动
	private move = (currNode: Point, his: Point[][] = [[]], stack: Point[] = []): void => {
		if (!currNode) return

		const currNodeStatus: TNodeState = this.getCurrNodeStatus(currNode)
		if (!currNodeStatus) {
			// 异常坐标状态
			return
		}
		// 可移动方向和坐标
		const nextNodes: INextNode[] = this.getNextNodes({
			pos: currNode,
			status: currNodeStatus,
		})

		// 记录当前步骤
		his[his.length - 1].push(currNode)
		stack.push(currNode)

		// 当前节点不能进了
		// 更新邻居的状态
		const neighbours: INextNode[] = this.getNeighbours(currNode)
		neighbours.forEach(t => {
			this.setMazeStatus(t.pos, this.getInletDirect(t.direct))
		})

		// 计算超标了
		if (his.length > 1000 || stack.length > 1000) {
			return
		}

		if (nextNodes.length > 0) {
			// 随机选择
			const next = _random(nextNodes)
			// 当前节点出路去了一条
			this.setMazeStatus(currNode, next.direct)
			this.move(next.pos, his, stack)
		} else {
			// 退回重新走
			if (stack.length === 2) {
				if (his[his.length - 1].length === 1) {
					his.pop()
				}
				return
			}
			stack.pop()
			// 上一个
			let next2 = stack.pop()
			if (!next2) return
			if (Array.isArray(next2)) {
				// new branch
				let len = his[his.length - 1].length
				if (len === 0) return
				if (len === 1) {
					his[his.length - 1] = []
				} else if (len > 1) {
					his.push([])
				}
				this.move(next2, his, stack)
			}
		}
	}
}

export const mazePoints = (options: IMazeOptions): Point[][] => {
	return new Maze(options).setup()
	// return maze.output
}
