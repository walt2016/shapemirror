import { _cos, _polar, _sin } from '@/math'
import { IMazeOptions, Maze } from './maze'
import { Point } from '@/types/point'

export class WaveMaze extends Maze {
	constructor(options: IMazeOptions) {
		super(options)
	}
	// override initMazeStatus = () => {
	//     const m = this.m - 1  //unitMaze.length - 1
	//     const n = this.n - 1 //unitMaze[0].length - 1

	//     this.mazeStatus = this.unitMaze.map(row => {
	//         return row.map(([x, y]) => {
	//             let up: TMoveState = 1
	//             let right: TMoveState = 1
	//             let bottom: TMoveState = 1
	//             let left: TMoveState = 1
	//             if (x === 0) {
	//                 left = 0
	//             }
	//             if (x === n) {
	//                 right = 0
	//             }
	//             if (y === 0) {
	//                 up = 0
	//             }
	//             if (y === m) {
	//                 bottom = 0
	//             }
	//             return [up, right, bottom, left]
	//         })
	//     })

	// }

	/**
	 * 点阵坐标
	 * @param unitMaze
	 * @param options
	 * @returns
	 */
	initMazePoints = () => {
		const { m, n, width, height, padding } = this
		const r = Math.floor(Math.min((width - padding * 2) / n, (height - padding * 2) / m))
		const [left, top] = [padding, padding]
		this.mazePoints = this.unitMaze.map(row => {
			return row.map(([i, j]) => {
				const a = (i * 180) / (n - 1)
				return [left + r * i, top + r * j * _sin(a)]
			})
		})
	}
}

export const waveMazePoints = (options: IMazeOptions): Point[][] => {
	return new WaveMaze(options).setup()
}
