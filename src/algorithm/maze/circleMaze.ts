import { _polar } from '@/math'
import { IMazeOptions, Maze, TMoveState } from './maze'
import { Point } from '@/types/point'

export class CircleMaze extends Maze {
	constructor(options: IMazeOptions) {
		super(options)
	}
	override initMazeStatus = () => {
		const m = this.m - 1 //unitMaze.length - 1
		const n = this.n - 1 //unitMaze[0].length - 1

		this.mazeStatus = this.unitMaze.map(row => {
			return row.map(([x, y]) => {
				let up: TMoveState = 1
				let right: TMoveState = 1
				let bottom: TMoveState = 1
				let left: TMoveState = 1
				if (x === 0) {
					left = 0
				}
				if (x === n) {
					right = 0
				}
				if (y === 0) {
					up = 0
				}
				if (y === m) {
					bottom = 0
				}
				return [up, right, bottom, left]
			})
		})
	}

	/**
	 * 点阵坐标
	 * @param unitMaze
	 * @param options
	 * @returns
	 */
	initMazePoints = () => {
		const { m, n, width, height, padding } = this
		const r = Math.floor(Math.min((width - padding * 2) / n, (height - padding * 2) / m)) / 2
		const o = [width / 2, height / 2]
		this.mazePoints = this.unitMaze.map(row => {
			return row.map(([i, j]) => {
				return _polar(o, r * (j + 1), (i * 360) / (n - 1))
			})
		})
	}
}

export const circleMazePoints = (options: IMazeOptions): Point[][] => {
	return new CircleMaze(options).setup()
}
