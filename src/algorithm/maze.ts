import { unitMatrix, _random } from '../math'
import { gridCrossPoints } from '../algorithm/grid'
import { IGridOptions } from '@/types/grid'
import { Point } from '@/types/point'

// type TDirect = 0 | 1 | 2 | 3
// 用0123表示 方向[up, right, bottom, left]
enum EDirect {
	up,
	right,
	bottom,
	left,
}
const DIRECTS = [0, 1, 2, 3]
const INLET_DIRECTS = [2, 3, 0, 1]

// 每个方向上的通过状态，0表示不能通过，1表示可以通过
type TMoveState = 0 | 1

// 节点状态, 4个方向
type TNodeState = [TMoveState, TMoveState, TMoveState, TMoveState]
type TMazeState = TNodeState[][]

// 下个节点
interface INextNode {
	status?: TMoveState
	direct: EDirect
	pos: Point
}
// 当前状态
const getCurrNodeStatus = (mazeStatus: TMazeState, currNode: Point): TNodeState => {
	const [x, y] = currNode
	const currNodeStatus = mazeStatus[y][x]
	return currNodeStatus
}

// 用0123表示 方向[up, right, bottom, left]
const getNextNodeByDirect = (currNode: Point, direct: EDirect): Point => {
	let [x, y] = currNode
	switch (direct) {
		case 0:
			y -= 1
			break
		case 1:
			x += 1
			break
		case 2:
			y += 1
			break
		case 3:
			x -= 1
			break
	}
	return [x, y]
}

// 反方向 进口方向
// [0,1,2,3]
// [2,3,0,1]
const getInletDirect = (direct: EDirect): EDirect => {
	return INLET_DIRECTS[direct]
}

// 邻居
const getNeighbours = (currNode: Point, n: number, m: number): INextNode[] => {
	return DIRECTS.map(t => {
		return {
			direct: t,
			pos: getNextNodeByDirect(currNode, t),
		}
	}).filter(t => {
		const [x, y] = t.pos
		return x >= 0 && y >= 0 && x < n && y < m
	})
}

// 4个方向上，哪个能走
// 只找 TMoveState =1的
const getNextNodes = ({ pos, status }: { pos: Point; status: TNodeState }): INextNode[] => {
	return status
		.map((t, i) => {
			return {
				status: t,
				direct: i,
				pos: t ? getNextNodeByDirect(pos, i) : null,
			}
		})
		.filter(t => t.status)
}

// 用0123表示 方向[up, right, bottom, left]
const setMazeStatus = (mazeStatus: TMazeState, currNode: Point, direct: EDirect): TMazeState => {
	const [x, y] = currNode
	if (x < 0 || y < 0) return mazeStatus
	if (!mazeStatus[y]) return mazeStatus
	const currNodeStatus: TNodeState = getCurrNodeStatus(mazeStatus, currNode)

	if (!currNodeStatus) return mazeStatus
	// 指定方向置0，其他保持不变
	currNodeStatus[direct] = 0
	return mazeStatus
}

// 初始化状态
// 约定四边状态
const initMazeStatus = (coors: Point[][]): TMazeState => {
	const m = coors.length - 1
	const n = coors[0].length - 1

	return coors.map(row => {
		return row.map(([x, y]) => {
			let up: TMoveState = 1
			let right: TMoveState = 1
			let bottom: TMoveState = 1
			let left: TMoveState = 1
			if (x === 0) {
				left = 0
			}
			if (x === n) {
				right = 0
			}
			if (y === 0) {
				up = 0
			}
			if (y === m) {
				bottom = 0
			}
			return [up, right, bottom, left]
		})
	})
}

// 移动
const move = (mazeStatus: TMazeState, currNode: Point, his: Point[][] = [[]], stack: Point[] = []): void => {
	if (!currNode) return
	const m = mazeStatus.length
	const n = mazeStatus[0].length

	const currNodeStatus: TNodeState = getCurrNodeStatus(mazeStatus, currNode)
	// 可移动方向和坐标
	const nextNodes: INextNode[] = getNextNodes({
		pos: currNode,
		status: currNodeStatus,
	})

	// 记录当前步骤
	his[his.length - 1].push(currNode)
	stack.push(currNode)

	// 当前节点不能进了
	// 更新邻居的状态
	const neighbours: INextNode[] = getNeighbours(currNode, n, m)
	neighbours.forEach(t => {
		setMazeStatus(mazeStatus, t.pos, getInletDirect(t.direct))
	})

	// 计算超标了
	if (his.length > 1000 || stack.length > 1000) {
		return
	}

	if (nextNodes.length > 0) {
		// 随机选择
		const next = _random(nextNodes)
		// 当前节点出路去了一条
		setMazeStatus(mazeStatus, currNode, next.direct)
		move(mazeStatus, next.pos, his, stack)
	} else {
		// 退回重新走
		if (stack.length === 2) {
			if (his[his.length - 1].length === 1) {
				his.pop()
			}
			return
		}
		stack.pop()
		// 上一个
		let next2 = stack.pop()
		if (!next2) return
		if (Array.isArray(next2)) {
			// new branch
			let len = his[his.length - 1].length
			if (len === 0) return
			if (len === 1) {
				his[his.length - 1] = []
			} else if (len > 1) {
				his.push([])
			}
			move(mazeStatus, next2, his, stack)
		}
	}
}

export const mazePoints = (options: IGridOptions): Point[][] => {
	const { height = 600, width = 800, o = [400, 300], r = 50, padding = 30, start = [0, 0] } = options
	const gridCross: Point[][] = gridCrossPoints({
		height,
		width,
		o,
		r,
		padding,
	})
	const coors: Point[][] = unitMatrix(gridCross)
	const mazeStatus: TMazeState = initMazeStatus(coors)
	const tree = [[]]
	const stack = []
	move(mazeStatus, start, tree, stack)
	return tree.map(t => t.map(([x, y]) => gridCross[y][x]))
}
