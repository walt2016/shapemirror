import { plainMatrix, translate, translateX, translateY } from '@/math'
import { IGridOptions } from '@/types/grid'
import { Point } from '@/types/point'
import { IPolarOptions } from '@/types/polar'

// 交叉点
// grid cross point
export const gridCrossPoints = (options: IGridOptions): Point[][] => {
	const { width = 800, height = 600, o = [400, 300], padding = 30, r = 50 } = options

	const psX = []
	const result = []

	// x轴
	const n = Math.ceil((width / 2 - padding) / r)
	Array.from({ length: n }).forEach((t, i) => {
		if (i === 0) {
			psX.push(o)
		} else {
			psX.unshift(translateX(o, -i * r))
			psX.push(translateX(o, i * r))
		}
	})

	// y轴
	let m = Math.ceil((height / 2 - padding) / r)
	Array.from({ length: m }).forEach((t, i) => {
		if (i === 0) {
			result.push(psX)
		} else {
			result.unshift(psX.map(t => translateY(t, -i * r)))
			result.push(psX.map(t => translateY(t, i * r)))
		}
	})
	return result
}

export const matrixPoints = ({ o = [400, 300], r = 50, n = 2, m = 2 }: IPolarOptions): Point[][] => {
	const psX = []
	const psY = []
	Array.from({ length: n }).forEach((t, i) => {
		if (i === 0) return
		psX.unshift(translate(o, [-i * r + r / 2, r / 2]))
		psX.push(translate(o, [i * r - r / 2, r / 2]))
	})

	Array.from({ length: m }).forEach((t, i) => {
		if (i === 0) {
			psY.push(psX)
		} else {
			psY.unshift(psX.map(t => translateY(t, -i * r)))
			if (i < m - 1) {
				psY.push(psX.map(t => translateY(t, i * r)))
			}
		}
	})

	return psY
}

// grid单元格点
export const gridCellPoints = (options?: IGridOptions): Point[][] => {
	const { width = 800, height = 600, o = [400, 300], padding = 30, r = 50 } = options ?? {}

	// x轴
	const n = Math.ceil((width / 2 - padding) / r)

	// y轴
	const m = Math.ceil((height / 2 - padding) / r)
	return matrixPoints({
		o,
		r,
		n,
		m,
	})
}

// 排版布局
export const layoutPoints = (total: string, options: IGridOptions): Point[] => {
	const { width = 800, height = 600, o = [400, 300], padding = 30, r = 50 } = options

	// '0', '2x2', '4x4', '10x8', '25'
	const nMap = {
		'2x2': 4,
		'4x4': 8,
		'10x8': 16,
		'18x12': 25,
	}

	const lr = (r * 5 * 4) / (nMap[total] || total)
	// // x轴
	const n = Math.ceil((width / 2 - padding) / lr)
	// y轴
	const m = Math.ceil((height / 2 - padding) / lr)
	// let n = 2, m = 2

	// switch (total) {
	//     case 4:
	//         n = 2;
	//         m = 2
	//         break;
	//     case 5:
	//         n = 5
	//         m = 1
	//     case 8:
	//         n = 3;
	//         m = 2
	//         break;
	//     case 16:
	//         n = 4;
	//         m = 4
	//         break;
	//     case 25:
	//         n = 5;
	//         m = 5
	//         break;
	// }

	const mps = matrixPoints({
		r: lr,
		n,
		m,
		o,
	})

	return plainMatrix(mps)
}
