import { _polar, translateY, _random } from '@/math'
import { PathMode } from '@/types/pathMode'
import { ITreeOptions } from '@/types/tree'
import { Point } from '@/types/point'
export const treeCurveTypes = ['QQ', 'QL', 'V', 'LL']

export const treePoints = (options: ITreeOptions): Point[][] => {
	const { o = [300, 200], n = 3, r = 100, a = -90, depth = 1, attenuation = 2 / 3, wriggle = 10, pathMode, treeCurve = 'QQ' } = options

	const result = new Array(depth)
	const _ra = () => {
		return wriggle ? _random(-wriggle, wriggle) : 0
	}

	const _iter = (o: Point, r: number, a: number, depth: number) => {
		const p = _polar(o, r, a + _ra())
		const a1 = a + 30 + _ra(),
			a2 = a - 30 + _ra()
		const r1 = r * attenuation,
			r2 = r * attenuation
		if (!result[depth]) {
			result[depth] = []
		}
		result[depth].push(o, p)

		if (depth > 0) {
			_iter(p, r1, a1, depth - 1)
			_iter(p, r2, a2, depth - 1)
		}
	}
	if (n == 1) {
		_iter(translateY(o, r) as Point, r, a, depth)
	} else {
		Array.from({ length: n }).forEach((_, i) => {
			_iter(o, r, a + (i * 360) / n, depth)
		})
	}

	return result.reverse()
}

export const treeModePoints = (options: ITreeOptions): Point[][] => {
	const { o = [300, 200], n = 3, r = 100, a = -90, depth = 1, attenuation = 2 / 3, wriggle = 10, pathMode } = options

	const result = new Array(depth)
	const _ra = () => {
		return wriggle ? _random(-wriggle, wriggle) : 0
	}

	const modeMap = {
		[PathMode.LINES]: ([o, p, p1, p2]) => {
			return [o, p]
		},
		[PathMode.SQUARES]: ([o, p, p1, p2]) => {
			return [o, p, p1, p2]
		},
		[PathMode.TRIANGLES]: ([o, , p1, p2]) => {
			return [o, p1, p2]
		},
	}
	const _mode = modeMap[pathMode] || modeMap[PathMode.TRIANGLES]
	const _iter = (o, r, a, depth): void => {
		const p = _polar(o, r, a + _ra())
		const a1 = a + 30 + _ra(),
			a2 = a - 30 + _ra()
		const r1 = r * attenuation,
			r2 = r * attenuation
		const p1 = _polar(p, r1, a1)
		const p2 = _polar(p, r2, a2)
		if (!result[depth]) {
			result[depth] = []
		}

		result[depth].push(..._mode([o, p, p1, p2]))
		// squares
		// result[depth].push(o, p, p1, p2)
		// triangle
		// result[depth].push(o, p1, p2)
		// switch (treeCurve) {
		//     case 'QL':
		//         // 曲线1 QL
		//         result[depth].push([o, [...p, ...p1]], [p, p2])
		//         break;
		//     case 'QQ':
		//         // 曲线2 QQ
		//         result[depth].push([o, [...p, ...p1]], [o, [...p, ...p2]])
		//         break;
		//     case 'V':
		//         // 二叉树 V
		//         result[depth].push([o, p1], [o, p2])
		//         break
		//     default:
		//         // 二叉树 LL
		//         result[depth].push([o, p, p1], [p, p2])
		// }

		if (depth > 0) {
			_iter(p1, r1, a1, depth - 1)
			_iter(p2, r2, a2, depth - 1)
		}
	}

	if (n == 1) {
		_iter(translateY(o, r), r, a, depth)
	} else {
		Array.from({ length: n }).forEach((_, i) => {
			_iter(o, r, a + (i * 360) / n, depth)
		})
	}

	return result.reverse()
}
