// 线段切分
import { _k, _polar, _rotate } from '@/math'
import { TSweepFlag } from '@/types'
import { _multiplePolar } from './traversal'
import { Point, PointSegment } from '@/types/point'
import { IPolarOptions } from '@/types/polar'

// 圆弧切分
export const polarPoints = (options: IPolarOptions): Point[][] => {
	const { o, r = 200, a = 0, n = 4, sweepFlag = true, m = 1 } = options
	const _x = (t: number) => {
		return Math.cos(t)
	}
	const _y = (t: number) => {
		return Math.sin(t)
	}
	const _r = (i: number) => {
		return (r * (m - i)) / m
	}

	return _multiplePolar({
		o,
		r: _r,
		a,
		n,
		m,
		sweepFlag,
		_x,
		_y,
	})
}
// bak
export const polar2Points = (options: IPolarOptions): Point[] => {
	const { o = [0, 0], r = 100, n = 6, a = 0, direction = 1 } = options

	const a1 = a
	const a2 = options.a2 || 360 + a1

	const ps = Array.from(
		{
			length: n,
		},
		(_, i) => {
			const a = a1 + (i * (a2 - a1)) / n
			return _polar(o, r, a)
		}
	)
	return direction === 1 ? ps : ps.reverse()
}

// 坐标网格线
export const polarGridPoints = (options: IPolarOptions): Point[][] => {
	const { o = [0, 0], r = 100, n = 6, a = 0, m = 5 } = options
	const a1 = a,
		a2 = 360 + a1
	return Array.from({ length: m }, (_, j) => {
		return Array.from(
			{
				length: n,
			},
			(_, i) => {
				const a = a1 + (i * (a2 - a1)) / n
				return _polar(o, r * (j + 1), a)
			}
		)
	})
}

// 等角
export const isometricPoints = (options: IPolarOptions): Point[] => {
	const { o = [0, 0], r = 100, a = 45, n = 10 } = options

	return Array.from(
		{
			length: n,
		},
		(_, i) => {
			return _polar(o, r, a * i)
		}
	)
}

// 根据线段 作图 多边形
// 逆时针
export const segToPolygonPoints = ([from, to]: PointSegment, n: number, sweepFlag?: TSweepFlag): Point[] => {
	// 内角
	const ia = 180 - 360 / n
	const points = [from, to]
	const fn = (p: Point, o: Point): void => {
		const next = _rotate(p, o, ia * (sweepFlag ?? true ? 1 : -1)).map(t => _k(t))
		points.push(next)
		const len = points.length
		if (len < n) {
			fn(o, next)
		}
	}

	fn(from, to)
	return points
}

// 等边 已知边，作多边形
export const polygonPoints = (options: IPolarOptions): Point[] => {
	const { o = [0, 0], r = 100, a = 45, n = 10, sweepFlag = true } = options

	const from = o
	const to = options.to || _polar(o, r, a)

	return segToPolygonPoints([from, to], n, sweepFlag)
}
