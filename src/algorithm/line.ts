import { Point } from '@/types/point'
import { _polar, _atan, _dis } from '../math'
import { eachNext } from '../utils'
// 线段分割： 分割数量
export const linePoints = (p1: Point, p2: Point, n: number): Point[] => {
	const r = _dis(p1, p2)
	const a = _atan(p1, p2)
	return Array.from(
		{
			length: n,
		},
		(_, index) => _polar(p1, (index * r) / n, a)
	)
}

// 折线
export const polylinePoints = (points: Point[], options: { m: number }): Point[] => {
	const { m = 100 } = options
	const ps = []
	eachNext(points, (t, index, next) => {
		ps.push(...linePoints(t, next, m))
	})
	return ps
}
