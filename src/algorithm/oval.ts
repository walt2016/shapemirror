// 开普勒卵形线(Kepler oval)一种特殊曲线，，它是孟格尔(Miinger)卵形线p= 2a cos"B取n=3的一种。
// 开普勒(Kepler,J.)在《天文学的光学部分》中，研究了二次曲线的互相转化问题，并给出了此曲线。

// 1. 笛卡尔卵形线
// 1.1 定义
// A，B是平面内两个定点，平面内满足m*PA+n*PB=b（b是定长，m，n是两个固定正数）的点P的轨迹称为笛卡儿卵形线。

// 1.2 光学性质
// 选取适当的 m,n 值，可使通过点 A 的光线经折射后，全部通过点 B。笛卡尔曾利用这个光学性质，选取笛卡尔卵形线作为透镜的截面形状，用来消除球面像差。

// 2. 卡西尼卵形线
// A，B 是平面内两个定点，AB=2c（c是定长），平面内满足 MA*MB=a^2(a是定长）的点 M 的轨迹称为卡西尼卵形线。

// (x2+y2)2−2c2(x2−y2)=a4−b4
// (x2+y2)2−2c2(x2−y2)=a4−b4
// 当 a=c 时的卡西尼卵形线，是双钮线
