// 手绘风格
import { _random } from '@/math'
import { IPolarOptions } from '@/types/polar'
import { polarPoints } from './polar'
import { _traversal } from './traversal'
import { Point } from '@/types/point'
export const _offset = (p: Point, offset = 5): Point => {
	return p.map(t => {
		return t + _random(-offset, offset)
	})
}

export const roughPoints = (options: IPolarOptions): Point[][] => {
	const matrix = polarPoints(options)

	const result = []
	const loop = true
	const iter = ({ points: [t, next] }) => {
		result.push([_offset(t), _offset(next)])
		result.push([_offset(t), _offset(next)])
	}

	matrix.forEach(points => {
		_traversal({ points, loop, iter })
	})

	return result
}
