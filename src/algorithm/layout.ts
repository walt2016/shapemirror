import { IlayoutOptions } from '@/types/layout'
import { plainMatrix } from '../math'
import { _squarePoints, _squarePoints4_4 } from './square'
export const layoutTypes = ['0', '2x2', '4x4']

// 排版布局
export const layoutPoints = (layout: string, options: IlayoutOptions) => {
	const { width = 800, height = 600, o = [400, 300] } = options

	const r = Math.floor(Math.min(width, height) / 4)

	let arr = []
	switch (layout) {
		case '2x2':
			arr = _squarePoints(o, r)
			break
		case '4x4':
			arr = plainMatrix(_squarePoints4_4(o, r))
			break
		default:
			arr = [o]
	}

	return arr
}
