import { _mid, _dis, _polar, _atan, _k, _sin, _o } from '@/math'
import { IPolarOptions } from '@/types/polar'
import { Point } from '@/types/point'
import { eachNext } from '@/utils'
export const sinPoints = (points: Point[], options?: IPolarOptions): Point[] => {
	let { k = 0, a = 0, w = 1 } = options ?? {} //  num = 36, r = 100,
	const ps = []
	const o = _o(points)
	const r = _dis(o, points[0])
	const num = _k(r / 3, 0)
	eachNext(points, (t, index, next) => {
		const m = _mid(t, next)
		a = _atan(o, m)
		ps.push(
			...Array.from(
				{
					length: num,
				},
				(t, i) => [_k((i * 360) / num + o[0] - 180), _k(r * _sin(w * ((i * 360) / num - 180) - a) + o[1] - k)]
			)
		)
	})
	return ps
}
