import { IPolarOptions } from '@/types/polar'
import { _multiplePolar } from './traversal'
import { Point } from '@/types/point'
// 玫瑰线的参数方程：
// 玫瑰线的极坐标方程为 ρ=a* sin(nθ), ρ=a*cos(nθ)；
// 直角坐标方程为 x=a* sin(nθ)* cos(θ), y=a*sin(nθ)* sin(θ)。
// 玫瑰线的三种特征：

// 1、叶子数n为奇数，叶片数=n，θ角在0-π内玫瑰线是闭合的；
// n为偶数，叶片数=2n，θ角取值在0-2π内玫瑰线才是闭合和完整的；
// 2、当n为非整数的有理数时，设为L/W,且L/W为简约分数,此时,L与W不可能同时为偶数。
// L决定玫瑰线的叶子数,W决定玫瑰线的闭合周期(Wπ或2Wπ,见特性3)及叶子的宽度,W越大,叶子越宽。
// 但W也会同时影响叶子数的多少,对同一奇数值L,在W分别取奇数和偶数值时,叶子数也是不同的。
// 3、当L或W中有一个为偶数时,玫瑰线的叶子数为2L,闭合周期为2Wπ。
// 当L或W同为奇数时,玫瑰线的叶子数为L,闭合周期为Wπ。
// 换句话说,生成偶数个叶子的玫瑰线, L或W中必须有且只有一个为偶数值,且L为叶子数的一半,而生成奇数个叶子的玫瑰线, L和W都必须为奇数,且L值就是叶子数。

export const rosePoints = (options: IPolarOptions): Point[][] => {
	const { o, r, n = 200, w = 4, a = 0, m = 1 } = options
	const _x = (t: number) => {
		return Math.sin(w * t) * Math.cos(t)
	}
	const _y = (t: number) => {
		return Math.sin(w * t) * Math.sin(t)
	}
	const _r = (i: number) => {
		return (r * (m - i)) / m
	}
	return _multiplePolar({
		o,
		r: _r,
		a,
		n,
		m,
		_x,
		_y,
	})
}
