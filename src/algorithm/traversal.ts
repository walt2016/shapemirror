// 遍历函数
import { toMatrix } from '@/math/array'
import { _k, _rad } from '@/math'
import { ITraversalOptions, ITraversalPolarOptions } from '@/types'
import { Point } from '@/types/point'

// 连续
// Continuous signal
// serial
export const _traversal = ({ ctx, points, n = 2, loop, iter, init, discrete = false }: ITraversalOptions): Point[] => {
	const len = points.length
	const result = []
	const _iter = (ps: Point[], i: number) => {
		result[result.length] = iter({ ctx, points: ps, index: i })
	}
	const _init = (ps?: Point[]) => {
		if (init) {
			result[result.length] = init({ ctx, point: ps ? ps[0] : points[0] })
		}
	}

	if (discrete) {
		// 离散
		const matrix = toMatrix(points, n)
		matrix.forEach((ps, i) => {
			if (ps.length === n) {
				_init(ps)
				_iter(ps, i)
			}
		})
	} else {
		if (init) {
			_init()
		}
		// 连续
		if (loop) {
			for (let i = 0; i < len; i += n - 1) {
				let ps = []
				for (let j = 0; j < n; j++) {
					ps.push(points[(i + j) % len])
				}
				_iter(ps, i)
			}
		} else {
			for (let i = 0; i < len - n + 1; i += n - 1) {
				const ps = []
				for (let j = 0; j < n; j++) {
					ps.push(points[i + j])
				}
				_iter(ps, i)
			}
		}
	}

	return result
}

// 极坐标遍历
export const _traversalPolar = ({
	n,
	o,
	r,
	a = 0,
	_x,
	_y,
	sweepFlag = true, // 1顺时针 0逆时针
}: ITraversalPolarOptions): Point[] => {
	// 最大角度
	const maxt = 360 //2 * Math.PI;
	// 根据增长弧度得循环次数
	// const maxi = Math.ceil(maxt / increasedAgnle);
	const points = []

	// 每次增长多少弧度
	const vt = (sweepFlag ? 1 : -1) * _rad(maxt / n)

	const _r = (i: number) => {
		return typeof r === 'function' ? r(i) : r
	}
	let x = 0
	let y = 0
	let t = a ? _rad(a) : 0
	for (let i = 0; i < n; i++) {
		x = _x(t)
		y = _y(t)
		t += vt
		points.push([x * _r(i) + o[0], y * _r(i) + o[1]].map(t => _k(t)))
	}
	return points
}

// 多层遍历极坐标
export const _multiplePolar = (options: ITraversalPolarOptions): Point[][] => {
	const { n, o, r, a, _x, _y, sweepFlag, m = 1 } = options
	const _r = (i: number) => {
		return typeof r === 'function' ? r(i) : r
	}

	const matrix = []
	for (let i = 0; i < m; i++) {
		matrix[matrix.length] = _traversalPolar({
			n,
			o,
			r: _r(i),
			a,
			_x,
			_y,
			sweepFlag,
		})
	}
	return matrix
}
