import { _random } from '@/math'
import { IAngle } from '@/types/base'

const _a = (a: number = 0, i: number = 0, n: number = 1): IAngle => {
    return { a: a + i * 360 / n / 2 }
}
const _b = (a: number = 0, i: number = 0, n: number = 1): IAngle => {
    return { a: a + i * 360 / n / 3 }
}
const _c = (a: number = 0, i: number = 0, n: number = 1): IAngle => {
    return { a: a + i * 360 / n / 4 }
}
const _symmetry = (a: number = 0, i: number = 0, n: number = 1): IAngle => {
    if (i % 2 === 0) {
        return {
            a: a + 360 / n / 4
        }
    }
    return {
        a: a - 360 / n / 4
    }

}
const _rand = (a: number = 0, i: number = 0, n: number = 1): IAngle => {
    return {
        a: a + _random(0, 360)
    }
}
const _none = (): IAngle => {
    return {
        a: 0
    }
}
// 角度函数
export const angleMapFn = {
    none: _none,
    symmetry: _symmetry,
    a: _a,
    b: _b,
    c: _c,
    random: _rand,

}

