import { IPolarOptions } from '@/types/polar'
import { _traversalPolar } from './traversal'
import { Point } from '@/types/point'

/**
 * 摆线，又称旋轮线、圆滚线，在数学中，摆线（Cycloid）被定义为，一个圆沿一条直线运动时，圆边界上一定点所形成的轨迹。
它是一般旋轮线的一种。摆线也是最速降线问题和等时降落问题的解。
摆线也是最速降线问题和等时降落问题的解。
方    程x=r*(t-sint)； y=r*(1-cost)
它的长度等于旋转圆直径的 4 倍。尤为令人感兴趣的是，它的长度是 一个不依赖于π的有理数。
 * @param param0 
 * @returns 
 */
export const cycloidPoints = ({ r = 15, o, a = 0, n = 200 }: IPolarOptions): Point[] => {
	const _x = (t: number): number => {
		return t - Math.sin(t)
	}
	const _y = (t: number): number => {
		return 1 - Math.cos(t) //  5 * (2 * Math.cos(t) + Math.cos(2 * t));
	}
	return _traversalPolar({
		o,
		r,
		a,
		n,
		_x,
		_y,
	})
}
