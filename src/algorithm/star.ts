import { Point } from '@/types/point'
import { edgeExtensionSegs } from './link'
import { neighbourSegCrossPoints } from './segCross'
// 芒星
export const starPoints = (points: Point[]): Point[] => {
	const len = points.length
	const segs = edgeExtensionSegs(points, len > 6 ? 0 : 1)
	const lcps = neighbourSegCrossPoints(segs)
	const result = []

	points.forEach((t, i) => {
		result.push(t)
		result.push(lcps[i - 1 >= 0 ? i - 1 : len - 1])
	})
	return result
}
