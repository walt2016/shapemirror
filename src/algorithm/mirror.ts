import { _o, _dis, _mid, _moveDis } from '../math'
import { isObject, _has, _type } from '../utils'
import { _mirror } from '../math/mirror'
import { newSeq } from '../math/arraySort'
import { getLeftPoint, getTopPoint, getRightPoint, getBottomPoint, getBoundingClientRectPoints } from './shapeRect'
import { angleMapFn } from './angle'
import { IMirrorProps } from '@/types/mirror'
import { Point } from '@/types/point'
// 外圈顶点
export const _borderPoints = (matrix: Point[][], mirror): Point[] => {
	const index = isObject(mirror) ? mirror.borderIndex : 0
	const len = matrix.length
	return matrix[index >= len || index <= 0 ? len - 1 : index]
}
// 镜像点
const _mirrorPoint = (point: Point, index: number, mirrorPoints: Point[]) => {
	return mirrorPoints ? mirrorPoints[index % mirrorPoints.length] : point
}

const _none = (points: Point[]) => {
	return points || []
}
const _seq = (ps: Point[], i: number): Point[] => {
	let len = ps.length
	return newSeq(ps, i % len)
}
const _refraction = (refraction: Function | number, i: number, len: number): number => {
	if (typeof refraction === 'function') {
		let ra = refraction(0, i, len).a
		return ra
	}
	return refraction
}
const _vertexOneline = (points: Point[], { scale, refraction, departure = 0, onelineOffset }, mirrorPoints: Point[]): Point[] => {
	const result = []
	const len = points.length
	const o = mirrorPoints ? _o(mirrorPoints) : _o(points)
	points.forEach((t, i) => {
		let mp = _mirrorPoint(t, i, mirrorPoints)
		mp = _moveDis(mp, o, departure)
		let ra = _refraction(refraction, i, len)
		let ps = _mirror(points, mp, scale, ra)
		result.push(t)
		result.push(..._seq(ps, i + onelineOffset))
	})
	return result
}

const _vertexMirror = (points: Point[], { scale, refraction, departure = 0 }, mirrorPoints: Point[]): Point[][] => {
	const o = mirrorPoints ? _o(mirrorPoints) : _o(points)
	const len = points.length
	const ps = points.map((t, i) => {
		let mp = _mirrorPoint(t, i, mirrorPoints)
		mp = _moveDis(mp, o, departure)
		let ra = _refraction(refraction, i, len)
		let ps = _mirror(points, mp, scale, ra)
		return ps
	})
	return [points, ...ps]
}
const _edgeOneline = (points: Point[], { scale, refraction, onelineOffset, departure = 0 }, mirrorPoints: Point[]): Point[] => {
	const result = []
	const o = mirrorPoints ? _o(mirrorPoints) : _o(points)
	const ms = _mid(points)
	const mms = mirrorPoints ? _mid(mirrorPoints) : null
	const len = points.length
	ms.forEach((t, i) => {
		let mp = _mirrorPoint(t, i, mms)
		mp = _moveDis(mp, o, departure)
		const ra = _refraction(refraction, i, len)
		const ps = _mirror(points, mp, scale, ra)
		result.push(points[i])
		result.push(..._seq(ps, i + onelineOffset))
	})
	return result
}
const _edgeMirror = (points: Point[], { scale, refraction, departure = 0 }, mirrorPoints: Point[]): Point[][] => {
	const o = mirrorPoints ? _o(mirrorPoints) : _o(points)
	const ms = _mid(points)
	const mms = mirrorPoints ? _mid(mirrorPoints) : null
	const len = points.length
	const ps = ms.map((t, i) => {
		let mp = _mirrorPoint(t, i, mms)
		mp = _moveDis(mp, o, departure)
		const ra = _refraction(refraction, i, len)
		const ps = _mirror(points, mp, scale, ra)
		return ps
	})
	return [points, ...ps]
}
const _edgeMidOneline = (points: Point[], { scale, refraction, onelineOffset, departure = 0 }, mirrorPoints: Point[]): Point[] => {
	const result = []
	const len = points.length
	const ms = _mid(points)
	const mms = mirrorPoints ? _mid(mirrorPoints) : null
	const o = mirrorPoints ? _o(mirrorPoints) : _o(points)
	ms.forEach((t, i) => {
		let mp = _mirrorPoint(t, i, mms)
		mp = _moveDis(mp, o, departure)
		const ra = _refraction(refraction, i, len)
		const ps = _mirror(ms, mp, scale, ra)
		result.push(points[i])
		result.push(..._seq(ps, i + onelineOffset))
	})
	return result
}
const _edgeMidMirror = (points: Point[], { scale, refraction, departure = 0 }, mirrorPoints: Point[]): Point[][] => {
	const len = points.length
	const ms = _mid(points)
	const mms = mirrorPoints ? _mid(mirrorPoints) : null
	const o = mirrorPoints ? _o(mirrorPoints) : _o(points)
	const ps = ms.map((t, i) => {
		let mp = _mirrorPoint(t, i, mms)
		mp = _moveDis(mp, o, departure)
		const ra = _refraction(refraction, i, len)
		const ps = _mirror(ms, mp, scale, ra)
		return ps
	})
	return [points, ...ps]
}

const _radiusMidOneline = (points, { scale, refraction, onelineOffset, departure = 0 }, mirrorPoints: Point[]): Point[] => {
	const result = []
	const len = points.length
	const o = mirrorPoints ? _o(mirrorPoints) : _o(points)
	points.forEach((t, i) => {
		let mp = _mirrorPoint(t, i, mirrorPoints)
		mp = _moveDis(mp, o, departure)
		const rm = _mid(mp, o)
		const ra = _refraction(refraction, i, len)
		const ps = _mirror(points, rm, scale, ra)
		result.push(t)
		result.push(..._seq(ps, i + onelineOffset))
	})
	return result
}
const _radiusMidMirror = (points: Point[], { scale, refraction, departure = 0 }, mirrorPoints: Point[]): Point[][] => {
	const len = points.length
	const o = mirrorPoints ? _o(mirrorPoints) : _o(points)
	const ps = points.map((t, i) => {
		let mp = _mirrorPoint(t, i, mirrorPoints)
		mp = _moveDis(mp, o, departure)
		const rm = _mid(mp, o)
		const ra = _refraction(refraction, i, len)
		const ps = _mirror(points, rm, scale, ra)
		return ps
	})
	return [points, ...ps]
}

const _leftVertexMirror = (points: Point[], { scale, refraction, departure = 0 }, mirrorPoints: Point[]): Point[][] => {
	let p = getLeftPoint(points)
	const o = mirrorPoints ? _o(mirrorPoints) : _o(points)
	const len = points.length
	p = _moveDis(p, o, departure)
	// let mp = _mirrorPoint(t, i, mirrorPoints)
	const ra = _refraction(refraction, 1, len)
	const ps = _mirror(points, p, scale, ra)
	return [points, ps]
}
const _topVertexMirror = (points: Point[], { scale, refraction, departure = 0 }, mirrorPoints: Point[]): Point[][] => {
	let p = getTopPoint(points)
	const o = mirrorPoints ? _o(mirrorPoints) : _o(points)
	p = _moveDis(p, o, departure)
	const len = points.length
	const ra = _refraction(refraction, 1, len)
	const ps = _mirror(points, p, scale, ra)
	return [points, ps]
}

const _rightVertexMirror = (points: Point[], { scale, refraction, departure = 0 }, mirrorPoints: Point[]): Point[][] => {
	let p = getRightPoint(points)
	const o = mirrorPoints ? _o(mirrorPoints) : _o(points)
	p = _moveDis(p, o, departure)
	const len = points.length
	const ra = _refraction(refraction, 1, len)
	const ps = _mirror(points, p, scale, ra)
	return [points, ps]
}

const _bottomVertexMirror = (points: Point[], { scale, refraction, departure = 0 }, mirrorPoints: Point[]): Point[][] => {
	let p = getBottomPoint(points)
	const o = mirrorPoints ? _o(mirrorPoints) : _o(points)
	p = _moveDis(p, o, departure)
	const len = points.length
	const ra = _refraction(refraction, 1, len)
	const ps = _mirror(points, p, scale, ra)
	return [points, ps]
}

const _rectVertexMirror = (points: Point[], { scale, refraction, departure = 0 }, mirrorPoints: Point[]): Point[][] => {
	const len = points.length
	const o = mirrorPoints ? _o(mirrorPoints) : _o(points)
	const { top, right, bottom, left } = getBoundingClientRectPoints(points)
	const ps = [top, right, bottom, left].map((t, i) => {
		let mp = _mirrorPoint(t, i, mirrorPoints)
		mp = _moveDis(mp, o, departure)
		const ra = _refraction(refraction, i, len)
		const ps = _mirror(points, mp, scale, ra)
		return ps
	})
	return [points, ...ps]
}

const onelineMapFn = {
	none: _none,
	vertex: _vertexOneline,
	edge: _edgeOneline,
	edgeMid: _edgeMidOneline,
	radiusMid: _radiusMidOneline,
	leftVertex: _leftVertexMirror,
	topVertex: _topVertexMirror,
	rightVertex: _rightVertexMirror,
	bottomVertex: _bottomVertexMirror,
	rectVertex: _rectVertexMirror,
}

const mirrorMapFn = {
	none: _none,
	vertex: _vertexMirror,
	edge: _edgeMirror,
	edgeMid: _edgeMidMirror,
	radiusMid: _radiusMidMirror,
	leftVertex: _leftVertexMirror,
	topVertex: _topVertexMirror,
	rightVertex: _rightVertexMirror,
	bottomVertex: _bottomVertexMirror,
	rectVertex: _rectVertexMirror,
}

export const mirrorTypes = Object.keys(mirrorMapFn)

// 镜像迭代 一笔画
export const _onelinePoints = (type: string, points: Point[], options, mirrorPoints: Point[]) => {
	const fn = onelineMapFn[type] || _none
	return fn(points, options, mirrorPoints)
}
// 镜像迭代
export const _mirrorIterPoints = (type: string, points: Point[], options, mirrorPoints: Point[]) => {
	const fn = mirrorMapFn[type] || _none
	return fn(points, options, mirrorPoints)
}

// 镜像点
export const _mirrorPoints = (points: Point[], mirror: IMirrorProps, mirrorPoints?: Point[]): Point[][] => {
	// if (isMatrix(points)) {
	//     let mirrorPoints = _borderPoints(points, mirror)
	//     return points.map(t => {
	//         return _mirrorPoints(t, mirror, mirrorPoints)
	//     }) as T
	// }

	let type,
		scale,
		oneline = false,
		onelineOffset = 1,
		refraction = 0,
		departure = 0
	if (isObject(mirror)) {
		type = mirror.type
		scale = mirror.scale || 1
		oneline = mirror.oneline
		onelineOffset = mirror.onelineOffset
		refraction = angleMapFn[mirror.refraction] || mirror.refraction
		departure = mirror.departure
	} else {
		type = mirror
	}
	const fn = oneline ? _onelinePoints : _mirrorIterPoints
	return fn(type, points, { scale, onelineOffset, refraction, departure }, mirrorPoints)
}

export const mirrorRefractions = Object.keys(angleMapFn)
