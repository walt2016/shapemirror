// 扫描填充线
import { getBounds } from './shapeRect'
import { edgeCrossPoints, neighbourSegCrossPoints } from './segCross'
import { PathMode } from '@/types/pathMode'
import { _dis, _lerp, _o, _polar } from '@/math'
import { toMatrix } from '@/math/array'
import { oppositeDirection } from '@/math/vec'
import { rayMix } from '@/math/arrayMix'
import { IFillOptions } from '@/types/fill'
import { Point } from '@/types/point'
import { Color } from '@/types/color'

// 横线
export const fillHorizontalPoints = ({ points, gridDensity }: IFillOptions): Point[] => {
	const rect = getBounds(points)
	const { left, right, top, bottom, width, height } = rect
	const n = height / gridDensity
	const segs = []
	const ps: Point[] = []
	const offset = gridDensity / 2
	for (let i = 0; i < n; i++) {
		const p = _lerp(top, bottom, i / n)
		const seg = [
			[left - offset, p + offset],
			[right + offset, p + offset],
		]
		segs[segs.length] = seg
	}
	const ps2 = edgeCrossPoints(points, segs)
	ps.push(...ps2)
	return ps
}
// 条纹
const _strip = (ps: Point[]): Point[] => {
	const len = ps.length
	const pp = []

	for (let i = 0; i <= len - 4; i += 4) {
		const t = ps[i]
		const next = ps[i + 1]
		const next2 = ps[i + 2]
		const next3 = ps[i + 3]
		const oop = oppositeDirection([t, next], [next3, next2])
		if (oop) {
			pp.push(t, next, next3, next2)
		} else {
			pp.push(t, next, next2, next3)
		}
	}
	return pp
}
// 横条纹
const fillHorizontalStripPoints = ({ points, gridDensity }: IFillOptions): Point[] => {
	const ps = fillHorizontalPoints({ points, gridDensity })
	return _strip(ps)
}

// 竖线
export const fillVerticalPoints = ({ points, gridDensity }: IFillOptions): Point[] => {
	const rect = getBounds(points)
	const { left, right, top, bottom, width, height } = rect
	const n = width / gridDensity
	const segs = []
	const offset = gridDensity / 2
	for (let i = 0; i < n; i++) {
		const p = _lerp(left, right, i / n)
		segs[segs.length] = [
			[p + offset, top - offset],
			[p + offset, bottom + offset],
		]
	}
	const ps = edgeCrossPoints(points, segs)
	return ps
}

// 竖条纹
const fillVerticalStripPoints = ({ points, gridDensity }: IFillOptions): Point[] => {
	const ps = fillVerticalPoints({ points, gridDensity })
	return _strip(ps)
}

// 网格线
export const fillGridPoints = ({ points, gridDensity }: IFillOptions): Point[] => {
	const ps = fillVerticalPoints({ points, gridDensity })
	const ps2 = fillHorizontalPoints({ points, gridDensity })
	return [...ps, ...ps2]
}

export const fillGridLineStripPoints = ({ points, gridDensity }: IFillOptions) => {
	const ps = fillVerticalPoints({ points, gridDensity })
	const ps2 = fillHorizontalPoints({ points, gridDensity })
	return [ps, ps2]
}

// 斜线
// /---backslash 反斜线 \---slash 正斜线
export const fillBackslashPoints = ({ points, gridDensity }: IFillOptions): Point[] => {
	const rect = getBounds(points)
	const { left, right, top, bottom, width, height } = rect
	const n = width / gridDensity
	const m = height / gridDensity
	const segs = []
	const leftTop = [left - gridDensity, top - gridDensity]
	const leftBottom = [left - gridDensity, bottom + gridDensity]
	const rightTop = [right + gridDensity, top - gridDensity]
	const rightBottom = [right + gridDensity, bottom - gridDensity]
	for (let i = 1; i <= Math.max(n, m); i++) {
		const p = _lerp(leftTop, leftBottom, i / m)
		const p2 = _lerp(leftTop, rightTop, i / n)
		segs[segs.length] = [p, p2]

		const p3 = _lerp(rightBottom, leftBottom, i / m)
		const p4 = _lerp(rightBottom, rightTop, i / n)
		segs[segs.length] = [p3, p4]
	}
	const ps = edgeCrossPoints(points, segs)
	return ps
}

// 射线
export const fillRayPoints = ({ points, rayDensity = 10 }: IFillOptions): Point[] => {
	const o = _o(points)
	const { width } = getBounds(points)
	const r = width
	const n = 360 / rayDensity
	const segs = []
	for (let i = 0; i < n; i++) {
		const p = _polar(o, r + 10, rayDensity * i)
		segs.push([o, p])
	}

	const ps = edgeCrossPoints(points, segs)
	return rayMix(o, ps)
}

const fillRayStripPoints = ({ points }: IFillOptions): Point[] => {
	const o = _o(points)
	const len = points.length
	const ps = []
	points.forEach((t, i) => {
		if (i % 2 === 0) {
			const next = points[(i + 1) % len]
			ps.push(o, t, next)
		}
	})
	return ps
}

const vertexLightPoints = ({ points, offset = 0.3 }: IFillOptions): Point[] => {
	const len = points.length
	const ps = []
	points.forEach((t, i) => {
		const to = (Math.floor((len - 1) / 2) + i) % len
		const p = _lerp(points[to], points[(to + 1) % len], offset)
		ps.push(t, p)
	})
	return ps
}

const vertexLightCrossPoints = ({ points, offset = 0.3 }: IFillOptions): Point[] => {
	const len = points.length
	const ps = []
	const segs = []
	points.forEach((t, i) => {
		const to = (Math.floor((len - 1) / 2) + i) % len
		const p = _lerp(points[to], points[(to + 1) % len], offset)
		segs.push([t, p])
	})
	const nscps = neighbourSegCrossPoints(segs)
	nscps.forEach((t, i) => {
		ps.push(points[i], t)
	})
	return ps
}

const vertexLightAreaPoints = ({ points, offset = 0.3 }: IFillOptions): Point[] => {
	const len = points.length
	const ps = []
	const segs = []
	points.forEach((t, i) => {
		const to = (Math.floor((len - 1) / 2) + i) % len
		const p = _lerp(points[to], points[(to + 1) % len], offset)
		segs.push([t, p])
	})
	const nscps = neighbourSegCrossPoints(segs)
	nscps.forEach((t, i) => {
		const next = points[(i + 1) % len]
		ps.push(points[i], t, next)
	})
	return ps
}

// const vertexLightAreaMtrixPoints = ({ points, offset = 0.3 }) => {
//     let len = points.length
//     let ps = []
//     let segs = []
//     points.forEach((t, i) => {
//         let to = (Math.floor((len - 1) / 2) + i) % len
//         let p = _lerp(points[to], points[(to + 1) % len], offset)
//         segs.push([t, p])
//     })
//     let nscps = neighbourSegCrossPoints(segs)
//     nscps.forEach((t, i) => {
//         let next = points[(i + 1) % len]
//         ps.push([points[i], t, next])
//     })
//     return ps
// }

// const edgeMidFractal = () => { }

// horizontal stripe 横纹
// 竖线为vertical line
// 十字线

export const fillPatternPointsMap = {
	none: () => [],
	horizontal: fillHorizontalPoints,
	horizontalStrip: fillHorizontalStripPoints,
	horizontalLineStrip: fillHorizontalPoints,
	vertical: fillVerticalPoints,
	verticalStrip: fillVerticalStripPoints,
	verticalLineStrip: fillVerticalPoints,
	grid: fillGridPoints,
	gridLineStrip: fillGridLineStripPoints,
	backslash: fillBackslashPoints,
	ray: fillRayPoints,
	rayStrip: fillRayStripPoints,
	vertexLight: vertexLightPoints,
	vertexLightCross: vertexLightCrossPoints,
	vertexLightArea: vertexLightAreaPoints,
	// vertexLightColorArea: vertexLightAreaMtrixPoints
}

export const fillPatterns = Object.keys(fillPatternPointsMap).concat(['taichi'])

const fillPatternPathModeMap = {
	rayStrip: PathMode.LINE_LOOP,
	vertexLightArea: PathMode.TRIANGLES,
	// vertexLightColorArea: PathMode.LINE_LOOP,
	default: PathMode.LINES,
	horizontalStrip: PathMode.SQUARES,
	verticalStrip: PathMode.SQUARES,
	verticalLineStrip: PathMode.LINE_STRIP,
	horizontalLineStrip: PathMode.LINE_STRIP,
	gridLineStrip: PathMode.LINE_STRIP,
	// ray: PathMode.POINTS
}
export const fillPatternMap = ({ pattern, points, gridDensity, rayDensity, offset }: IFillOptions) => {
	const fn = fillPatternPointsMap[pattern]
	let ps = fn({ points, gridDensity, rayDensity, offset })
	const pathMode = fillPatternPathModeMap[pattern] || fillPatternPathModeMap['default']

	let color
	if (pathMode === PathMode.TRIANGLES) {
		ps = toMatrix(ps, 3)
		color = {
			type: Color.ColorCircle,
			fill: 'black',
		}
	}

	return {
		points: ps,
		pathMode,
		color,
	}
}
