import { IFractalOptions } from '@/types/fractal'
import { _squarePoints } from './square'
import { Point } from '@/types/point'

const _peano = (o: Point, r: number, depth: number, sweep: boolean, startIndex: number, a: number): Point[] => {
	const points = _squarePoints(o, r, sweep, startIndex, a)
	if (depth === 0) {
		return points
	} else {
		const ps = []
		points.forEach((t, i) => {
			if (i === 0) {
				ps.push(..._peano(t, r / 2, depth - 1, !sweep, startIndex, a))
			} else if (i === 1) {
				ps.push(..._peano(t, r / 2, depth - 1, sweep, startIndex, a))
			} else if (i === 2) {
				ps.push(..._peano(t, r / 2, depth - 1, sweep, startIndex, a))
			} else if (i === 3) {
				ps.push(..._peano(t, r / 2, depth - 1, !sweep, startIndex ? 0 : 2, a))
			}
		})
		return ps
	}
}

export const peanoPoints = (options: IFractalOptions): Point[] => {
	const { o, r, depth, a } = options
	const points = _peano(o, r, depth, false, 0, a)
	return points
}
