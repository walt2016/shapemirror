// // 阿基米德螺线
// 一个点在射线上匀速向外运动，同时射线以w的速度转动，点的轨迹就被称为阿基米德螺旋线或等速螺线。
// 阿基米德螺旋线的极坐标公式可以表示为：
// r = a + b ∗ θ r = a+b*\theta
// r=a+b∗θ
// 在直角坐标系下，利用极坐标系到直角坐标的公式，其公式可以被改写为：
// x = r ∗ c o s θ y = r ∗ s i n θ x = r*cos\theta\\ y = r*sin\theta
// x=r∗cosθ
// y=r∗sinθ

// x = ( a + b ∗ θ ) ∗ c o s θ y = ( a + b ∗ θ ) ∗ s i n θ x = (a+b*\theta)*cos\theta\\ y = (a+b*\theta)*sin\theta
// x=(a+b∗θ)∗cosθ
// y=(a+b∗θ)∗sinθ

// 角速度和线速度的概念来控制螺线的形状，生成其他螺旋线：
// x = v t ∗ c o s ( w t ) y = v t ∗ c o s ( w t ) x = vt*cos(wt)\\ y = vt*cos(wt)
// x=vt∗cos(wt)
// y=vt∗cos(wt)
import { IPolarOptions } from '@/types/polar'
import { _polar, _rad } from '../math'
import { _traversalPolar } from './traversal'
import { Point } from '@/types/point'

// Archimedean spiral
// 阿基米德螺线（亦称等速螺线），得名于公元前三世纪希腊数学家阿基米德。
// 阿基米德螺线是一个点匀速离开一个固定点的同时又以固定的角速度绕该固定点转动而产生的轨迹。

// 阿基米德螺旋公式可以用指定的半径r，圆周速度v，直线运动速度w来表示
// x(t)=vt*cos(wt), y(t)=vt*sin(wt)
export const spiralPoints = (options: IPolarOptions): Point[][] => {
	const { r = 100, o, a = 0, n = 200, v = 1, w = 5, m = 2 } = options
	// const rad = _rad(a) // Math.PI
	// const _x = (t: number) => {
	//     return (v * t) * Math.cos(w * t + rad);
	// }
	// const _y = (t: number) => {
	//     return (v * t) * Math.sin(w * t + rad);
	// }

	const matrix = []
	for (let i = 0; i < m; i++) {
		const rad = _rad(a + (i * 360) / m)
		const _x = (t: number) => {
			return v * t * Math.cos(w * t + rad)
		}
		const _y = (t: number) => {
			return v * t * Math.sin(w * t + rad)
		}
		const points = _traversalPolar({
			o,
			r,
			n,
			_x,
			_y,
		})
		matrix.push(points)
	}
	return matrix
}

// 螺旋线点
export const spiral2Points = (options: IPolarOptions): Point[][] => {
	const { o = [0, 0], r = 10, n = 6, a = 0, m = 10 } = options
	const a1 = a
	const a2 = 360 + a1
	return Array.from({ length: m }, (_, j) => {
		return Array.from(
			{
				length: n,
			},
			(_, i) => {
				const a = a1 + (i * (a2 - a1)) / n
				return _polar(o, r * (j + 1) + (r * i) / n, a)
			}
		)
	})
}
