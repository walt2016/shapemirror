import { IBoundsPoints } from '@/types'
import { IBounds } from '@/types/base'
import { Point } from '@/types/point'

// 四至
const getLeft = (points: Point[]): number => {
	return Math.min(...points.map(t => t[0]))
}
export const getLeftPoint = (points: Point[]): Point => {
	const leftX = getLeft(points)
	return points.filter(t => t[0] === leftX)[0]
}

const getRight = (points: Point[]): number => {
	return Math.max(...points.map(t => t[0]))
}
export const getRightPoint = (points: Point[]): Point => {
	const rightX = getRight(points)
	return points.filter(t => t[0] === rightX)[0]
}

const getTop = (points: Point[]): number => {
	return Math.min(...points.map(t => t[1]))
}
export const getTopPoint = (points: Point[]): Point => {
	const topY = getTop(points)
	return points.filter(t => t[1] === topY)[0]
}

const getBottom = (points: Point[]): number => {
	return Math.max(...points.map(t => t[1]))
}
export const getBottomPoint = (points: Point[]): Point => {
	const bottomY = getBottom(points)
	return points.filter(t => t[1] === bottomY)[0]
}

export const getBounds = (points: Point[]): IBounds => {
	const bottom = getBottom(points),
		left = getLeft(points),
		top = getTop(points),
		right = getRight(points)
	return {
		bottom,
		left,
		top,
		right,
		width: Math.abs(left - right),
		height: Math.abs(top - bottom),
	}
}

export const getBoundingClientRectPoints = (points: Point[]): IBoundsPoints => {
	const bottom = getBottomPoint(points),
		left = getLeftPoint(points),
		top = getTopPoint(points),
		right = getRightPoint(points)
	return {
		bottom,
		left,
		top,
		right,
		width: Math.abs(left[0] - right[0]),
		height: Math.abs(top[1] - bottom[1]),
	}
}
