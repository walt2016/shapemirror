import { isPoints, rotatePoints } from '@/math'
import { newSeq } from '@/math/arraySort'
import { Point } from '@/types/point'

// 单位正方形
// 顺时针
export const unitSquare: Point[] = [
	[1, -1],
	[-1, -1],
	[-1, 1],
	[1, 1],
]
// 逆时针
const unitSquareSweep: Point[] = [
	[1, -1],
	[1, 1],
	[-1, 1],
	[-1, -1],
]
// [[-1, 1], [1, 1], [1, -1], [-1, -1]]
// 放大缩小
// 支持多点围绕中点旋转
export const _unitScale = (p: Point | Point[], radio: number, a?: number) => {
	if (isPoints(p)) {
		if (a) {
			p = rotatePoints(p, a)
		}
		return p.map(t => _unitScale(t, radio))
	}
	return [p[0] * radio, p[1] * radio]
}

// 线段平移
export const _unitMove = <T extends Point | Point[]>(p: T, to: Point, radio?: number, a?: number): T => {
	if (isPoints(p)) {
		if (radio) {
			const points = _unitScale(p, radio, a)
			return _unitMove(points, to)
		}
		return p.map(t => _unitMove(t, to)) as T
	} else {
		return [p[0] + to[0], p[1] + to[1]] as T
	}
}

export const _squarePoints = (o: Point, r: number, sweep?: boolean, startIndex?: number, a?: number): Point[] => {
	let arr = [...(sweep ? unitSquareSweep : unitSquare)]
	if (startIndex) {
		arr = newSeq(arr, startIndex)
	}
	return _unitMove(arr, o, r, a)
}

// 2x2
// _squarePoints
// 4x4
export const _squarePoints4_4 = (o: Point, r: number): Point[][] => {
	const arr = _squarePoints(o, r)
	return arr.map(t => _squarePoints(t, r / 2))
}
