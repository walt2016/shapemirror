import { peachHeartPoints, cartesianCardioidPoints, heartPoints } from './heart'
import { cycloidPoints } from './cycloid'
import { IPolarOptions } from '@/types/polar'
import { Point } from '@/types/point'
export const curvePointsConfig = {
	peachHeart: peachHeartPoints,
	cartesianCardioid: cartesianCardioidPoints,
	cycloid: cycloidPoints,
	heart: heartPoints,
}
export const curveShapeTypes = Object.keys(curvePointsConfig)

export const _curvePoints = (type: string, options: IPolarOptions): Point[][] => {
	const fn = curvePointsConfig[type] || curvePointsConfig.peachHeart
	return fn(options)
}
