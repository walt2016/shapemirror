import { rayMix, raySegs } from '../math/arrayMix'
import { plainMatrix, _move } from '../math'
import { segCrossPoints, neighbourSegCrossPoints } from './segCross'
import { Point, PointSegment } from '@/types/point'
// 多边形连接线
export const linkPoints = (points: Point[]): Point[] => {
	let lps = []
	for (let i = 0; i < points.length - 1; i++) {
		const p = points[i]
		lps = [...lps, ...rayMix(p, points.slice(i + 1))]
	}
	return lps
}

// 不包含边连线段
export const linkSegments = (points: Point[]): PointSegment[] => {
	const segs = []
	for (let i = 0, len = points.length; i < len - 2; i++) {
		const p = points[i]
		segs.push(...raySegs(p, points.slice(i + 2, i === 0 ? len - 1 : len)))
	}
	return segs
}

// 多边形连接线交叉点
export const linkCrossPoints = (points: Point[]): Point[] => {
	const segs = linkSegments(points)
	return segCrossPoints(segs)
}
// 线段延长线
const lineExtension = (p1: Point, p2: Point, iterNum: number = 0): PointSegment => {
	let p = _move(p2, p1, p2)
	while (iterNum-- > 0) {
		p = _move(p, p1, p)
	}
	return [p2, p]
}

export const edgeExtensionSegs = (points: Point[], iterNum: number): PointSegment[] => {
	const len = points.length
	const segs1 = []
	const segs2 = []
	points.forEach((t, i) => {
		const next = points[(i + 1) % len]
		const seg1 = lineExtension(t, next, iterNum)
		const seg2 = lineExtension(next, t, iterNum)
		segs1.push(seg1)
		segs2.push(seg2)
	})
	const n = segs1.length
	const segs = []
	segs1.forEach((t, i) => {
		segs.push(t)
		segs.push(segs2[(i + 2) % n])
	})
	return segs
}
// 延长线
export const edgeExtensionPoints = (points: Point[], iterNum: number): Point[] => {
	return plainMatrix(edgeExtensionSegs(points, iterNum))
}

// 延长线交叉点
export const edgeExtensionCrossPoints = (points: Point[], iterNum?: number): Point[] => {
	let segs = edgeExtensionSegs(points, iterNum)
	return neighbourSegCrossPoints(segs)
}

// 带状
export const stripPoints = (points: Point[], n: number): Point[][] => {
	const len = points.length
	const matrix = []
	for (let i = 0; i <= len - n; i++) {
		const stack = []
		for (let j = 0; j < n; j++) {
			const p = points[i + j]
			stack.push(p)
		}
		matrix.push(stack)
	}
	return matrix
}

export const stripLoopPoints = (points: Point[], n: number): Point[][] => {
	const len = points.length
	const matrix = []
	for (let i = 0; i < len; i++) {
		const stack = []
		for (let j = 0; j < n; j++) {
			const p = points[(i + j) % len]
			stack.push(p)
		}
		matrix.push(stack)
	}
	return matrix
}

export const stripFanPoints = (points: Point[], n: number): Point[][] => {
	const [o, ...rest] = points
	const len = rest.length
	const matrix = []
	for (let i = 0; i <= len - n; i++) {
		const stack = [o]
		for (let j = 0; j < n; j++) {
			const p = rest[(i + j) % len]
			stack.push(p)
		}
		matrix.push(stack)
	}
	return matrix
}
