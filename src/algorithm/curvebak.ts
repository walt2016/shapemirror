import { eachNext } from '@/utils'
import { _mid, _dis, _polar, _atan, _k, _sin } from '@/math'
import { _mirror } from '@/math/mirror'
import { IControlOptions } from '@/types'
import { Point } from '@/types/point'
import { IArcOptions } from '@/types/arc'

// 计算控制点
export const calc = (
	p1: number[],
	p2: number[],
	{
		radiusRatio = 1,
		angleOffset = 0,
		orient, //向心
	}: IControlOptions
) => {
	const mid = _mid(p1, p2)
	const r = _dis(p1, mid)
	const cp1 = _polar(mid, radiusRatio * r, orient ? _atan(p1, p2) - 90 + angleOffset : angleOffset)
	const cp2 = _mirror(cp1, mid)
	return {
		p1,
		p2,
		cp1,
		cp2,
		mid,
		r,
	}
}

// 曲线
export const curvePoints = (points: Point[], options: IControlOptions) => {
	const ps = []
	const cs = []
	eachNext(points, (t, index, next) => {
		const p = calc(t, next, options)
		cs[cs.length] = p.cp1
		cs[cs.length] = p.cp2
	})
	eachNext(points, (t, index, next) => {
		ps[ps.length] = t
		ps[ps.length] = [...cs[2 * index], ...next]
	})
	return ps
}

// 波形
export const wavePoints = (points: Point[], options: IControlOptions) => {
	const ps = []
	const cps = []
	const mids = []
	eachNext(points, (t, index, next) => {
		const p = calc(t, next, options)
		cps[cps.length] = p.cp1
		cps[cps.length] = p.cp2
		mids[mids.length] = p.mid
	})
	eachNext(points, (t, index, next) => {
		ps[ps.length] = t
		ps[ps.length] = [...cps[2 * index], ...mids[index]]
		ps[ps.length] = [...cps[2 * index + 1], ...next]
	})
	return ps
}

// 锯齿形
export const sawtoothPoints = (points: Point[], options: IControlOptions) => {
	const ps = []
	const cps = []
	eachNext(points, (t, index, next) => {
		const p = calc(t, next, options)
		cps[cps.length] = p.cp1
		cps[cps.length] = p.cp2
	})
	eachNext(points, (t, index, next) => {
		ps[ps.length] = t
		ps[ps.length] = cps[2 * index]
		ps[ps.length] = cps[2 * index + 1]
		ps[ps.length] = next
	})
	return ps
}

// 半圆弧
export const semicirclePoints = (points: Point[], { sweepFlag, xAxisRotation = 0, largeArcFlag }: IArcOptions) => {
	// A rx ry x-axis-rotation large-arc-flag sweep-flag x y
	const ps = []
	eachNext(points, (t, index, next) => {
		const r = _dis(t, next) / 2
		ps[ps.length] = [t, [r, r, xAxisRotation, largeArcFlag ? 1 : 0, sweepFlag ? 1 : 0, ...next]]
	})
	return ps
}

// 椭圆弧 elliptical arc 椭圆弧
export const ellipticalPoints = (points: Point[], { sweepFlag, radiusRatio = 1, xAxisRotation = 0, largeArcFlag }: IArcOptions) => {
	// A rx ry x-axis-rotation large-arc-flag sweep-flag x y
	const ps = []
	// const n = _recycle(points, recycleIndex)
	eachNext(points, (t, index, next) => {
		const r = _dis(t, next) / 2
		ps[ps.length] = t
		ps[ps.length] = [r, radiusRatio * r, xAxisRotation, largeArcFlag ? 1 : 0, sweepFlag ? 1 : 0, ...next]
	})
	return ps
}
