import { rotatePoints, _scale } from '@/math'
import { PointSegment } from '@/types/point'

// 控制点
export const controlPoints = ([from, to]: PointSegment, skew: number = 0, amplitude: number = 1): PointSegment => {
	const [c1, c2] = rotatePoints([from, to], skew + 90)
	if (amplitude !== 1) {
		return _scale([c1, c2], amplitude)
	}
	return [c1, c2]
}
