import { ICurveOptions } from '@/types/curve'
import { _cos, _sin, _k, _move, _o, _mid, _lerp } from '../math'
import { controlPoints } from './controlPoints'
import { _traversal } from './traversal'
import { Point, PointSegment } from '@/types/point'

const _koch = ([from, to]: PointSegment, depth: number, skew: number, amplitude: number): Point[] => {
	const [c1, c2] = controlPoints([from, to], skew, amplitude)
	const p1 = _lerp(from, to, 1 / 3)
	const p2 = _lerp(from, to, 2 / 3)
	const ps = [from, p1, c1, p2, to]
	if (depth === 0) {
		return [from]
	} else if (depth === 1) {
		return ps
	} else {
		depth -= 1
		const ps2 = []
		ps.forEach((t, i) => {
			const next = ps[i + 1]
			if (next) {
				const arr = _koch([t, next], depth, skew, amplitude)
				if (i === 0) {
					ps2.push(...arr)
				} else {
					ps2.push(...arr.slice(1))
				}
			}
		})
		return ps2
	}
}
export const kochPoints = ({ points, depth = 0, skew = 0, amplitude = 1, loop = false, discrete = false }: ICurveOptions): Point[] => {
	const len = points.length
	const result = []
	const iter = ({ points: [t, next], index: i }) => {
		const ps = _koch([t, next], depth, skew, amplitude)
		if (i === 0) {
			result.push(...ps)
		} else if (i === len - 1) {
			result.push(...ps.slice(1, ps.length - 1))
		} else {
			result.push(...ps.slice(1))
		}
	}
	const n = 2
	_traversal({ points, n, loop, iter, discrete })

	return result
}
