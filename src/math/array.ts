// 数组切割
export const toMatrix = <T>(arr: T[], n: number, noseToTail?: boolean): T[][] => {
	let matrix = []
	// arr.length > 62500会溢出
	// let fn = (arr) => {
	//     matrix.push(arr.slice(0, n))
	//     if (arr.length > n) {
	//         fn(arr.slice(n))
	//     }
	// }
	// fn(arr, n)
	// return matrix
	const len = arr.length
	for (let i = 0; i < len; i++) {
		if (i % n === 0) {
			if (noseToTail) {
				// 首位相接
				matrix.length && matrix[matrix.length - 1].push(arr[i])
			}
			matrix[matrix.length] = [arr[i]]
		} else {
			matrix[matrix.length - 1].push(arr[i])
		}
	}
	return matrix
}

// 首位相接
export const noseToTail = <T>(arr: T[][]): T[][] => {
	const len = arr.length
	arr.forEach((t, i) => {
		if (i < len - 1) {
			const next = arr[i + 1]
			t.push(next[0])
		}
	})
	return arr
}

// 数组分组
export const groupBy = <T>(arr: T[], fn: { (t: any): string; (arg0: T, arg1: number): string }): { [key: string]: T } => {
	let group = {}
	arr.forEach((t, i) => {
		let type = fn(t, i)
		group[type] = group[type] || []
		group[type].push(t)
	})
	return group
}
