// 质数
// 判断质数
export const isPrime = (num: number): boolean => {
    for (let i = 2; i < Math.sqrt(num); i++) {
        if (num % i === 0) {
            return false
        }
    }
    return true
}

// 寻找质数字 n-m之间
export const getPrime = (n: number, m: number): number[] => {
    let plist = []
    for (let i = n; i < m; i++) {
        if (isPrime(i)) {
            plist.push(i)
        }

    }
    return plist
}

//  每N个数的质数个数
export const countPrime = (n: number, m: number): number[] => {
    let countList = []
    for (let i = 0; i < m; i++) {
        let plist = getPrime(i * n, (i + 1) * n)
        countList.push(plist.length)
    }
    return countList
}


// 公约数 【最大】
export const gcd = (a: number, b: number): number => {
    if (b === 0) return a
    return gcd(b, a % b)
}

// 判断互为质数
export const checkPrime = (a: number, b: number): boolean => {
    return gcd(a, b) === 1
}


// 分解质因数
export const primeFactorize = (num: number): number[] => {
    let list = []
    let fn = (num: number) => {
        let flag = true
        for (let i = 2; i <= Math.sqrt(num); i++) {
            if (num % i === 0) {
                list.push(i)
                fn(num / i)
                flag = false
                break
            }
        }
        if (flag) {
            list.push(num)
        }
    }
    fn(num)
    return list

}

// 判断对称数
export const isSymmetry = (num: number): boolean => {
    let arr = String(num).split('').reverse()
    if (arr.join('') === String(num)) {
        return true
    }
    return false
}


// 最小公倍数least common multiple
// 两个数的乘积等于这两个数的最大公约数与最小公倍数的乘积
export const lcm = (a: number, b: number): number => {
    return a * b / gcd(a, b)
}


// 多个数的最大公约数
// 先求出两个的最大公因数，然后再用求出来的最大公因数和第三个求，依次类推
// 数组的最大公约数
export const gcd2 = (arr: number[]): number => {
    if (Array.isArray(arr)) {
        let len = arr.length
        if (len === 2) {
            return gcd(arr[0], arr[1])
        } else if (len > 2) {
            let [a, b, ...rest] = arr
            let g = gcd(a, b)
            return gcd2([g, ...rest])
        }
    }
}



// 多个数的最小公倍数
export const lcm2 = (arr: number[]): number => {
    let len = arr.length
    if (len === 2) {
        return lcm(arr[0], arr[1])
    } else if (len > 2) {
        let [a, b, ...rest] = arr
        let g = lcm(a, b)
        return lcm2([g, ...rest])
    }
    return arr[0]
}
