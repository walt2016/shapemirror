import { Point } from '@/types/point'

//两个元素交换位置
export const swapArr = <T>(arr: T[], x: number, y: number): T[] => {
	// arr[x] = arr.splice(y, 1, arr[x])[0];
	arr.splice(x, 1, ...arr.splice(y, 1, arr[x]))
	return arr
}
//从第几个开始重新编号,循环
export const newSeq = <T>(arr: T[], index: number): T[] => {
	arr.splice(0, 0, ...arr.splice(index, arr.length - index))
	return arr
}

//邻居互换  数组,固定序号
export const neighborSwap = <T>(arr: T[], index?: number): T[] => {
	if (index === undefined) {
		for (let i = 0; i < arr.length; i = i + 2) {
			swapArr(arr, i, i + 1)
		}
		return arr
	} else {
		let fix = arr.splice(index, 1)[0]
		for (let i = 0; i < arr.length; i = i + 2) {
			swapArr(arr, i, i + 1)
		}
		arr.splice(index, 0, fix)
		return arr
	}
}

//隔位重排
export const intervalSort = <T>(arr: T[], index?: number): T[] => {
	if (index !== undefined) {
		newSeq(arr, index)
	}
	for (let i = 0; i < arr.length - 2; i++) {
		swapArr(arr, i + 1, i + 2)
	}
	return arr
}

//错位排序
export const misplaced = <T>(arr: T[]): T[] => {
	for (let i = 0; i < arr.length / 2 + 1; i++) {
		var cur = arr.pop()
		arr.splice(i * 2 + 1, 0, cur)
	}
	return arr
}

//奇偶数排序oddEvenSort
export const paritySort = <T>(arr: T[]): T[] => {
	let seq = arr.map((t, i) => {
		return i
	})

	seq.sort((a, b) => {
		if (a % 2 === 0) {
			if (b % 2 !== 0) {
				return -1
			}
			return 0
		} else {
			return 1
		}
	})

	return seq.map(t => arr[t])
}

// 对角点diagonal point
export const diagonal = (points: Point[]): Point[] => {
	let n = points.length
	let mid = Math.floor(n / 2)

	let ps = []
	for (let i = 0; i < mid; i++) {
		ps[ps.length] = points[i]
		ps[ps.length] = points[mid + i]
	}
	return ps
}

//数组乱序 random
export const shuffle = <T>(arr: T[], index: number): T[] => {
	if (index === undefined) {
		for (let i = arr.length - 1; i >= 0; i--) {
			let rIndex = Math.floor(Math.random() * (i + 1))
			// 打印交换值
			let temp = arr[rIndex]
			arr[rIndex] = arr[i]
			arr[i] = temp
		}
		return arr
	}

	let res = []
	// 取出固定值
	let fix = arr.splice(index, 1)[0]
	for (let i = arr.length - 1; i >= 0; i--) {
		let rIndex = Math.floor(Math.random() * (i + 1))
		res.push(arr[rIndex])
		arr.splice(rIndex, 1)
	}
	// 将固定值放入指定位置
	res.splice(index, 0, fix)
	return res
}

// 按指定顺序排序
// 未指定排最后
export const sortByOrder = (arr: string[], order: string[]): string[] => {
	return arr.sort((a, b) => {
		let aIndex = order.findIndex(t => new RegExp(`^${t}$`).test(a)) // ||
		let bIndex = order.findIndex(t => new RegExp(`^${t}$`).test(b)) //||
		if (aIndex === -1) return 1
		if (bIndex === -1) return 1
		return aIndex - bIndex

		// if (order.indexOf(a) === -1) return 1
		// return order.indexOf(a) - order.indexOf(b)
	})
}
