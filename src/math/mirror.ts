// 根据数组计算
import { IMirrorProps } from '@/types/mirror'
import { isPoints, _mid, _dis, _atan, _polar, _k } from './index'
import { Point } from '@/types/point'

// 镜像
// Angle of Refraction折射角
// index of Refraction折射角
export const _mirror = <T extends Point | Point[]>(p: T, o: Point, scale?: number, refraction?: number): T => {
	if (isPoints(p)) {
		let n = p.length
		return p.map((t, i) => _mirror(t, o, scale, refraction)) as T
	}
	if (refraction) {
		let r = _dis(p, o) * scale
		let a = _atan(p, o)
		let ra = a + refraction
		return _polar(o, r, ra) as T
	}
	if (scale === undefined || scale === 1) {
		return [2 * o[0] - p[0], 2 * o[1] - p[1]].map(t => _k(t)) as T
	}
	return [(scale + 1) * o[0] - scale * p[0], (scale + 1) * o[1] - scale * p[1]].map(t => _k(t)) as T
}
// 边镜像
export const edgeMirror = (points: Point[], options: IMirrorProps): Point[][] => {
	let { ratio = 1, refraction } = options
	let midPoints = _mid(points)
	return midPoints.map(t => {
		return _mirror(points, t, ratio, refraction)
	})
}
