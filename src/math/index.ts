//四舍五入保留2位小数（若第二位小数为0，则保留一位小数）

import { IPoint } from '@/types/point'
import { _type } from '../utils'
import { PointMatrix, Point, Points } from '@/types/point'

// keepTwoDecimal
export function _k(num: number, decimal: number = 2): number {
	if (decimal == null) {
		return num
	}
	const g = Math.pow(10, decimal)
	return Math.round(num * g) / g
}

export const isPoint = (p: any): p is Point => Array.isArray(p)
// 二维数组 points [p1,p2]
// 点队列 vector
export const isPoints = (p: any): p is Points => Array.isArray(p) && p.every((t: any) => isPoint(t))

// 点阵  [points1,points2]
// matrix ( a2-D array)
export const isPointMatrix = (p: any): p is PointMatrix => Array.isArray(p) && p.every(t => isPoints(t))

// 转弧度
export const _rad = (angle: number): number => (angle * Math.PI) / 180
export const _deg = (r: number): number => _k((r * 180) / Math.PI)
// 正玄
export const _sin = (angle: number, decimal?: number): number => _k(Math.sin(_rad(angle)), decimal)

// 余玄
export const _cos = (angle: number, decimal?: number): number => _k(Math.cos(_rad(angle)), decimal)

export const _tan = (angle: number, decimal?: number): number => _k(Math.tan(_rad(angle)), decimal)

// 极坐标
export const _polar = (o: Point = [0, 0], r: number = 0, a: number = 0): Point => {
	return [o[0] + r * _cos(a), o[1] + r * _sin(a)].map(t => _k(t))
}

export const _polarRad = (o: Point = [0, 0], r = 0, a = 0): Point => {
	return [o[0] + r * Math.cos(a), o[1] + r * Math.sin(a)].map(t => _k(t))
}

const _midPoint = (p1: Point, p2: Point): Point => {
	return [(p1[0] + p2[0]) / 2, (p1[1] + p2[1]) / 2].map(t => _k(t))
}

// 中点 midpoint
export const _mid = <T extends Point | Point[]>(p1: T, p2?: T): T => {
	if (isPoints(p1)) {
		let n = p1.length
		return p1.map((t: Point, index: number) => {
			let next = p1[(index + 1) % n]
			return _midPoint(t, next)
		}) as T
	}
	return _midPoint(p1 as Point, p2 as Point) as T
}

// 距离
export const _dis = (p1: Point = [0, 0], p2: Point = [0, 0], decimal: number = 2): number => {
	let dx = p1[0] - p2[0]
	let dy = p1[1] - p2[1]
	return _k(Math.sqrt(dx * dx + dy * dy), decimal)
}

// 相等
export const equals = (p1: Point, p2: Point): boolean => {
	let equals = true
	for (let i = p1.length - 1; i >= 0; --i) {
		if (p1[i] != p2[i]) {
			equals = false
			break
		}
	}
	return equals
}
// 夹角 toAngle
export const _angleRad = (p1: Point = [0, 0], p2: Point = [0, 0]): number => {
	return Math.atan2(p2[1] - p1[1], p2[0] - p1[0])
}

export const _atan = (p1: Point = [0, 0], p2: Point = [0, 0]): number => {
	return _deg(_angleRad(p1, p2))
}

// 三点夹角
export const _angleToRad = (A: Point, O: Point, B: Point): number => {
	let OA = _dis(O, A)
	let OB = _dis(O, B)
	let AB = _dis(A, B)
	let cosO = (OA * OA + OB * OB - AB * AB) / (2 * OA * OB)
	return Math.acos(cosO)
}

// 三点夹角
// angleTo
// cosO = (OA*OA + OB*OB - AB*AB ) / 2*OA*OB
export const includedAngle = (A: Point, O: Point, B: Point): number => {
	return _deg(_angleToRad(A, O, B))
}

// 中心点 _o
export const _o = (points: Point[]): Point => {
	let n = points.length
	let sum = points.reduce(
		(sum, t) => {
			sum[0] += t[0]
			sum[1] += t[1]
			return sum
		},
		[0, 0]
	)
	return sum.map(t => t / n)
}

// 放大缩小
export const _scale = <T extends Point | Point[]>(p: T, amplitude: number, o?: Point): T => {
	if (isPoints(p)) {
		const o2 = o || _o(p)
		return p.map(t => _scale(t, amplitude, o2)) as T
	}
	const deta = [p[0] - o[0], p[1] - o[1]]
	return [o[0] + deta[0] * amplitude, o[1] + deta[1] * amplitude] as T
}

// 线段平移
// 移动  相对距离
export const _move = <T extends Point | Point[]>(p: T, from: Point, to: Point, radio?: number): T => {
	let deta = [to[0] - from[0], to[1] - from[1]]
	if (isPoints(p)) {
		if (radio) {
			const points = p.map(t => [t[0] + deta[0], t[1] + deta[1]])
			return _scale(points, radio) as T
		}
		return p.map(t => [t[0] + deta[0], t[1] + deta[1]]) as T
	} else {
		return [p[0] + deta[0], p[1] + deta[1]] as T
	}
}
// // 移动 绝对距离r
export const _moveDis = (p: Point, o: Point, r: number): Point => {
	let a = _angleRad(o, p)
	return _polarRad(p, r, a)
}

export const translateX = <T extends Point | Point[]>(points: T, x: number = 100): T => {
	if (isPoints(points)) {
		return points.map(t => translateX(t, x)) as T
	}
	return [points[0] + x, points[1]] as T
}

export const translateY = <T extends Point | Point[]>(points: T, y: number = 100): T => {
	if (isPoints(points)) {
		return points.map(t => translateY(t, y)) as T
	}
	return [points[0], points[1] + y] as T
}

// 平移
// [number, number] | IPoint | number
export const translate = <T extends Point | Point[]>(points: T, options: Point | number | IPoint): T => {
	if (isPoints(points)) {
		return points.map(t => translate(t, options)) as T
	}
	if (Array.isArray(options)) {
		const [x, y] = options
		return [points[0] + x, points[1] + y] as T
	} else if (typeof options === 'object') {
		const { x, y } = options
		return [points[0] + x, points[1] + y] as T
	}

	if (typeof options === 'number') {
		return translateX(points, options)
	}
}

// 旋转 围绕o点旋转a角度
export const rotate = <T extends Point | Point[]>(points: T, o: Point, a: number): T => {
	if (isPoints(points)) {
		return points.map(t => rotate(t, o, a)) as T
	}
	let r = _dis(points, o)
	return _polar(o, r, a) as T
}

// 其中（rx0，ry0）是旋转中心，（x，y）待旋转点，（x0，y0）旋转后的新坐标。
export const _rotate = (p: Point, o: Point, a: number): Point => {
	if (a === 0) return p
	const [x, y] = p
	const [rx0, ry0] = o
	const x0 = (x - rx0) * _cos(a) - (y - ry0) * _sin(a) + rx0
	const y0 = (x - rx0) * _sin(a) + (y - ry0) * _cos(a) + ry0
	return [x0, y0].map(t => _k(t))
}

// 线段中点旋转
export const rotatePoints = (points: Point[], a: number, o?: Point): Point[] => {
	const o2 = o || _o(points)
	return points.map(t => {
		return _rotate(t, o2, a)
	})
}

export const _lerp = <T extends number | number[]>(p1: T, p2: T, d: number): T => {
	if (Array.isArray(p1)) {
		return p1.map((t, i) => _k(t + (p2[i] - t) * d)) as T
	} else if (typeof p1 === 'number' && typeof p2 === 'number') {
		return _k(p1 + (p2 - p1) * d) as T
	}
}

// 垂直,转90度
export const vertical = <T extends Point | Point[]>(points: T, v: number = 1): T => {
	if (isPoints(points)) {
		const len = points.length
		return points.map((t, index) => vertical(t, index === 0 || index === len ? 1 : -1)) as T
	}
	return [points[1], v * points[0]] as T
}

// 数组降维
export const plainMatrix = (matrix: Point[][], infinite = false): Point[] => {
	const points = []
	matrix.forEach(t => points.push(...t))
	return points

	// if (isPoints(matrix)) {
	//     let ps = []
	//     if (infinite) {
	//         // 降至1维
	//         matrix.forEach(t => {
	//             if (isPoints(t)) {
	//                 ps.push(...plainMatrix(t, infinite))
	//             } else {
	//                 ps.push(t)
	//             }
	//         });
	//     } else {
	//         // 降1维
	//         matrix.forEach(t => ps.push(...t))
	//     }
	//     return ps
	// } else if (Array.isArray(matrix)) {
	//     return matrix
	// }
}

// 坐标
// 给点阵标记单位坐标
export const unitMatrix = (matrix: Point[][]): Point[][] => {
	let result = []
	if (isPoints(matrix)) {
		matrix.forEach((t, y) => result.push(t.map((t, x) => [x, y])))
	} else if (Array.isArray(matrix)) {
		result = matrix.map((t, i) => i)
	}
	return result
}
// 随机数
export const _random = <T extends number | any>(start: T | T[], end?: T): T | undefined => {
	if (Array.isArray(start)) {
		let index = Math.floor(Math.random() * start.length)
		return start[index]
	}
	if (typeof start === 'number') {
		if (typeof end === 'number') {
			return Math.floor(Math.random() * (end ?? 1 - start) + start) as T
		}
		return Math.floor(Math.random() * (1 - start) + start) as T
	}
	return undefined
}

// 转向量
export const _vec = (p1: number[] = [], p2: number[] = []) => {
	return p1.map((t, i) => p2[i] - t)
}

const _rotateVec = (v1: number[], radians: number) => {
	const s = Math.sin(radians),
		c = Math.cos(radians)

	const [x, y] = v1
	return [x * c - y * s, y * c + x * s]
}
export const _verticalVec = (v: number[] = []) => {
	return _rotateVec(v, -90)
}

export const _addVec = (v: number[] = [], p: number[] = []) => {
	return v.map((t, i) => t + p[i])
}

// 知一边，求正方形四点
export const _squarePoints = (p1: number[], p2: number[], a: number = 0) => {
	const v = _vec(p1, p2)
	const vv = _rotateVec(v, -90 + a)
	return [p1, p2, _addVec(vv, p2), _addVec(vv, p1)]
}

// 点到线段的距离
export const _disToSeg = (p: number[], [o, t]): number => {
	let a = _angleToRad(p, o, t)
	return _dis(p, o) * Math.sin(a)
}

// 点到线段的垂足点
export const _footPoint = (p: number[], [o, t]): Point => {
	let a = _angleToRad(p, o, t)
	let r = _dis(p, o) * Math.cos(a)
	return _polarRad(o, r, a + _angleRad(o, p))
}
