// 变形

import { Point } from '@/types/point'
import { diagonal, paritySort, misplaced, intervalSort, neighborSwap, shuffle } from './arraySort'
import { Transform } from '@/types/transform'

const transformConfig = {
	[Transform.None]: <T>(e: T): T => e,
	[Transform.Diagonal]: diagonal,
	[Transform.ParitySort]: paritySort,
	[Transform.Misplaced]: misplaced,
	[Transform.IntervalSort]: intervalSort,
	[Transform.NeighborSwap]: neighborSwap,
	[Transform.Shuffle]: shuffle,
}

export const _transform = <T = Point | Point[]>(p: T, transform: string): T => {
	return transformConfig[transform] ? transformConfig[transform](p) : p
}
export const transformTypes = Object.keys(transformConfig)
