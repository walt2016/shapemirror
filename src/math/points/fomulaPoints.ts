import { IPolarOptions } from '@/types/polar'
import { _move, _polar, _tan, _sin, _cos } from '../index'
import { Point } from '@/types/point'

// 直线
export const linePoints = (options: IPolarOptions): Point[] => {
	let { m = 360, r = 100, o = [0, 0] } = options

	return Array.from(
		{
			length: m,
		},
		(t, i) => _move(o, o, [o[0] + ((i - m / 2) * r) / m, o[1]])
	)
}

// 圆形
export const circlePoints = (options: IPolarOptions): Point[] => {
	let { num = 360, r = 100, a = 0, o = [0, 0] } = options
	return Array.from(
		{
			length: num,
		},
		(t, i) => _polar(o, r, (i * 360) / num + a)
	)
}
// 正玄曲线
// y=Asin(ωx+φ)+k
export const sinPoints = (options: IPolarOptions): Point[] => {
	let { num = 360, r = 100, k = 0, a = 0, w = 1, o = [0, 0] } = options
	return Array.from(
		{
			length: num,
		},
		(t, i) => [(i * 360) / num + o[0] - 180, r * _sin(w * ((i * 360) / num - 180) - a) + o[1] - k]
	)
}
// 余弦
export const cosPoints = (options: IPolarOptions): Point[] => {
	let { num = 360, r = 100, k = 0, a = 0, w = 1, o = [0, 0] } = options
	return Array.from(
		{
			length: num,
		},
		(t, i) => [(i * 360) / num + o[0] - 180, r * _cos(w * ((i * 360) / num - 180) - a) + o[1] - k]
	)
}
// 正切
export const tanPoints = (options: IPolarOptions): Point[] => {
	let { num = 360, o = [0, 0] } = options

	return Array.from(
		{
			length: num,
		},
		(t, i) => [(i * 360) / num + o[0] - 180, _tan((i * 360) / num) + o[1]]
	)
}
