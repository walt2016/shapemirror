// 数组混合

import { Point, PointSegment } from '@/types/point'

// 射线混排
export const rayMix = (p: Point, arr: Point[]): Point[] => {
	const arr2 = []
	arr.forEach(t => {
		arr2.push(p, t)
	})
	return arr2
}

export const raySegs = (p: Point, arr: Point[]): PointSegment[] => {
	const segs = []
	arr.forEach(t => {
		segs.push([p, t])
	})
	return segs
}
