import { IContent } from '@/types/ui'
import { mapShapeFn } from './registShape'
const initGl = (canvas: HTMLCanvasElement) => {
	let gl: WebGLRenderingContext
	try {
		gl = canvas.getContext('webgl') || (canvas.getContext('experimental-webgl') as WebGLRenderingContext)
		// gl.viewportWidth = canvas.width;
		// gl.viewportHeight = canvas.height;
		// gl.viewport
	} catch (e) {}
	if (!gl) {
		alert('Could not initialise WebGL, sorry :-(')
	}
	return gl
}

// interface IWebglOptions {
//   width: string| number;
//   height:string | number;
//   shapes: any[]
// }
export const _webgl = (options: IContent, props = {}) => {
	const { width = 800, height = 600, shapes } = options
	const canvas = document.createElement('canvas')
	canvas.width = typeof width === 'number' ? width : parseFloat(width)
	canvas.height = typeof height === 'number' ? height : parseFloat(height)
	const gl = initGl(canvas)
	if (gl) {
		shapes.forEach(t => {
			let fn = mapShapeFn(t.shape)
			fn && fn(gl, t, props)
		})
	}
	return canvas
}
