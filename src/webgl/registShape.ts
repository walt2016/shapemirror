import { _triangle } from './triangle'
import { _square } from './square'
import { _cube } from './cube'
// import { _ball } from './ball'
// import { _demo } from './demo';
import { _line } from './line'

const shapeMap = {
	default: _triangle,
	glTriangle: _triangle,
	glSquare: _square,
	glCube: _cube,
	// glBall: _ball,
	glLine: _line,
	//   glDemo:_demo
}

export const mapShapeFn = t => {
	return shapeMap[t] //|| shapeMap['default']
}
