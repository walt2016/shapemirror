// import vertex from './glsl/demo.vert'
// import fragment from './glsl/demo.frag'

// import { defineProgram } from './webgl'
// export const _demo = (gl: WebGLRenderingContext) => {
//     webGLStart(gl)
// }

// var shaderProgram;

// function initShaders(gl: WebGLRenderingContext) {
//     // var fragmentShader = getShader(gl, "shader-fs");
//     // var vertexShader = getShader(gl, "shader-vs");

//     // shaderProgram = gl.createProgram();
//     // gl.attachShader(shaderProgram, vertexShader);
//     // gl.attachShader(shaderProgram, fragmentShader);
//     // gl.linkProgram(shaderProgram);
//     shaderProgram = defineProgram(gl, vertex, fragment);

//     if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
//         alert("Could not initialise shaders");
//     }

//     gl.useProgram(shaderProgram);

//     shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
//     gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

//     shaderProgram.textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
//     gl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);

//     shaderProgram.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aVertexNormal");
//     gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);

//     shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
//     shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
//     shaderProgram.nMatrixUniform = gl.getUniformLocation(shaderProgram, "uNMatrix");
//     shaderProgram.samplerUniform = gl.getUniformLocation(shaderProgram, "uSampler");
//     shaderProgram.useLightingUniform = gl.getUniformLocation(shaderProgram, "uUseLighting");
//     shaderProgram.ambientColorUniform = gl.getUniformLocation(shaderProgram, "uAmbientColor");
//     shaderProgram.lightingDirectionUniform = gl.getUniformLocation(shaderProgram, "uLightingDirection");
//     shaderProgram.directionalColorUniform = gl.getUniformLocation(shaderProgram, "uDirectionalColor");
// }

// var cubeTextures = Array();
// function initTexture(gl: WebGLRenderingContext) {
//     var cubeImage = new Image();

//     for (var i = 0; i < 3; i++) {
//         var texture = gl.createTexture();
//         texture.image = cubeImage;
//         cubeTextures.push(texture);
//     }

//     cubeImage.onload = function () {
//         handleLoadedTexture(cubeTextures);
//     }
//     cubeImage.src = "cube.png";
// }

// function handleLoadedTexture(gl: WebGLRenderingContext, texture: WebGLTexture) {
//     gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

//     gl.bindTexture(gl.TEXTURE_2D, texture[0]);
//     gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture[0].image);
//     gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
//     gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);

//     gl.bindTexture(gl.TEXTURE_2D, texture[1]);
//     gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture[1].image);
//     gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
//     gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

//     gl.bindTexture(gl.TEXTURE_2D, texture[2]);
//     gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture[2].image);
//     gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
//     gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
//     gl.generateMipmap(gl.TEXTURE_2D);

//     gl.bindTexture(gl.TEXTURE_2D, null);
// }

// var mvMatrix = mat4.create();
// var mvMatrixStack = []
// var pMatrix = mat4.create();

// function mvPushMatrix() {
//     var copy = mat4.create();
//     mat4.set(mvMatrix, copy);
//     mvMatrixStack.push(copy);
// }

// function mvPopMatrix() {
//     if (mvMatrixStack.length == 0) {
//         throw "No Matrix to Pop!";
//     }
//     mvMatrix = mvMatrixStack.pop();
// }

// function degToRad(degrees) {
//     return degrees * Math.PI / 180;
// }
// function setMatrixUniforms(gl: WebGLRenderingContext) {
//     gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
//     gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);

//     var normalMatrix = mat3.create();
//     mat4.toInverseMat3(mvMatrix, normalMatrix);
//     mat3.transpose(normalMatrix);
//     gl.uniformMatrix3fv(shaderProgram.nMatrixUniform, false, normalMatrix);
// }

// var cubeVertexPositionBuffer;
// var cubeVertexTextureCoordBuffer;
// var cubeVertexIndexBuffer;

// function initBuffers(gl: WebGLRenderingContext) {

//     cubeVertexPositionBuffer = gl.createBuffer();
//     gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexPositionBuffer);
//     vertices = [
//         // Front face
//         -1.0, -1.0, 1.0,
//         1.0, -1.0, 1.0,
//         1.0, 1.0, 1.0,
//         -1.0, 1.0, 1.0,

//         // Back face
//         -1.0, -1.0, -1.0,
//         -1.0, 1.0, -1.0,
//         1.0, 1.0, -1.0,
//         1.0, -1.0, -1.0,

//         // Top face
//         -1.0, 1.0, -1.0,
//         -1.0, 1.0, 1.0,
//         1.0, 1.0, 1.0,
//         1.0, 1.0, -1.0,

//         // Bottom face
//         -1.0, -1.0, -1.0,
//         1.0, -1.0, -1.0,
//         1.0, -1.0, 1.0,
//         -1.0, -1.0, 1.0,

//         // Right face
//         1.0, -1.0, -1.0,
//         1.0, 1.0, -1.0,
//         1.0, 1.0, 1.0,
//         1.0, -1.0, 1.0,

//         // Left face
//         -1.0, -1.0, -1.0,
//         -1.0, -1.0, 1.0,
//         -1.0, 1.0, 1.0,
//         -1.0, 1.0, -1.0,
//     ];
//     gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
//     cubeVertexPositionBuffer.itemSize = 3;
//     cubeVertexPositionBuffer.numItems = 24;

//     cubeVertexNormalBuffer = gl.createBuffer();
//     gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexNormalBuffer);
//     var vertexNormals = [
//         // Front face
//         0.0, 0.0, 1.0,
//         0.0, 0.0, 1.0,
//         0.0, 0.0, 1.0,
//         0.0, 0.0, 1.0,

//         // Back face
//         0.0, 0.0, -1.0,
//         0.0, 0.0, -1.0,
//         0.0, 0.0, -1.0,
//         0.0, 0.0, -1.0,

//         // Top face
//         0.0, 1.0, 0.0,
//         0.0, 1.0, 0.0,
//         0.0, 1.0, 0.0,
//         0.0, 1.0, 0.0,

//         // Bottom face
//         0.0, -1.0, 0.0,
//         0.0, -1.0, 0.0,
//         0.0, -1.0, 0.0,
//         0.0, -1.0, 0.0,

//         // Right face
//         1.0, 0.0, 0.0,
//         1.0, 0.0, 0.0,
//         1.0, 0.0, 0.0,
//         1.0, 0.0, 0.0,

//         // Left face
//         -1.0, 0.0, 0.0,
//         -1.0, 0.0, 0.0,
//         -1.0, 0.0, 0.0,
//         -1.0, 0.0, 0.0
//     ];
//     gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexNormals), gl.STATIC_DRAW);
//     cubeVertexNormalBuffer.itemSize = 3;
//     cubeVertexNormalBuffer.numItems = 24;

//     cubeVertexTextureCoordBuffer = gl.createBuffer();
//     gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureCoordBuffer);
//     var textureCoords = [
//         // Front face
//         0.0, 0.0,
//         1.0, 0.0,
//         1.0, 1.0,
//         0.0, 1.0,

//         // Back face
//         1.0, 0.0,
//         1.0, 1.0,
//         0.0, 1.0,
//         0.0, 0.0,

//         // Top face
//         0.0, 1.0,
//         0.0, 0.0,
//         1.0, 0.0,
//         1.0, 1.0,

//         // Bottom face
//         1.0, 1.0,
//         0.0, 1.0,
//         0.0, 0.0,
//         1.0, 0.0,

//         // Right face
//         1.0, 0.0,
//         1.0, 1.0,
//         0.0, 1.0,
//         0.0, 0.0,

//         // Left face
//         0.0, 0.0,
//         1.0, 0.0,
//         1.0, 1.0,
//         0.0, 1.0,
//     ];
//     gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoords), gl.STATIC_DRAW);
//     cubeVertexTextureCoordBuffer.itemSize = 2;
//     cubeVertexTextureCoordBuffer.numItems = 24;

//     cubeVertexIndexBuffer = gl.createBuffer();
//     gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndexBuffer);
//     var cubeVertexIndices = [
//         0, 1, 2, 0, 2, 3,    // Front face
//         4, 5, 6, 4, 6, 7,    // Back face
//         8, 9, 10, 8, 10, 11,  // Top face
//         12, 13, 14, 12, 14, 15, // Bottom face
//         16, 17, 18, 16, 18, 19, // Right face
//         20, 21, 22, 20, 22, 23  // Left face
//     ]

//     gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeVertexIndices), gl.STATIC_DRAW);
//     cubeVertexIndexBuffer.itemSize = 1;
//     cubeVertexIndexBuffer.numItems = 36;
// }

// var rCubeX = 0;
// var SpeedX = 0;

// var rCubeY = 0;
// var SpeedY = 0;

// var z = -5.0;

// var filter = 0;

// // function drawScene(gl: WebGLRenderingContext) {
// //     gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
// //     gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

// //     mat4.perspective(45, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0, pMatrix);

// //     mat4.identity(mvMatrix);

// //     mat4.translate(mvMatrix, [0.0, 0.0, z]);

// //     mvPushMatrix();
// //     mat4.rotate(mvMatrix, degToRad(rCubeX), [1, 0, 0]);
// //     mat4.rotate(mvMatrix, degToRad(rCubeY), [0, 1, 0]);

// //     gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexPositionBuffer);
// //     gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, cubeVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

// //     gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexNormalBuffer);
// //     gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, cubeVertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

// //     gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureCoordBuffer);
// //     gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, cubeVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

// //     gl.activeTexture(gl.TEXTURE0);
// //     gl.bindTexture(gl.TEXTURE_2D, cubeTextures[filter]);
// //     gl.uniform1i(shaderProgram.samplerUniform, 0);
// //     var lighting = (document.getElementById("lighting") as HTMLInputElement).checked;
// //     gl.uniform1i(shaderProgram.useLightingUniform, lighting ? 1 : 0);
// //     if (lighting) {
// //         gl.uniform3f(
// //             shaderProgram.ambientColorUniform,
// //             parseFloat((document.getElementById("ambientR") as HTMLInputElement).value),
// //             parseFloat((document.getElementById("ambientG") as HTMLInputElement).value),
// //             parseFloat((document.getElementById("ambientB") as HTMLInputElement).value)
// //         );
// //     }

// //     var lightingDirection = [
// //         parseFloat(document.getElementById("lightDirectionX").value),
// //         parseFloat(document.getElementById("lightDirectionY").value),
// //         parseFloat(document.getElementById("lightDirectionZ").value)
// //     ];
// //     var adjustedLD = vec3.create();
// //     vec3.normalize(lightingDirection, adjustedLD);
// //     vec3.scale(adjustedLD, -1);
// //     gl.uniform3fv(shaderProgram.lightingDirectionUniform, adjustedLD);
// //     gl.uniform3f(
// //         shaderProgram.directionalColorUniform,
// //         parseFloat(document.getElementById("directionalR").value),
// //         parseFloat(document.getElementById("directionalG").value),
// //         parseFloat(document.getElementById("directionalB").value)
// //     );

// //     gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndexBuffer);
// //     setMatrixUniforms();
// //     gl.drawElements(gl.TRIANGLES, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

// //     mvPopMatrix();
// // }

// var lastTime = 0;
// function animate() {
//     var timeNow = new Date().getTime();
//     if (lastTime != 0) {
//         var elapsed = timeNow - lastTime;

//         rCubeX += (SpeedX * elapsed) / 1000.0;
//         rCubeY += (SpeedY * elapsed) / 1000.0;
//     }
//     lastTime = timeNow;
// }

// var currentlyPressedKeys = {};

// function handleKeyDown(event) {
//     currentlyPressedKeys[event.keyCode] = true;
//     if (String.fromCharCode(event.keyCode) == "F") {
//         filter += 1;
//         if (filter == 3) {
//             filter = 0;
//         }
//     }
// }

// function handleKeyUp(event) {
//     currentlyPressedKeys[event.keyCode] = false;

// }
// function handleKeys() {
//     if (currentlyPressedKeys[33]) {
//         z -= 0.05;
//     }

//     if (currentlyPressedKeys[34]) {
//         z += 0.05;
//     }

//     if (currentlyPressedKeys[37]) {
//         SpeedY--;
//     } else if (SpeedY < 0) {
//         SpeedY++;
//     }

//     if (currentlyPressedKeys[39]) {
//         SpeedY++;
//     } else if (SpeedY > 0) {
//         SpeedY--;
//     }

//     if (currentlyPressedKeys[38]) {
//         SpeedX--;
//     } else if (SpeedX < 0) {
//         SpeedX++;
//     }

//     if (currentlyPressedKeys[40]) {
//         SpeedX++;
//     } else if (SpeedX > 0) {
//         SpeedX--;
//     }
// }

// // function update() {
// //     requestAnimFrame(update);
// //     handleKeys();
// //     drawScene();
// //     animate();
// // }

// function webGLStart(gl: WebGLRenderingContext) {
//     //     var canvas = document.getElementById("canvas1") as HTMLCanvasElement;
//     // const gl2=    canvas.getContext('webgl')
//     // initGL(canvas);
//     initShaders(gl);
//     initBuffers(gl);
//     initTexture(gl);

//     gl.clearColor(0.0, 0.0, 0.0, 1.0);
//     gl.enable(gl.DEPTH_TEST);
// }
