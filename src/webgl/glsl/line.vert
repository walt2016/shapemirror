uniform mat4 modelViewProjection;
uniform float width;
uniform vec2 resolution;

attribute vec4 color;
attribute vec3 from,to;
attribute vec2 point;

varying vec4 vColor;
varying vec2 vPoint;

void main(){
    vec4 clip0=modelViewProjection*vec4(from,1.);
    vec4 clip1=modelViewProjection*vec4(to,1.);
    
    vec2 screen0=resolution*(.5*clip0.xy/clip0.w+.5);
    vec2 screen1=resolution*(.5*clip1.xy/clip1.w+.5);
    
    vec2 xBasis=normalize(screen1-screen0);
    vec2 yBasis=vec2(-xBasis.y,xBasis.x);
    
    // Offset the original points:
    vec2 pt0=screen0+width*point.x*yBasis;
    vec2 pt1=screen1+width*point.x*yBasis;
    
    vec2 pt=mix(pt0,pt1,point.y);
    vec4 clip=mix(clip0,clip1,point.y);
    
    gl_Position=vec4(clip.w*(2.*pt/resolution-1.),clip.z,clip.w);
    vColor=color.abgr;// mix(.abgr, aToColor.abgr, aPosition.y);
}