import fragment from './glsl/line.frag'
import vertext from './glsl/line.vert'
import { defineProgram } from './webgl'
export const _line = gl => {
	let program = defineProgram(gl, vertext, fragment)
}
