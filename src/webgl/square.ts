import { defineProgram } from './webgl'
import fragment from './glsl/square.frag'
import vertex from './glsl/square.vert'

export const _square = gl => {
	// 初始化着色器
	let program = defineProgram(gl, vertex, fragment)

	// 指定将要用来清理绘图区的颜色
	// gl.clearColor(0., 0.0, 0.0, 1.0);
	// 清理绘图区
	gl.clear(gl.COLOR_BUFFER_BIT)
	// 绘制顶点
	gl.drawArrays(gl.POINTS, 0, 1)
	// const positions = [
	//     1.0, 1.0,
	//     -1.0, 1.0,
	//     1.0, -1.0,
	//     -1.0, -1.0,
	// ];

	// let buffers = initBuffers(gl, positions)

	// drawScene(gl,program,buffers)

	return gl
}
