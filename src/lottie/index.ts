import lottie from 'lottie-web'
import { IContent } from '@/types/ui'
import { _query } from '@/ui'

export const _lottie = (data: IContent) => {
	const callback = (panel: HTMLElement): void => {
		lottie.loadAnimation({
			container: panel, //this.animationRef.current,
			renderer: 'svg',
			loop: true,
			autoplay: true,
			animationData: data, //t.content //
			// assetsPath: CDN_URL,
		})
	}
	return callback
	// return dom.div('',{
	//     class: name
	// })

	// animation.play();
}

// animation.pause();
