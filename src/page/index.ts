import json from '@/assets/json/data.json'
import '@/assets/css/index.less'
import logo from '@/assets/img/logo.jpeg'
import { _append, _div, _remove } from '@/ui'
import { _btn } from '@/ui/core/btn'
import { _list, _listItem } from '@/ui/core/list'
import { _input } from '@/ui/core/input'
import { _select } from '@/ui/core/select'



const hi = _div("hi webpack", {
  class: 'red'
})
const btn = _btn("say hi", {
  class: 'red'
}, {
  click: () => {
    _append(hi, "!")
    _append(hi, _btn("hehe"))
    _append(hi, ["hehe", "---", _btn("dede", {
      class: 'red'
    })])
  }
})
const backBtn = _btn("back", {
  class: 'blue'
}, {
  click: () => {
    _remove(hi, "last")
  }
})
// const list = _list("list")
const searchBtn = _btn('search', {
  class: 'green',

}, {
  click: () => {
    _append(hi, box.value)

  }
})
const box = _input("hi", {
  class: 'green'
})
const addListBtn = _btn("addList", {
  class: 'yellow'
}, {
  click: () => {
    // list.appendChild(_listItem(box.value))
  }
})
const img = new Image()
img.src = logo


const select = _select('sh', {
  options: [{
    label: 'bj',
    value: 'bj'
  }, {
    label: 'sh',
    value: 'sh'
  }, {
    label: 'sz',
    value: 'sz'
  }]
})

const content = _div([img, json, hi, btn, box, searchBtn, backBtn, addListBtn,  select, 'end'], {
  class: 'content'
})
const app = document.getElementById("app")
app.appendChild(content)