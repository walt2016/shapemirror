
import { _fallmenu, _append } from '@/ui'
import config from '@/data/fallmenu'
let app = document.getElementById("app")
let tabs = _fallmenu(config.menu, {
    ...config,
    root: app
})
_append(app, tabs)