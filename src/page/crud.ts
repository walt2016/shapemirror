import config from '@/data/crud'
import { _grid, _gridRows, _form, _append } from '@/ui'
import data from '@/data/data.json'
import { _empty } from '@/ui/core/dom'
import { _random } from '@/math'
import { IDataItem } from '@/types'

const fields = config.fields
const randomFields = fields.filter(t => t.type === 'random')
const indexField = fields.find(t => t.type === 'index')
const app = document.getElementById('app')
let data2 = Array.from({
	length: 3,
}).map((t, i) => {
	const item: IDataItem = data[0]
	randomFields.forEach(t => {
		item[t.field] = _random.apply(Math, t.rang || [0, 10])
	})

	if (indexField) {
		item.index = i + 1
	}

	// Object.keys(item).forEach(field => {
	//   const fieldItem = fields.find(t => t.field === field)
	//   const type = fieldItem.type
	//   switch (type) {
	//     case "index":
	//       item[field] = i + 1
	//       break;
	//     case 'random':
	//       item[field] = _random.apply(Math, fieldItem.rang)
	//       break
	//   }

	// })
	return {
		...item,
	}
})

const form = _form(config, {
	add: (e, { model }) => {
		const rows = []
		rows.push(_gridRows(config.fields, model, data2.length))
		data2.push(model)

		_append(app.querySelector('table>tbody') as HTMLElement, rows)
	},
	reset: () => {
		data2 = []
		_empty(app.querySelector('table>tbody'))
	},
})
_append(app, form)

const render = () => {
	const grid = _grid(config, data2, {
		del: (e, { model }) => {
			const { rowIndex } = model
			data2.splice(rowIndex, 1)
		},
	})

	_append(app, grid)
}

render()
