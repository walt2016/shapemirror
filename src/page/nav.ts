
import { _nav, _append, _layout, _events, _closest } from '@/ui'
import { toggleInspector } from '@/ui/petal/g2'
import { hideAside } from '@/ui/petal/aside'
import config from '@/data/nav3'
import { store } from '@/store'
import { plainTreeMenu } from '@/utils'
import { IMenuItem } from '@/types/menu'

const app = document.getElementById("app") as HTMLElement
const layout = _layout([".header", ".main .aside",
    ".main .screen-viewer", ".main .main-right .inspector",
    ".main .main-right .grammar", ".main .main-right .graphic"], app)
const [header, aside, screen, inspector] = layout
_events(screen, {
    click: (e) => {
        const el = e.target
        hideAside()
        const panel = _closest(el, '.panel')
        if (!panel || /(webgl|^gl.*?)/.test(panel.getAttribute('name'))) {
            return
        }
        const viewer = _closest(el, '.screen-viewer')
        if (!viewer) return
        if (store.isH5) {
            toggleInspector('inspector')
        }
    }
})

const list: IMenuItem[] = plainTreeMenu(config.menu)
store.setAllPages(list)

const nav = _nav(config.menu, {
    ...config,
    root: app
})
_append(header, nav)
