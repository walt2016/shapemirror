

import { _div, _append, _btn } from '@/ui'


let hi = _div("hi webpack", {
  class: 'red'
})
let btn = _btn("say hi", {
  class: 'red'
}, {
  click: () => {
    _append(hi, "!")
  }
})

let app = document.getElementById("app")
_append(app, [hi, btn])