import fields from '@/data/fields'
import { _grid } from '@/ui'
import data from '@/data/data.json'

let data2 = Array.from({ length: 10 }).map(() => data[0])
let form = _grid({ fields }, data2)
document.getElementById("app").appendChild(form)
