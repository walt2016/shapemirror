import config from './webpack.config.js'
import CopyPlugin from 'copy-webpack-plugin'
export default {
  ...config,
  mode:'development',
  entry: {
    // main: path.resolve('./src/index.js'),
    popup: path.resolve('./src/popup.js')
  },
  plugins:[
    ...config.plugins,
    new CopyPlugin({
      patterns: [{
        from: path.join(__dirname, './src/assets/chrome/manifest.json')
      }]
    }),
  ]

}