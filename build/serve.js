import config from './dev.js'
export default {
  ...config,
  
  devServer: {
    port: 8686,
    open: true,
    hot: true,
    // 启动gzip压缩
    compress: true
  }
}