import path from 'path'
import webpack from 'webpack'
// webpack.CleanPlugin
import HtmlWebpackPlugin from 'html-webpack-plugin'
// webpack.HtmlWebpackPlugin

import MiniCssExtractPlugin from 'mini-css-extract-plugin'
const {
  CleanPlugin,
  ProgressPlugin
} = webpack

const ROOT_PATH = path.join(path.resolve(), '..');
export default {
  mode: 'development',
  entry: {
    main: path.resolve('./src/index.ts'),
    // popup: path.resolve('./src/popup.js')
  },
  output: {
    filename: '[name][hash].js'
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: [
                [
                  "@babel/preset-env",
                  {
                    "targets": {
                      "chrome": "58",
                      "ie": "11"
                    },
                    "corejs": "3",
                    "useBuiltIns": "usage"
                  }
                ]
              ]
            }
          },
          {
            loader: "ts-loader",

          }
        ],
        exclude: /node_modules/
      },
      {
        test: /\.js$/,
        use: ['babel-loader'],
        exclude: /node_modules/,
        // 导入时自动适配扩展名
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.(css|less)$/,
        use: [
          // 'style-loader', 
          MiniCssExtractPlugin.loader,
          'css-loader', 'less-loader']
      },
      {
        test: /\.(png|jpe?g|gif)$/,
        use: [{
          loader: 'url-loader',

          options: {
            limit: 1000,
            name: 'img/[name].[ext]'
            // esModule: false,
            // fallback: {
            //   loader: require.resolve('file-loader'),
            //   options: {
            //     name: `img/[name].[hash:8].[ext]`
            //   }
            // }
          }
        }],

      },
      // {
      //   test: /\.(svg)$/,
      //   loader: 'file-loader'
      // },
      {
        test: /\.svg$/,
        use: ['raw-loader']
      },
      {
        test: /\.(glsl|frag|vert)$/,
        loader: 'raw-loader'
      }
    ]

  },
  plugins: [
    new ProgressPlugin(),
    new CleanPlugin(),
    new HtmlWebpackPlugin({
      template: './src/index.htm',
      filename: 'index.html'
    }),
    // new HtmlWebpackPlugin({
    //   template: './src/popup.htm',
    //   filename:'popup.html'
    // }),

    new MiniCssExtractPlugin({
      filename: '[name][hash].css'
    })
  ],
  resolve: {
    alias: {
      '@': path.resolve('./src'),
      // path.resolve(ROOT_PATH, 'src')
    },
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.css', '.less', '.json']
  }
}