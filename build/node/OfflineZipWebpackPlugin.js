const fs = require("fs-extra");
const path = require("path");
const JSZip = require("jszip");
const { RawSource } = require("webpack-sources");

const zip = new JSZip();

// 必须放在htmlWebpackPlugin之后，否则html不再编译阶段

/**
 * 生成zip离线包脚本，通过callback回调上传离线包
 * @param filename
 * @param outputPath
 * @param callback
 * @param excludeDir
 */

module.exports = class OfflineZipWebpackPlugin {
  constructor(options) {
    this.options = options;
  }

  excludeFiles(files) {
    const { excludeDir } = this.options;
    const newFiles = {};
    if (Array.isArray(excludeDir)) {
      excludeDir.forEach((dirName) => {
        for (let path in files) {
          if (!path.includes(dirName)) {
            newFiles[path] = files[path];
          }
        }
      });
      return newFiles;
    }
    return files;
  }

  apply(compiler) {
    const { filename, callback, outputPath } = this.options;
    let newOutputPath = "";
    compiler.hooks.emit.tapAsync(
      "OfflineZip",
      (compilation, pluginCallback) => {
        const folder = zip.folder(filename);

        for (let filename in compilation.assets) {
          const source = compilation.assets[filename].source();
          folder.file(filename, source);
        }

        zip.files = this.excludeFiles(zip.files);

        zip
          .generateAsync({
            type: "nodebuffer",
          })
          .then((content) => {
            if (!fs.existsSync(outputPath)) {
              fs.mkdirSync(outputPath);
            } else {
              fs.emptyDirSync(outputPath);
            }

            newOutputPath = path.join(outputPath, filename + ".zip");

            const relativePath = path.relative(
              compilation.options.output.path,
              newOutputPath
            );
            // 需要相对路径
            compilation.assets[relativePath] = new RawSource(content);
            console.log("newOutputPath", newOutputPath);

            pluginCallback();
          });
      }
    );

    compiler.hooks.done.tap("OfflineZip", () => {
      if (callback) callback(newOutputPath);
    });
  }
};