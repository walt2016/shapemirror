const moduleRules = require("./node/moduleRules")
const plugins = require("./node/dev.plugins")
const path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    app: './src/index.ts'
  },
  output: {
    // filename: 'bundle.js',
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, '../dist')
  },
  plugins,
  module: {
    rules: moduleRules
  }
}