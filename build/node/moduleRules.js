const rules = [
  // 在webpack2中，loaders 被替换成了 rules 其实就是loader的规则
  //  实现 css 打包
  {
      test: /\.css$/,
      use: ['style-loader', 'css-loader']
          // test 说明了当前 loader 能处理那些类型的文件
          // use 则指定了 loader 的类型。
          // 注意：数组中的loader不能省略扩展名
  },
  // 实现 scss 打包
  {
    test: /\.scss$/,
    // 注意 是sass-loader ，不是 scss-loader
    use: ['style-loader', 'css-loader', 'sass-loader']
  },
  // 实现 less 打包
  {
    test: /\.less$/,
    use: ['style-loader', 'css-loader', 'less-loader']
  },
  // 实现 url 资源打包
  {
    // 图片文件使用 url-loader 来处理
    test: /\.(png|jpg|gif|ttf)$/,
    use: [{
      loader: 'url-loader',
      // options 为可以配置的选项
      options: {
        limit: 8192
        // limit=8192表示将所有小于8kb的图片都转为base64形式
        // （其实应该说超过8kb的才使用 url-loader 来映射到文件，否则转为data url形式）
      }
    }]
    //保证输出的图片名称与之前命名的图片名称保持一致(目前只是支持这样的写法，                   webpack3 没有响应的options进行配置)
    // use:'url-loader?limit=8192&name=imgs/[name].[ext]' 
  },
  // 实现 ES6转 ES5
  {
    test: /\.js$/,
    exclude: /(node_modules)/, // exclude 排除的意思，把 node_modules文件夹排除编译之外
    use: {
      loader: 'babel-loader',
      // options: {
      //   // presets 预设列表（一组插件）加载和使用
      //   presets: ['env'], // 也可以写成presets:['babel-preset-env']
      //   // 低版本浏览器
      //   plugins: ['transform-runtime'] // 加载和使用的插件列表
      // }
    }
  },
  // 提取 css模块（如果使用这个模块的话，需要把之前的CSS打包模块注释掉，不然会重复）
  // {
  //   test: /\.css$/,
  //   use: ExtractTextPlugin.extract({
  //     fallback: "style-loader",
  //     use: "css-loader"
  //   })
  // }

  //   {
  //   test: /\.css$/,
  //   use: [
  //     'style-loader',
  //     'css-loader'
  //   ]
  // },
  // {
  //   test: /\.(png|svg|jpg|gif)$/,
  //   use: [
  //     'file-loader'
  //   ]
  // }
]


module.exports = rules