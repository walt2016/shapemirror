const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {
  CleanWebpackPlugin
} = require('clean-webpack-plugin');
const webpack = require('webpack');
// var {
//   WebpackManifestPlugin
// } = require('webpack-manifest-plugin');
// const MiniCssExtractPlugin = require("mini-css-extract-plugin");
// const {
//   BundleAnalyzerPlugin
// } = require('webpack-bundle-analyzer');
// const WebpackPwaManifest = require('webpack-pwa-manifest');

// const WebpackManifest = require('webpack-manifest');
// const exp = require('constants');

const plugins = [

  // 编译前删除dist内文件
  new CleanWebpackPlugin(),
  // 该插件用来生成一个.html文件
  new HtmlWebpackPlugin({
    // title: '管理输出',
    filename: 'index.htm',
    template: path.join(__dirname, '../src/assets/index.htm'),
    minify: {
      // 压缩HTML文件
      removeComments: true, // 移除HTML中的注释
      collapseWhitespace: true, // 删除空白符与换行符
      minifyCSS: true, // 压缩内联css
    },
    // script 标签位于 html 文件的 body 底部
    inject: true,
    // 
    // meta: {viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no'},
    // filename:'./assets/index.htm'
    // filename:(entryName)=>`./assets/${entryName}.html`
  }),
  // new webpack.EnvironmentPlugin(['NODE_ENV', 'DEBUG']),
  new webpack.DefinePlugin({
    PRODUCTION: JSON.stringify(true),
    VERSION: JSON.stringify('5fa3b9'),
    BROWSER_SUPPORTS_HTML5: true,
    // TWO: '1+1',
    // 'typeof window': JSON.stringify('object'),
    // 'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    // 'process.env.NODE_ENV': JSON.stringify('production')
    // 'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    'process.env.NODE_ENV':"production",
    'process.env.DEBUG': JSON.stringify(process.env.DEBUG),
  }),
  // 分析性能
  // new BundleAnalyzerPlugin(),
  // new WebpackPwaManifest({
  //   // 清单文件的版本，这个必须写，而且必须是2
  //   "manifest_version": 2,
  //   // 插件的名称
  //   "name": "demo",
  //   // // 插件的版本
  //   // "version": "1.0.0",
  //   // // 插件描述
  //   // "description": "简单的Chrome扩展demo",
  //   // // 图标，一般偷懒全部用一个尺寸的也没问题
  //   // // icons:[{
  //   // //   src:"img/icon.png",
  //   // //   sizes:[16,48,128]
  //   // // }],
  //   // "icons": {
  //   //   "16": "img/icon.png",
  //   //   "48": "img/icon.png",
  //   //   "128": "img/icon.png"
  //   // },
  //   // // 会一直常驻的后台JS或后台页面
  //   // "background": {
  //   //   // 2种指定方式，如果指定JS，那么会自动生成一个背景页
  //   //   "page": "background.html"
  //   //   //"scripts": ["js/background.js"]
  //   // },
  //   // // 浏览器右上角图标设置，browser_action、page_action、app必须三选一
  //   // "browser_action": {
  //   //   "default_icon": "img/icon.png",
  //   //   // 图标悬停时的标题，可选
  //   //   "default_title": "这是一个示例Chrome插件",
  //   //   "default_popup": "popup.html"
  //   // },
  //   // name: 'My Progressive Web App',
  //   // short_name: 'MyPWA',
  //   // description: 'My awesome Progressive Web App!',
  //   // background_color: '#ffffff',
  //   // crossorigin: 'use-credentials', //can be null, use-credentials or anonymous
  //   // icons: [{
  //   //     src: path.resolve('src/assets/logo.png'),
  //   //     sizes: [96, 128, 192, 256, 384, 512] // multiple sizes
  //   //   },
  //   //   {
  //   //     src: path.resolve('src/assets/logo2.jpeg'),
  //   //     size: '1024x1024' // you can also use the specifications pattern
  //   //   }
  //   // ]
  // }),
  // new WebpackManifest({

  // })

  // new WebpackManifestPlugin(), // ManifestPlugin方法可以接受一些选项参数options,如 new ManifestPlugin(options)
  // new MiniCssExtractPlugin({ // 提取css插件
  //   filename: "[name].css",
  //   chunkFilename: "[id].css"
  // })

  // new webpack.optimize.LimitChunkCountPlugin({
  //   // Options...
  //   maxChunks: 5, // Must be greater than or equal to one
  //   minChunkSize: 1000
  // })
  // 热更新插件
  // new webpack.HotModuleReplacementPlugin({
  //   // Options...
  // })
  // new webpack.HotModuleReplacementPlugin()
]

// export default plugins
module.exports = plugins