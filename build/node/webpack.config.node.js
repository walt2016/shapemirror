const path = require('path');
const RemoveCommentPlugin = require('./removeCommentPlugin');
const rules = require("./moduleRules")
const {
  CleanWebpackPlugin
} = require('clean-webpack-plugin');
const OfflineZipWebpackPlugin =require("./OfflineZipWebpackPlugin")

module.exports = {
  mode: 'development',
  entry: './src/index.ts',
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, './dist'),
  },
  module: {
    rules
    // rules: [
    //     {
    //         test: /\.js$/,
    //         use: ['babel-loader'],
    //         exclude: /node_modules/,
    //     },
    // ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new RemoveCommentPlugin(),
    new OfflineZipWebpackPlugin({
      filename: "test",//paths.zipFileName,
      outputPath: "zip", //paths.zipDir,
      excludeDir: ["/static/media"],
      callback: (zipPath) => {
        uploadFile(zipPath);
      },
    }),
  ]
};