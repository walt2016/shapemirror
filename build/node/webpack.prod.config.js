
import moduleRules from "./node/moduleRules"
import plugins from "./node/prod.plugins"
const path = require('path');
module.exports={
  mode: 'production',
  entry: {
    app: './src/index.js'
  },
  output: {
    // filename: 'bundle.js',
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, '../dist')
  },
  plugins,
  module: {
    rules: moduleRules
  }
}