# shape mirror

#### Description
svg canvas webgl

### demo
http://walt2016.gitee.io/shapemirror

### PC screenshot
<img src="./screenshot/20220730151010.png" width='605'>
<img src="./screenshot/20220730151448.png" width='605'>

### H5 screenshot
<img src="./screenshot/20220807220902.jpg" width='300'>
<img src="./screenshot/20220807220905.jpg" width='300'>
